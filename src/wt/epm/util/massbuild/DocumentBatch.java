/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * REVISION HISTORY ---
 * 18-Oct-2012 nkudav  created
 *
 */
package wt.epm.util.massbuild;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.ikea.ipim.sbom.builders.AbstractSupplierTableBuilder;
import com.ikea.ipim.sbom.processors.TransformAndShareSBOMUtility;
import com.ptc.windchill.uwgm.common.pdm.PDMStateContainerHelper;
import com.ptc.windchill.uwgm.common.pdm.SimplePdmState;

import wt.build.BuildHelper;
import wt.build.BuildServiceUtility;
import wt.build.BuildableLink;
import wt.epm.EPMDocConfigSpec;
import wt.epm.EPMDocument;
import wt.epm.EPMDocumentMaster;
import wt.epm.build.EPMBuildRule;
import wt.epm.delegate.EPMOperationWrapper;
import wt.epm.util.EPMConfigSpecFilter;
import wt.epm.util.EPMFilters;
import wt.fc.ObjectIdentifier;
import wt.fc.ObjectReference;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.fc.collections.CollectionsHelper;
import wt.fc.collections.WTArrayList;
import wt.fc.collections.WTCollection;
import wt.fc.collections.WTHashSet;
import wt.fc.collections.WTKeyedHashMap;
import wt.fc.collections.WTKeyedMap;
import wt.fc.collections.WTValuedHashMap;
import wt.fc.collections.WTValuedMap;
import wt.folder.Folder;
import wt.identity.IdentityFactory;
import wt.org.WTOrganization;
import wt.ownership.OwnershipHelper;
import wt.part.WTPart;
import wt.part.build.WTPartBuildHelper;
import wt.pom.Transaction;
import wt.projmgmt.admin.Project2;
import wt.query.ClassAttribute;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.sandbox.InteropInfo;
import wt.sandbox.SandboxConfigSpec;
import wt.sandbox.SandboxHelper;
import wt.util.WTAttributeNameIfc;
import wt.util.WTException;
import wt.util.WTProperties;
import wt.util.WTPropertyVetoException;
import wt.vc.Iterated;
import wt.vc.IterationInfo;
import wt.vc.VersionControlHelper;
import wt.vc.config.ConfigSpec;
import wt.vc.wip.CheckoutLink;
import wt.vc.wip.WorkInProgressHelper;
import wt.vc.wip.WorkInProgressState;
import wt.vc.wip.Workable;

public class DocumentBatch implements Batch {

    private static Logger log = null;
    private static boolean DEBUG_ENABLED = false;
    private static boolean INFO_ENABLED = false;
    private static File errDir = null;
    private static Long currentBatchId = 0L;
    private static ArrayList batchList = new ArrayList();
    private static int noOfBatches = 100;

    private String successFileName = null;
    private String failedFileName = null;
    private String builtTargetsFileName = null;

    private Long batchId = 0L;
    private int size = 0;
    private String projName = null;
    private String orgName = null;
    private WTOrganization org = null;
    private Project2 proj = null;

    private ArrayList<Long> lineNumbers = new ArrayList<Long>();
    private ArrayList<String> docNumbers = new ArrayList<String>();
    private ArrayList<String> docRevisions = new ArrayList<String>();
    private ArrayList<Long> docIterations = new ArrayList<Long>();
    private ArrayList<Long> docIds = new ArrayList<Long>();

    private ArrayList documents = new ArrayList();
    private ArrayList errors = new ArrayList();
    private WTKeyedHashMap doc2Target = new WTKeyedHashMap();
    
    private static Logger methodServerLogger = Logger.getLogger(DocumentBatch.class);
    
    private static WTHashSet builtParts = new WTHashSet(1000, CollectionsHelper.VERSION_FOREIGN_KEY);

    public static void reset() {
        batchList = new ArrayList();
        currentBatchId = 0L;
        builtParts = new WTHashSet(1000, CollectionsHelper.VERSION_FOREIGN_KEY);
    }

    private static void logInfo(String logMessage) {
        if (INFO_ENABLED) {
            log.info(logMessage);
        }
    }

    private static void logDebug(String logMessage) {
        if (DEBUG_ENABLED) {
            log.debug(logMessage);
        }
    }

    private static void logError(String logMessage) {
        log.error(logMessage);
    }

    static {
        try {
            log = BulkPartBuild.getLogger();
            DEBUG_ENABLED = log.isDebugEnabled();
            INFO_ENABLED = log.isInfoEnabled();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private DocumentBatch(int aSize, int numBatches, String aFilePath, String aOrgName, String aProjName) {
        size = aSize;
        noOfBatches = numBatches;
        batchId = currentBatchId;
        projName = aProjName;
        orgName = aOrgName;
        currentBatchId++;
        logInfo("New batch created. id = " + batchId);
    }

    @Override
    public Long getBatchId() {
        return batchId;
    }

    private void addLine(Long lineNumber, String docNumber, String revision, Long iteration, Long docId) {
        lineNumbers.add(lineNumber);
        docNumbers.add(docNumber);
        docRevisions.add(revision);
        docIterations.add(iteration);
        docIds.add(docId);
    }

    public String getProjectName() {
        return projName;
    }

    public String getOrgName() {
        return orgName;
    }

    public static DocumentBatch getAvailableBatch() {
        if (batchList.size() > noOfBatches) {
            DocumentBatch b = (DocumentBatch) batchList.get(0);
            batchList.remove(0);
            return b;
        }
        for (Iterator<DocumentBatch> i = batchList.iterator(); i.hasNext();) {
            DocumentBatch b = i.next();
            if (b.lineNumbers.size() == b.size) {
                i.remove();
                return b;
            }
        }
        return null;
    }

    public static boolean hasMoreBatches() {
        return (batchList.size() > 0);
    }

    public static DocumentBatch next() {
        DocumentBatch b = (DocumentBatch) batchList.get(0);
        batchList.remove(0);
        return b;
    }

    public static boolean addLineToBatch(int aSize, int numBatches, String aFilePath, long lineNumber, String line) {
        logInfo("Entering DocumentBatch.addLineToBatch. Line number: " + lineNumber);
        if (!(line.startsWith("#") || line.length() == 0)) {
            StringTokenizer stringTokens = new StringTokenizer(line, ",");
            if (stringTokens.countTokens() < 4) {
                logError("Line " + lineNumber + " ignored. Not enough values to process." + stringTokens.countTokens());
                return false;
            }
            String docNumber = stringTokens.nextToken().trim();
            String revision = stringTokens.nextToken().trim();
            String iterString = stringTokens.nextToken().trim();
            String orgName = stringTokens.nextToken().trim();

            String docIdString = null;
            String projName = null;

            if (stringTokens.hasMoreTokens()) {
                docIdString = stringTokens.nextToken().trim();
            }

            if (stringTokens.hasMoreTokens()) {
                projName = stringTokens.nextToken().trim();
            }
            if ((projName != null) && projName.trim().equals("")) {
                projName = null;
            }

            if ((docNumber == null) || (docNumber.equals(""))) {
                logError("Line " + lineNumber + " ignored. docNumber not entered.");
                return false;
            }
            if ((revision == null) || (revision.equals(""))) {
                logError("Line " + lineNumber + " ignored. revision not entered.");
                return false;
            }
            if ((iterString == null) || (iterString.equals(""))) {
                logError("Line " + lineNumber + " ignored. iteration not entered.");
                return false;
            }
            if ((orgName == null) || (orgName.equals(""))) {
                logError("Line " + lineNumber + " ignored. orgName not entered.");
                return false;
            }

            Long iteration = null;
            if (iterString != null && !iterString.equals("")) {
                try {
                    iteration = Long.parseLong(iterString);
                } catch (NumberFormatException nfe) {
                    logError("Line " + lineNumber + " ignored. iteration is not a number.");
                    return false;
                }
            }

            Long docId = null;
            if (docIdString != null && !docIdString.equals("")) {
                try {
                    docId = Long.parseLong(docIdString);
                } catch (NumberFormatException nfe) {
                    logError("Line " + lineNumber + " ignored. docId is not a number.");
                    return false;
                }
            }

            DocumentBatch batch = null;
            for (Iterator<DocumentBatch> i = batchList.iterator(); i.hasNext();) {
                DocumentBatch b = i.next();
                if (b.lineNumbers.size() < b.size) {
                    if (!b.docNumbers.contains(docNumber)) {
                        if (orgName.equals(b.getOrgName())) {
                            if (((projName == null) && (b.getProjectName() == null))
                                || ((projName != null) && projName.equals(b.getProjectName()))) {
                                batch = b;
                                break;
                            }
                        }
                    }
                }
            }
            if (batch == null) {
                batch = new DocumentBatch(aSize, numBatches, aFilePath, orgName, projName);
                batchList.add(batch);
            }
            batch.addLine(lineNumber, docNumber, revision, iteration, docId);
            return true;
        } else {
            logInfo("Line " + lineNumber + " ignored. It is commented out.");
            return false;
        }
    }


    @Override
    public void printFailedBatch(String file) {
        if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < lineNumbers.size(); i++) {
                String error = (String) errors.get(i);
                if ((error == null) || (error.trim().equals(""))) {
                    continue;                    
                }
                sb.append("" + (Long) lineNumbers.get(i));
                sb.append(",");
                sb.append("" + batchId);
                sb.append(",");
                sb.append((String) docNumbers.get(i));
                sb.append(",");
                sb.append((String) docRevisions.get(i));
                sb.append(",");
                sb.append((Long) docIterations.get(i));
                sb.append(",");
                sb.append((String) orgName);
                sb.append(",");
                sb.append(((Long) docIds.get(i) == null) ? "" : docIds.get(i));
                sb.append(",");
                sb.append((projName == null) ? "" : projName);
                sb.append(",");
                sb.append((String) errors.get(i));
                sb.append("\r\n");
            }
            if(sb.length() > 0) {
                logInfo("Printing failed batch to file " + file);
                File f = new File(errDir, file);
                fw = new FileWriter(f, true);
                fw.write(sb.toString());
            }           
        } catch (IOException ioe) {
            logError("Batch.printFailedBatch(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } finally {
            try {
            	if (fw != null)
            		fw.close();
			} catch (IOException e) {
				logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
				e.printStackTrace();
			}
        }
    }

    
    @Override
    public void printSuccessfulBatch(String file) {
        if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < lineNumbers.size(); i++) {
                if ((errors.get(i) == null) || ("".equals(errors.get(i)))) {
                    sb.append("" + (Long) lineNumbers.get(i));
                    sb.append(",");
                    sb.append("" + batchId);
                    sb.append(",");
                    sb.append((String) docNumbers.get(i));
                    sb.append(",");
                    sb.append((String) docRevisions.get(i));
                    sb.append(",");
                    sb.append((Long) docIterations.get(i));
                    sb.append(",");
                    sb.append((String) orgName);
                    sb.append(",");
                    sb.append(((Long) docIds.get(i) == null) ? "" : docIds.get(i));
                    sb.append(",");
                    sb.append((projName == null) ? "" : projName);
                    sb.append(",");
                    sb.append((String) ((errors.get(i) == null) ? "" : errors.get(i)));
                    sb.append("\r\n");
                }
            }
            if(sb.length() > 0){
                File f = new File(errDir, file);
                fw = new FileWriter(f, true);
                fw.write(sb.toString());
            }            
        } catch (IOException ioe) {
            logError("Batch.printSuccessfulBatch(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } finally {
            try {
            	if (fw != null)
            		fw.close();
			} catch (IOException e) {
				logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
				e.printStackTrace();
			}
        }
    }


    @Override
    public void printBatch(String file) {
        logInfo("Printing batch to file " + file);
        if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < lineNumbers.size(); i++) {
                sb.append("" + (Long) lineNumbers.get(i));
                sb.append(",");
                sb.append("" + batchId);
                sb.append(",");
                sb.append((String) docNumbers.get(i));
                sb.append(",");
                sb.append((String) docRevisions.get(i));
                sb.append(",");
                sb.append((Long) docIterations.get(i));
                sb.append(",");
                sb.append((String) orgName);
                sb.append(",");
                sb.append(((Long) docIds.get(i) == null) ? "" : docIds.get(i));
                sb.append(",");
                sb.append((projName == null) ? "" : projName);
                sb.append(",");
                sb.append((String) ((errors.get(i) == null) ? "" : errors.get(i)));
                sb.append("\r\n");
            }
            if(sb.length() > 0){
                File f = new File(errDir, file);
                fw = new FileWriter(f, true);
                fw.write(sb.toString());
            }
        } catch (IOException ioe) {
            logError("Batch.printBatch(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } finally {
            try {
				if (fw != null)
					fw.close();
			} catch (IOException e) {
				logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
				e.printStackTrace();
			}
        }
    }

    @Override
    public void printRow(String file, int i) {
        if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {
            File f = new File(errDir, file);
            fw = new FileWriter(f, true);

            StringBuffer sb = new StringBuffer();
            sb.append("" + (Long) lineNumbers.get(i));
            sb.append(",");
            sb.append("" + batchId);
            sb.append(",");
            sb.append((String) docNumbers.get(i));
            sb.append(",");
            sb.append((String) docRevisions.get(i));
            sb.append(",");
            sb.append((Long) docIterations.get(i));
            sb.append(",");
            sb.append((String) orgName);
            sb.append(",");
            sb.append(((Long) docIds.get(i) == null) ? "" : docIds.get(i));
            sb.append(",");
            sb.append((projName == null) ? "" : projName);
            sb.append(",");
            sb.append((String) ((errors.get(i) == null) ? "" : errors.get(i)));
            sb.append("\r\n");

            fw.write(sb.toString());
        } catch (IOException ioe) {
            logError("Batch.printRow(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } finally {
            try {
            	if (fw != null)
            		fw.close();
			} catch (IOException e) {
				logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
				e.printStackTrace();
			}
        }
    }

    public void printPart(String file, WTPart builtPart) {
        if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {
            File f = new File(errDir, file);
            fw = new FileWriter(f, true);

            StringBuffer sb = new StringBuffer();
            sb.append(IdentityFactory.getDisplayIdentity(builtPart));
            sb.append(",");
            sb.append("" + PersistenceHelper.getObjectIdentifier(builtPart));
            sb.append(",");

            sb.append("\r\n");

            fw.write(sb.toString());
        } catch (IOException ioe) {
            logError("Batch.printPart(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } finally {
            try {
            	if (fw != null)
            		fw.close();
			} catch (IOException e) {
				logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
				e.printStackTrace();
			}
        }
    }

    @Override
    public void printToBeBuiltPart(String file) {
    	logInfo("Printing to be built parts to file " + file);
    	if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {
            for (int i = 0; i < lineNumbers.size(); i++) {
                EPMDocument doc = (EPMDocument) documents.get(i);
                if (doc == null) {
                    continue;
                }
                WTCollection parts = (WTCollection) doc2Target.get(doc);

                if (parts != null) {
                    if(fw == null && !parts.isEmpty()){
                        File f = new File(errDir, file);
                        fw = new FileWriter(f, true);
                    }
                    for (Iterator iter = parts.persistableIterator(); iter.hasNext();) {
                        WTPart builtPart = (WTPart) iter.next();

                        StringBuffer sb = new StringBuffer();
                        sb.append(IdentityFactory.getDisplayIdentity(builtPart));
                        sb.append(",");
                        sb.append("" + PersistenceHelper.getObjectIdentifier(builtPart));
                        sb.append("\r\n");
                        fw.write(sb.toString());
                    }
                }
            }
        } catch (IOException ioe) {
            logError("Batch.printToBeBuiltPart(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } catch (WTException e) {
        	logError("Batch.printToBeBuiltPart(String file) exception printing batch");
			e.printStackTrace();
            return;
		} finally {
            try {
            	if (fw != null)
            		fw.close();
			} catch (IOException e) {
				logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
				e.printStackTrace();
			}
		}
    }

    @Override
    public boolean validateBatch() throws Exception {
        logInfo("Entering DocumentBatch.validateBatch. validating batch - id " + this.batchId);
        boolean validBatch = true;
        org = lookupOrganization(orgName);
        if (org == null) {
            for (int i = 0; i < lineNumbers.size(); i++) {
                documents.add(null);
                errors.add("Organization " + orgName + " not found");
            }
            return false;
        }
        proj = null;
        if (projName != null) {
            proj = lookupProject(projName);
            if (proj == null) {
                for (int i = 0; i < lineNumbers.size(); i++) {
                    documents.add(null);
                    errors.add("Project  " + projName + " not found");
                }
                return false;
            }
        }

        ArrayList idList = new ArrayList();
        for (int i = 0; i < docIds.size(); i++) {
            if (docIds.get(i) != null) {
                idList.add(docIds.get(i));
            }
        }
        long[] ids = new long[idList.size()];
        String[] numbers = new String[docNumbers.size()-idList.size()];
        for (int i = 0; i < idList.size(); i++) {
            ids[i] = (Long) idList.get(i);
        }

        int numInd =0;
        for (int i = 0; i < docNumbers.size(); i++) {
            if (docIds.get(i) == null) {
                numbers[numInd++] = (String) docNumbers.get(i);
            }
        }

        QuerySpec qs = new QuerySpec(EPMDocument.class);
        if (ids.length > 0) {
            qs.appendWhere(new SearchCondition(EPMDocument.class,
                WTAttributeNameIfc.OBJECT_IDENTIFIER + "." + ObjectIdentifier.ID,
                ids),
                0);
            if (numbers.length >0) {
                qs.appendOr();
                qs.appendWhere(new SearchCondition(EPMDocument.class,
                    EPMDocument.NUMBER,
                    numbers, true, false),
                    0);
            }
        } else {
            qs.appendWhere(new SearchCondition(EPMDocument.class,
                EPMDocument.NUMBER,
                numbers, true, false),
                0);
        }

        ClassAttribute latest = new ClassAttribute(EPMDocument.class, Iterated.ITERATION_INFO + "." + IterationInfo.LATEST);
        qs.appendAnd();
        qs.appendWhere(new SearchCondition(latest, SearchCondition.IS_TRUE), 0);

        qs.appendAnd();
        qs.appendWhere(new SearchCondition(EPMDocument.class,
            EPMDocument.ORGANIZATION_REFERENCE + '.' + WTAttributeNameIfc.REF_OBJECT_ID,
            SearchCondition.EQUAL,
            PersistenceHelper.getObjectIdentifier(org).getId()),
            new int[]{0});

        methodServerLogger.debug("Query spec for browsing EPMS is ---> " + qs.toString());
        
        QueryResult allIterations = PersistenceHelper.manager.find(qs);

        HashMap number2Iterations = new HashMap();
        while (allIterations.hasMoreElements()) {
            EPMDocument doc = (EPMDocument) allIterations.nextElement();
            WTHashSet iterations = (WTHashSet) number2Iterations.get(((EPMDocumentMaster) doc.getMaster()).getNumber());
            if (iterations == null) {
                iterations = new WTHashSet();
                number2Iterations.put(((EPMDocumentMaster) doc.getMaster()).getNumber(), iterations);
            }
            iterations.add(doc);
        }

        WTHashSet docsInError = new WTHashSet();
        //revision,iteration,orgName,<projectName>
        for (int i = 0; i < lineNumbers.size(); i++) {
            String number = (String) docNumbers.get(i);
            Long docId = (Long) docIds.get(i);
            Long iterId = (Long) docIterations.get(i);
            String rev = (String) docRevisions.get(i);
            EPMDocument doc = null;
            String error = null;
            WTHashSet iterations = (WTHashSet) number2Iterations.get(number);
            if (iterations != null) {
                for (Iterator<EPMDocument> j = iterations.persistableIterator(EPMDocument.class, true); j.hasNext();) {
                    EPMDocument iter = j.next();
                    Long id = PersistenceHelper.getObjectIdentifier(iter).getId();
                    String version = VersionControlHelper.getVersionIdentifier(iter).getSeries().getValue();
                    String iteration = VersionControlHelper.getIterationIdentifier(iter).getValue();
                    if (version.equals(rev) && iteration.equals("" + iterId)) {
                        if ((docId == null) || (docId.equals(id))) {
                            if ((projName == null && !wt.epm.util.EPMContainerHelper.isProject(iter.getContainerReference())) || (proj != null && ObjectReference.newObjectReference(proj).equals(iter.getContainerReference()))) {
                                if (iter.getCheckoutInfo().getState().equals(WorkInProgressState.CHECKED_IN)) {
                                    doc = iter;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (doc == null) {
                    error = "matching version/iteration not found ";
                    if (docId != null) {
                        error = error + " OR document Id may not be matching ";
                    }
                    if (projName != null) {
                        error = error + " OR document may not be in project " + projName;
                    }
                    validBatch = false;
                } else if (WorkInProgressHelper.isCheckedOut(doc)) {
                    error = "Document is checked out";
                    validBatch = false;
                } else if (OwnershipHelper.isOwned((Workable) doc)) {
                    error = "The document is not in shared folder";
                    validBatch = false;
                } else if (!doc.isVerified()) {
                    error = "Unverified Document";
                    validBatch = false;
                }
            } else {
                error = "User does not have access to document OR document may not be latest iteration OR document may exist in different organization";
                validBatch = false;
            }
            documents.add(doc);
            errors.add(error);
            if (error != null) {
                if (doc != null) {
                    docsInError.add(doc);
                }
            }
        }

        WTHashSet docs = new WTHashSet();
        for (Iterator i=documents.iterator(); i.hasNext();) {
            EPMDocument d = (EPMDocument) i.next();
            if (d != null) {
                if (!docsInError.contains(d)) {
                    docs.add(d);
                }
            }
        }

        doc2Target = new WTKeyedHashMap();
        if (!docs.isEmpty()) {
            WTKeyedHashMap part2Doc = new WTKeyedHashMap();
            WTCollection rules = new WTHashSet();
            try {
                rules = BuildHelper.service.getRulesToBeBuiltForSources(docs, CollectionsHelper.EMPTY_COLLECTION, true);
            } catch (WTException e) {
                for (int i = 0; i < lineNumbers.size(); i++) {
                    EPMDocument doc = (EPMDocument) documents.get(i);
                    if ((doc != null) && (!docsInError.contains(doc))) {
                        try {
                            WTCollection r = BuildHelper.service.getRulesToBeBuiltForSources(new WTHashSet(Arrays.asList(doc)), CollectionsHelper.EMPTY_COLLECTION, true);
                            rules.addAll(r);
                        } catch (WTException ex) {
                            ex.printStackTrace();
                            errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+ex.getLocalizedMessage():ex.getLocalizedMessage());
                            validBatch = false;
                            docsInError.add(doc);
                        }
                    }
                }
            }

            for (Iterator i = rules.persistableIterator(); i.hasNext();) {
                EPMBuildRule rule = (EPMBuildRule) i.next();
                EPMDocument doc = (EPMDocument) rule.getBuildSource();
                WTPart part = (WTPart) rule.getBuildTarget();
                WTHashSet parts = (WTHashSet) doc2Target.get(doc);
                if (parts == null) {
                    parts = new WTHashSet();
                    doc2Target.put(doc, parts);
                }
                parts.add(part);

                WTHashSet docsForPart = (WTHashSet) part2Doc.get(part);
                if (docsForPart == null) {
                    docsForPart = new WTHashSet();
                    part2Doc.put(part, docsForPart);
                }
                docsForPart.add(doc);
            }

            for (int i = 0; i < lineNumbers.size(); i++) {
                EPMDocument doc = (EPMDocument) documents.get(i);
                if ((doc == null) || (!docs.contains(doc))) {
                    continue;
                }
                if (docsInError.contains(doc)) {
                    doc2Target.remove(doc);
                    validBatch = false;
                    continue;
                }
                WTCollection parts = (WTCollection) doc2Target.get(doc);
                boolean partError = false;
                if (parts != null) {
                    for (Iterator j=parts.persistableIterator(); j.hasNext();) {
                        WTPart p = (WTPart) j.next();
                        if (builtParts.contains(p)) {
                            errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+"part is already built for document. "+IdentityFactory.getDisplayIdentity(p):"part is already built for document. "+IdentityFactory.getDisplayIdentity(p));
                            partError = true;
                            doc2Target.remove(doc);
                            docsInError.add(doc);
                        }


                        WTHashSet docsForPart = (WTHashSet) part2Doc.get(p);
                        if (docsForPart != null) {
                            for (Iterator k=docsForPart.persistableIterator(); k.hasNext();) {
                                EPMDocument d = (EPMDocument) k.next();
                                if (WorkInProgressHelper.isCheckedOut(d)) {
                                    errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+"Document associated to part is checked out "+IdentityFactory.getDisplayIdentity(p)+" doc = "+IdentityFactory.getDisplayIdentity(d):"Document associated to part is checked out "+IdentityFactory.getDisplayIdentity(p)+" doc = "+IdentityFactory.getDisplayIdentity(d));
                                    partError = true;
                                    doc2Target.remove(doc);
                                    docsInError.add(doc);
                                } else if (OwnershipHelper.isOwned((Workable) d)) {
                                    errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+"Document associated to part is not in shared folder "+IdentityFactory.getDisplayIdentity(p)+" doc = "+IdentityFactory.getDisplayIdentity(d):"Document associated to part is not in shared folder "+IdentityFactory.getDisplayIdentity(p)+" doc = "+IdentityFactory.getDisplayIdentity(d));
                                    partError = true;
                                    doc2Target.remove(doc);
                                    docsInError.add(doc);
                                } else if (!d.isVerified()) {
                                    errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+"Document associated to part is unverified "+IdentityFactory.getDisplayIdentity(p)+" doc = "+IdentityFactory.getDisplayIdentity(d):"Document associated to part is unverified "+IdentityFactory.getDisplayIdentity(p)+" doc = "+IdentityFactory.getDisplayIdentity(d));
                                    partError = true;
                                    doc2Target.remove(doc);
                                    docsInError.add(doc);
                                }
                            }
                        }
                        if ((WorkInProgressHelper.isCheckedOut(p)) || (OwnershipHelper.isOwned((Workable) p))) {
                            errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+"associated part is checked out / new for document. "+IdentityFactory.getDisplayIdentity(p):"associated part is checked out for document. "+IdentityFactory.getDisplayIdentity(p));
                            partError = true;
                            doc2Target.remove(doc);
                            docsInError.add(doc);
                        }
                        if (partError) {
                            validBatch = false;
                            break;
                        } else {
                            builtParts.add(p);
                        }
                    }
                } else {
                    errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+"part can not be built - see method server logs for more details. ":"part can not be built - see method server logs for more details. ");
                    partError = true;
                    doc2Target.remove(doc);
                    docsInError.add(doc);
                    validBatch = false;
                }
            }
        }

        logInfo("Leaving DocumentBatch.validateBatch. Valid batch: " + validBatch);
        return validBatch;
    }

    @Override
    public void setFiles(String aSuccessFile, String aFailedFile, String aBuiltTargetsFileName) {
        successFileName = aSuccessFile;
        failedFileName = aFailedFile;
        builtTargetsFileName = aBuiltTargetsFileName;
    }

    @Override
    public void build(boolean recreate) throws Exception {
        logInfo("Entering DocumentBatch.build with batch id = " + this.batchId);

        WTHashSet allParts = new WTHashSet();
        for (int i = 0; i < lineNumbers.size(); i++) {
            EPMDocument doc = (EPMDocument) documents.get(i);
            if (doc != null) {
                String error = (String) errors.get(i);
                if ((error == null) || ("".equals(error.trim()))) {
                    WTCollection parts = (WTCollection) doc2Target.get(doc);
                    if (parts != null) {
                        allParts.addAll(parts);
                    }
                }
            }
        }

        logInfo("Documents = " + documents);
        logInfo("Line numbers = " + lineNumbers);
        logInfo("doc2Target = "+doc2Target);
        logInfo("allParts = "+allParts);
        if (!allParts.isEmpty()) {
            ConfigSpec cs = getConfigSpec(proj);
            WTHashSet builtObjs = new WTHashSet();
            Transaction trx = null;
            try {
                trx = new Transaction();
                trx.start();

                if (recreate) {
                    Folder checkoutFolder = WorkInProgressHelper.service.getCheckoutFolder();
                    WTValuedHashMap checkedOutParts = checkout((Workable[]) allParts.toArray(new Workable[0]), " parts ", checkoutFolder, false);

                    try {
                        WTCollection links = BuildServiceUtility.expand(checkedOutParts.wtValues(), BuildableLink.class, BuildableLink.PERSISTABLE_ROLE, null, true, true);
                        PersistenceHelper.manager.delete(new WTHashSet(links));

                        WTKeyedMap dbrMap = WTPartBuildHelper.service.getDeletedBuildReferences2(checkedOutParts.wtValues());
                        WTHashSet dbrs = new WTHashSet();
                        for (Iterator i = dbrMap.wtKeySet().persistableIterator(); i.hasNext();) {
                            WTPart part = (WTPart) i.next();
                            dbrs.addAll((WTCollection)dbrMap.get(part));
                        }
                        PersistenceHelper.manager.delete(dbrs);
                     
                        WTValuedMap tgts = BuildHelper.service.buildTarget(checkedOutParts.wtValues(), cs);
                        WTCollection checkedIn  = WorkInProgressHelper.service.checkin(tgts.wtValues(), "Build By utility - BulkPartBuild");
                        builtObjs.addAll(checkedIn);
                        this.printSuccessfulBatch(successFileName);
                        
                        trx.commit();
                        trx = null;
                        
                    } catch (WTException e) {
                        e.printStackTrace();
                        logInfo(e.getLocalizedMessage());
                        logInfo("Error occurred while building the batch. Building the documents individually . batch id " + this.getBatchId());
                        
                        //this would make checked out copies getting rolled back. we need to rollback because if the rollback is in progress then it would not be able to start new trx in the subsequent individual build
                        trx.rollback(); 
                        trx = null;
                        
                        WTHashSet tgtsToBuild = new WTHashSet();
                        for (int i = 0; i < documents.size(); i++) {
                            try {
                                EPMDocument doc = (EPMDocument) documents.get(i);
                                if (doc == null) {
                                    continue;
                                }
                                WTHashSet tgts = (WTHashSet) doc2Target.get(doc);
                                if (tgts == null) {
                                    continue;
                                }
                                
                                checkedOutParts = checkout((Workable[]) tgts.toArray(new Workable[0]), " parts ", checkoutFolder, false);
                                
                                tgtsToBuild = new WTHashSet(checkedOutParts.wtValues());

                                WTCollection links = BuildServiceUtility.expand(tgtsToBuild, BuildableLink.class, BuildableLink.PERSISTABLE_ROLE, null, true, true);
                                PersistenceHelper.manager.delete(new WTHashSet(links));

                                WTKeyedMap dbrMap = WTPartBuildHelper.service.getDeletedBuildReferences2(tgtsToBuild);
                                WTHashSet dbrs = new WTHashSet();
                                for (Iterator j = dbrMap.wtKeySet().persistableIterator(); j.hasNext();) {
                                    WTPart part = (WTPart) j.next();
                                    dbrs.addAll((WTCollection)dbrMap.get(part));
                                }
                                PersistenceHelper.manager.delete(dbrs);

                                WTValuedMap builtTgts = BuildHelper.service.buildTarget(tgtsToBuild, cs);
                                WTCollection checkedIn  = WorkInProgressHelper.service.checkin(builtTgts.wtValues(), "Build By utility - BulkPartBuild");
                                builtObjs.addAll(checkedIn);
                                printRow(successFileName, i);
                            } catch (WTException X) {
                                X.printStackTrace();
                                logInfo(X.getLocalizedMessage());
                                errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+X.getLocalizedMessage():X.getLocalizedMessage());
                                WorkInProgressHelper.service.undoCheckouts(tgtsToBuild);
                            } catch (Exception X) {
                                X.printStackTrace();
                                logInfo(X.getLocalizedMessage());
                                errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+X.getLocalizedMessage():X.getLocalizedMessage());
                                WorkInProgressHelper.service.undoCheckouts(tgtsToBuild);
                            }
                        }
                       
                    }
                } else {
                    try {
                        WTValuedMap tgts = BuildHelper.service.buildTarget(allParts, cs);
                        builtObjs.addAll(tgts.wtValues());
                        this.printSuccessfulBatch(successFileName);
                        trx.commit();
                        trx = null;
                    } catch (WTException e) {
                        e.printStackTrace();
                        logInfo(e.getLocalizedMessage());
                        logInfo("Error occurred while building the batch. Building the documents individually . batch id " + this.getBatchId());
                        
                        trx.rollback();
                        trx = null;
                                               
                        for (int i = 0; i < documents.size(); i++) {
                            if ((errors.get(i) == null) || ("".equals(errors.get(i)))) {
                                EPMDocument doc = (EPMDocument) documents.get(i);
                                if (doc == null) {
                                    continue;
                                }
                                WTHashSet tgts = (WTHashSet) doc2Target.get(doc);
                                if (tgts == null) {
                                    continue;
                                }
                                if (!tgts.isEmpty()) {
                                    try {
                                        WTValuedMap builtTgts = BuildHelper.service.buildTarget(tgts, cs);
                                        builtObjs.addAll(builtTgts.wtValues());
                                        printRow(successFileName, i);
                                    } catch (WTException X) {
                                        X.printStackTrace();
                                        logInfo(X.getLocalizedMessage());
                                        errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+X.getLocalizedMessage():X.getLocalizedMessage());
                                    }
                                }
                            }
                        }
                    }
                }

                if (!builtObjs.isEmpty()) {
                    builtParts.addAll(builtObjs);
                    for (Iterator i = builtObjs.persistableIterator(); i.hasNext();) {
                        WTPart part = (WTPart) i.next();
                        printPart(builtTargetsFileName, part);
                    }
                    EPMOperationWrapper.publishViewablesForDocs(new WTArrayList(builtObjs));
                }
               
            } finally {
                if (trx != null) {
                    trx.rollback();
                }
                	
                printFailedBatch(failedFileName);
            }
        }
        logInfo("Leaving DocumentBatch.build");
    }

    @Override
    public void cleanup() {
        doc2Target = new WTKeyedHashMap();
    }

    private ConfigSpec getConfigSpec(Project2 proj) throws WTException {
        ConfigSpec cs = null;
        if (projName != null) {
            try {
                cs = SandboxConfigSpec.newSandboxConfigSpec(proj.getContainerReference(),
                                                            true, /* use sandbox baseline */
                                                            false, /* Do not use personal checkout */
                                                            null, /* current princi]pal */
                                                            new EPMConfigSpecFilter(EPMDocConfigSpec.newEPMDocConfigSpec(), EPMFilters.NO_WORKING));
                                                            /* user config spec is PDM Latest/No Working Copies. Needed to find dependents that are not
                                                             * shared to Sandbox.
                                                             */
            } catch (WTPropertyVetoException wtpve) {
                throw new WTException(wtpve);
            }
        } else {
            cs = new EPMConfigSpecFilter(EPMDocConfigSpec.newEPMDocConfigSpec(), EPMFilters.NO_WORKING);
        }
        return cs;
    }

    public static WTOrganization lookupOrganization(String name) throws WTException {
        QuerySpec qs = new QuerySpec(WTOrganization.class);
        qs.appendWhere(new SearchCondition(WTOrganization.class,
                                           WTOrganization.NAME,
                                           SearchCondition.EQUAL,
                                           name), new int[]{0});
        QueryResult orgs = PersistenceHelper.manager.find(qs);
        if (orgs.hasMoreElements()) {
            return (WTOrganization) orgs.nextElement();
        }
        return null;
    }

    public static Project2 lookupProject(String name) throws WTException {
        QuerySpec qs = new QuerySpec(Project2.class);
        qs.appendWhere(new SearchCondition(Project2.class,
            Project2.NAME,
            SearchCondition.EQUAL,
            name), new int[]{0});
        QueryResult qr = PersistenceHelper.manager.find(qs);
        if (qr.hasMoreElements()) {
            return (Project2) qr.nextElement();
        }
        return null;
    }
    
    public static EPMDocument substituteEPMForNonShared(EPMDocument theSharedDoc) throws WTException {
        if (SandboxHelper.isCheckedOutToSandbox(theSharedDoc)) {
            theSharedDoc = (EPMDocument) SandboxHelper.service.getOriginalCopyOf(theSharedDoc);
        }
        return theSharedDoc;
    }

    public static WTValuedHashMap checkout(Workable[] objects, String type, Folder checkoutFolder, boolean allowNonLatestCheckout) throws WTException, WTPropertyVetoException {
        WTArrayList checkoutObjs = new WTArrayList(Arrays.asList(objects));
        WTCollection checkOutLinks = WorkInProgressHelper.service.checkout(checkoutObjs, checkoutFolder, "Build By utility - BulkPartBuild", allowNonLatestCheckout);

        WTValuedHashMap checkedoutCopies = new WTValuedHashMap();
        Iterator i_checkedOut = checkOutLinks.persistableIterator();
        while (i_checkedOut.hasNext()) {
            CheckoutLink cLink = (CheckoutLink) i_checkedOut.next();
            Workable wCopy = cLink.getWorkingCopy();
            Workable oCopy = cLink.getOriginalCopy();
            checkedoutCopies.put(oCopy, wCopy);
        }

        return checkedoutCopies;
    }

    static {
        try {
            WTProperties wtproperties = WTProperties.getLocalProperties();
            String errDirString = wtproperties.getProperty("wt.logs.dir");
            errDir = new File(errDirString);
            if (!errDir.exists()) {
                errDir = null;
            }

        } catch (Exception e) {
            System.err.println("Error initializing wt.logs.dir");
            e.printStackTrace(System.err);
        }
    }
}
