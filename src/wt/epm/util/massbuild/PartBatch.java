package wt.epm.util.massbuild;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import wt.build.BuildHelper;
import wt.build.BuildServiceUtility;
import wt.build.BuildTarget;
import wt.build.BuildableLink;
import wt.epm.EPMDocConfigSpec;
import wt.epm.EPMDocument;
import wt.epm.build.EPMBuildRule;
import wt.epm.delegate.EPMOperationWrapper;
import wt.epm.util.EPMConfigSpecFilter;
import wt.epm.util.EPMFilters;
import wt.fc.ObjectIdentifier;
import wt.fc.ObjectReference;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.fc.collections.CollectionsHelper;
import wt.fc.collections.WTArrayList;
import wt.fc.collections.WTCollection;
import wt.fc.collections.WTHashSet;
import wt.fc.collections.WTKeyedHashMap;
import wt.fc.collections.WTKeyedMap;
import wt.fc.collections.WTValuedHashMap;
import wt.fc.collections.WTValuedMap;
import wt.folder.Folder;
import wt.identity.IdentityFactory;
import wt.org.WTOrganization;
import wt.ownership.OwnershipHelper;
import wt.part.WTPart;
import wt.part.WTPartMaster;
import wt.part.build.WTPartBuildHelper;
import wt.pom.Transaction;
import wt.projmgmt.admin.Project2;
import wt.query.ClassAttribute;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.sandbox.SandboxConfigSpec;
import wt.util.WTAttributeNameIfc;
import wt.util.WTException;
import wt.util.WTProperties;
import wt.util.WTPropertyVetoException;
import wt.vc.Iterated;
import wt.vc.IterationInfo;
import wt.vc.VersionControlHelper;
import wt.vc.config.ConfigSpec;
import wt.vc.wip.CheckoutLink;
import wt.vc.wip.WorkInProgressHelper;
import wt.vc.wip.Workable;

public class PartBatch implements Batch {
    private static Logger log = null;
    private static boolean DEBUG_ENABLED = false;
    private static boolean INFO_ENABLED = false;
    private static File errDir = null;
    private static Long currentBatchId = 0L;
    private static ArrayList batchList = new ArrayList();
    private static int noOfBatches = 100;

    private String successFileName = null;
    private String failedFileName = null;
    private String builtTargetsFileName = null;

    private Long batchId = 0L;
    private int size = 0;
    private String projName = null;
    private String orgName = null;
    private String subclass = null;
    private WTOrganization org = null;
    private Project2 proj = null;

    private WTKeyedHashMap part2Doc = new WTKeyedHashMap();

    private ArrayList<Long> lineNumbers = new ArrayList<Long>();
    private ArrayList<String> partNumbers = new ArrayList<String>();
    private ArrayList<String> partRevisions = new ArrayList<String>();
    private ArrayList<Long> partIterations = new ArrayList<Long>();
    private ArrayList<Long> partIds = new ArrayList<Long>();

    private ArrayList parts = new ArrayList();
    private ArrayList errors = new ArrayList();

    private static WTHashSet builtParts = new WTHashSet(1000, CollectionsHelper.VERSION_FOREIGN_KEY);
    WTHashSet partsInError = new WTHashSet();

    public static void reset() {
        batchList = new ArrayList();
        currentBatchId = 0L;
        builtParts = new WTHashSet(1000, CollectionsHelper.VERSION_FOREIGN_KEY);
    }

    private static void logInfo(String logMessage) {
        if (INFO_ENABLED) {
            log.info(logMessage);
        }
    }

    private static void logDebug(String logMessage) {
        if (DEBUG_ENABLED) {
            log.debug(logMessage);
        }
    }

    private static void logError(String logMessage) {
        log.error(logMessage);
    }

    static {
        try {
            log = BulkPartBuild.getLogger();
            DEBUG_ENABLED = log.isDebugEnabled();
            INFO_ENABLED = log.isInfoEnabled();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private PartBatch(int aSize, int numBatches, String aFilePath, String aOrgName, String aProjName, String aSubclass) {
        size = aSize;
        batchId = currentBatchId;
        currentBatchId++;
        projName = aProjName;
        orgName = aOrgName;
        subclass = aSubclass;
        noOfBatches = numBatches;
        logInfo("New batch created. id = "+batchId);
        return;
    }

    @Override
    public Long getBatchId() {
        return batchId;
    }

    private void addLine(Long lineNumber, String partNumber, String revision, Long iteration, Long partId) {
        lineNumbers.add(lineNumber);
        partNumbers.add(partNumber);
        partRevisions.add(revision);
        partIterations.add(iteration);
        partIds.add(partId);
    }

    public String getProjectName() {
        return projName;
    }

    public String getOrgName() {
        return orgName;
    }

    public String getSubclass() {
        return subclass;
    }

    public static PartBatch getAvailableBatch() {
        if (batchList.size() > noOfBatches) {
            PartBatch b = (PartBatch) batchList.get(0);
            batchList.remove(0);
            return b;
        }
        for (Iterator<PartBatch> i=batchList.iterator(); i.hasNext();) {
            PartBatch b = i.next();
            if (b.lineNumbers.size() == b.size) {
                i.remove();
                return b;
            }
        }
        return null;
    }

    public static boolean hasMoreBatches() {
        return (batchList.size() > 0);
    }

    public static PartBatch next() {
        PartBatch b = (PartBatch) batchList.get(0);
        batchList.remove(0);
        return b;
    }

    public static boolean addLineToBatch(int aSize, int numBatches, String aFilePath, long lineNumber, String line) {
        logInfo("Entering PartBatch.addLineToBatch. Line number: " + lineNumber);
        if (!(line.startsWith("#") || line.length() == 0)) {
            StringTokenizer stringTokens = new StringTokenizer(line, ",");
            if (stringTokens.countTokens() < 4) {
                logError("Line "+lineNumber+" ignored. Not enough values to process."+stringTokens.countTokens());
                return false;
            }
            String partNumber = stringTokens.nextToken().trim();
            String revision = stringTokens.nextToken().trim();
            String iterString = stringTokens.nextToken().trim();
            String orgName = stringTokens.nextToken().trim();

            String partIdString = null;
            String projName = null;
            String subclass = null;

            if(stringTokens.hasMoreTokens()) {
                partIdString = stringTokens.nextToken().trim();
            }

            if(stringTokens.hasMoreTokens()) {
                projName = stringTokens.nextToken().trim();
            }
            if ((projName != null) && projName.trim().equals("")) {
                projName = null;
            }

            if(stringTokens.hasMoreTokens()) {
                subclass = stringTokens.nextToken().trim();
            }
            if ((subclass != null) && subclass.trim().equals("")) {
                subclass = null;
            }

            if ((partNumber == null) || (partNumber.equals(""))) {
                logError("Line "+lineNumber+" ignored. partNumber not entered.");
                return false;
            }
            if ((revision == null) || (revision.equals(""))) {
                logError("Line "+lineNumber+" ignored. revision not entered.");
                return false;
            }
            if ((iterString == null) || (iterString.equals(""))) {
                logError("Line "+lineNumber+" ignored. iteration not entered.");
                return false;
            }
            if ((orgName == null) || (orgName.equals(""))) {
                logError("Line "+lineNumber+" ignored. orgName not entered.");
                return false;
            }

            Long iteration = null;
            if(iterString != null && !iterString.equals("")) {
                try {
                    iteration = Long.parseLong(iterString);
                } catch (NumberFormatException nfe) {
                    logError("Line "+lineNumber+" ignored. iteration is not a number.");
                    return false;
                }
            }

            Long partId = null;
            if(partIdString != null && !partIdString.equals("")) {
                try {
                    partId = Long.parseLong(partIdString);
                } catch (NumberFormatException nfe) {
                    logError("Line "+lineNumber+" ignored. partId is not a number.");
                    return false;
                }
            }

            PartBatch batch = null;
            for (Iterator<PartBatch> i=batchList.iterator(); i.hasNext();) {
                PartBatch b = i.next();
                if (b.lineNumbers.size() < b.size) {
                    if (!b.partNumbers.contains(partNumber)) {
                        if (orgName.equals(b.getOrgName())) {
                            if (((projName == null) && ( b.getProjectName() == null)) ||
                                ((projName != null) && projName.equals(b.getProjectName())) ) {
                                    batch = b;
                                    break;
                            }
                        }
                    }
                }
            }
            if (batch == null) {
                batch = new PartBatch(aSize, numBatches, aFilePath, orgName, projName, subclass);
                batchList.add(batch);
            }
            logInfo("Calling PartBatch.addLine");
            batch.addLine(lineNumber, partNumber, revision, iteration, partId);
            logInfo("Leaving PartBatch.addLineToBatch");
            return true;
        } else {
            logInfo("Line "+lineNumber+" ignored. It is commented out.");
            logInfo("Leaving PartBatch.addLineToBatch");
            return false;
        }
    }

    @Override
    public void printSuccessfulBatch(String file) {
        logInfo("Printing failed batch to file " + file);
        if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < lineNumbers.size(); i++) {
                String error = (String) errors.get(i);
                if ((error == null) || (error.trim().equals(""))) {
                    sb.append(""+(Long) lineNumbers.get(i));
                    sb.append(",");
                    sb.append(""+batchId);
                    sb.append(",");
                    sb.append((String) partNumbers.get(i));
                    sb.append(",");
                    sb.append((String) partRevisions.get(i));
                    sb.append(",");
                    sb.append((Long) partIterations.get(i));
                    sb.append(",");
                    sb.append((String) orgName);
                    sb.append(",");
                    sb.append(((Long) partIds.get(i) == null)? "" :  partIds.get(i));
                    sb.append(",");
                    sb.append((projName == null) ? "" : projName);
                    sb.append(",");
                    sb.append((subclass == null) ? "" : subclass);
                    sb.append(",");
                    sb.append((String) ((errors.get(i) == null) ? "" : errors.get(i)));
                    sb.append("\r\n");
                }
            }            
            if(sb.length() > 0) {
                File f = new File(errDir, file);
                fw = new FileWriter(f, true);
                fw.write(sb.toString());
            }            
        } catch (IOException ioe) {
            logError("Batch.printSuccessfulBatch(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } finally {
            try {
                if (fw != null)
                    fw.close();
            } catch (IOException e) {
                logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
                e.printStackTrace();
            }
        }
    }



    @Override
    public void printFailedBatch(String file) {
        if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < lineNumbers.size(); i++) {
                String error = (String) errors.get(i);
                if ((error == null) || (error.trim().equals("")) ||"".equals(error)) {
                    continue;
                }
                sb.append(""+(Long) lineNumbers.get(i));
                sb.append(",");
                sb.append(""+batchId);
                sb.append(",");
                sb.append((String) partNumbers.get(i));
                sb.append(",");
                sb.append((String) partRevisions.get(i));
                sb.append(",");
                sb.append((Long) partIterations.get(i));
                sb.append(",");
                sb.append((String) orgName);
                sb.append(",");
                sb.append(((Long) partIds.get(i) == null)? "" :  partIds.get(i));
                sb.append(",");
                sb.append((projName == null) ? "" : projName);
                sb.append(",");
                sb.append((subclass == null) ? "" : subclass);
                sb.append(",");
                sb.append((String) ((errors.get(i) == null) ? "" : errors.get(i)));
                sb.append("\r\n");
            }
            if(sb.length() > 0 ) {
                logInfo("Printing failed batch to file " + file);
                File f = new File(errDir, file);
                fw = new FileWriter(f, true);
                fw.write(sb.toString());
            }
            
        } catch (IOException ioe) {
            logError("Batch.printFailedBatch(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } finally {
            try {
                if (fw != null)
                    fw.close();
            } catch (IOException e) {
                logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void printBatch(String file) {
        logInfo("Printing batch to file " + file);
        if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < lineNumbers.size(); i++) {
                sb.append(""+(Long) lineNumbers.get(i));
                sb.append(",");
                sb.append(""+batchId);
                sb.append(",");
                sb.append((String) partNumbers.get(i));
                sb.append(",");
                sb.append((String) partRevisions.get(i));
                sb.append(",");
                sb.append((Long) partIterations.get(i));
                sb.append(",");
                sb.append((String) orgName);
                sb.append(",");
                sb.append(((Long) partIds.get(i) == null)? "" :  partIds.get(i));
                sb.append(",");
                sb.append((projName == null) ? "" : projName);
                sb.append(",");
                sb.append((subclass == null) ? "" : subclass);
                sb.append(",");
                sb.append((String) ((errors.get(i) == null) ? "" : errors.get(i)));
                sb.append("\r\n");
            }
            if(sb.length() > 0) {
                File f = new File(errDir, file);
                fw = new FileWriter(f, true);
                fw.write(sb.toString());               
            }
        } catch (IOException ioe) {
            logError("Batch.printBatch(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } finally {
            try {
                if (fw != null)
                    fw.close();
            } catch (IOException e) {
                logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void printRow(String file, int i) {
        if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {
            File f = new File(errDir, file);
            fw = new FileWriter(f, true);

            StringBuffer sb = new StringBuffer();
            sb.append("" + (Long) lineNumbers.get(i));
            sb.append(",");
            sb.append("" + batchId);
            sb.append(",");
            sb.append((String) partNumbers.get(i));
            sb.append(",");
            sb.append((String) partRevisions.get(i));
            sb.append(",");
            sb.append((Long) partIterations.get(i));
            sb.append(",");
            sb.append((String) orgName);
            sb.append(",");
            sb.append(((Long) partIds.get(i) == null) ? "" : partIds.get(i));
            sb.append(",");
            sb.append((projName == null) ? "" : projName);
            sb.append(",");
            sb.append((String) ((errors.get(i) == null) ? "" : errors.get(i)));
            sb.append("\r\n");

            fw.write(sb.toString());
        } catch (IOException ioe) {
            logError("Batch.printRow(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } finally {
            try {
                if (fw != null)
                    fw.close();
            } catch (IOException e) {
                logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
                e.printStackTrace();
            }
        }
    }

    public void printPart(String file, WTPart builtPart) {
        if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {
            File f = new File(errDir, file);
            fw = new FileWriter(f, true);

            StringBuffer sb = new StringBuffer();
            sb.append(IdentityFactory.getDisplayIdentity(builtPart));
            sb.append(",");
            sb.append("" + PersistenceHelper.getObjectIdentifier(builtPart));
            sb.append("\r\n");
            fw.write(sb.toString());
        } catch (IOException ioe) {
            logError("Batch.printPart(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } finally {
            try {
                if (fw != null)
                    fw.close();
            } catch (IOException e) {
                logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void printToBeBuiltPart(String file) {
        logInfo("Printing to be built parts to file " + file);
        if (errDir == null) {
            return;
        }
        FileWriter fw = null;
        try {

            for (int i = 0; i < lineNumbers.size(); i++) {
                WTPart builtPart = (WTPart) parts.get(i);

                if(builtPart != null && !partsInError.contains(builtPart)) {
                    if(fw == null){
                        File f = new File(errDir, file);
                        fw = new FileWriter(f, true);
                    }
                    StringBuffer sb = new StringBuffer();
                    sb.append(IdentityFactory.getDisplayIdentity(builtPart));
                    sb.append(",");
                    sb.append("" + PersistenceHelper.getObjectIdentifier(builtPart));
                    sb.append("\r\n");
                    fw.write(sb.toString());                    
                }
            }

        } catch (IOException ioe) {
            logError("Batch.printToBeBuiltPart(String file) exception printing batch");
            ioe.printStackTrace();
            return;
        } finally {
            try {
                if (fw != null)
                    fw.close();
            } catch (IOException e) {
                logError("Batch.printFailedBatch(String file) exception printing batch. Filename = " + file);
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean validateBatch() throws Exception {
        logInfo("Entering PartBatch.validateBatch. validating batch - id "+this.batchId);
        boolean validBatch = true;
        org = lookupOrganization(orgName);
        if (org == null) {
            for (int i = 0; i < lineNumbers.size(); i++) {
                parts.add(null);
                errors.add("Organization "+orgName+" not found");
            }
            return false;
        }
        proj = null;
        if (projName != null) {
            proj = lookupProject(projName);
            if (proj == null) {
                for (int i = 0; i < lineNumbers.size(); i++) {
                    parts.add(null);
                    errors.add("Project  "+projName+" not found");
                }
                return false;
            }
        }


        ArrayList idList = new ArrayList();
        for (int i=0; i<partIds.size(); i++) {
            if (partIds.get(i) != null) {
                idList.add(partIds.get(i));
            }
        }
        long[] ids = new long[idList.size()];
        String[] numbers = new String[partNumbers.size()-idList.size()];
        for (int i=0; i<idList.size(); i++) {
            ids[i] = (Long) idList.get(i);
        }

        int numInd =0;
        for (int i=0; i<partNumbers.size(); i++) {
            if (partIds.get(i) == null) {
                numbers[numInd++] = (String) partNumbers.get(i);
            }
        }

        Class partClass = WTPart.class;
        if (subclass != null) {
            partClass = Class.forName(subclass);
        }

        QuerySpec qs = new QuerySpec(partClass);
        if(ids.length > 0) {
            qs.appendWhere(new SearchCondition(partClass,
                            WTAttributeNameIfc.OBJECT_IDENTIFIER+ "." + ObjectIdentifier.ID,
                            ids),
                  0);
            if (numbers.length >0) {
                qs.appendOr();
                qs.appendWhere(new SearchCondition(partClass,
                        WTPart.NUMBER,
                        numbers, true, false),
                    0);
            }
        } else {
            qs.appendWhere(new SearchCondition(partClass,
                                WTPart.NUMBER,
                                numbers, true, false),
                            0);
        }

        ClassAttribute latest = new ClassAttribute(partClass, Iterated.ITERATION_INFO + "." + IterationInfo.LATEST);
        qs.appendAnd();
        qs.appendWhere(new SearchCondition(latest, SearchCondition.IS_TRUE), 0);

        qs.appendAnd();
        qs.appendWhere (new SearchCondition (partClass,
                                             WTPart.ORGANIZATION_REFERENCE+'.'+WTAttributeNameIfc.REF_OBJECT_ID,
                                             SearchCondition.EQUAL,
                                             PersistenceHelper.getObjectIdentifier(org).getId()),
                        new int[] {0});

        QueryResult allIterations = PersistenceHelper.manager.find(qs);

        HashMap number2Iterations = new HashMap();
        while (allIterations.hasMoreElements()) {
            WTPart part = (WTPart) allIterations.nextElement();
            WTHashSet iterations = (WTHashSet) number2Iterations.get(((WTPartMaster) part.getMaster()).getNumber());
            if (iterations == null) {
                iterations = new WTHashSet();
                number2Iterations.put(((WTPartMaster) part.getMaster()).getNumber(), iterations);
            }
            iterations.add(part);
        }

        for (int i = 0; i < lineNumbers.size(); i++) {
            String number = (String) partNumbers.get(i);
            WTPart part = null;
            String error = "";

            Long partId = (Long) partIds.get(i);
            Long iterId = (Long) partIterations.get(i);
            String rev = (String) partRevisions.get(i);
            WTHashSet iterations = (WTHashSet) number2Iterations.get(number);
            if (iterations != null) {
                for (Iterator<WTPart> j = iterations.persistableIterator(partClass, true); j.hasNext();) {
                    WTPart iter = j.next();
                    Long id = PersistenceHelper.getObjectIdentifier(iter).getId();
                    String version = VersionControlHelper.getVersionIdentifier(iter).getSeries().getValue();
                    String iteration = VersionControlHelper.getIterationIdentifier(iter).getValue();
                    if (version.equals(rev) && iteration.equals("" + iterId)) {
                        if ((partId == null) || (partId.equals(id))) {
                            if ((projName == null && !wt.epm.util.EPMContainerHelper.isProject(iter.getContainerReference())) || (proj != null && ObjectReference.newObjectReference(proj).equals(iter.getContainerReference()))) {
                                part = iter;
                                break;
                            }
                        }
                    }
                }
                if (part == null) {
                    error = "matching version/iteration not found ";
                    if (partId != null) {
                        error = error + " OR part Id may not be matching ";
                    }
                    if (projName != null) {
                        error = error + " OR part may not be in project " + projName;
                    }
                    validBatch = false;
                } else if (WorkInProgressHelper.isCheckedOut(part)) {
                    error = "Part is checked out";
                    validBatch = false;
                } else if (OwnershipHelper.isOwned((Workable) part)) {
                    error = "The part is not in shared folder";
                    validBatch = false;
                }
            } else {
                error = "User does not have access to part OR part may not be latest iteration OR part may exist in different organization OR part subclass may be different";
                validBatch = false;
            }
            parts.add(part);
            errors.add(error);
            if (!"".equals(error)) {
                if (part != null) {
                    partsInError.add(part);
                }
            }
        }


        WTHashSet prts = new WTHashSet();
        for (Iterator i=parts.iterator(); i.hasNext();) {
            WTPart p = (WTPart) i.next();
            if (p != null) {
                if (!partsInError.contains(p)) {
                    prts.add(p);
                }
            }
        }

        part2Doc = new WTKeyedHashMap();

        if (!prts.isEmpty()) {
            WTCollection rules = new WTHashSet();
            try {
                rules = BuildHelper.service.getRulesToBeBuiltForSources(prts, CollectionsHelper.EMPTY_COLLECTION, true);
            } catch (WTException e) {
                for (int i = 0; i < lineNumbers.size(); i++) {
                    WTPart p = (WTPart) parts.get(i);
                    if ((p != null) && (!partsInError.contains(p))) {
                        try {
                            WTCollection r = BuildHelper.service.getRulesToBeBuiltForSources(new WTHashSet(Arrays.asList(p)), CollectionsHelper.EMPTY_COLLECTION, true);
                            rules.addAll(r);
                        } catch (WTException ex) {
                            ex.printStackTrace();
                            errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+ex.getLocalizedMessage():ex.getLocalizedMessage());
                            validBatch = false;
                            partsInError.add(p);
                        }
                    }
                }
            }

            for (Iterator i = rules.persistableIterator(); i.hasNext();) {
                EPMBuildRule rule = (EPMBuildRule) i.next();
                EPMDocument doc = (EPMDocument) rule.getBuildSource();
                WTPart part = (WTPart) rule.getBuildTarget();
                WTHashSet docsForPart = (WTHashSet) part2Doc.get(part);
                if (docsForPart == null) {
                    docsForPart = new WTHashSet();
                    part2Doc.put(part, docsForPart);
                }
                docsForPart.add(doc);
            }

            for (int i = 0; i < lineNumbers.size(); i++) {
                WTPart part = (WTPart) parts.get(i);
                if (part == null) {
                    continue;
                }
                
                if (partsInError.contains(part)) {
                    validBatch = false;
                    part2Doc.remove(part);
                    continue;
                }

                if (builtParts.contains(part)) {
                    errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+"part is already built. "+IdentityFactory.getDisplayIdentity(part):"part is already built. "+IdentityFactory.getDisplayIdentity(part));
                    validBatch = false;
                    part2Doc.remove(part);
                    partsInError.add(part);
                }

                WTCollection docsForPart = (WTCollection) part2Doc.get(part);
                if (docsForPart != null) {
                    for (Iterator k=docsForPart.persistableIterator(); k.hasNext();) {
                        EPMDocument d = (EPMDocument) k.next();
                        if (WorkInProgressHelper.isCheckedOut(d)) {
                            errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+"Document associated to part is checked out "+IdentityFactory.getDisplayIdentity(part)+" doc = "+IdentityFactory.getDisplayIdentity(d):"Document associated to part is checked out "+IdentityFactory.getDisplayIdentity(part)+" doc = "+IdentityFactory.getDisplayIdentity(d));
                            validBatch = false;
                            part2Doc.remove(part);
                            partsInError.add(part);
                        } else if (OwnershipHelper.isOwned((Workable) d)) {
                            errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+"Document associated to part is not in shared folder "+IdentityFactory.getDisplayIdentity(part)+" doc = "+IdentityFactory.getDisplayIdentity(d):"Document associated to part is not in shared folder "+IdentityFactory.getDisplayIdentity(part)+" doc = "+IdentityFactory.getDisplayIdentity(d));
                            validBatch = false;
                            part2Doc.remove(part);
                            partsInError.add(part);
                        } else if (!d.isVerified()) {
                            errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+"Document associated to part is unverified "+IdentityFactory.getDisplayIdentity(part)+" doc = "+IdentityFactory.getDisplayIdentity(d):"Document associated to part is unverified "+IdentityFactory.getDisplayIdentity(part)+" doc = "+IdentityFactory.getDisplayIdentity(d));
                            validBatch = false;
                            part2Doc.remove(part);
                            partsInError.add(part);
                        }
                    }
                } else {
                    errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+"part can not be built - see method server logs for more details. ":"part can not be built - see method server logs for more details. ");
                    validBatch = false;
                    part2Doc.remove(part);
                    partsInError.add(part);
                }
            }
        }
        logInfo("Leaving PartBatch.validateBatch. Valid batch: " + validBatch);
        return validBatch;
    }

    @Override
    public void build(boolean recreate) throws Exception {
        logInfo("Entering PartBatch.build with batch id = " + this.batchId);
        logInfo ("Parts = " + parts);
        logInfo("Line numbers = " + lineNumbers);
        logInfo("part2Doc = "+ part2Doc);

        WTHashSet allParts = new WTHashSet();
        allParts.addAll(part2Doc.wtKeySet());
        logInfo("allParts = "+ allParts);
        
        if (!allParts.isEmpty()) {
            ConfigSpec cs = getConfigSpec(proj);
            WTHashSet builtObjs = new WTHashSet();
            Transaction trx = null;
            try {
                trx = new Transaction();
                trx.start();
            
                if (recreate) {
                    Folder checkoutFolder = WorkInProgressHelper.service.getCheckoutFolder();
                    WTValuedHashMap checkedOutParts = checkout((Workable[]) allParts.toArray(new Workable[0]), " parts ", checkoutFolder, false);
                    try {
                        WTCollection links = BuildServiceUtility.expand(checkedOutParts.wtValues(), BuildableLink.class, BuildableLink.PERSISTABLE_ROLE, null, true, true);
                        PersistenceHelper.manager.delete(new WTHashSet(links));

                        WTKeyedMap dbrMap = WTPartBuildHelper.service.getDeletedBuildReferences2(checkedOutParts.wtValues());
                        WTHashSet dbrs = new WTHashSet();
                        for (Iterator i = dbrMap.wtKeySet().persistableIterator(); i.hasNext();) {
                            WTPart part = (WTPart) i.next();
                            dbrs.addAll((WTCollection)dbrMap.get(part));
                        }
                        PersistenceHelper.manager.delete(dbrs);

                        WTValuedMap tgts = BuildHelper.service.buildTarget(checkedOutParts.wtValues(), cs);
                        WTCollection checkedIn  = WorkInProgressHelper.service.checkin(tgts.wtValues(), "Build By utility - BulkPartBuild");
                        builtObjs.addAll(checkedIn);
                        this.printSuccessfulBatch(successFileName);
                        trx.commit();
                        trx=null;
                    } catch (WTException e) {
                        e.printStackTrace();
                        logInfo(e.getMessage());
                        logInfo("Error occurred while building the batch. Building the parts individually. batch id " + this.getBatchId());
                        
                        trx.rollback();
                        trx=null;

                        WTHashSet tgtsToBuild = new WTHashSet();
                        for (int i = 0; i < parts.size(); i++) {
                            try {
                                
                                WTPart part = (WTPart) parts.get(i);
                                if (part == null) {
                                    continue;
                                }
                                
                                checkedOutParts = checkout((Workable[])new WTPart[] {part}, " parts ", checkoutFolder, false);
                                tgtsToBuild = new WTHashSet(checkedOutParts.wtValues());
                                
                                WTCollection links = BuildServiceUtility.expand(tgtsToBuild, BuildableLink.class, BuildableLink.PERSISTABLE_ROLE, null, true, true);
                                PersistenceHelper.manager.delete(new WTHashSet(links));
                                
                                WTKeyedMap dbrMap = WTPartBuildHelper.service.getDeletedBuildReferences2(tgtsToBuild);
                                WTHashSet dbrs = new WTHashSet();
                                for (Iterator j = dbrMap.wtKeySet().persistableIterator(); j.hasNext();) {
                                    WTPart part2 = (WTPart) j.next();
                                    dbrs.addAll((WTCollection)dbrMap.get(part2));
                                }
                                PersistenceHelper.manager.delete(dbrs);
                                WTValuedMap builtTgts = BuildHelper.service.buildTarget(tgtsToBuild, cs);
                                WTCollection checkedIn  = WorkInProgressHelper.service.checkin(builtTgts.wtValues(), "Build By utility - BuildAssociatedPart");
                                builtObjs.addAll(checkedIn);
                                printRow(successFileName, i);
                            } catch (WTException X) {
                                X.printStackTrace();
                                logInfo(X.getMessage());
                                errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+X.getLocalizedMessage():X.getLocalizedMessage());
                                logInfo("Number in catch: " + partNumbers.get(i));
                                WorkInProgressHelper.service.undoCheckouts(tgtsToBuild);
                            } catch (Exception X) {
                                X.printStackTrace();
                                logInfo(X.getLocalizedMessage());
                                errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+X.getLocalizedMessage():X.getLocalizedMessage());
                                WorkInProgressHelper.service.undoCheckouts(tgtsToBuild);
                            }
                        }
                        
                    }

                } else {
                    try {
                        WTValuedMap tgts = BuildHelper.service.buildTarget(allParts, cs);
                        builtObjs.addAll(tgts.wtValues());
                        this.printSuccessfulBatch(successFileName);
                        trx.commit();
                        trx = null;
                    } catch (WTException e) {
                        e.printStackTrace();
                        logInfo(e.getMessage());
                        logInfo("Error occurred while building the batch. Building the parts individually. batch id " + this.getBatchId());
                        
                        trx.rollback();
                        trx=null;

                        for (int i = 0; i < parts.size(); i++) {
                            if ((errors.get(i) == null) || ("".equals(errors.get(i)))) {
                                WTPart part = (WTPart) parts.get(i);
                                if (part == null) {
                                    continue;
                                }
                                try {
                                    BuildTarget builtTgt = BuildHelper.service.buildTarget(part, cs);
                                    builtObjs.add(builtTgt);
                                    printRow(successFileName, i);
                                } catch (WTException X) {
                                    X.printStackTrace();
                                    logInfo(X.getMessage());
                                    errors.set(i, (errors.get(i) != null)? errors.get(i)+", "+X.getLocalizedMessage():X.getLocalizedMessage());
                                }
                            }
                        }
                    }
                }

                if (!builtObjs.isEmpty()) {
                    builtParts.addAll(builtObjs);
                    for (Iterator i = builtObjs.persistableIterator(); i.hasNext();) {
                        WTPart part = (WTPart) i.next();
                        printPart(builtTargetsFileName, part);
                    }
                    EPMOperationWrapper.publishViewablesForDocs(new WTArrayList(builtObjs));
                }
               
            } finally {
                if (trx != null) {
                    trx.rollback();
                }
                
                printFailedBatch(failedFileName);
            }                
        }
        logInfo("Leaving PartBatch.build");
    }

    @Override
    public void cleanup() {
        part2Doc = new WTKeyedHashMap();
    }

    @Override
    public void setFiles(String aSuccessFile, String aFailedFile, String aBuiltTargetsFileName) {
        successFileName = aSuccessFile;
        failedFileName = aFailedFile;
        builtTargetsFileName = aBuiltTargetsFileName;
    }

    private ConfigSpec getConfigSpec(Project2 proj) throws WTException {
        ConfigSpec cs = null;
        if (projName != null) {
            try {
                cs = SandboxConfigSpec.newSandboxConfigSpec(proj.getContainerReference(),
                    true,   /* use sandbox baseline */
                    false,  /* Do not use personal checkout */
                    null,   /* current principal */
                    new EPMConfigSpecFilter(EPMDocConfigSpec.newEPMDocConfigSpec(), EPMFilters.NO_WORKING));
                                            /* user config spec is PDM Latest/No Working Copies. Needed to find dependents that are not
                                             * shared to Sandbox.
                                             */
            } catch (WTPropertyVetoException wtpve) {
                throw new WTException(wtpve);
            }
        } else {
            cs = new EPMConfigSpecFilter(EPMDocConfigSpec.newEPMDocConfigSpec(), EPMFilters.NO_WORKING);
        }
        return cs;
    }

    public static WTOrganization lookupOrganization(String name) throws WTException {
        QuerySpec qs = new QuerySpec(WTOrganization.class);
        qs.appendWhere(new SearchCondition( WTOrganization.class,
                                            WTOrganization.NAME,
                                            SearchCondition.EQUAL,
                                            name), new int[] { 0});
        QueryResult orgs = PersistenceHelper.manager.find(qs);
        if ( orgs.hasMoreElements() ) {
            return (WTOrganization) orgs.nextElement();
        }
        return null;
    }

    public static Project2 lookupProject(String name) throws WTException {
        QuerySpec qs = new QuerySpec (Project2.class);
        qs.appendWhere (new SearchCondition (Project2.class,
                                             Project2.NAME,
                                             SearchCondition.EQUAL,
                                             name), new int[] { 0});
        QueryResult qr = PersistenceHelper.manager.find (qs);
        if (qr.hasMoreElements ()) {
            return (Project2) qr.nextElement ();
        }
        return null;
    }

    public static WTValuedHashMap checkout(Workable[] objects, String type, Folder checkoutFolder, boolean allowNonLatestCheckout) throws WTException, WTPropertyVetoException {
        WTArrayList checkoutObjs = new WTArrayList(Arrays.asList(objects));
        WTCollection checkOutLinks = WorkInProgressHelper.service.checkout(checkoutObjs, checkoutFolder, "Build By utility - BulkPartBuild", allowNonLatestCheckout);

        WTValuedHashMap checkedoutCopies = new WTValuedHashMap();
        Iterator i_checkedOut = checkOutLinks.persistableIterator();
        while (i_checkedOut.hasNext()) {
            CheckoutLink cLink = (CheckoutLink) i_checkedOut.next();
            Workable wCopy = cLink.getWorkingCopy();
            Workable oCopy = cLink.getOriginalCopy();
            checkedoutCopies.put(oCopy, wCopy);
        }

        return checkedoutCopies;
    }

    static {
        try {
            WTProperties wtproperties = WTProperties.getLocalProperties();
            String errDirString = wtproperties.getProperty("wt.logs.dir");
            errDir = new File(errDirString);
            if (!errDir.exists()) {
                errDir = null;
            }

        } catch (Exception e) {
            System.err.println("Error initializing wt.logs.dir");
            e.printStackTrace(System.err);
        }
    }
}
