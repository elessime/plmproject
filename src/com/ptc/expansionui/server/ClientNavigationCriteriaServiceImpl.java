///* bcwti
// *
// * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
// *
// * This software is the confidential and proprietary information of PTC
// * and is subject to the terms of a software license agreement. You shall
// * not disclose such confidential information and shall use it only in accordance
// * with the terms of the license agreement.
// *
// * ecwti
// */
//package com.ptc.expansionui.server;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Locale;
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.ResourceBundle;
//import java.util.Set;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
//
//import org.apache.log4j.Logger;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import com.extjs.gxt.ui.client.util.Format;
//import com.google.gwt.user.server.rpc.RemoteServiceServlet;
//import com.ptc.core.components.beans.DataUtilityExceptionHandler;
//import com.ptc.core.components.util.TimeZoneHelper;
//import com.ptc.core.meta.common.TypeIdentifier;
//import com.ptc.expansionui.client.ClientNavigationCriteriaService;
//import com.ptc.expansionui.client.ExpansionCriteriaConstants;
//import com.ptc.expansionui.client.ExpansionCriteriaConstants.ECExceptionConstants;
//import com.ptc.expansionui.client.ExpansionCriteriaConstants.ECParamsConstants;
//import com.ptc.expansionui.client.ExpansionCriteriaJSONConstants;
//import com.ptc.expansionui.client.ui.ExpansionCriteriaUtil;
//import com.ptc.expansionui.client.ui.beans.ClientNavigationCriteriaBean;
//import com.ptc.expansionui.client.ui.beans.ExpansionCriteriaBean;
//import com.ptc.expansionui.client.ui.beans.FilterBean;
//import com.ptc.expansionui.client.ui.beans.SavedExpansionCriteriaBean;
//import com.ptc.expansionui.client.ui.beans.SavedExpansionCriteriaBean.VaraintSpecMenuStatus;
//import com.ptc.expansionui.client.ui.beans.VerificationResultBean;
//import com.ptc.expansionui.server.populator.FilterPopulator;
//import com.ptc.wpcfg.doc.VariantSpec;
//
//import wt.configuration.BaselineConfigurationConfigSpec;
//import wt.configuration.ConfigurationHelper;
//import wt.doc.WTDocument;
//import wt.epm.EPMDocConfigSpec;
//import wt.epm.EPMDocument;
//import wt.epm.workspaces.EPMAsStoredConfigSpec;
//import wt.epm.workspaces.EPMWorkspace;
//import wt.epm.workspaces.EPMWorkspaceHelper;
////import wt.facade.expansioncriteria.ExpansionCriteriaFacade;
//import wt.fc.ObjectIdentifier;
//import wt.fc.ObjectNoLongerExistsException;
//import wt.fc.ObjectReference;
//import wt.fc.Persistable;
//import wt.fc.PersistenceHelper;
//import wt.fc.QueryResult;
//import wt.fc.ReferenceFactory;
//import wt.fc.WTReference;
//import wt.fc.collections.CollectionsHelper;
//import wt.fc.collections.WTArrayList;
//import wt.fc.collections.WTCollection;
//import wt.fc.collections.WTHashSet;
//import wt.fc.collections.WTSet;
//import wt.filter.FilterVerificationResult;
//import wt.filter.NavCriteriaCacheId;
//import wt.filter.NavCriteriaContext;
//import wt.filter.NavigationCriteria;
////import wt.filter.NavigationCriteriaConstants;
//import wt.filter.NavigationCriteriaHelper;
//import wt.filter.NavigationFilter2;
//import wt.generic.GenericHelper;
//import wt.generic.Genericizable;
//import wt.inf.container.WTContainer;
//import wt.inf.container.WTContainerRef;
//import wt.lifecycle.State;
//import wt.log4j.LogR;
//import wt.org.WTPrincipal;
//import wt.org.WTUser;
//import wt.part.WTPart;
//import wt.part.WTPartBaselineConfigSpec;
//import wt.part.WTPartConfigSpec;
//import wt.part.WTPartHelper;
//import wt.part.WTPartMaster;
//import wt.part.WTProductConfiguration;
//import wt.part.WTProductConfigurationMaster;
//import wt.part.alternaterep.WTPartAlternateRep;
//import wt.pom.PersistenceException;
//import wt.query.QuerySpec;
//import wt.query.SearchCondition;
//import wt.session.SessionHelper;
//import wt.session.SessionServerHelper;
//import wt.templateutil.processor.ServletSessionCookie;
//import wt.templateutil.processor.ServletSessionCookieManager;
//import wt.type.TypedUtility;
//import wt.util.ClassCache;
//import wt.util.WTAttributeNameIfc;
//import wt.util.WTException;
//import wt.util.WTPropertyVetoException;
//import wt.util.WTRuntimeException;
//import wt.util.WTStandardDateFormat;
//import wt.vc.Iterated;
//import wt.vc.Mastered;
//import wt.vc.baseline.ManagedBaseline;
//import wt.vc.config.BaselineConfigSpec;
//import wt.vc.config.ConfigHelper;
//import wt.vc.config.ConfigSpec;
//import wt.vc.config.ConfigSpecVerificationResult;
//import wt.vc.config.PersistableConfigSpecDelegate;
//
///**
// * The Class ClientNavigationCriteriaServiceImpl.
// */
//public class ClientNavigationCriteriaServiceImpl extends RemoteServiceServlet implements
//        ClientNavigationCriteriaService {
//    private static final long serialVersionUID = 1L;
//
//    /** The Constant CLASSNAME. */
//    private static final String CLASSNAME = ClientNavigationCriteriaServiceImpl.class.getName();
//
//    /** The Constant log. */
//    private static final Logger log = LogR.getLogger(CLASSNAME);
//
//    private final String componentRB = "com.ptc.core.ui.componentRB";
//
//    private static final String CONFIG_TYPE = "configType";
//
//    private static final String AS_STORED_CONFIG_TYPE = "asStoredConfig";
//
//    private static final String AS_MATURED_CONFIG_TYPE = "asMatured";
//
//    private static final String SEED_OID_KEY = "seedOid";
//
//    private static final String OID_KEY = "oid";
//
//    private static final String PFSB_VS_NC_CACHE_COOKIE_ID = "PFSB-VS-NC";
//
//    private static final String PFSB_VS_APP_NAME = "com.ptc.windchill.enterprise.productfamily.pfsb.client.PFSB";
//
//    private static final ReferenceFactory REFERENCE_FACTORY = new ReferenceFactory();
//
//    // List of Strings holding already processed (saved/updated) filter information
//    private List<String> processedFilterInfo = new ArrayList<String>();
//
//    // Maximum number of Strings that the above list will hold at any point in time
//    public static final short MAXSIZE = 5;
//
//    /*
//     * A Map where each key is a String representation of a NavCriteriaCacheId object and the value is a String holding
//     * filter information that is currently in use.
//     */
//    private static Map<String, String> currentFilterInfo = new HashMap<String, String>();
//
//    private static UpdateNavCriteriaDelegateFactory updateNavCriteriaDelegateFactory = new UpdateNavCriteriaDelegateFactory();
//    
//    /**
//     * Gets the default navigation criteria id.
//     *
//     * @param seedObjects
//     *            the seed objects
//     * @param context
//     *            the context
//     * @param clientInfoBean
//     *            the client info bean
//     *
//     * @return the default navigation criteria id
//     */
//    @Override
//    public String getDefaultNavigationCriteriaID(List<String> seedObjects, ClientNavigationCriteriaBean clientInfoBean,
//            Map<String, String> context, Map<String, Object> appMap) {
//        if (log.isDebugEnabled()) {
//            log.debug("Enter => getDefaultNavigationCriteriaID(): seedObjects: " + seedObjects
//                    + " clientInfoBean: " + clientInfoBean + " context: " + context);
//        }
//        String ncid = getDefaultNCID(seedObjects, clientInfoBean, context, appMap);
//
//        if (log.isDebugEnabled()) {
//            log.debug("getDefaultNavigationCriteriaID: Completed getting the default NCID: " + ncid);
//        }
//        return ncid;
//    }
//
//    @Override
//    public String getDefaultNavigationCriteriaID(List<String> seedObjects, ClientNavigationCriteriaBean clientInfoBean,
//            Map<String, String> context) {
//        return getDefaultNavigationCriteriaID(seedObjects, clientInfoBean, context, null);
//    }
//
//    /**
//     * Handles the different use cases for getting the default Navigation criteria id
//     *
//     * @param seedObjects
//     *            - the seed objects
//     * @param clientInfoBean
//     *            - the clientInfoBean
//     * @param context
//     *            - the context
//     * @return
//     */
//    private String getDefaultNCID(List<String> seedObjects, ClientNavigationCriteriaBean clientInfoBean,
//            Map<String, String> context, Map<String, Object> appMap) {
//        String ncid = "";
//        try {
//            boolean isNCReset = false;
//            boolean isNCRevert = false;
//            boolean isSystemDefault = true;
//
//            if (context != null && context.get(ExpansionCriteriaConstants.IS_SYSTEM_DEFAULT_EC) != null) {
//                isSystemDefault = Boolean.parseBoolean(context.get(ExpansionCriteriaConstants.IS_SYSTEM_DEFAULT_EC));
//            }
//            if (!isSystemDefault) {
//                // If a user defined saved filter is being used and if it has been deleted or unshared, flag it
//                if (ExpansionCriteriaConstants.App_List.contains(clientInfoBean.getAppName())) {
//                    isNCRevert = Boolean.valueOf(defaultSavedFilterInValid(clientInfoBean.getAppName()));
//                }
//            }
//
//            NavigationCriteria nc = getDefaultNavigationCriteria(seedObjects, context, appMap);
//
//            if (!isSystemDefault) {
//                // If a user defined saved filter is defined, if it does not apply to the top node,
//                // if the config spec has apply to top set, flag it
//                if (ExpansionCriteriaConstants.App_List.contains(clientInfoBean.getAppName())) {
//                    boolean isApplyToTop = nc.isApplyToTopLevelObject();
//                    if (isApplyToTop) {
//                        String id = applyNavigationCriteria(seedObjects.get(0), nc);
//
//                        // If the top level part does not resolve to an iteration or if the iteration is different in
//                        // PSB, remove apply to top
//                        if (id == null ||
//                                ((NavigationCriteriaConstants.PSB_APP_NAME.equals(clientInfoBean.getAppName())))
//                                        && !(removeORFromObject(id).equals(removeORFromObject(seedObjects.get(0))))) {
//                            try {
//                                // Get the json from the current navigation criteria
//                                String json = NavigationCriteriaHelper.service.getJSONFromNavigationCriteria(nc);
//                                // Create a new navigation criteria from the current navigation Criteria's json
//                                NavigationCriteria resetNC = NavigationCriteriaHelper.service
//                                        .getNavigationCriteriaFromJSON(json);
//
//                                resetNC.setApplyToTopLevelObject(false);
//                                resetNC.setName("");
//                                isNCReset = true;
//                                nc = resetNC;
//                            } catch (WTPropertyVetoException wtpve) {
//                                log.error("Error setting the apply to top flag on nc" + nc, wtpve);
//                            }
//                        }
//                    }
//                    // Update the user defined default saved filter, with the needed config specs, for a Variant Spec
//                    updateDefaultSavedNavigationCriteria(nc, clientInfoBean, context, appMap);
//
//                }
//            }
//            if (log.isDebugEnabled()) {
//                log.debug("nc from getDefaultNavigationCriteria(): " + nc);
//            }
//            ncid = NavigationCriteriaHelper.service.cacheNavigationCriteria(nc, getNavCriteriaCacheId(clientInfoBean));
//
//            if (isNCReset) {
//                ncid += ExpansionCriteriaConstants.DEFAULT_SAVED_NC_RESET;
//            }
//
//            if (isNCRevert) {
//                ncid += ExpansionCriteriaConstants.DEFAULT_SAVED_NC_REVERT;
//            }
//        } catch (WTException e) {
//            log.error("Can't get Default NC ID for " + seedObjects, e);
//        }
//
//        return ncid;
//    }
//
//    /**
//     * Update the navigation criteria with the needed configspecs and filters for a VariantSpec, if it is a user defined default
//     * saved filter
//     *
//     * @param nc
//     * @param bean
//     * @throws WTException
//     */
//    private void updateDefaultSavedNavigationCriteria(NavigationCriteria nc,
//            ClientNavigationCriteriaBean clientInfoBean, Map<String, String> context, Map<String, Object> appMap)
//                    throws WTException {
//
//        // If the context is null, return
//        if (context == null) {
//            return;
//        }
//
//        // If there is no variant spec oid in the context and if the nav context info does not have the BOMContext
//        // information, return
//        if (context.get(ExpansionCriteriaConstants.VARIANTSPEC_OID) == null
//                && (appMap == null
//                        || (appMap != null && appMap.get(ExpansionCriteriaConstants.NAV_CONTEXT_INFO) == null))) {
//            return;
//        }
//        String app = clientInfoBean.getAppName();
//        if (nc != null) {
//            final ExpansionCriteriaBean bean = new ExpansionCriteriaBean();
//            NavCriteriaContext navCtx = new NavCriteriaContext();
//            navCtx.setApplicationName(app);
//            String ncOid = NavigationCriteriaHelper.service.getUserDefaultNavCriteriaId(navCtx);
//            if (ncOid != null && !ncOid.isEmpty()) {
//                bean.setNcOid(ncOid);
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put(ExpansionCriteriaConstants.VARIANTSPEC_OID,
//                        context.get(ExpansionCriteriaConstants.VARIANTSPEC_OID));
//                params.put(ExpansionCriteriaConstants.EDIT_EC_ACTION_METHOD, app);
//                bean.setParams(params);
//                bean.setAppMap(appMap);
//                // Set the configspecs on the user defined default navigation criteria,
//                NavigationCriteria updatedNavCriteria = updateNavigationCriteria(bean);
//                nc.setConfigSpecs(updatedNavCriteria.getConfigSpecs());
//                //This is a "save" fix for SPR 7765179. After consulting with project coordinator and PO
//                //We decided to provide a "nice" fix during next spring because its to risky to providing it now
//                if( NavigationCriteriaConstants.PSBVS_APP_NAME.equals(app) ) {
//                    try {
//                        nc.setFilters(updatedNavCriteria.getFilters());
//                    } catch (WTPropertyVetoException e) {
//                        log.error("Problem occured during setting filters to navigation criteria " + e.getMessage());
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * Method to determine if the user defined default navigation criteria has been deleted or unshared
//     * 
//     * @param appName
//     * @return
//     */
//    @Override
//    public String defaultSavedFilterInValid(String appName) {
//        NavCriteriaContext navContext = new NavCriteriaContext();
//        navContext.setApplicationName(appName);
//        try {
//            String defaultNcOid = NavigationCriteriaHelper.service.getUserDefaultNavCriteriaId(navContext);
//            NavigationCriteria defaultNC = null;
//
//            if (defaultNcOid != null && !defaultNcOid.isEmpty()) {
//                try {
//                    // Try to inflate the Navigation Criteria oid
//                    defaultNC = (NavigationCriteria) REFERENCE_FACTORY.getReference(defaultNcOid)
//                            .getObject();
//                }
//                // If the default Navigation Criteria has been deleted, the default saved filter is invalid
//                catch (WTRuntimeException wte) {
//                    if (wte.getNestedThrowable() instanceof ObjectNoLongerExistsException) {
//                        log.debug("Object no longer exists: " + defaultNcOid, wte);
//                        return "true";
//                    }
//                }
//
//                WTUser user = (WTUser) SessionHelper.manager.getPrincipal();
//                // If the default Navigation Criteria belongs to another user and it is not shared, the default saved
//                // filter is invalid
//                if (!defaultNC.getOwner().equals(user) && !defaultNC.isSharedToAll()) {
//                    return "true";
//                }
//            }
//        } catch (WTException wte) {
//            log.error("Can't determine if the default saved filter is invalid -", wte);
//        }
//        return "false";
//    }
//
//    @Override
//    public String getDefaultNavigationCriteriaIDWithOptionFilter(List<String> seedObjects,
//            ClientNavigationCriteriaBean clientInfoBean,
//            Map<String, String> context, String oid) {
//
//        if (log.isDebugEnabled()) {
//            log.debug("Enter => getDefaultNavigationCriteriaIDWithOptionFilter(): seedObjects: " + seedObjects
//                    + " clientInfoBean: " + clientInfoBean + " context: " + context + " Page Context: " + oid);
//        }
//
//        if (oid != null && !oid.isEmpty()) {
//            context.put(ExpansionCriteriaConstants.VARIANTSPEC_OID, oid);
//        }
//
//        String ncid = getDefaultNCID(seedObjects, clientInfoBean, context, null);
//        if (log.isDebugEnabled()) {
//            log.debug("getDefaultNavigationCriteriaIDWithOptionFilter: Completed getting the default NCID: " + ncid);
//        }
//        return ncid;
//    }
//
//    /**
//     * Gets the baseline navigation criteria id (NCID).
//     *
//     * @param baseline
//     *            the baseline
//     * @param seedObjects
//     *            the seed objects
//     * @param clientInfoBean
//     *            the client info bean
//     * @return the baseline navigation criteria id
//     */
//    public String getBaselineNavigationCriteriaID(ManagedBaseline baseline, List<String> seedObjects,
//            ClientNavigationCriteriaBean clientInfoBean) {
//        if (log.isDebugEnabled()) {
//            log.debug("Entry => getBaselineNavigationCriteriaID().");
//            log.debug("baseline: " + baseline + " seedObjects: " + seedObjects + " clientInfoBean: " + clientInfoBean);
//        }
//        String ncid = "";
//        try {
//            NavigationCriteria nc = NavigationCriteria.newNavigationCriteria();
//            List<ConfigSpec> configSpecs = new ArrayList<ConfigSpec>();
//            EPMDocConfigSpec epmDoc = EPMDocConfigSpec.newEPMDocConfigSpec(BaselineConfigSpec
//                    .newBaselineConfigSpec(baseline));
//            configSpecs.add(epmDoc);
//            nc.setConfigSpecs(configSpecs);
//            populateCentricity(nc, getInflatedCollection(seedObjects), clientInfoBean, null);
//            ncid = NavigationCriteriaHelper.service.cacheNavigationCriteria(nc, getNavCriteriaCacheId(clientInfoBean));
//        } catch (WTException e) {
//            log.error("Can't get Baseline NC ID for " + seedObjects, e);
//        } catch (WTPropertyVetoException e) {
//            log.error("Can't get Baseline NC ID for " + seedObjects, e);
//        }
//        if (log.isDebugEnabled()) {
//            log.debug("Exit => getBaselineNavigationCriteriaID(): Completed getting the baseline NCID: " + ncid);
//        }
//        return ncid;
//    }
//
//    /**
//     * Gets the as stored navigation criteria id (NCID).
//     *
//     * @param seedObjects
//     *            the seed objects
//     * @param clientInfoBean
//     *            the client info bean
//     * @return the as stored navigation criteria id, if a NavigationCriteria object could be created; a null, otherwise.
//     * @exception WTException
//     *                - if any of the invoked methods threw this exception
//     */
//    public String getAsStoredNavigationCriteriaID(List<String> seedObjects, ClientNavigationCriteriaBean clientInfoBean)
//            throws WTException {
//        if (log.isDebugEnabled()) {
//            log.debug("getAsStoredNavigationCriteriaID: Starting getting the NCID SeedObjects:" + seedObjects
//                    + " ApplicationInfo: " + clientInfoBean);
//        }
//
//        WTCollection wtCollection = getInflatedCollection(seedObjects);
//        NavigationCriteria nc = getAsStoredNavigationCriteria(wtCollection, clientInfoBean);
//        String ncid = null;
//        if (nc != null) {
//            ncid = NavigationCriteriaHelper.service.cacheNavigationCriteria(nc,
//                    getNavCriteriaCacheId(clientInfoBean));
//        }
//
//        if (log.isDebugEnabled()) {
//            log.debug("getAsStoredNavigationCriteriaID: Completed getting the AsStored NCID: " + ncid);
//        }
//        return ncid;
//    }
//
//    /**
//     * Gets the as stored navigation criteria for the input parameters.
//     *
//     * @param seedObjects
//     *            WTCollection of seed objects
//     * @param clientInfoBean
//     *            the client info bean
//     * @return the as stored navigation criteria, if at least one of the input seeds has an associated
//     *         EPMAsStoredConfig; a null, otherwise.
//     * @exception WTException
//     *                if any of the invoked methods threw this exception.
//     */
//    public NavigationCriteria getAsStoredNavigationCriteria(WTCollection seedObjects,
//            ClientNavigationCriteriaBean clientInfoBean)
//                    throws WTException {
//        if (log.isDebugEnabled()) {
//            log.debug("Enter => getAsStoredNavigationCriteria().");
//            log.debug("seedObjects: " + seedObjects + " clientInfoBean: " + clientInfoBean);
//        }
//        NavigationCriteria nc = null;
//
//        try {
//            nc = NavigationCriteria.newNavigationCriteria();
//            @SuppressWarnings("unchecked")
//            ArrayList<EPMDocument> list = new ArrayList<EPMDocument>(
//                    compressToCADDocs(seedObjects.persistableCollection()));
//
//            /*
//             * Check if at least one of the EPMDocuments in the above list has as associated EPMAsStoredConfig and is
//             * also the owner.
//             */
//            ObjectReference epmAsStoredConfigRef;
//            Persistable epmAsStoredConfig;
//            boolean asStoredConfigExists = false;
//            for (EPMDocument epmDocument : list) {
//                epmAsStoredConfigRef = epmDocument.getAsStoredConfigReference();
//                if ((epmAsStoredConfig = epmAsStoredConfigRef.getObject()) != null) {
//                    if (log.isDebugEnabled()) {
//                        log.debug("epmAsStoredConfig: " + epmAsStoredConfig);
//                    }
//                    asStoredConfigExists = true;
//                    break;
//                }
//            }
//
//            if (asStoredConfigExists == false) {
//                log.debug("Returning a null, as none of the input seeds has an associated EPMAsStoredConfig.");
//                return null;
//            }
//
//            EPMAsStoredConfigSpec asStored = EPMAsStoredConfigSpec.newEPMAsStoredConfigSpec(list);
//
//            /*
//             * The allowLatestSubstitutes attribute on a newly created EPMAsStoredConfigSpec instance is set to true by
//             * default. However, it should be set to false here, since the "Apply latest for unresolved dependents"
//             * option on the Edit Filter UI (rather than this attribute) should really determine whether or not to
//             * return the latest iteration of a master, if the as-stored configuration does not contain any iteration of
//             * that master (SPR 4857081).
//             */
//            asStored.setAllowLatestSubstitutes(false);
//            EPMDocConfigSpec epmDoc = EPMDocConfigSpec.newEPMDocConfigSpec(asStored);
//
//            List<ConfigSpec> configSpecs = new ArrayList<ConfigSpec>();
//            configSpecs.add(epmDoc);
//            nc.setConfigSpecs(configSpecs);
//            populateCentricity(nc, seedObjects, clientInfoBean, null);
//        } catch (WTPropertyVetoException e) {
//            /*
//             * Based on the current implementation of EPMAsStoredConfigSpec.newEPMAsStoredConfigSpec() and
//             * EPMAsStoredConfigSpec.setAllowLatestSubstitutes(), a WTPropertyVetoException may not be thrown; in the
//             * event of such an exception being thrown, it will be wrapped in a WTException instance and re-thrown
//             */
//            throw new WTException(e);
//        }
//
//        if (log.isDebugEnabled()) {
//            log.debug("Exit => getAsStoredNavigationCriteria(): NavigationCriteria for seedObjects: " + seedObjects
//                    + " is: " + nc);
//        }
//        return nc;
//    }
//
//    /**
//     * Gets the workspace navigation criteria id (NCID).
//     *
//     * @param workspaceName
//     *            the name of the workspace
//     * @param seedObjects
//     *            the seed objects
//     * @param clientInfoBean
//     *            the client info bean
//     * @return the workspace navigation criteria id
//     * @exception WTException
//     *                - if any of the invoked methods threw this exception
//     */
//    public String getWorkspaceNavigationCriteriaID(String workspaceName, List<String> seedObjects,
//            ClientNavigationCriteriaBean clientInfoBean) throws WTException {
//        if (log.isDebugEnabled()) {
//            log.debug("getWorkspaceNavigationCriteriaID: Starting getting the NCID SeedObjects:" + seedObjects
//                    + " ApplicationInfo: " + clientInfoBean + " workspaceName:" + workspaceName);
//        }
//        return getWorkspaceNavigationCriteriaID(getWorkspace(workspaceName), getInflatedCollection(seedObjects),
//                clientInfoBean);
//    }
//
//    /**
//     * Gets the workspace navigation criteria id (NCID).
//     *
//     * @param workspace
//     *            EPMWorkspace
//     * @param seedObjects
//     *            WTCollection of seeds
//     * @param clientInfoBean
//     *            the client info bean
//     * @return the workspace navigation criteria id
//     * @exception WTException
//     *                - if any of the invoked methods threw this exception
//     */
//    public String getWorkspaceNavigationCriteriaID(EPMWorkspace workspace, WTCollection seedObjects,
//            ClientNavigationCriteriaBean clientInfoBean) throws WTException {
//        if (log.isDebugEnabled()) {
//            log.debug("Enter => getWorkspaceNavigationCriteriaID().");
//            log.debug(" seedObjects: " + seedObjects + " clientInfoBean: " + clientInfoBean +
//                    " workspace: " + (workspace == null ? null : workspace.getName()));
//        }
//
//        NavigationCriteria nc = NavigationCriteria.newNavigationCriteria();
//
//        populateCentricity(nc, seedObjects, clientInfoBean, workspace);
//
//        // Having set centricity information on nc, set the configuration specifications as appropriate.
//        List<ConfigSpec> configSpecs = new ArrayList<ConfigSpec>();
//        String applicableType = nc.getApplicableType();
//
//        if (log.isDebugEnabled()) {
//            log.debug("applicableType: " + applicableType);
//        }
//
//        // Add only the configuration specification that is relevant for the value in applicableType
//        if (applicableType.equals(WTPart.class.getName())) {
//            log.debug("Adding part configuration specification to the NC object.");
//            configSpecs.addAll(EPMWorkspaceHelper.getPartConfigSpecs(
//                    EPMWorkspaceHelper.getWorkspacePartNavigationCriteria(workspace),
//                    WTContainerRef.newWTContainerRef((ObjectIdentifier) workspace.getContainerReference().getKey())));
//        }
//        else {
//            log.debug("Adding document configuration specification to the NC object.");
//            configSpecs.addAll(EPMWorkspaceHelper.getDocConfigSpecs(
//                    EPMWorkspaceHelper.getWorkspaceDocNavigationCriteria(workspace),
//                    WTContainerRef.newWTContainerRef((ObjectIdentifier) workspace.getContainerReference().getKey())));
//        }
//
//        nc.setConfigSpecs(configSpecs);
//
//        String ncid = NavigationCriteriaHelper.service.cacheNavigationCriteria(nc,
//                getNavCriteriaCacheId(clientInfoBean));
//
//        if (log.isDebugEnabled()) {
//            log.debug("getWorkspaceNavigationCriteriaID: Completed getting the NCID: " + ncid);
//        }
//        return ncid;
//    }
//
//    // In terms of an As Stored, only EPMDocument can be a member.
//    // So when computing, if an As Stored is valid, only use
//    // the EPMDocuments that are seeds.
//    @SuppressWarnings("unchecked")
//    private Collection compressToCADDocs(Collection seeds) {
//        // If the original collection is empty then just return it
//        if (seeds == null || seeds.isEmpty()) {
//            return seeds;
//        }
//
//        // Else compress it into a collection of CADDocuments
//        // Does not need to be a WT collection because
//        // Collection should already a List of persistable,
//        // not references.
//        ArrayList list = new ArrayList();
//        for (Object o : seeds) {
//            if (o instanceof EPMDocument) {
//                list.add(o);
//            }
//        }
//        return list;
//    }
//
//    public static Class getContextClass(WTCollection wtCollection, boolean ifNullGetFirstClass) throws WTException {
//        Class context = null;
//        // With reference to the logic in UICollectionHelper.java at X-12:
//        // For heterogeneous seeds [WTParts or WTDocuments and (EPMDocuments and/or NewEPMDocuments)], returns
//        // EPMDocument.class;
//        // For heterogeneous seeds (WTDocuments and WTParts), returns WTPart.class;
//        // For homogeneous seeds (WTParts), returns WTPart.class;
//        // For homogeneous seeds (EPMDocuments and/or NewEPMDocuments), returns EPMDocument.class;
//        // For homogeneous seeds (WTDocuments), returns WTDocument.class.
//        for (Iterator i = wtCollection.persistableIterator(); i.hasNext();) {
//            Object obj = i.next();
//            if (obj instanceof EPMDocument) {
//                context = EPMDocument.class;
//                break;
//            }
//            else if ((obj instanceof WTPart) && ((context == null) || (context != EPMDocument.class))) {
//                context = WTPart.class;
//            }
//            else if ((obj instanceof WTDocument) && (context == null)) {
//                context = WTDocument.class;
//            }
//        }
//        // if context is still null then get the first object context
//        if (context == null && ifNullGetFirstClass) {
//            Iterator iterator2 = wtCollection.persistableIterator();
//            if (iterator2.hasNext()) {
//                Object obj = iterator2.next();
//                if (obj instanceof ObjectReference) {
//                    obj = ((ObjectReference) obj).getObject();
//                }
//                context = obj.getClass();
//            }
//        }
//        return context;
//    }
//
//    /**
//     * Gets the centricity type.
//     *
//     * @param wtCollection
//     *            the wt collection
//     * @param ncid
//     *            the ncid
//     * @param clientInfoBean
//     *            the client info bean
//     * @return the centricity type
//     */
//    public String getCentricityType(WTCollection wtCollection, String ncid,
//            ClientNavigationCriteriaBean clientInfoBean) {
//        return getCentricityType(wtCollection, getNavigationCriteriaFromCookie(ncid, clientInfoBean));
//    }
//
//    /**
//     * Gets the centricity type.
//     *
//     * @param wtCollection
//     *            the wt collection
//     * @param json
//     *            the json
//     * @return the centricity type
//     * @throws WTException
//     *             the wT exception
//     */
//    public String getCentricityType(WTCollection wtCollection, String json) throws WTException {
//        return getCentricityType(wtCollection, NavigationCriteriaHelper.service.getNavigationCriteriaFromJSON(json));
//    }
//
//    /**
//     * Gets the centricity type.<BR>
//     * 1> Return applicable value if it is set<BR>
//     * 2> Return EPMDocument if collection contain EPM seeds<BR>
//     * 3> Return WTPart if collection contain Part seeds<BR>
//     * 4> Return the first type of the first object in the collection<BR>
//     * 5> Else null
//     *
//     * @param wtCollection
//     *            the wt collection
//     * @param nc
//     *            the nc
//     * @return the centricity type
//     */
//    public String getCentricityType(WTCollection wtCollection, NavigationCriteria nc) {
//        // 1. check for applicable type value in NCO
//        if (nc != null && nc.getApplicableType() != null && !nc.getApplicableType().isEmpty()) {
//            return nc.getApplicableType();
//        }
//        try {
//            // 2. Get the context class depending on the priority. If no class found then get the class of First seed
//            // object
//            Class context = getContextClass(wtCollection, true);
//            return context.getCanonicalName();
//        } catch (WTException e) {
//            log.error("Error while getting centricity information: " + wtCollection, e);
//        }
//        return null;
//    }
//
//    /**
//     * Sets the attributes centricity and applicableType on the input NavigationCriteria object, based on the input
//     * arguments.
//     *
//     * @param nc
//     *            - the input NavigationCriteria object.
//     * @param wtCollection
//     *            - a WTCollection of input seeds.
//     * @param cNCBean
//     *            - a ClientNavigationCriteriaBean object holding centricity information.
//     * @param workspace
//     *            - if an EPMWorkspace instance was passed in, the attribute applicableType is set to a value depending
//     *            on whether or not the processing type set for the workspace is "part-centric"; on the other hand, if a
//     *            null was passed in, applicableType is set to a value based on the type of seeds that are passed in the
//     *            input collection.
//     * @return - the input NavigationCriteria object, with its centricity and applicableType attributes set.
//     * @exception -
//     *                a WTException, if any of the invoked APIs threw this exception.
//     */
//    private NavigationCriteria populateCentricity(NavigationCriteria nc, WTCollection wtCollection,
//            ClientNavigationCriteriaBean cNCBean, EPMWorkspace workspace) throws WTException {
//        log.debug("Enter => populateCentricity().");
//
//        // Fetch centricity information from the input bean
//        boolean centricity = false;
//        if (cNCBean != null) {
//            centricity = cNCBean.getCentricity();
//            if (log.isDebugEnabled()) {
//                log.debug("centricity: " + centricity);
//            }
//        }
//
//        try {
//            // Set the centricity attribute using the above fetched value
//            nc.setCentricity(centricity);
//
//            // Set the applicable type based on input
//            if (workspace != null) {
//                boolean isPartCentric = workspace.isPartCentricProcessing();
//
//                if (log.isDebugEnabled()) {
//                    log.debug("isPartCentric: " + isPartCentric);
//                }
//
//                if (isPartCentric) {
//                    nc.setApplicableType(WTPart.class.getName());
//                }
//                else {
//                    nc.setApplicableType(EPMDocument.class.getName());
//                }
//            }
//            else {
//                Class context = getContextClass(wtCollection, false);
//
//                if (log.isDebugEnabled()) {
//                    log.debug("context: " + context);
//                }
//
//                if (context != null) {
//                    nc.setApplicableType(context.getCanonicalName());
//                }
//            }
//        } catch (WTPropertyVetoException e) {
//            log.error("Exception while setting centricity information: ", e);
//        }
//        return nc;
//    }
//
//    public static WTCollection getInflatedCollection(List<String> seedObjects) throws WTException {
//        WTCollection wtCollection = new WTArrayList();
//        if (seedObjects != null && !seedObjects.isEmpty()) {
//            ReferenceFactory factory = new ReferenceFactory();
//            // To fix spr 2009946.
//            // To create list to contain VR or OR reference, otherwise, invalid type exception will be thrown by
//            // the add function because the list contains OR by default.
//            if (seedObjects.toString().indexOf("VR:") != -1) {
//                wtCollection = new WTArrayList(10, CollectionsHelper.VERSION_FOREIGN_KEY);
//            }
//            for (String str : seedObjects) {
//                // Local Object Reference (LOR): when a CAD doc is saved to work space (visible only in embedded mode
//                // thru CREO) . e.g: LOR:5840998246267224069
//                // Ignore the Local Object Reference (LOR) since the navigation criteria is not applicable to LOR.
//                if ((str == null) || (str.indexOf("LOR:") != -1)) {
//                    continue;
//                }
//                wtCollection.add(factory.getReference(str));
//            }
//            wtCollection.inflate();
//        }
//        return wtCollection;
//    }
//
//    /**
//     * This method reorder persistable collection before sending it to NC Services. <BR>
//     * NC Services takes the first element of the Collection and create the default NCO based on that <BR>
//     * In here we are looking for case where heterogenous objects are passed to EC.<BR>
//     * This method will reorder EPMDocument to first, then WTPart and then rest objects type <BR>
//     */
//    private WTCollection reOrderCollection(final WTCollection collection) throws WTException {
//        final WTCollection epmCollection = new WTArrayList();
//        final WTCollection partCollection = new WTArrayList();
//        final WTCollection otherCollection = new WTArrayList();
//        for (Iterator i = collection.persistableIterator(); i.hasNext();) {
//            Object obj = i.next();
//            if (obj instanceof EPMDocument) {
//                // EPMDOcument has got the highest priority keep it first in the list.
//                epmCollection.add(obj);
//            }
//            else if (obj instanceof WTPart) {
//                // WTPart has got the second highest priority
//                partCollection.add(obj);
//            }
//            else {
//                // rest all objects will be added in the last
//                otherCollection.add(obj);
//            }
//        }
//        epmCollection.addAll(partCollection);
//        epmCollection.addAll(otherCollection);
//        return epmCollection;
//    }
//
//    public NavigationCriteria getDefaultNavigationCriteria(List<String> seedOids, Map<String, String> context) {
//        return getDefaultNavigationCriteria(seedOids, context, null);
//    }
//
//    public NavigationCriteria getDefaultNavigationCriteria(List<String> seedOids, Map<String, String> context,
//            Map<String, Object> appMap) {
//        log.debug("Enter => getDefaultNavigationCriteria(List<String>, Map<String, String>).");
//        try {
//            WTCollection seedObjects = null;
//            if (seedOids != null && !seedOids.isEmpty()) {
//                seedObjects = reOrderCollection(getInflatedCollection(seedOids));
//            }
//            return getDefaultNavigationCriteria(seedObjects, context, appMap);
//
//        } catch (WTException e) {
//            log.error("Can't get Default NC for " + seedOids, e);
//        }
//        log.debug("Returning a null.");
//        return null;
//    }
//
//    public NavigationCriteria getDefaultNavigationCriteria(WTCollection seedObjects, Map<String, String> context) {
//        return getDefaultNavigationCriteria(seedObjects, context, null);
//    }
//
//    public NavigationCriteria getDefaultNavigationCriteria(WTCollection seedObjects, Map<String, String> context,
//            Map<String, Object> appMap) {
//        if (log.isDebugEnabled()) {
//            log.debug("Enter => getDefaultNavigationCriteria(WTCollection, Map<String, String>).");
//            log.debug("seedObjects: " + seedObjects + " context: " + context);
//        }
//        try {
//
//            if (seedObjects != null) {
//                // if passed object is Part configuration then get the default top part
//                WTCollection wtProductConfigurationCollection = null;
//                Iterator itrPartConfig = seedObjects.persistableIterator(WTProductConfiguration.class, true);
//                if (itrPartConfig.hasNext()) {
//                    wtProductConfigurationCollection = new WTArrayList();
//                }
//                while (itrPartConfig.hasNext()) {
//                    WTProductConfiguration target = (WTProductConfiguration) itrPartConfig.next();
//                    WTPart wtPart = getTopWTPartForConfiguration(target);
//                    wtProductConfigurationCollection.add(wtPart);
//                    if (log.isDebugEnabled()) {
//                        log.debug("getDefaultNavigationCriteria(): Configuration: " + target
//                                + " passed, so getting the NC for top part: " + wtPart);
//                    }
//                }
//                if (wtProductConfigurationCollection != null) {
//                    seedObjects.removeAll(WTProductConfiguration.class, true);
//                    seedObjects.addAll(wtProductConfigurationCollection);
//                }
//            }
//
//            NavCriteriaContext navContext = new NavCriteriaContext();
//            navContext.setMap(appMap);
//            Boolean forceSetApplyToTopValue = null;
//            boolean checkForLatestIteration = false;
//            // if the context passed is null, system default configspec is used
//            boolean isSystemDefault = true;
//            String appName = null;
//            if (context != null) {
//                final String variantSpecOid = context.get(ExpansionCriteriaConstants.VARIANTSPEC_OID);
//                if (variantSpecOid != null) {
//                    if (log.isDebugEnabled()) {
//                        log.debug("variantSpecOid :" + variantSpecOid);
//                    }
//                    navContext.setVariantSpecRef((ObjectReference) REFERENCE_FACTORY.getReference(variantSpecOid));
//                    navContext.setApplicableType(VariantSpec.class);
//                    navContext.setApplicationName(NavigationCriteriaConstants.PSBVS_APP_NAME);
//                }
//
//                String strType = context.get(ExpansionCriteriaConstants.URL_TYPE);
//                if (strType != null) {
//                    if (log.isDebugEnabled()) {
//                        log.debug("Type passed as:" + strType);
//                    }
//
//                    String className = ServerHelper.getClassNameFromTypeName(strType);
//                    if (log.isDebugEnabled()) {
//                        log.debug("className from ServerHelper.getClassNameFromTypeName(): " + className);
//                    }
//                    // added this check to avoid null pointer exception for invalid type name,
//                    // When initializing ConfigSpec UI with Type
//                    if (className != null && !className.isEmpty()) {
//                        navContext.setApplicableType(Class.forName(className));
//                    }
//
//                }
//                String wsName = context.get(ExpansionCriteriaConstants.WORKSPACE_NAME);
//                if (wsName != null) {
//                    if (log.isDebugEnabled()) {
//                        log.debug("Workspace Name passed as:" + wsName);
//                    }
//                    navContext.setWorkspaceRef(ObjectReference.newObjectReference(getWorkspace(wsName)));
//                }
//                String containerOid = context.get(ExpansionCriteriaConstants.URL_CONTAINER_OID);
//                if (containerOid != null && !containerOid.isEmpty()) {
//                    if (log.isDebugEnabled()) {
//                        log.debug("Container Oid passed as:" + containerOid);
//                    }
//                    WTContainerRef containerRef = getContainerRef(containerOid);
//                    navContext.setContainerRef(containerRef);
//                }
//                appName = context.get(ExpansionCriteriaConstants.URL_APP);
//                if (appName != null) {
//                    if (log.isDebugEnabled()) {
//                        log.debug("Application Name passed as:" + appName);
//                    }
//                    navContext.setApplicationName(appName);
//                }
//
//                addToNavCriteriaContextMap(context, navContext, NavCriteriaContext.CONTEXT_OBJECT_TYPE_ID);
//
//                addToNavCriteriaContextMap(context, navContext, NavCriteriaContext.IS_WORKSPACE_CONTEXT);
//                addToNavCriteriaContextMap(context, navContext, ExpansionCriteriaConstants.IS_SYSTEM_DEFAULT_EC);
//
//                String applyToTopValue = context.get(ExpansionCriteriaConstants.SET_APPLY_TO_TOP_FOR_DEFAULT_NC);
//                if (applyToTopValue != null) {
//                    forceSetApplyToTopValue = Boolean.parseBoolean(applyToTopValue);
//                    // If force apply to top is false then no need to check for latest iteration
//                    checkForLatestIteration = Boolean.parseBoolean(context
//                            .get(ExpansionCriteriaConstants.CHECK_FOR_LATEST_ITERATION));
//                    if (log.isDebugEnabled()) {
//                        log.debug("Set Apply To Top Value passed as:" + forceSetApplyToTopValue);
//                        log.debug("Check For Latest Iteration value passed as :" + checkForLatestIteration);
//                    }
//                }
//
//                isSystemDefault = Boolean
//                        .parseBoolean(context.get(ExpansionCriteriaConstants.IS_SYSTEM_DEFAULT_EC));
//            }
//
//            if (log.isDebugEnabled()) {
//                log.debug("Calling NavigationCriteriaHelper.service.getDefaultNavigationCriteria(): seedObjects: "
//                        + seedObjects + " navContext: " + navContext);
//            }
//
//            NavigationCriteria nc = NavigationCriteriaHelper.service.getDefaultNavigationCriteria(seedObjects,
//                    navContext);
//
//            if (log.isDebugEnabled()) {
//                log.debug("nc from NavigationCriteriaHelper.service.getDefaultNavigationCriteria(): " + nc);
//            }
//
//            if (forceSetApplyToTopValue != null && isSystemDefault) {
//                nc.setApplyToTopLevelObject(forceSetApplyToTopValue);
//            }
//
//            // If apply to top for latest iteration is set then perform the check if passed object is latest or not.
//            if (checkForLatestIteration && seedObjects != null && seedObjects.size() == 1 && isSystemDefault) {
//                String contextId = PersistenceHelper.getObjectIdentifier(
//                        (Persistable) seedObjects.persistableIterator().next()).toString();
//                String newContextId = applyNavigationCriteria(contextId, nc);
//                // If new object is different from the latest object then do not set the apply to top.
//                // This is as per the standard requirement of StructureBrowsers.
//                if (newContextId != null
//                        && !removeORFromObject(newContextId).equals(removeORFromObject(contextId))) {
//                    if (log.isDebugEnabled()) {
//                        log.debug("Resetting Apply To Top Value as context is not the latest.");
//                    }
//                    nc.setApplyToTopLevelObject(false);
//                }
//            }
//            // Adding check for AsMatured Config Spec, since they will be the new default config spec for history
//            // objects.
//            if (NavigationCriteriaHelper.isAsMaturedNavCriteria(nc)) {
//                nc.setApplyToTopLevelObject(false);
//                nc.setUseDefaultForUnresolved(true);
//            }
//
//            // Set centricity if the input Map exists and contains centricity information.
//            String centricity;
//            if (nc != null && context != null
//                    && (centricity = context.get(ExpansionCriteriaJSONConstants.CENTRICITY)) != null) {
//                if (log.isDebugEnabled()) {
//                    log.debug("centricity: " + centricity);
//                }
//                nc.setCentricity(Boolean.parseBoolean(centricity));
//            }
//
//            if (nc != null && NavigationCriteriaConstants.MATRIX_EDITOR_APP_NAME.equals(appName)) {
//                nc = ExpansionCriteriaFacade.getInstance().updateNavigationCriteria(nc, navContext);
//            }
//
//            return nc;
//        } catch (ClassNotFoundException e) {
//            log.error("Can't get type for " + context, e);
//        } catch (WTPropertyVetoException e) {
//            log.error("Can't get Default NC ID for " + seedObjects, e);
//        } catch (WTException e) {
//            log.error("Can't get Default NC ID for " + seedObjects, e);
//        }
//        return null;
//    }
//
//    private void addToNavCriteriaContextMap(Map<String, String> context, NavCriteriaContext navContext, String key) {
//        if (key == null || context == null || navContext == null) {
//            return;
//        }
//
//        String value = context.get(key);
//        if (value != null) {
//            Map<String, Object> map = null;
//            if (navContext.getMap() == null) {
//                map = new HashMap<String, Object>();
//                navContext.setMap(map);
//            }
//            else {
//                map = navContext.getMap();
//            }
//            if (log.isDebugEnabled()) {
//                log.debug(key + " passed as:" + value);
//            }
//            map.put(key, value);
//        }
//    }
//
//    public static WTContainerRef getContainerRef(String containerOid) {
//        if (containerOid == null || containerOid.trim().length() == 0) {
//            return null;
//        }
//
//        WTContainerRef containerRef = null;
//
//        boolean currentAccessLevel = SessionServerHelper.manager.setAccessEnforced(false);
//        try {
//            WTContainer container = (WTContainer) REFERENCE_FACTORY.getReference(containerOid).getObject();
//            containerRef = WTContainerRef.newWTContainerRef(container);
//        } catch (WTRuntimeException e) {
//            log.error("Unable to fetch a container for " + containerOid, e);
//        } catch (WTException e) {
//            log.error("Unable to fetch a container for " + containerOid, e);
//        } finally {
//            SessionServerHelper.manager.setAccessEnforced(currentAccessLevel);
//        }
//
//        return containerRef;
//    }
//
//    private WTPart getTopWTPartForConfiguration(WTProductConfiguration product_configuration)
//            throws WTPropertyVetoException, WTException, PersistenceException {
//        WTProductConfigurationMaster master = (WTProductConfigurationMaster) product_configuration.getMaster();
//        WTPartMaster part_master = (WTPartMaster) master.getConfigurationFor();
//
//        BaselineConfigurationConfigSpec config_spec = ConfigurationHelper
//                .getConfigSpecForConfiguration(product_configuration);
//
//        QueryResult query_results = ConfigHelper.service.filteredIterationsOf(part_master, config_spec);
//        WTPart iteration_based_on_baseline = (WTPart) query_results.nextElement();
//        return iteration_based_on_baseline;
//    }
//
//    /**
//     * Gets the nav criteria cache id.
//     *
//     * @param clientInfoBean
//     *            the client info bean
//     *
//     * @return the nav criteria cache id
//     *
//     * @throws WTException
//     *             the WT exception
//     */
//    public static NavCriteriaCacheId getNavCriteriaCacheId(ClientNavigationCriteriaBean clientInfoBean)
//            throws WTException {
//        log.debug("Enter => getNavCriteriaCacheId().");
//        if (clientInfoBean == null) {
//            throw new WTException("getNavCriteriaCacheId() was invoked with a null argument.");
//        }
//        String remoteAddr = (clientInfoBean != null && clientInfoBean.getRemoteAddr() != null) ? clientInfoBean
//                .getRemoteAddr() : getRemoteAddr();
//        String sessionId = (clientInfoBean != null && clientInfoBean.getSessionId() != null) ? clientInfoBean
//                .getSessionId() : getSessionId();
//
//        if (log.isDebugEnabled()) {
//            log.debug("remoteAddr: " + remoteAddr + " sessionId: " + sessionId);
//        }
//        return new NavCriteriaCacheId(remoteAddr, sessionId, clientInfoBean.getAppName());
//    }
//
//    /**
//     * Gets the JSON for navigation criteria id.
//     *
//     * @param id
//     *            the id
//     * @param clientInfoBean
//     *            the client info bean
//     *
//     * @return the jSO nfor navigation criteria id
//     */
//    @Override
//    public String getJSONforNavigationCriteriaID(String id, ClientNavigationCriteriaBean clientInfoBean) {
//        if (log.isDebugEnabled()) {
//            log.debug("getJSONforNavigationCriteriaID: Begin getting the JSON from the NCID: " + id
//                    + " clientInfoBean: " + clientInfoBean);
//        }
//        NavigationCriteria nc = getNavigationCriteriaFromCookie(id, clientInfoBean);
//        if (nc == null) {
//            log.debug("Returning an error, as getNavigationCriteriaFromCookie() returned a null.");
//            return ECExceptionConstants.NCID_INVALID_EXCEPTION;
//        }
//        return getJSONForNavigationCriteria(nc);
//    }
//
//    public String getJSONForNavigationCriteria(NavigationCriteria nc) {
//        if (nc == null) {
//            return "";
//        }
//        try {
//            return encodeJSONContent(NavigationCriteriaHelper.service.getJSONFromNavigationCriteria(nc), null);
//        } catch (WTException e) {
//            log.error("Can't get JSON for NavigationCriteria " + nc, e);
//        }
//        return "";
//    }
//
//    /**
//     * This will be called to update an EC from a JSON String.
//     *
//     * @param id
//     *            The EC id
//     * @param json
//     *            the JSON string containing the full NC
//     * @param ncOid
//     *            the NC oid
//     * @param clientInfoBean
//     *            the client info bean
//     *
//     * @return JSON String corresponding to the filter that was previously stored in currentFilterInfo
//     */
//    @Override
//    public String updateNavigationCriteria(String id, String json, String ncOid,
//            ClientNavigationCriteriaBean clientInfoBean) {
//        log.debug("Enter => updateNavigationCriteria().");
//        String oldJSON = null;
//
//        if (log.isDebugEnabled()) {
//            log.debug("updateNavigationCriteria: Begin updating NCID: [" + id + "] ncOid: [" + ncOid
//                    + "] ApplicationInfo: ["
//                    + clientInfoBean + "] JSON: " + json);
//        }
//        try {
//            NavCriteriaCacheId cacheID = getNavCriteriaCacheId(clientInfoBean);
//            NavigationCriteria nc = null;
//            if (ncOid != null) {
//                try {
//                    nc = (NavigationCriteria) REFERENCE_FACTORY.getReference(ncOid).getObject();
//                } catch (WTRuntimeException wte) {
//                    // The navigation criteria could be stale, in which case, set it to null
//                    if (wte.getNestedThrowable() instanceof ObjectNoLongerExistsException) {
//                        log.debug("Object no longer exists : " + ncOid, wte);
//                        nc = null;
//                    }
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("nc from ncOid: " + nc);
//                }
//            } /*
//               * else { nc = NavigationCriteriaHelper.service.getCachedNavigationCriteria(id, cacheID); }
//               */
//
//            if (nc != null) { // Navigation Criteria exists, update the NC
//                if (ncOid == null) {
//                    // Now make sure to remove old filter information from cached NC
//                    nc.getFilters().clear();
//                }
//                nc = NavigationCriteriaHelper.service.updateNavigationCriteriaFromJSON(nc, json);
//                if (log.isDebugEnabled()) {
//                    log.debug("nc from updateNavigationCriteriaFromJSON(): " + nc);
//                }
//            }
//            else { // NC does not exist in the cache, try to create from JSON
//                nc = NavigationCriteriaHelper.service.getNavigationCriteriaFromJSON(json);
//            }
//
//            // for some reason NavigationCriteria is null.
//            if (nc == null) {
//                log.debug("Returning an error, since nc is null.");
//                return ECExceptionConstants.NCID_INVALID_EXCEPTION;
//            }
//
//            // decode name as it should not contain any encoded elements.
//            nc.setName(ExpansionCriteriaUtil.decodeHTMLContent(nc.getName()));
//
//            // If the application is matrix editor, update the NavCriteria to remove any ATO filters added by the
//            // service NavigationCriteriaHelper.service.updateNavigationCriteriaFromJSON(nc, json)
//            String appName = clientInfoBean.getAppName();
//            if (appName != null && NavigationCriteriaConstants.MATRIX_EDITOR_APP_NAME.equals(appName)) {
//                UpdateNavCriteriaDelegate delegate = updateNavCriteriaDelegateFactory.getDelegate(appName);
//                if (delegate != null) {
//                    delegate.removeOptionFilters(nc);
//                }
//            }
//
//            // now save the updated NC in the cache
//            NavigationCriteriaHelper.service.updateCachedNavigationCriteria(id, nc, cacheID);
//            if (log.isDebugEnabled()) {
//                log.debug("updateNavigationCriteria: Finished updation NCID:[" + id + "] ncOid:[" + nc
//                        + "] navCriteriaCacheId:["
//                        + cacheID + "]");
//            }
//
//            // The below call needs to be tested for all use cases that result in a call to this method.
//            oldJSON = setCurrentFilterInfo(cacheID.toString(), ncOid, json);
//        } catch (WTPropertyVetoException e) {
//            log.error("Exception while updating the NC.", e);
//        } catch (WTException e) {
//            log.error("Exception while updating the NC.", e);
//        }
//
//        return oldJSON;
//    }
//
//    /**
//     * Save the NC in the DB.
//     *
//     * @param savingData
//     * @param clientInfoBean
//     *
//     * @return the String
//     */
//    @Override
//    public Map<String, String> saveNavigationCriteria(Map<String, String> savingData,
//            ClientNavigationCriteriaBean clientInfoBean) {
//        return saveNavigationCriteria(savingData, clientInfoBean, null);
//
//    }
//
//    /**
//     * Save the NC in the DB.
//     *
//     * @param savingData
//     * @param clientInfoBean
//     * @param appMap
//     *            application context specific map
//     *
//     * @return the String
//     */
//    @Override
//    public Map<String, String> saveNavigationCriteria(Map<String, String> savingData,
//            ClientNavigationCriteriaBean clientInfoBean, Map<String, Object> appMap) {
//        log.debug("Enter => saveNavigationCriteria().");
//
//        if (log.isDebugEnabled()) {
//            log.debug("savingData: " + savingData + " clientInfoBean: " + clientInfoBean);
//        }
//
//        String ncid = savingData.get(ExpansionCriteriaConstants.NCID);
//        String name = savingData.get(ExpansionCriteriaConstants.EC_NAME);
//        String sharedToAll = savingData.get(ExpansionCriteriaConstants.SHARE_TO_ALL);
//        String seedOid = savingData.get(ExpansionCriteriaConstants.URL_SEED_OID);
//        String seedType = savingData.get(ExpansionCriteriaConstants.SEED_TYPE);
//        String wcAppName = savingData.get(ExpansionCriteriaConstants.WORKSPACE_APP_NAME);
//        final String varSpecOid = savingData.get(ExpansionCriteriaConstants.VARIANTSPEC_OID);
//        final boolean isDownloadOrCheckoutApp = (ExpansionCriteriaConstants.COLLECTOR_APP_NAME.equalsIgnoreCase(
//                wcAppName) || NavigationCriteriaConstants.CHECKOUT_APP_NAME.equalsIgnoreCase(wcAppName));
//
//        Map<String, String> aMap = new HashMap<>(8);
//        aMap.put(ExpansionCriteriaConstants.NCID, ncid);
//
//        /*
//         * When saving a filter from the MAPSB/SAPSB UI, the Save event is sometimes dispatched from both the upstream
//         * and downstream sides, which results in two threads racing with each other in this method. This is avoided by
//         * synchronizing the threads as seen below.
//         */
//        synchronized (this) {
//            try {
//                if (ncid == null) {
//                    throw new WTException("Null NCID is passed. ClientNavigationCriteriaBean is: " + clientInfoBean);
//                }
//
//                // SPR: 2130922
//                WTUser loggedInUser = (WTUser) SessionHelper.getPrincipal();
//
//                if (log.isDebugEnabled()) {
//                    log.debug("Calling NavigationCriteriaHelper.service.getNavigationCriteria() => name: " + name
//                            + " loggedInUser: " + loggedInUser);
//                }
//                /*
//                 * Changes for SPR 7339813 - Filter definition is not getting updated if user performs subsequent update
//                 * from PSB Passing parameter as true, because current config specs needed to compare with cached Config
//                 * Specs.
//                 */
//                NavigationCriteria nc = NavigationCriteriaHelper.service.getNavigationCriteria(name, loggedInUser,
//                        true);
//
//                if (log.isDebugEnabled()) {
//                    log.debug("nc from NavigationCriteriaHelper.service.getNavigationCriteria(): " + nc);
//                }
//
//                if (seedType == null) {
//                    seedType = getSeedTypeFromSeedOid(seedOid);
//                }
//
//                if (log.isDebugEnabled()) {
//                    log.debug("seedType: " + seedType);
//                }
//
//                if (nc != null) {
//                    String applicableType = nc.getApplicableType();
//                    if (log.isDebugEnabled()) {
//                        log.debug("applicableType: " + applicableType);
//                    }
//
//                    if (!isDownloadOrCheckoutApp && !areTypesComparable(seedType, applicableType)) {
//                        aMap.put(ECExceptionConstants.SEED_TYPE_MISMATCH, "true");
//                        if (log.isDebugEnabled()) {
//                            log.debug("Seed type does not match the NC's applicable type. Hence returning: " + aMap);
//                        }
//                        return aMap;
//                    }
//                }
//
//                NavCriteriaCacheId cacheID = getNavCriteriaCacheId(clientInfoBean);
//
//                if (log.isDebugEnabled()) {
//                    log.debug("Calling NavigationCriteriaHelper.service.getCachedNavigationCriteria() => ncid: " + ncid
//                            + " cacheID: " + cacheID);
//                }
//
//                NavigationCriteria cachedNC = NavigationCriteriaHelper.service.getCachedNavigationCriteria(ncid,
//                        cacheID);
//
//                if (cachedNC == null) {
//                    throw new WTException(
//                            "NavigationCriteriaHelper.service.getCachedNavigationCriteria() returned a null.");
//                }
//
//                if (log.isDebugEnabled()) {
//                    log.debug("cachedNC from NavigationCriteriaHelper.service.getCachedNavigationCriteria(): "
//                            + cachedNC + " name: " + cachedNC.getName() + " applicableType: "
//                            + cachedNC.getApplicableType());
//                }
//
//                WTUser cachedNCOwner = null;
//                final ObjectReference ncOwnerRef = cachedNC.getOwnerReference();
//                boolean isNCOwnerPartAlternateRep = false;
//                if (ncOwnerRef != null) {
//                    final Persistable ncOwner = ncOwnerRef.getObject();
//                    if (ncOwner instanceof WTPartAlternateRep) {
//                        isNCOwnerPartAlternateRep = true;
//                        cachedNCOwner = loggedInUser;
//                    }
//                    else if (ncOwner instanceof WTUser) {
//                        cachedNCOwner = (WTUser) ncOwner;
//                    }
//                }
//
//                if (log.isDebugEnabled()) {
//                    log.debug("cachedNCOwner: " + cachedNCOwner);
//                }
//
//                /*
//                 * If navigation criteria is persistent and the NC owner doesn't match with the logged-in user or the
//                 * given name is not equal to the cached navigation criteria name then create a deep copy of NC and set
//                 * the current user
//                 */
//                boolean isPersistent = PersistenceHelper.isPersistent(cachedNC);
//                if (log.isDebugEnabled()) {
//                    log.debug("isPersistent: " + isPersistent);
//                }
//
//                if (isPersistent &&
//                        (nc == null || !cachedNCOwner.equals(loggedInUser) || !name.equals(cachedNC.getName()))) {
//                    cachedNC = NavigationCriteriaHelper.service.getDeepCopy(cachedNC);
//                    if (log.isDebugEnabled()) {
//                        log.debug("cachedNC from NavigationCriteriaHelper.service.getDeepCopy(): " + cachedNC);
//                    }
//                    cachedNC.setOwner(loggedInUser);
//                }
//                else {
//                    cachedNC.setOwner(loggedInUser);
//                }
//                cachedNC.setName(name);
//                cachedNC.setSharedToAll(Boolean.parseBoolean(sharedToAll));
//
//                String JSONFromNC = NavigationCriteriaHelper.service.getJSONFromNavigationCriteria(cachedNC);
//                aMap.put(ExpansionCriteriaConstants.JSON_STRING, JSONFromNC);
//
//                if (log.isDebugEnabled()) {
//                    log.debug("JSONFromNC: " + JSONFromNC);
//                }
//
//                boolean alreadyProcessedFilter = false;
//                if (nc != null && processedFilterInfo.contains(nc.toString() + "=>" + JSONFromNC)) {
//                    log.debug("Encountered an already processed filter.");
//                    alreadyProcessedFilter = true;
//                }
//
//                if (seedType != null) {
//                    if (log.isDebugEnabled()) {
//                        log.debug("Setting applicableType to: " + seedType + " for cachedNC: " + cachedNC);
//                    }
//
//                    if (isNCOwnerPartAlternateRep) {
//                        cachedNC.setApplicableType(WTPart.class.getCanonicalName());
//
//                    }
//                    else if (!isDownloadOrCheckoutApp) {
//                        cachedNC.setApplicableType(seedType);
//                    }
//                }
//
//                boolean isUpdateCase = (nc != null || PersistenceHelper.isPersistent(cachedNC));
//                if (log.isDebugEnabled()) {
//                    log.debug("isUpdateCase: " + isUpdateCase);
//                }
//
//                String cachedNCOID = null;
//                UpdateNavCriteriaDelegate delegate = null;
//                /*
//                 * Changes for SPR 7339813 - Filter definition is not getting updated if user performs subsequent update
//                 * from PSB If same config specs already exists in processedFilterInfo & user still try to save it,
//                 * changes were not getting updated because alreadyProcessedFilter flag was returning yes, Thus added
//                 * one more flag needToProcessAgain
//                 */
//                boolean needToProcessAgain = false;
//                String currentNCJson = NavigationCriteriaHelper.service.getJSONFromNavigationCriteria(nc);
//                if (!currentNCJson.equalsIgnoreCase(JSONFromNC)) {
//                    needToProcessAgain = true;
//                }
//                if ((!alreadyProcessedFilter) || needToProcessAgain) {
//                    final String appName = clientInfoBean.getAppName();
//                    // Remove any inappropriate configspecs prior to saving the NavigationCriteria
//                    delegate = updateNavCriteriaDelegateFactory.getDelegate(appName);
//                    if (delegate != null) {
//                        cachedNC = delegate.removeConfigSpecsFromNavigationCriteria(cachedNC);
//                    }
//
//                    cachedNC = NavigationCriteriaHelper.service.saveNavigationCriteria(cachedNC);
//
//                    if (log.isDebugEnabled()) {
//                        log.debug("cachedNC from NavigationCriteriaHelper.service.saveNavigationCriteria(): "
//                                + cachedNC +
//                                " applicableType: " + cachedNC.getApplicableType());
//                    }
//
//                    if (delegate != null) {
//                        // prepare the nav criteria for being cached
//                        final NavCriteriaContext navContext = new NavCriteriaContext();
//                        if (ClassCache.isValidClass(seedType)) {
//                            navContext.setApplicableType(ClassCache.getClass(seedType));
//                        }
//                        navContext.setApplicationName(appName);
//                        if (varSpecOid != null) {
//                            final ObjectReference variantSpecRef = (ObjectReference) REFERENCE_FACTORY
//                                    .getReference(varSpecOid);
//                            navContext.setVariantSpecRef(variantSpecRef);
//                        }
//                        navContext.setMap(appMap);
//                        // Added to fix the current filter cache update for Config Context.
//                        if (isNCOwnerPartAlternateRep) {
//                            navContext.setSeeds(CollectionsHelper.singletonWTSet(ncOwnerRef.getObject()));
//                        }
//                        // Cache the persisted OID before calling the delegate
//                        // some delegates returns a modified non-persisted NC (PAR, PFM)
//                        cachedNCOID = cachedNC.toString();
//                        aMap.put(ExpansionCriteriaConstants.NC_OID, cachedNCOID);
//                        cachedNC = delegate.updateNavigationCriteria(cachedNC, navContext);
//                    }
//
//                    if (!isNCOwnerPartAlternateRep) {
//                        NavigationCriteriaHelper.service.updateCachedNavigationCriteria(ncid, cachedNC, cacheID);
//                    }
//                    if (log.isDebugEnabled()) {
//                        log.debug("cachedNC from NavigationCriteriaHelper.service.updateCachedNavigationCriteria(): "
//                                + cachedNC
//                                + " applicableType: " + cachedNC.getApplicableType());
//                    }
//                }
//
//                JSONFromNC = NavigationCriteriaHelper.service.getJSONFromNavigationCriteria(cachedNC);
//                aMap.put(ExpansionCriteriaConstants.JSON_STRING, JSONFromNC);
//
//                // If the delegate is null, cache the NC_OID
//                // If the delegate is not null, NC_OID is already cached,
//                // but if delegate returns a persistent cachedNC, update the map with new one.
//                if (delegate == null || PersistenceHelper.isPersistent(cachedNC)) {
//                    cachedNCOID = cachedNC.toString();
//                    aMap.put(ExpansionCriteriaConstants.NC_OID, cachedNCOID);
//                }
//
//                // Set the value if filter is being updated
//                aMap.put(ExpansionCriteriaConstants.IS_FILTER_UPDATE_CASE, isUpdateCase + "");
//
//                aMap.put(ExpansionCriteriaConstants.NCID, ncid);
//
//                if (!alreadyProcessedFilter) {
//                    /*
//                     * Having saved and updated the filter, store it in processedFilterInfo. Thus, if the downstream (or
//                     * upstream) side of the MAPSB/SAPSB UI attempts to save a filter that is already saved by the
//                     * upstream (or downstream) side, it can be ignored by checking the data in processedFilterInfo
//                     */
//                    addToProcessedFilterInfo(cachedNCOID, JSONFromNC);
//
//                    /*
//                     * Add the currently processed filter information to currentFilterInfo along with the corresponding
//                     * session ID information
//                     */
//                    if (!isNCOwnerPartAlternateRep) {
//                        setCurrentFilterInfo(cacheID.toString(), cachedNCOID, JSONFromNC);
//                    }
//                }
//            } catch (WTException e) {
//                if (e instanceof wt.pom.UniquenessException) {
//                    log.debug("Adding the key UNIQUE_NESS_EXCEPTION to aMap.");
//                    aMap.put(ECExceptionConstants.UNIQUE_NESS_EXCEPTION, "true");
//                }
//                else {
//                    log.debug("Adding the key NCID_INVALID_EXCEPTION to aMap.");
//                    aMap.put(ECExceptionConstants.NCID_INVALID_EXCEPTION, e.toString());
//                }
//                log.error("Problem in saving NavigationCriteria: ncid = " + ncid, e);
//            } catch (Exception e) {
//                log.debug("Adding the key NCID_INVALID_EXCEPTION to aMap.");
//                aMap.put(ECExceptionConstants.NCID_INVALID_EXCEPTION, e.toString());
//                log.error("Problem in saving NavigationCriteria: ncid = " + ncid, e);
//            }
//        }
//        return aMap;
//    }
//
//    /**
//     * Fetches the JSON String from the value stored against the input key in currentFilterInfo.
//     *
//     * @param cacheID
//     *            - Input key (a String representation of a NavCriteriaCacheId object) using which to fetch the output
//     *            JSON String.
//     * @return - JSON String extracted from the value stored against the input key in currentFilterInfo.
//     */
//    private static String getJSONFromCurrentFilterInfo(String cacheID) {
//        log.debug("Enter => getJSONFromCurrentFilterInfo().");
//
//        int ptrIndex;
//        String currentJSON = null, temp = currentFilterInfo.get(cacheID);
//        if (temp != null && (ptrIndex = temp.indexOf("=>")) != -1) {
//            // Extract the JSON String from the value in temp.
//            currentJSON = temp.substring(ptrIndex + 2).trim();
//        }
//
//        if (log.isDebugEnabled()) {
//            log.debug("Returning: " + currentJSON);
//        }
//        return currentJSON;
//    }
//
//    /**
//     * Adds the input parameters to the Map in currentFilterInfo.
//     *
//     * @param cacheID
//     *            - String representation of a NavCriteriaCacheId object, holding session ID information. This is added
//     *            as a key to currentFilterInfo.
//     * @param ncOid
//     *            - Object ID of the NavigationCriteria instance to be added.
//     * @param JSON
//     *            - JSON representation of the corresponding filter to be added.
//     * @return - the value corresponding to the input key that was previously stored in currentFilterInfo, if available;
//     *         otherwise, a null.
//     */
//    public static String setCurrentFilterInfo(String cacheID, String ncOid, String JSON) {
//        log.debug("Enter => setCurrentFilterInfo().");
//
//        // Fetch the JSON String corresponding to the previously stored filter information, if any
//        int ptrIndex;
//        String oldJSON = null, temp = currentFilterInfo.get(cacheID);
//        if (temp != null && (ptrIndex = temp.indexOf("=>")) != -1) {
//            oldJSON = temp.substring(ptrIndex + 2).trim();
//        }
//        if (log.isDebugEnabled()) {
//            log.debug("oldJSON: " + oldJSON);
//        }
//
//        /*
//         * Add cacheID as the key and a String composed of both the NC object ID and its JSON representation as the
//         * value to the Map in currentFilterInfo
//         */
//        String inputStr = ncOid + "=>" + JSON;
//        currentFilterInfo.put(cacheID, inputStr);
//
//        if (log.isDebugEnabled()) {
//            log.debug("currentFilterInfo: " + currentFilterInfo + " oldJSON: " + oldJSON);
//        }
//
//        return oldJSON;
//    }
//
//    public static String getCurrentFilterInfo(String cacheID) {
//        log.debug("Enter => getCurrentFilterInfo().");
//
//        // Fetch the JSON String corresponding to the previously stored filter information, if any
//        int ptrIndex;
//        String oldJSON = null, temp = currentFilterInfo.get(cacheID);
//        if (temp != null && (ptrIndex = temp.indexOf("=>")) != -1) {
//            oldJSON = temp.substring(ptrIndex + 2).trim();
//        }
//        if (log.isDebugEnabled()) {
//            log.debug("oldJSON: " + oldJSON);
//        }
//
//        return oldJSON;
//    }
//
//    /**
//     * Adds the input object ID and the JSON representation to the collection in processedFilterInfo. The addition is
//     * done in such a way that the resulting size of the collection is limited to MAXSIZE; this is achieved by clearing
//     * one or more objects from the beginning of the collection. The method does nothing if the input object ID and the
//     * JSON representation already exist in the collection.
//     *
//     * Note: A String of the form "<Object ID>=><JSON representation>" is added to the collection, where <Object ID> and
//     * <JSON representation> are the values in the input parameters ncOid and JSON respectively.
//     *
//     * @param ncOid
//     *            - Object ID of the NavigationCriteria instance to be added.
//     * @param JSON
//     *            - JSON representation of the corresponding filter to be added.
//     */
//    private void addToProcessedFilterInfo(String ncOid, String JSON) {
//        log.debug("Enter => addToProcessedFilterInfo().");
//        int size = processedFilterInfo.size();
//        if (size >= MAXSIZE) {
//            processedFilterInfo.subList(0, (size - MAXSIZE + 1)).clear();
//        }
//
//        String inputStr = ncOid + "=>" + JSON;
//        if (!processedFilterInfo.contains(inputStr)) {
//            processedFilterInfo.add(inputStr);
//        }
//
//        if (log.isDebugEnabled()) {
//            log.debug("processedFilterInfo: " + processedFilterInfo);
//        }
//        log.debug("Exit => addToProcessedFilterInfo().");
//    }
//
//    /**
//     * Checks if any of the input NavigationCriteria object IDs figure in the collection in processedFilterInfo, and if
//     * they do, removes the corresponding objects from the collection.
//     *
//     * @param ncOids
//     *            - Set of input NavigationCriteria object IDs to be looked for in processedFilterInfo.
//     */
//    private void delFromProcessedFilterInfo(Set<String> ncOids) {
//        log.debug("Enter => delFromProcessedFilterInfo().");
//        String str, oid;
//        int ptrIndex;
//        Iterator<String> iter = processedFilterInfo.iterator();
//
//        while (iter.hasNext()) {
//            str = iter.next();
//            if (log.isDebugEnabled()) {
//                log.debug("str: " + str);
//            }
//            if ((ptrIndex = str.indexOf("=>")) > 0) {
//                oid = str.substring(0, ptrIndex).trim();
//                if (log.isDebugEnabled()) {
//                    log.debug("oid: " + oid);
//                }
//                if (ncOids.contains(oid)) {
//                    log.debug("Removing the current object from processedFilterInfo.");
//                    iter.remove();
//                }
//            }
//        }
//        log.debug("Exit => delFromProcessedFilterInfo().");
//    }
//
//    /**
//     * Checks if any of the input NavigationCriteria object IDs figures in the value corresponding to the input key in
//     * the Map in currentFilterInfo, and if it does, removes the corresponding entry from the Map.
//     *
//     * @param cacheID
//     *            - String representation of a NavCriteriaCacheId object, holding session ID information.
//     * @param ncOids
//     *            - Set of input NavigationCriteria object IDs to be looked for in currentFilterInfo.
//     */
//    private static void delFromCurrentFilterInfo(String cacheID, Set<String> ncOids) {
//        log.debug("Enter => delFromCurrentFilterInfo().");
//        int ptrIndex;
//        String key, value, oid;
//        Set<Map.Entry<String, String>> entries = currentFilterInfo.entrySet();
//
//        for (Map.Entry<String, String> entry : entries) {
//            key = entry.getKey();
//            value = entry.getValue();
//
//            if (log.isDebugEnabled()) {
//                log.debug("key: " + key + " value: " + value);
//            }
//
//            if (key.equals(cacheID) && (ptrIndex = value.indexOf("=>")) > 0) {
//                oid = value.substring(0, ptrIndex).trim();
//                if (ncOids.contains(oid)) {
//                    log.debug("Removing the entry from currentFilterInfo.");
//                    currentFilterInfo.remove(key);
//                    break;
//                }
//            }
//        }
//        log.debug("Exit => delFromCurrentFilterInfo().");
//    }
//
//    /**
//     * Lets the caller know if the input types are part of the same class or type hierarchy.
//     *
//     * @param type1
//     *            - String representation of the first type; can be a fully qualified class name, or a type name such as
//     *            "wt.epm.EPMDocument|org.rnd.DefaultEPMDocument".
//     * @param type2
//     *            - String representation of the second type; can be a fully qualified class name, or a type name.
//     * @return - a true, if type2 (or type1) is either the same as, or is a super-type of type1 (or type2); a false,
//     *         otherwise.
//     */
//    @SuppressWarnings("unchecked")
//    private boolean areTypesComparable(String type1, String type2) {
//        if (log.isDebugEnabled()) {
//            log.debug("Enter => areTypesComparable().");
//            log.debug("type1: " + type1 + " type2: " + type2);
//        }
//
//        try {
//            Class class1 = Class.forName(type1);
//            Class class2 = Class.forName(type2);
//
//            if (class2.isAssignableFrom(class1) || class1.isAssignableFrom(class2)) {
//                log.debug("Returning a true.");
//                return true;
//            }
//        } catch (ClassNotFoundException cnfe) {
//            log.debug(
//                    "Unable to fetch a Class instance for one of the input types. Attempting to fetch their type identifiers.");
//            TypeIdentifier tid1 = TypedUtility.getTypeIdentifier(type1);
//            TypeIdentifier tid2 = TypedUtility.getTypeIdentifier(type2);
//
//            if (tid1 != null && tid2 != null) {
//                if (tid1.isEquivalentTypeIdentifier(tid2) || tid1.isDescendedFrom(tid2) || tid2.isDescendedFrom(tid1)) {
//                    log.debug("Returning a true.");
//                    return true;
//                }
//            }
//        }
//
//        log.debug("Returning a false.");
//        return false;
//    }
//
//    /*
//     * (non-Javadoc)
//     *
//     * @see com.ptc.expansionui.client.ClientNavigationCriteriaService#hideNavCriteria(java.util.List)
//     */
//    @Override
//    public void hideNavCriteria(List<String> oids) {
//        // Candidate of removal. This method is no longer used from EC Code
//        try {
//            NavigationCriteriaHelper.service.hideNavCriteria(getObjectIdentifierList(oids));
//        } catch (WTException e) {
//            log.error("Problem in hiding Navigation Criterias oids" + oids, e);
//        }
//    }
//
//    /*
//     * (non-Javadoc)
//     *
//     * @see com.ptc.expansionui.client.ClientNavigationCriteriaService#showNavCriteria(java.util.List)
//     */
//    @Override
//    public void showNavCriteria(List<String> oids) {
//        // Candidate of removal. This method is no longer used from EC Code
//        try {
//            NavigationCriteriaHelper.service.showNavCriteria(getObjectIdentifierList(oids));
//        } catch (WTException e) {
//            log.error("Problem in showing Navigation Criterias oids" + oids, e);
//        }
//    }
//
//    /*
//     * (non-Javadoc)
//     *
//     * @see com.ptc.expansionui.client.ClientNavigationCriteriaService#applyNavigationCriteria(java.lang.String,
//     * java.lang.String)
//     */
//    @Override
//    public String applyNavigationCriteria(String contextId, String jsonString) {
//        if (log.isDebugEnabled()) {
//            log.debug("applyNavigationCriteria: Starting applying the JSON NC to the context object: " + contextId
//                    + " jsonString: " + jsonString);
//        }
//        if (jsonString == null) {
//            return contextId;
//        }
//
//        NavigationCriteria nco = null;
//        try {
//            nco = NavigationCriteriaHelper.service.getNavigationCriteriaFromJSON(jsonString);
//            if (nco != null) {
//                return applyNavigationCriteria(contextId, nco);
//            }
//        } catch (Exception e) {
//            log.error("Problem in getting NCO from services using JSON =", e);
//        }
//        return contextId;
//    }
//
//    /**
//     * Apply navigation criteria.
//     *
//     * @param contextId
//     *            the context id
//     * @param nco
//     *            the nco
//     * @return the string
//     */
//    public String applyNavigationCriteria(String contextId, NavigationCriteria nco) {
//        if (log.isDebugEnabled()) {
//            log.debug("applyNavigationCriteria: Starting applying the NC to the context object: " + contextId
//                    + " nco: " + nco);
//        }
//        if (contextId == null) {
//            return null;
//        }
//        Persistable persistable = null;
//        try {
//            boolean isPartAlternateRep = PARHelper.isPartAlternateRep(contextId);
//            if (isPartAlternateRep) {
//                // In the case of the PAR, the nco is for the WTPart structure and not the context object, which is a
//                // WTPartAlternateRep.
//                // So the nco will never select a different version of the context object, so return the contextId and
//                // bypass this logic.
//                return contextId;
//            }
//
//            ReferenceFactory factory = new ReferenceFactory();
//            persistable = factory.getReference(contextId).getObject();
//
//            if (persistable instanceof WTProductConfiguration) {
//                return contextId;
//            }
//        } catch (WTRuntimeException e1) {
//            log.error("Problem in getting the object of the contextid=" + contextId, e1);
//        } catch (WTException e1) {
//            log.error("Problem in getting the object of the contextid=" + contextId, e1);
//        }
//
//        if (persistable instanceof Iterated || persistable instanceof Mastered) {
//            if (nco == null) {
//                return contextId;
//            }
//            Mastered master = persistable instanceof Iterated ? ((Iterated) persistable).getMaster()
//                    : (Mastered) persistable;
//            try {
//                Iterated newIt = null;
//                QueryResult qr = ConfigHelper.service.filteredIterationsOf(master,
//                        nco.getConfigSpecsToProcess(master.getClass()));
//                if (qr != null) {
//                    if (qr.hasMoreElements()) {
//                        newIt = (Iterated) qr.nextElement();
//                    }
//                    if (newIt != null) {
//                        ObjectIdentifier oid = wt.fc.PersistenceHelper.getObjectIdentifier(newIt);
//                        if (log.isDebugEnabled()) {
//                            log.debug("applyNavigationCriteria: Completed applying the JSON to the context object: "
//                                    + contextId
//                                    + " new context object: " + oid.toString());
//                        }
//                        return oid.toString();
//                    }
//                }
//                return null;
//            } catch (Exception e) {
//                log.error("Problem in applying the filter on the context object", e);
//            }
//            return null;
//        }
//        else {
//            return contextId;
//        }
//    }
//
//    /**
//     * Verify abd Gets the jSO nfor navigation criteria oid.
//     *
//     * @param oid
//     *            the oid
//     * @param seedOids
//     *            the seed oids
//     *
//     * @return the json for navigation criteria oid
//     */
//    @Override
//    public VerificationResultBean verifyAndGetJSONforNcOid(String oid, List<String> seedOids, String seedType) {
//        VerificationResultBean verificationResultBean = new VerificationResultBean(false, null, null);
//        try {
//            ReferenceFactory refFactory = new ReferenceFactory();
//            WTReference ref = refFactory.getReference(oid);
//            NavigationCriteria navCriteria = (NavigationCriteria) ref.getObject();
//            navCriteria = NavigationCriteriaHelper.service.getPopulatedNavigationCriteria(navCriteria);
//
//            // Filtering seedOids based on applicable type.
//            seedType = navCriteria.getApplicableType();
//            List<String> typedSeedOids = new ArrayList<String>();
//
//            if (seedType != null && seedOids != null) {// seedType is null in VarientSpec. where seedOids could be null
//                                                       // in PPB (Related Assembly Filter)
//                for (String seedOid : seedOids) {
//                    if (seedOid.indexOf(seedType) != -1)
//                        typedSeedOids.add(seedOid);
//                }
//            }
//            else {
//                typedSeedOids = seedOids;
//            }
//
//            return verifyNavigationCriteria(navCriteria, typedSeedOids, seedType, null);
//        } catch (WTException e) {
//            log.error("Problem in getting json for the NCOID " + oid, e);
//            e.printStackTrace();
//        }
//        if (log.isDebugEnabled()) {
//            log.debug("verifyAndGetJSONforNcOid: VerificationResultBean:" + verificationResultBean);
//        }
//        return verificationResultBean;
//    }
//
//    private VerificationResultBean verifyNavigationCriteria(NavigationCriteria navCriteria, List<String> seedOids,
//            String seedType, Map<String, String> context)
//                    throws WTException {
//        if (navCriteria == null) {
//            return new VerificationResultBean(false, null, null);
//        }
//        VerificationResultBean verificationResultBean = new VerificationResultBean(true, null, null);
//        WTCollection seedObjects = null;
//        if (seedOids != null && !seedOids.isEmpty()) {
//            seedObjects = reOrderCollection(getInflatedCollection(seedOids));
//        }
//        NavCriteriaContext navContext = new NavCriteriaContext();
//        navContext.setSeeds(seedObjects);
//        try {
//            if (seedType != null) {
//                String className = ServerHelper.getClassNameFromTypeName(seedType);
//                if (log.isDebugEnabled()) {
//                    log.debug("className from ServerHelper.getClassNameFromTypeName(): " + className);
//                }
//                navContext.setApplicableType(Class.forName(className));
//            }
//        } catch (Exception e) {
//            log.error("Problem in verifyNavigationCriteria setApplicableType");
//            e.printStackTrace();
//        }
//        List<String> messages = new ArrayList<String>();
//        boolean isValidCS = verifiedAndUpdateConfigSpecs(navCriteria, navContext, messages);
//        // if all the config specs are invalid then get the default config spec
//        String contextApp = null;
//        if (context != null) {
//            contextApp = context.get(ExpansionCriteriaConstants.URL_APP);
//        }
//        if (navCriteria.getConfigSpecs().size() == 0
//                && !PFSB_VS_NC_CACHE_COOKIE_ID.equals(contextApp)
//                && !PFSB_VS_APP_NAME.equals(contextApp)) {
//            NavigationCriteria defaultNC = getDefaultNavigationCriteria(seedObjects, context, null);
//            navCriteria.setConfigSpecs(defaultNC.getConfigSpecs());
//            if (verificationResultBean != null) {
//                verificationResultBean.setDefaultConfigSpec(true);
//            }
//        }
//        boolean isValidFilters = verifiedAndUpdateFilters(navCriteria, navContext, messages);
//        // if either CS or Filters are not valid then set the valid flag to false.
//        if (verificationResultBean != null) {
//            if (!isValidCS || !isValidFilters) {
//                verificationResultBean.setValid(false);
//                verificationResultBean.setMessages(messages);
//            }
//        }
//        verificationResultBean.setJson(encodeJSONContent(NavigationCriteriaHelper.service
//                .getJSONFromNavigationCriteria(navCriteria), seedOids));
//        return verificationResultBean;
//    }
//
//    // this is for XSS security
//    private String encodeJSONContent(String json, List<String> seedOids) {
//        try {
//            JSONObject jsonNavCriteria = new JSONObject(json);
//            if (!jsonNavCriteria.isNull(NavigationCriteria.NAME)) {
//                jsonNavCriteria.put(NavigationCriteria.NAME,
//                        (jsonNavCriteria.getString(NavigationCriteria.NAME)).replace(">", "&gt;").replace("<", "&lt;"));
//            }
//            if (!jsonNavCriteria.isNull(NavigationCriteria.CONFIG_SPECS)) {
//                JSONArray jsonConfigSpecs = jsonNavCriteria.getJSONArray(NavigationCriteria.CONFIG_SPECS);
//                for (int i = 0; i < jsonConfigSpecs.length(); i++) {
//                    JSONObject jsonConfigSpec = jsonConfigSpecs.getJSONObject(i);
//                    JSONArray arrayKey = jsonConfigSpec.names();
//                    for (int j = 0; j < arrayKey.length(); j++) {
//                        // for each element of ConfigSpec check if it is JSONObject and it has element as label
//                        // this code will encode the label element of JSONObject inside the ConfigSpec
//                        try {
//                            String strJSON = (String) arrayKey.get(j);
//                            // For as stored config type add the seedOids information as it is used while JSON to
//                            // ConfigType conversion in EPMDocConfigSpecDelegate
//                            // TODO - find if we can do it in better way
//                            if (seedOids != null && !seedOids.isEmpty() && CONFIG_TYPE.equals(strJSON)) {
//                                if (AS_STORED_CONFIG_TYPE.equals(jsonConfigSpec.getString(strJSON))) {
//                                    jsonConfigSpec.put(OID_KEY, seedOids);
//                                }
//                                else if (AS_MATURED_CONFIG_TYPE.equals(jsonConfigSpec.getString(CONFIG_TYPE))) {
//                                    // If the value of seedOid is null then use the seedOid instead.
//                                    String seedOid = jsonConfigSpec.getString(SEED_OID_KEY);
//                                    if (seedOid == null || seedOid.equals("null")) {
//                                        jsonConfigSpec.put(SEED_OID_KEY, seedOids.get(0));
//                                    }
//                                }
//                            }
//
//                            JSONObject jsonInner = jsonConfigSpec.getJSONObject(strJSON);
//                            jsonInner.put(PersistableConfigSpecDelegate.LABEL_KEY,
//                                    (jsonInner.getString(PersistableConfigSpecDelegate.LABEL_KEY)).replace(">", "&gt;")
//                                            .replace("<", "&lt;"));
//                        } catch (JSONException e) {
//                            // either it is not JSONObject or label not found, keep going...
//                            continue;
//                        }
//                    }
//                }
//            }
//            return jsonNavCriteria.toJSONString();
//        } catch (JSONException je) {
//            log.error("Problem in encodeJSONContent: ", je);
//        }
//        return json;
//    }
//
//    /**
//     * Get List of nav criteria with visibility
//     *
//     * @param seedType
//     *
//     * @return List<SavedExpansionCriteriaBean>
//     */
//
//    @SuppressWarnings("static-access")
//    @Override
//    public List<SavedExpansionCriteriaBean> getListOfNavigationCriteriaWithVisibility(String appName, String seedOid,
//            String seedType) {
//        if (log.isDebugEnabled()) {
//            log.debug("Enter => getListOfNavigationCriteriaWithVisibility(): appName: " + appName + " seedOid: " +
//                    seedOid + " seedType: " + seedType);
//        }
//
//        WTPrincipal user = null;
//        Locale locale = null;
//        Map<NavigationCriteria, Boolean> map = null;
//        ArrayList<SavedExpansionCriteriaBean> saveECBeanList = new ArrayList<SavedExpansionCriteriaBean>();
//
//        try {
//            locale = SessionHelper.getLocale();
//            user = SessionHelper.getPrincipal();
//            if (seedType == null) {
//                seedType = getSeedTypeFromSeedOid(seedOid);
//                if (log.isDebugEnabled()) {
//                    log.debug("seedType from getSeedTypeFromSeedOid(): " + seedType);
//                }
//            }
//
//            if (NavigationCriteriaConstants.CONFIGURATION_CONTEXT_APP_NAME.equals(appName)) {
//                seedType = WTPart.class.getCanonicalName();
//            }
//
//            NavCriteriaContext navContext = new NavCriteriaContext();
//            navContext.setApplicationName(appName);
//            String defaultNcOid = NavigationCriteriaHelper.service.getUserDefaultNavCriteriaId(navContext);
//
//            // when app name is either 'Checkout' or 'Download' get list for both seed types (EPMDocument and WTPart)
//            if (NavigationCriteriaConstants.ADD_TO_WS_APP_NAME.equals(appName)
//                    || NavigationCriteriaConstants.CHECKOUT_APP_NAME.equalsIgnoreCase(appName)) {
//                map = NavigationCriteriaHelper.service.listNavigationCriteriaWithVisibility((WTUser) user, null,
//                        WTPart.class.getCanonicalName(), true, true);
//
//                if (map != null) {
//                    map.putAll(NavigationCriteriaHelper.service.listNavigationCriteriaWithVisibility((WTUser) user,
//                            null, EPMDocument.class.getCanonicalName(), true, true));
//                }
//            }
//            else {
//                map = NavigationCriteriaHelper.service.listNavigationCriteriaWithVisibility((WTUser) user, null,
//                        seedType,
//                        true, true);
//            }
//
//            Iterator<Entry<NavigationCriteria, Boolean>> itr = map.entrySet().iterator();
//            while (itr.hasNext()) {
//                Map.Entry<NavigationCriteria, Boolean> entry = itr.next();
//                NavigationCriteria navCriteria = entry.getKey();
//                String ncOid = navCriteria.toString();
//                SavedExpansionCriteriaBean ecb = new SavedExpansionCriteriaBean();
//
//                // if current ncOid is the same as default NcOid, set the filter as default
//                if (defaultNcOid != null && defaultNcOid.equals(ncOid)) {
//                    ecb.setDefaultEC(true);
//                }
//                ecb.setOid(ncOid);
//                ecb.setName(Format.htmlEncode(navCriteria.getName()));
//                ecb.setShowEC(entry.getValue());
//                ecb.setShareEC(navCriteria.isSharedToAll());
//                ecb.setType(navCriteria.getApplicableType());
//                try {
//                    // Owner name is displayed on the manage filter UI
//                    ecb.setCreator(((WTUser) navCriteria.getOwner()).getName());
//                    // Owner Oid will be used to decide if current user can delete\modify the saved filter.
//                    ecb.setOwner(navCriteria.getOwner().getPersistInfo().getObjectIdentifier().toString());
//                } catch (Exception e) {
//                    if (log.isDebugEnabled()) {
//                        log.debug("Problem in getting the owner of the Navigation Criteria " + navCriteria + " User : "
//                                + user
//                                + " Ignoring the exception and setting owner name as Secured Information.", e);
//                    }
//                    String securedMessage = DataUtilityExceptionHandler.handleException("Saved Filter Owner Name", e);
//                    ecb.setCreator(securedMessage);
//                    ecb.setOwner(securedMessage);
//                }
//                WTStandardDateFormat sdf = new WTStandardDateFormat();
//                ResourceBundle bundle = ResourceBundle.getBundle(componentRB, locale);
//                String format = bundle.getString("STANDARD_DATE_TIME_ZONE_DISPLAY_FORMAT");
//                ecb.setLastModified(sdf.format(navCriteria.getPersistInfo().getModifyStamp(), format, locale,
//                        TimeZoneHelper.getLocalTimeZone()));
//                saveECBeanList.add(ecb);
//            }
//        } catch (WTException e) {
//            log.error("Problem in getListOfNavigationCriteriaWithVisibility(): user: " + user + " locale: " + locale,
//                    e);
//        }
//
//        log.debug("Exit => getListOfNavigationCriteriaWithVisibility().");
//        return saveECBeanList;
//    }
//
//    /**
//     * Gets navigation criteria id.
//     *
//     * @param id
//     *            the ncid
//     * @param clientInfoBean
//     *            the client info bean
//     *
//     * @return ncOid
//     */
//    @Override
//    public SavedExpansionCriteriaBean getSavedNavigationCriteriaList(String seedOid, String seedType, String id,
//            ClientNavigationCriteriaBean clientInfoBean) {
//        if (log.isDebugEnabled()) {
//            log.debug("getSavedNavigationCriteriaList: Starting getting the ncOid from the NCID:[" + id
//                    + "] ApplicationInfo: " + clientInfoBean);
//        }
//        SavedExpansionCriteriaBean saveExpansionCriteriaBean = new SavedExpansionCriteriaBean();
//        try {
//            String ncOid = "";
//            if (id != null && !id.isEmpty()) {
//                NavigationCriteria nc = NavigationCriteriaHelper.service.getCachedNavigationCriteria(id,
//                        getNavCriteriaCacheId(clientInfoBean));
//                if (nc != null) {
//                    if (PersistenceHelper.isPersistent(nc)) {
//                        ncOid = nc.toString();
//                    }
//                    else if (nc.getName() != null) { // in case of user sets a saved filter as default filter, ncOid
//                                                     // needs to be set as the nc name
//                        ncOid = nc.getName();
//                    }
//                }
//                else {
//                    List<String> _seedOid = new ArrayList<String>();
//                    _seedOid.add(seedOid);
//                    Map<String, String> context = new HashMap<String, String>();
//                    context.put(ExpansionCriteriaConstants.URL_APP, clientInfoBean.getAppName());
//                    nc = getDefaultNavigationCriteria(_seedOid, context, null);
//                    NavigationCriteriaHelper.service.updateCachedNavigationCriteria(id, nc,
//                            getNavCriteriaCacheId(clientInfoBean));
//                }
//            }
//            List<SavedExpansionCriteriaBean> navigationCriteriaList = getListOfNavigationCriteriaWithVisibility(
//                    clientInfoBean.getAppName(), seedOid, seedType);
//            Collections.sort(navigationCriteriaList);
//            saveExpansionCriteriaBean.setOid(ncOid);
//            saveExpansionCriteriaBean.setSavedExpansionCiteriaList(navigationCriteriaList);
//            saveExpansionCriteriaBean.setVaraintSpecMenuStatus(isShowVariantSpecMenu(seedOid));
//            if (log.isTraceEnabled()) {
//                log.trace("getSavedNavigationCriteriaList: Selected NC =[" + ncOid + "] NCList = "
//                        + navigationCriteriaList
//                        + " isShowVariantSpecMenu = " + saveExpansionCriteriaBean.getVaraintSpecMenuStatus());
//            }
//        } catch (Exception e) {
//            log.error("Can't get ncOid for ncid " + id, e);
//        }
//        return saveExpansionCriteriaBean;
//    }
//
//    private VaraintSpecMenuStatus isShowVariantSpecMenu(String seedOid)
//            throws WTException {
//        VaraintSpecMenuStatus status = VaraintSpecMenuStatus.HIDE;
//        WTReference reference = null;
//        try {
//            reference = new ReferenceFactory().getReference(seedOid);
//            Persistable part = reference.getObject();
//            if (part instanceof Genericizable) {
//                Genericizable genericizable = (Genericizable) part;
//                if (GenericHelper.isConfigurable(genericizable)) {
//                    status = VaraintSpecMenuStatus.SHOW;
//                }
//            }
//            else if (part instanceof WTPartAlternateRep) {
//                final WTPartAlternateRep par = (WTPartAlternateRep) part;
//                final ObjectReference vcRef = par.getVariantSpecReference();
//                if (vcRef == null || vcRef.getReferencedClass() == null) {
//                    status = VaraintSpecMenuStatus.SHOW;
//                }
//            }
//        } catch (Exception e) {
//            log.trace("Unable to create a WTReference from the oid.");
//        }
//        return status;
//    }
//
//    /**
//     * This method will be called when ECUI will be updated. This method will set the updated ncid in the cookie with
//     * the key of hash and the oid.
//     *
//     * @param key
//     *            the hashcode\timestamp
//     * @param ncid
//     *            the ncid
//     * @param clientInfoBean
//     *            the client info bean
//     * @param oids
//     *            the oids
//     */
//    @Override
//    public void updateECCache(ExpansionCriteriaBean ecBean) {
//        if (log.isDebugEnabled()) {
//            log.debug("ClientNavigationCriteriaServiceImpl.updateECCache(): " + ecBean);
//        }
//        String remoteAddr = (ecBean != null && ecBean.getRemoteAddress() != null) ? ecBean.getRemoteAddress()
//                : getRemoteAddr();
//        String sessionId = (ecBean != null && ecBean.getSessionId() != null) ? ecBean.getSessionId() : getSessionId();
//        ServletSessionCookieManager cookieManager = new ServletSessionCookieManager(remoteAddr, sessionId);
//
//        // Cookie Type will be combination of application name + some constant
//        String cookieType = ecBean.getApplicationName() + ClientNavigationCriteriaService.EC_SESSION_COOKIE;
//
//        List<String> oids = null;
//        if (ecBean.isNewEcId()) {
//            // if ncid has been changed get the old seed oids to update ec cache
//            oids = ecBean.getOldSeedOids();
//        }
//        else {
//            oids = ecBean.getSeedOids();
//        }
//
//        // Keeping cookieId as blank string because cookiemanger doesnt accept null value
//        String cookieId = "";
//        // oids list will null in PSE for draft mode
//        if (oids == null || oids.isEmpty()) {
//            cookieId = ecBean.getType();
//        }
//        else if (oids != null && oids.size() == 1) {
//            // Cookie Id will be root level object
//            cookieId = oids.get(0);
//        }
//
//        // The hash is just an automatically generated string (by PSE) that would be part of the cookie key
//        String hash = ecBean.getHashKey();
//
//        ServletSessionCookie cookie = cookieManager.getCookie(cookieId, cookieType);
//        if (cookie != null) {
//            cookie.put(hash, ecBean.getECId());
//            if (log.isDebugEnabled()) {
//                log.debug("ClientNavigationCriteriaServiceImpl.updateECCache() remoteAddr= " + remoteAddr
//                        + " sessionId= " + sessionId
//                        + " cookieId=" + cookieId + " ncid=" + ecBean.getECId() + " hash=" + hash);
//            }
//            // call saveCookies so that the most recently used list of cookies is also saved.
//            cookieManager.saveCookie(cookie);
//        }
//
//        if (log.isDebugEnabled()) {
//            log.debug("ClientNavigationCriteriaServiceImpl.updateECCache() Done!!");
//        }
//    }
//
//    /**
//     * This method will return the Remote Address.
//     *
//     * @return the remote addr
//     */
//    public static String getRemoteAddr() {
//        ServletRequestAttributes sar = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        if (sar != null && sar.getRequest() != null) {
//            HttpServletRequest req = sar.getRequest();
//            return req.getRemoteAddr();
//        }
//        return null;
//    }
//
//    /**
//     * This method will return the session id.
//     *
//     * @return the session id
//     */
//    public static String getSessionId() {
//        ServletRequestAttributes sar = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        if (sar != null && sar.getRequest() != null) {
//            HttpServletRequest req = sar.getRequest();
//            HttpSession hs = req.getSession();
//            return hs.getId();
//        }
//        return null;
//    }
//
//    /**
//     * This method will delete the navigation criteria.
//     *
//     * @param ncOids
//     *            the nc oids
//     *
//     * @return void
//     */
//    @Override
//    public void deleteNavigationCriteria(String ncid, HashSet<String> ncOids,
//            ClientNavigationCriteriaBean clientNavBean) {
//        log.debug("Enter => deleteNavigationCriteria().");
//
//        ReferenceFactory refFactory = new ReferenceFactory();
//        WTSet navCriteriaSet = new WTHashSet();
//        WTSet navFilters = new WTHashSet();
//        try {
//            for (String ncOid : ncOids) {
//                navCriteriaSet.add(refFactory.getReference(ncOid));
//            }
//            navCriteriaSet.inflate();
//            for (Object ncObj : navCriteriaSet.persistableCollection()) {
//                if (ncObj instanceof NavigationCriteria) {
//                    NavigationCriteria ncToDelete = (NavigationCriteria) ncObj;
//                    navFilters.addAll(ncToDelete.getFilters());
//                }
//            }
//
//            NavCriteriaCacheId cacheID = getNavCriteriaCacheId(clientNavBean);
//            NavigationCriteria nc = NavigationCriteriaHelper.service.getCachedNavigationCriteria(ncid, cacheID);
//            NavigationCriteria deepCopy = NavigationCriteriaHelper.service.getDeepCopy(nc);
//
//            NavigationCriteriaHelper.service.deleteNavigationCriterias(navCriteriaSet);
//
//            // Remove objects corresponding to the deleted filters from processedFilterInfo (if any).
//            delFromProcessedFilterInfo(ncOids);
//
//            /*
//             * If any of the deleted filters figures in currentFilterInfo for the session ID in cacheID, remove the
//             * corresponding entry from the Map
//             */
//            delFromCurrentFilterInfo(cacheID.toString(), ncOids);
//
//            /* id Deleted EC is applied in structure and also modified and saved in cache then do following */
//            if (nc != null) {
//                String jsonObj = NavigationCriteriaHelper.service.getJSONFromNavigationCriteria(nc);
//
//                boolean filterInStale = false;
//
//                if (!navFilters.isEmpty()) {
//                    Collection<NavigationFilter2> filters = nc.getFilters();
//                    for (NavigationFilter2 filter : filters) {
//                        if (navFilters.contains(filter) && PersistenceHelper.isPersistent(filter)) {
//                            // deep copy if in-session nc has any stale filter
//                            filterInStale = true;
//                            break;
//                        }
//                    }
//                }
//
//                if (navCriteriaSet.contains(nc) || filterInStale) {
//                    nc = NavigationCriteriaHelper.service.updateNavigationCriteriaFromJSON(deepCopy, jsonObj);
//                    NavigationCriteriaHelper.service.updateCachedNavigationCriteria(ncid, nc, cacheID);
//                }
//
//            }
//
//        } catch (WTRuntimeException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } catch (WTException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//        log.debug("Exit => deleteNavigationCriteria().");
//    }
//
//    /**
//     * This method will return the login user.
//     *
//     * @return String
//     */
//    @Override
//    public String getLoginUser() {
//        String loginUserOid = null;
//        try {
//            loginUserOid = SessionHelper.getPrincipal().getPersistInfo()
//                    .getObjectIdentifier().toString();
//        } catch (WTException e) {
//            log.error("Problem in getting login user name ", e);
//        }
//        return loginUserOid;
//    }
//
//    @Override
//    public String copyNavigationCriteria(String json, String ncid, ClientNavigationCriteriaBean clientInfoBean) {
//
//        String name = "";
//        String savedNcOid = null;
//        try {
//            JSONObject jsonNavCriteria = new JSONObject(json);
//            if (!jsonNavCriteria.isNull(NavigationCriteria.NAME)) {
//                name = ExpansionCriteriaUtil.decodeHTMLContent(jsonNavCriteria.getString(NavigationCriteria.NAME));
//                jsonNavCriteria.put(NavigationCriteria.NAME, name);
//            }
//            NavigationCriteria newNavCriteria = null;
//            NavigationCriteria oldNavCriteria = null;
//            newNavCriteria = NavigationCriteriaHelper.service.getNavigationCriteriaFromJSON(jsonNavCriteria.toString());
//            oldNavCriteria = NavigationCriteriaHelper.service.getNavigationCriteria(newNavCriteria.getName());
//            oldNavCriteria = NavigationCriteriaHelper.service.copyNavCriteria(newNavCriteria, oldNavCriteria);
//            savedNcOid = oldNavCriteria.toString();
//            NavigationCriteriaHelper.service.saveNavigationCriteria(oldNavCriteria);
//            if (ncid != null && !"".equals(ncid)) {
//                updateNavigationCriteria(ncid, json, savedNcOid, clientInfoBean);
//            }
//            /*
//             * Changes for SPR 7339813 - Filter definition is not getting updated if user performs subsequent update
//             * from PSB Config Specs was not getting added to processedFilterInfo, if filter name already exists.
//             */
//            String JSONFromNC = NavigationCriteriaHelper.service.getJSONFromNavigationCriteria(oldNavCriteria);
//            addToProcessedFilterInfo(savedNcOid, JSONFromNC);
//        } catch (WTException e) {
//            log.error("Can't copyNavigationCriteria Navigation Criteria " + name, e);
//        } catch (JSONException e1) {
//            log.error("Can't copyNavigationCriteria Navigation Criteria " + name, e1);
//        }
//        if (log.isDebugEnabled()) {
//            log.debug(
//                    "ClientNavigationCriteriaServiceImpl.copyNavigationCriteria() Saved Filter Updated Succesfully Name: "
//                            + name + " savedNcOid: " + savedNcOid + " ncid:" + ncid);
//        }
//        return savedNcOid;
//    }
//
//    /**
//     * This method will toggle the show value of expansion criteria.
//     *
//     * @param oids
//     *            the oids
//     */
//    @Override
//    public void toggleShowHideNavCriteria(HashSet<String> oids) {
//        try {
//            NavigationCriteriaHelper.service.toggleShowHideNavCriteria(getObjectIdentifierList(oids));
//        } catch (WTException e) {
//            log.error("Problem in toggleing in show/hide Navigation Criterias : " + oids, e);
//        }
//
//    }
//
//    private Collection<ObjectIdentifier> getObjectIdentifierList(Collection<String> oids) throws WTException {
//        List<ObjectIdentifier> list = new ArrayList<ObjectIdentifier>();
//        ReferenceFactory factory = new ReferenceFactory();
//        if (oids != null && oids.size() > 0) {
//            WTCollection wtCollection = new WTArrayList();
//            for (String str : oids) {
//                wtCollection.add(factory.getReference(str));
//            }
//            wtCollection.inflate();
//
//            for (Iterator iterator = wtCollection.persistableIterator(); iterator.hasNext();) {
//                Object object = iterator.next();
//                if (object instanceof Persistable) {
//                    list.add(((Persistable) object).getPersistInfo().getObjectIdentifier());
//                }
//            }
//        }
//        return list;
//    }
//
//    /**
//     * This method will toggle the share attribute of expansion criteria to all users.
//     *
//     * @param oids
//     *            the oids
//     */
//    @Override
//    public void toggleSharedToAll(HashSet<String> oids) {
//        try {
//            NavigationCriteriaHelper.service.toggleSharedToAll(getObjectIdentifierList(oids));
//        } catch (WTException e) {
//            log.error("Problem in toggleing in share Navigation Criterias to all : " + oids, e);
//        }
//
//    }
//
//    /**
//     * This method will set the expansion criteria as the user defined default filter.
//     *
//     * @param appName
//     *            the app name
//     * @param ncOid
//     *            Navigation Criteria Oid
//     */
//    @Override
//    public void setUserDefaultNavCriteria(String appName, String ncOid) {
//        try {
//            NavCriteriaContext navContext = new NavCriteriaContext();
//            navContext.setApplicationName(appName);
//            navContext.getMap().put(NavCriteriaContext.USER_DEFAULT_FILTER_OID, ncOid);
//            NavigationCriteriaHelper.service.setUserDefaultNavCriteria(navContext);
//        } catch (WTException e) {
//            log.error("Problem in setting Navigation Criteria as default filter : " + ncOid, e);
//        }
//
//    }
//
//    /**
//     * This method will remove the user defined default filter.
//     *
//     * @param appName
//     *            the app name
//     * @param ncOid
//     *            Navigation Criteria Oid
//     */
//    @Override
//    public void unsetUserDefaultNavCriteria(String appName, String ncOid) {
//        try {
//            NavCriteriaContext navContext = new NavCriteriaContext();
//            navContext.setApplicationName(appName);
//            navContext.getMap().put(NavCriteriaContext.USER_DEFAULT_FILTER_OID, ncOid);
//            NavigationCriteriaHelper.service.unsetUserDefaultNavCriteria(navContext);
//        } catch (WTException e) {
//            log.error("Problem in unsetting Navigation Criteria as default filter : " + ncOid, e);
//        }
//    }
//
//    @SuppressWarnings("static-access")
//    @Override
//    public SavedExpansionCriteriaBean saveAsNavigationCriteria(String ncOid, String copyName) {
//
//        if (log.isDebugEnabled()) {
//            log.debug(
//                    "ClientNavigationCriteriaServiceImpl -> saveAsNavigationCriteria => Start savingAs expansionCriteria.");
//        }
//        SavedExpansionCriteriaBean savedECBean = new SavedExpansionCriteriaBean();
//        Locale locale = null;
//        ReferenceFactory refFactory = new ReferenceFactory();
//        WTReference ref;
//        try {
//            ref = refFactory.getReference(ncOid);
//            NavigationCriteria navCriteria = (NavigationCriteria) ref.getObject();
//            navCriteria = NavigationCriteriaHelper.service.getPopulatedNavigationCriteria(navCriteria);
//            navCriteria.setName(copyName);
//            navCriteria = NavigationCriteriaHelper.service.getDeepCopy(navCriteria);
//            WTPrincipal currentPrincipal = SessionHelper.getPrincipal();
//            if (currentPrincipal instanceof WTUser) {
//                navCriteria.setOwner((WTUser) currentPrincipal);
//            }
//            navCriteria = NavigationCriteriaHelper.service.saveNavigationCriteria(navCriteria);
//
//            locale = SessionHelper.getLocale();
//
//            savedECBean.setOid(navCriteria.toString());
//            savedECBean.setName(navCriteria.getName());
//            savedECBean.setShowEC(true);
//            savedECBean.setShareEC(navCriteria.isSharedToAll());
//            savedECBean.setCreator(((WTUser) navCriteria.getOwner()).getName());
//            savedECBean.setType(navCriteria.getApplicableType());
//
//            WTStandardDateFormat sdf = new WTStandardDateFormat();
//            String format = null;
//            ResourceBundle bundle = ResourceBundle.getBundle(componentRB, locale);
//            format = bundle.getString("STANDARD_DATE_TIME_ZONE_DISPLAY_FORMAT");
//            savedECBean.setLastModified(sdf.format(navCriteria.getPersistInfo().getModifyStamp(), format, locale,
//                    TimeZoneHelper.getLocalTimeZone()));
//            savedECBean.setOwner(navCriteria.getOwner().getPersistInfo().getObjectIdentifier().toString());
//
//        } catch (WTException e) {
//            if (e instanceof wt.pom.UniquenessException) {
//                savedECBean.setException(ECExceptionConstants.UNIQUE_NESS_EXCEPTION);
//            }
//            log.error("ClientNavigationCriteriaServiceImpl -> Problem in savingAs Navigation Criteria.", e);
//        } catch (WTPropertyVetoException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        return savedECBean;
//
//    }
//
//    @Override
//    public String getCachedNavigationCriteria(String id, ClientNavigationCriteriaBean clientInfoBean) {
//        try {
//            return getNcOidIfPersistedNcid(id, getNavCriteriaCacheId(clientInfoBean));
//        } catch (WTException e) {
//            log.error("Can't get ncOid for ncid " + id, e);
//        }
//        return "";
//    }
//
//    /**
//     * Gets navigation criteria Oid if NCID is persisted.
//     *
//     * @param id
//     *            the ncid
//     * @param clientInfoBean
//     *            the client info bean
//     *
//     * @return ncOid
//     */
//    private String getNcOidIfPersistedNcid(String id, NavCriteriaCacheId navCache) {
//        if (log.isDebugEnabled()) {
//            log.debug("getNcOidIfPersistedNcid(): Begin getting the ncOid from the NCID: " + id
//                    + " ApplicationInfo: " + navCache);
//        }
//        String ncOid = "";
//        NavigationCriteria nc = null;
//        try {
//            nc = NavigationCriteriaHelper.service.getCachedNavigationCriteria(id, navCache);
//            if (log.isDebugEnabled()) {
//                log.debug("nc from NavigationCriteriaHelper.service.getCachedNavigationCriteria(): " + nc);
//            }
//
//            if (nc == null) {
//                log.debug("Returning an error, as nc is null.");
//                return ECExceptionConstants.NCID_INVALID_EXCEPTION;
//            }
//            else if (PersistenceHelper.isPersistent(nc)) {
//                ncOid = nc.toString();
//            }
//        } catch (WTException e) {
//            log.error("Can't get ncOid for ncid " + id, e);
//        }
//        if (log.isTraceEnabled()) {
//            log.trace("getNcOidIfPersistedNcid(): Returning [" + ncOid + "]");
//        }
//        return ncOid;
//    }
//
//    // Based on com.ptc.windchill.cadx.common.util.WorkspaceUtilities.getWorkspace
//    @SuppressWarnings("deprecation")
//    private EPMWorkspace getWorkspace(String workspaceName) throws WTException {
//        EPMWorkspace workspace = null;
//
//        // Validate that Workspace name is non null and non empty
//        if ((workspaceName == null) || (workspaceName.length() == 0)) {
//            return (workspace);
//        }
//
//        // Should the WS be cached.
//
//        WTPrincipal principal = SessionHelper.manager.getPrincipal();
//
//        QuerySpec querySpec = new QuerySpec(EPMWorkspace.class);
//        SearchCondition sc = new SearchCondition(EPMWorkspace.class,
//                EPMWorkspace.NAME, SearchCondition.EQUAL,
//                workspaceName);
//        querySpec.appendWhere(sc, 0, -1);
//
//        // Append reference to owner condition - SPR 1090105
//        querySpec.appendAnd();
//        SearchCondition principalSearchCondition = new SearchCondition(EPMWorkspace.class,
//                EPMWorkspace.PRINCIPAL_REFERENCE + "." +
//                        WTAttributeNameIfc.REF_OBJECT_ID,
//                SearchCondition.EQUAL,
//                PersistenceHelper.getObjectIdentifier(principal).getId());
//        querySpec.appendWhere(principalSearchCondition, 0, -1);
//
//        QueryResult qr = PersistenceHelper.manager.find(querySpec);
//        // Find the Workspace - If one is returned, it is
//        // the named workspace owned by the user.
//        while (qr != null && qr.hasMoreElements()) {
//            workspace = (EPMWorkspace) qr.nextElement();
//
//            break;
//        }
//
//        return workspace;
//    }
//
//    @Override
//    public String getJSONForDefaultNavigationCriteria(List<String> seedObjects,
//            ClientNavigationCriteriaBean clientInfoBean,
//            Map<String, String> context) {
//        return getJSONForDefaultNavigationCriteria(seedObjects, clientInfoBean, context, null);
//    }
//
//    @Override
//    public String getJSONForDefaultNavigationCriteria(List<String> seedObjects,
//            ClientNavigationCriteriaBean clientInfoBean,
//            Map<String, String> context, Map<String, Object> appMap) {
//        if (log.isDebugEnabled()) {
//            log.debug("ClientNavigationCriteriaServiceImpl -> getJSONFromDefaultNavigationCriteria: SeedObjects:"
//                    + seedObjects
//                    + " ApplicationInfo: " + clientInfoBean);
//        }
//        String json = "";
//        try {
//            json = NavigationCriteriaHelper.service.getJSONFromNavigationCriteria(getDefaultNavigationCriteria(
//                    seedObjects, context, appMap));
//        } catch (WTException e) {
//            log.error(
//                    "ClientNavigationCriteriaServiceImpl -> getJSONFromDefaultNavigationCriteria => Can't get the JSON for Default NC "
//                            + seedObjects,
//                    e);
//        }
//        return json;
//    }
//
//    @Override
//    public String renameNavigationCriteria(String ncOid, String newName) {
//        NavigationCriteria navCriteriaObj = null;
//        String oldName = null;
//        try {
//            ReferenceFactory refFactory = new ReferenceFactory();
//            WTReference ref = refFactory.getReference(ncOid);
//            navCriteriaObj = (NavigationCriteria) ref.getObject();
//            oldName = navCriteriaObj.getName();
//            navCriteriaObj.setName(newName);
//            NavigationCriteriaHelper.service.saveNavigationCriteria(navCriteriaObj);
//        } catch (WTException e) {
//            if (e instanceof wt.pom.UniquenessException) {
//                return ECExceptionConstants.UNIQUE_NESS_EXCEPTION;
//            }
//            log.error("Problem in renaming Navigation Criteria, ncOid : " + ncOid, e);
//        } catch (WTPropertyVetoException e) {
//            log.error("Problem in renaming Navigation Criteria, ncOid=" + ncOid, e);
//        }
//        if (log.isDebugEnabled()) {
//            log.debug("ClientNavigationCriteriaServiceImpl.renameNavigationCriteria()Renamed Succesfully to: "
//                    + newName + "oldName: " + oldName + "ncOid: " + ncOid);
//        }
//        return navCriteriaObj.toString();
//    }
//
//    /**
//     * Method apply the navigation criteria and put it to cookie cache. This method does not save the navigation
//     * criteria. This method expect the list of seed objects and cookie type i.e. application name.<BR>
//     * If passed NCID is null this method will get the default NCID based on the seed objects.<BR>
//     * If passed JSON is null this method will get the JSON based on the NCID.<BR>
//     * If apply to top is set in the JSON then this method applies the NC on the seed object and passes the result in
//     * the return ECBean.
//     *
//     * @param ExpansionCriteriaBean
//     *            the bean
//     * @return ExpansionCriteriaBean
//     *
//     */
//    @SuppressWarnings("deprecation")
//    @Override
//    public ExpansionCriteriaBean applyNavigationCriteria(ExpansionCriteriaBean bean) {
//        try {
//            if (log.isDebugEnabled()) {
//                log.debug("Enter => applyNavigationCriteria(ExpansionCriteriaBean).");
//                log.debug("bean: " + bean);
//            }
//
//            if (bean.getApplicationName() == null) {
//                // Use the default cookie when application name is null
//                bean.setApplicationName(ClientNavigationCriteriaService.DEFAULT_APP);
//            }
//
//            List<String> originalOids = bean.getSeedOids();
//            ClientNavigationCriteriaBean clientBean = new ClientNavigationCriteriaBean(bean.getApplicationName(),
//                    bean.getRemoteAddress(),
//                    bean.getSessionId());
//
//            Map<String, String> context = new HashMap<String, String>();
//            if (bean.getParams() != null) {
//                // This is to check if apply to top needs to be set to true
//                context.putAll(bean.getParams());
//            }
//            if (bean.getECId() == null) {
//                // if NCID is null then get the default NCID
//                if (log.isDebugEnabled()) {
//                    log.debug(
//                            "ClientNavigationCriteriaServiceImpl.applyNavigationCriteria() NCID is null, so getting the default NCID");
//                }
//                bean.setECId(getDefaultNavigationCriteriaID(originalOids, clientBean, context));
//            }
//            if (bean.getNavCriteriaJSONString() == null) {
//                // if JSON is null then set the JSON based on NCID
//                if (log.isDebugEnabled()) {
//                    log.debug(
//                            "ClientNavigationCriteriaServiceImpl.applyNavigationCriteria() JSON is null, so getting the JSON from NCID");
//                }
//                bean.setNavCriteriaJSONString(getJSONforNavigationCriteriaID(bean.getECId(), clientBean));
//            }
//            
//            if(Boolean.parseBoolean(bean.getParams().get(ExpansionCriteriaConstants.VARIANTSPEC_DIALOG))) {
//                final NavigationCriteria updatedNavCriteria = updateNavigationCriteria(bean);
//                String updatedJson = getJSONForNavigationCriteria(updatedNavCriteria);
//                bean.setNavCriteriaJSONString(updatedJson);
//            }
//            
//            String jsonstr = bean.getNavCriteriaJSONString();
//            JSONObject jsonObject = new org.json.JSONObject(jsonstr);
//            boolean isApplyToTop = jsonObject.getBoolean(ExpansionCriteriaJSONConstants.APPLY_TO_TOP_LEVEL_OBJECT);
//            bean.setApplyToTopNode(isApplyToTop);
//
//            // if you want to use the same ecid
//            String useExistingEcId = bean.getParams().get("useExistingECId");
//            if (log.isDebugEnabled()) {
//                log.debug("ClientNavigationCriteriaServiceImpl.applyNavigationCriteria() USE EXISTING EC ID:"
//                        + useExistingEcId);
//            }
//
//            // Apply the configuration specification on the root node and check if the root node changes upon applying
//            // it;
//            // do this check only when:
//            // 1. isApplyToTop is true
//            // 2. Only one root seed is passed
//            // 3. When useExistingECId parameter is not set
//            // 4. When the applicable type passed in is null, OR the applicable type passed in is not null and the
//            // the type of seed OIDs is same as the applicable type OR the type of seed OIDs is same as the master type,
//            // if the master type is not null.
//
//            boolean has_original_oids = ((originalOids != null) && (originalOids.size() > 0));
//            boolean has_no_existing_ecid = ((useExistingEcId == null) || useExistingEcId.isEmpty()
//                    || !Boolean.valueOf(useExistingEcId));
//
//            String bean_type = bean.getType();
//            boolean has_valid_type = (bean_type == null);
//            if (!has_valid_type) {
//                List<String> been_seed_oid_type_list = bean.getSeedOidsType();
//                if ((been_seed_oid_type_list != null) && been_seed_oid_type_list.size() > 0) {
//                    String bean_seed_oid_type = been_seed_oid_type_list.get(0);
//                    has_valid_type = areTypesComparable(bean_type, bean_seed_oid_type);
//                    if (!has_valid_type) {
//                        String bean_master_type = bean.getMasterType();
//                        if (bean_master_type != null) {
//                            has_valid_type = areTypesComparable(bean_master_type, bean_seed_oid_type);
//                        }
//                    }
//                }
//            }
//
//            if (isApplyToTop && has_original_oids && has_no_existing_ecid && has_valid_type) {
//                // find if we got a new Context
//                String contextId = bean.getSeedOids().get(0);
//                String newContextId = applyNavigationCriteria(contextId, jsonstr);
//
//                if (newContextId == null) {
//                    log.debug("Unable to resolve the top-level object to a valid version.");
//
//                    if (Boolean.parseBoolean(bean.getParams().get(ECParamsConstants.ALWAYS_RESOLVE_ROOT_NODE))) {
//                        /*
//                         * This method was invoked from the Edit Filter client; simply return the bean after setting the
//                         * JSON string on it to a value that indicates an error
//                         */
//                        log.debug("Setting the JSON string on the input bean to a value that indicates an error.");
//                        bean.setNavCriteriaJSONString(ECExceptionConstants.INVALID_OBJECT_EXCEPTION);
//                        return bean;
//                    }
//                    else if (Boolean.parseBoolean(bean.getParams().get(ExpansionCriteriaConstants.SAVED_FILTER))) {
//                        /*
//                         * The root node was not resolvable to a valid version using the saved filter selected from the
//                         * Saved Filters drop-down menu; set a couple of attributes on the input bean to indicate this
//                         * result and return.
//                         */
//                        // Use this appName from the context instead of the default app_name to construct the cacheId
//                        String appName = bean.getParams().get(ExpansionCriteriaConstants.EDIT_EC_ACTION_METHOD);
//                        bean.setRootNodeUnresolvable();
//                        // This behavior of clearing the apply to top option on the configspec in the filter, applies to
//                        // only PSB, VariantSpec Info page and Matrix Editor
//                        if (ExpansionCriteriaConstants.App_List.contains(appName)) {
//                            bean.setRootNodeUnresolvable();
//                        }
//                        else {
//                            NavCriteriaCacheId cacheID = getNavCriteriaCacheId(clientBean);
//                            cacheID.setCookieType(appName);
//
//                            String json = getJSONFromCurrentFilterInfo(cacheID.toString());
//                            // if the json is empty, try to create the cache using the default app_name
//                            if (json == null || json.isEmpty()) {
//                                NavCriteriaCacheId defaultcacheID = getNavCriteriaCacheId(clientBean);
//                                json = getJSONFromCurrentFilterInfo(defaultcacheID.toString());
//                            }
//                            bean.setOldNavCriteriaJSONString(json);
//                            bean.setRootNodeUnresolvable();
//                            return bean;
//                        }
//                    }
//                }
//                else if (!removeORFromObject(newContextId).equals(removeORFromObject(contextId))) {
//
//                    // Save the old context, as it is used to update the EC cache.
//                    bean.setOldSeedOids(bean.getSeedOids());
//
//                    // this is a new context
//                    bean.setSeedOid(newContextId);
//                    List<String> seedObjects = new ArrayList<String>();
//                    seedObjects.add(newContextId);
//                    bean.setSeedOids(seedObjects);
//
//                    /*
//                     * To fix 6907311 when seed has changes reset seed object type with type hierarchy name so that
//                     * client used correct Identifier with type hierarchy.
//                     */
//                    Object newSeedObject = REFERENCE_FACTORY.getReference(newContextId).getObject();
//                    TypeIdentifier ti = TypedUtility.getTypeIdentifier(newSeedObject);
//                    List<String> seedOidsType = new ArrayList<String>();
//                    seedOidsType.add(ti.getTypename());
//                    bean.setSeedOidsType(seedOidsType);
//
//                    // get a new EC ID for the new context
//                    // this is to set the apply to top value for the case where root level object changes.
//                    // map.put(ExpansionCriteriaConstants.SET_APPLY_TO_TOP_FOR_DEFAULT_NC,
//                    // Boolean.toString(isApplyToTop));
//                    String newEcId = getDefaultNavigationCriteriaID(seedObjects, clientBean,
//                            context);
//
//                    bean.setOldEcId(bean.getECId());
//                    bean.setECId(newEcId);
//                }
//            }
//
//            // Update the current in-session nav criteria with the saved filter nav criteria
//            final NavigationCriteria updatedNavCriteria = updateNavigationCriteria(bean);
//
//            if (bean.isRootNodeUnresolvable()) {
//                // If the root node is unresolvable, clear the apply to top node, if it is set
//                boolean applyToTop = updatedNavCriteria.isApplyToTopLevelObject();
//                if (applyToTop) {
//                    // Set the apply To Top to false
//                    updatedNavCriteria.setApplyToTopLevelObject(false);
//                    // Clear the name indicating it is a default filter
//                    updatedNavCriteria.setName("");
//                }
//
//                // Clear the NCOid indicating it is an unpersisted NC
//                bean.setNcOid(null);
//                bean.setOldNavCriteriaJSONString(jsonstr);
//            }
//
//            if (updatedNavCriteria != null) {
//                jsonstr = encodeJSONContent(
//                        NavigationCriteriaHelper.service.getJSONFromNavigationCriteria(updatedNavCriteria),
//                        bean.getSeedOids());
//                bean.setNavCriteriaJSONString(jsonstr);
//            }
//
//            final String ecId = bean.getECId();
//            String oldJSON = updateNavigationCriteria(ecId, jsonstr, bean.getNcOid(), clientBean);
//
//            // Update the bean with the filter information that was previously in use, if it is not already set
//            if (bean.isRootNodeUnresolvable()) {
//                bean.setOldNavCriteriaJSONString(jsonstr);
//            }
//            else {
//                bean.setOldNavCriteriaJSONString(oldJSON);
//            }
//
//            /*
//             * Set isFilterUpdated on the input bean to true, regardless of whether the filter was modified or not,
//             * since it really does not make much sense to implement a compare logic between two NC objects (based on
//             * inputs from John and Chris).
//             */
//            bean.setFilterUpdated(true);
//
//            // Update the name provided this method was invoked from the Edit Filter client.
//            if (!Boolean.parseBoolean(bean.getParams().get(ExpansionCriteriaConstants.SAVED_FILTER)) ) {
//                NavigationCriteria updatedNC = NavigationCriteriaHelper.service.getCachedNavigationCriteria(ecId,
//                        getNavCriteriaCacheId(clientBean));
//                String name = updatedNC.getName();
//                if (name != null && !name.endsWith(ExpansionCriteriaConstants.FILTER_UPDATED)) {
//                    // Update the name to indicate that the filter was updated; this is used by the Saved Filters UI.
//                    updatedNC.setName(name + ExpansionCriteriaConstants.FILTER_UPDATED);
//                }
//                // Update the JSON from the NC object
//                bean.setNavCriteriaJSONString(encodeJSONContent(NavigationCriteriaHelper.service
//                        .getJSONFromNavigationCriteria(updatedNC), bean.getSeedOids()));
//            }
//
//            // Update the EC Cache
//            updateECCache(bean);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            log.error(ex.toString());
//        }
//
//        return bean;
//    }
//
//    /**
//     * Calls <code>UpdateNavCriteriaDelegate</code> registered for application name specified in the given bean to
//     * update the <code>NavigationCriteria</code> identified by ncOid in the bean.
//     *
//     * @param bean
//     * @return Updated navigation criteria
//     * @throws WTException
//     */
//    private NavigationCriteria updateNavigationCriteria(final ExpansionCriteriaBean bean) throws WTException {
//        NavigationCriteria updatedNavCriteria = null;
//        final String ncOid = bean.getNcOid();
//        if (ncOid != null && !ncOid.isEmpty()) {
//            final WTReference savedFilterNCRef = REFERENCE_FACTORY.getReference(ncOid);
//            NavigationCriteria savedFilterNC = (NavigationCriteria) savedFilterNCRef.getObject();
//            savedFilterNC = NavigationCriteriaHelper.service.getPopulatedNavigationCriteria(savedFilterNC);
//            if (savedFilterNC != null && PersistenceHelper.isPersistent(savedFilterNC)) {
//                final String appName = bean.getParams().get(ExpansionCriteriaConstants.EDIT_EC_ACTION_METHOD);
//                if (appName != null && !appName.isEmpty()) {
//                    UpdateNavCriteriaDelegate delegate = updateNavCriteriaDelegateFactory.getDelegate(appName);
//                    if (delegate != null) {
//                        final WTCollection seedObjs = getInflatedCollection(bean.getSeedOids());
//                        final WTCollection partAltReps = seedObjs.subCollection(WTPartAlternateRep.class);
//                        final NavCriteriaContext navContext = new NavCriteriaContext();
//                        navContext.setSeeds(seedObjs);
//                        navContext.setApplicationName(appName);
//                        final String variantSpecOid = bean.getParams().get(ExpansionCriteriaConstants.VARIANTSPEC_OID);
//                        if (variantSpecOid != null && !variantSpecOid.isEmpty()) {
//                            final WTReference variantSpecRef = REFERENCE_FACTORY.getReference(variantSpecOid);
//                            if (VariantSpec.class.isAssignableFrom(variantSpecRef.getReferencedClass())) {
//                                navContext.setVariantSpecRef((ObjectReference) variantSpecRef);
//                                navContext.setApplicableType(VariantSpec.class);
//                            }
//                        }
//                        else if (partAltReps != null && partAltReps.size() == 1) {
//                            final WTPartAlternateRep partAltRep = (WTPartAlternateRep) partAltReps.persistableIterator()
//                                    .next();
//                            final ObjectReference varintSpecRef = partAltRep.getVariantSpecReference();
//                            if (varintSpecRef != null && varintSpecRef.getReferencedClass() != null) {
//                                navContext.setVariantSpecRef(varintSpecRef);
//                            }
//                        }
//                        navContext.setMap(bean.getAppMap());
//                        updatedNavCriteria = delegate.updateNavigationCriteria(savedFilterNC, navContext);
//                    }
//                }
//            }
//        }
//
//        return updatedNavCriteria;
//    }
//
//    @Override
//    public ExpansionCriteriaBean rePopulatePartConfiguration(ExpansionCriteriaBean bean) {
//        try {
//
//            if (log.isDebugEnabled()) {
//                log.debug("ClientNavigationCriteriaServiceImpl.rePopulatePartConfiguration() Starting ecBean= " + bean);
//            }
//
//            Map<String, String> params = bean.getParams();
//            List<String> originalOids = bean.getSeedOids();
//            WTReference wtReference = REFERENCE_FACTORY.getReference(originalOids.get(0));
//            WTProductConfiguration partConfiguration = (WTProductConfiguration) wtReference.getObject();
//            wtReference = REFERENCE_FACTORY.getReference(bean.getParams().get(
//                    ExpansionCriteriaConstants.WTProductConfiguration_BasePart_ID));
//            WTPartMaster wtPartMaster = (WTPartMaster) wtReference.getObject();
//
//            applyNavigationCriteria(bean);
//            ClientNavigationCriteriaBean clientNavBean = new ClientNavigationCriteriaBean(bean.getApplicationName(),
//                    bean.getRemoteAddress(), bean.getSessionId());
//
//            NavigationCriteria nc = getNavigationCriteriaFromCookie(bean.getECId(), clientNavBean);
//
//            Persistable part = ConfigurationHelper.service.getConfiguredVersionOrMaster(partConfiguration);
//
//            /*
//             * since the meaning of option "Apply only to unresolved parts" has reversed, therefore take 1's complement
//             * of this option before passing it to the service
//             */
//            boolean applyOnlyToUnresolvedPart = !Boolean.parseBoolean(params
//                    .get(ExpansionCriteriaConstants.ECParamsConstants.APPLY_ONLY_TO_UNRESOLVED_PARTS));
//
//            String jsonstr = bean.getNavCriteriaJSONString();
//            JSONObject jsonObject = new org.json.JSONObject(jsonstr);
//            boolean isApplyToTop = jsonObject.getBoolean(ExpansionCriteriaJSONConstants.APPLY_TO_TOP_LEVEL_OBJECT);
//
//            if (part instanceof Mastered) {
//                partConfiguration = WTPartHelper.service.littlePop(partConfiguration, wtPartMaster,
//                        nc.getConfigSpecs(), applyOnlyToUnresolvedPart, nc.isUseDefaultForUnresolved());
//            }
//            else {
//                if (applyOnlyToUnresolvedPart && isApplyToTop) {
//                    partConfiguration = WTPartHelper.service.littlePop(partConfiguration, ((WTPart) part).getMaster(),
//                            nc.getConfigSpecs(), applyOnlyToUnresolvedPart, nc.isUseDefaultForUnresolved());
//                }
//                else {
//                    partConfiguration = WTPartHelper.service.littlePop(partConfiguration, part,
//                            nc.getConfigSpecs(), applyOnlyToUnresolvedPart, nc.isUseDefaultForUnresolved());
//                }
//            }
//
//            String repopulatedOid = PersistenceHelper.getObjectIdentifier(partConfiguration).toString();
//            bean.setOldEcId(bean.getECId());
//            bean.setECId(getDefaultNavigationCriteriaID(originalOids, clientNavBean, params));
//            List<String> seedObjects = new ArrayList<String>();
//            seedObjects.add(repopulatedOid);
//            bean.setSeedOids(seedObjects);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            log.error(e.toString());
//        } catch (WTException e) {
//            e.printStackTrace();
//            log.error(e.toString());
//        }
//        return bean;
//    }
//
//    private String removeORFromObject(String newContextId) {
//        // remove the OR(if there) from the object name and then compare
//        if (newContextId != null) {
//            int lastIndex = newContextId.lastIndexOf(":");
//            int index = newContextId.indexOf(":");
//            if (lastIndex != index) {
//                newContextId = newContextId.substring(index + 1);
//            }
//        }
//        return newContextId;
//    }
//
//    private NavCriteriaCacheId getNavCriteriaCacheId(ExpansionCriteriaBean bean)
//            throws WTException {
//        if (bean == null) {
//            throw new WTException("ExpansionCriteriaBean is Null");
//        }
//        String remoteAddr = bean.getRemoteAddress() != null ? bean.getRemoteAddress() : getRemoteAddr();
//        String sessionId = bean.getSessionId() != null ? bean.getSessionId() : getSessionId();
//        return new NavCriteriaCacheId(remoteAddr, sessionId, bean.getApplicationName());
//    }
//
//    /**
//     * Method to get the navigation criteria JSON.<BR>
//     * Ecid = if empty string is passed it will create a new ecid and create nav criteria and put in cache <BR>
//     * Ecid = some id it will create a new nacigationcriteria and update the cache with the given ecid <BR>
//     * Ecid= object id of the persisted navigation criteria, it will refresh the object and use this for further
//     * processing and will also update the cache with the object id as key.
//     *
//     * @param bean
//     *            the bean
//     * @param context
//     *            the context
//     *
//     * @return the verification result bean
//     */
//    @Override
//    public String getNavigationCriteriaJSON(ExpansionCriteriaBean bean, Map<String, String> context) {
//        return verifyAndGetJSON(bean, context).getJson();
//    }
//
//    /**
//     * Verify and get json.<BR>
//     * Ecid = if empty string is passed it will create a new ecid and create nav criteria and put in cache <BR>
//     * Ecid = some id it will create a new nacigationcriteria and update the cache with the given ecid <BR>
//     * Ecid= object id of the persisted navigation criteria, it will refresh the object and use this for further
//     * processing and will also update the cache with the object id as key.
//     *
//     * @param bean
//     *            the bean
//     * @param context
//     *            the context
//     *
//     * @return the verification result bean
//     */
//    public VerificationResultBean verifyAndGetJSON(ExpansionCriteriaBean bean, Map<String, String> context) {
//        // Check if actionName is in context map, if yes create a local variable
//        String actionName = "";
//        if (context.containsKey("actionName")) {
//            actionName = context.get("actionName");
//        }
//        NavigationCriteria nc = null;
//        VerificationResultBean verificationResultBean = new VerificationResultBean(true, null, null);
//        try {
//            NavCriteriaCacheId cacheId = getNavCriteriaCacheId(bean);
//            String ecid = (bean.getECId() == null) ? "" : bean.getECId();
//            if (log.isDebugEnabled()) {
//                log.debug("ClientNavigationCriteriaServiceImpl.getNavigationCriteriaJSON() Starting NCID:=" + ecid
//                        + " cacheId: " + cacheId);
//            }
//
//            // 1. If ecid is SavedEC then get it from database
//            if (removeORFromObject(ecid).startsWith(NavigationCriteria.class.getName())) {
//                ReferenceFactory refFactory = new ReferenceFactory();
//                WTReference ref = refFactory.getReference(ecid);
//                NavigationCriteria navCriteria = (NavigationCriteria) ref.getObject();
//                nc = NavigationCriteriaHelper.service.getPopulatedNavigationCriteria(navCriteria);
//                bean.setNcOid(ecid);
//            }
//
//            if (nc == null) {
//                // 2. look in the cache.
//                nc = NavigationCriteriaHelper.service.getCachedNavigationCriteria(ecid, cacheId);
//            }
//
//            if (nc == null) {
//                // 3. if nc is still null then check if we need to get the default nc
//                if (ecid.isEmpty()) {
//                    // This will generate the NCID in the development mode, where ncid is passed as blank/null
//                    nc = getDefaultNavigationCriteria(bean.getSeedOids(), context, null);
//                }
//                else {
//                    log.warn("NCID is neither in Cache not Persisted. Still generating the" +
//                            "NC object as default one , NavCriteriaCacheId:=" + cacheId + " NCID:" + ecid);
//                    // This will generate the NC for all the NCID's (actually invalid NCID's)
//                    // This is used by the ATO. Here there own generated ID is passed as NCID
//                    // TODO find some way to block this when session gets expired.
//                    nc = getDefaultNavigationCriteria(bean.getSeedOids(), context, null);
//                }
//            }
//
//            // TODO This will never get executed as we are genearing NC for all the NCID's
//            // if still NC is null then throw the exception
//            if (nc == null) {
//                log.warn("Invalid NCID is passed to the system, NavCriteriaCacheId:=" + cacheId + " NCID:" + ecid);
//                verificationResultBean.setJson(ECExceptionConstants.NCID_INVALID_EXCEPTION);
//                return verificationResultBean;
//            }
//
//            // now we should have a navcriteria, cache it/update the cache
//            // using the given ncid or generate one
//            if (ecid.isEmpty()) {
//                ecid = NavigationCriteriaHelper.service.cacheNavigationCriteria(nc, cacheId);
//                bean.setECId(ecid);
//            }
//            else {
//                // If actionName is Configure remove RC and LC configSpecs from NavigationCriteria
//                if (actionName.equals("configure")) {
//                    UpdateNavCriteriaDelegate delegate = updateNavCriteriaDelegateFactory
//                            .getDelegate(bean.getApplicationName());
//                    nc = delegate.removeConfigSpecsFromNavigationCriteria(nc);
//                }
//                NavigationCriteriaHelper.service.updateCachedNavigationCriteria(ecid, nc, cacheId);
//            }
//
//            // if in session filter is updated no need to set the NcOid
//            if (bean.getNcOid() == null && nc.getName() != null
//                    && !nc.getName().endsWith(ExpansionCriteriaConstants.FILTER_UPDATED)) {
//                // Getting the id of the saved filter. This is used by the Saved Filter list to select a item in list
//                if (PersistenceHelper.isPersistent(nc)) {
//                    // Not setting the id in NcOid as it is causing Edit Filter to save in DB all the time.
//                    bean.getParams().put(ExpansionCriteriaConstants.NC_OID, nc.toString());
//                }
//            }
//
//            // Set the centricity attribute on the NC object
//            if (log.isDebugEnabled()) {
//                log.debug("Setting centricity on the NC object to: " + bean.isCentricity());
//            }
//            nc.setCentricity(bean.isCentricity());
//
//            verificationResultBean = verifyNavigationCriteria(nc, bean.getSeedOids(), bean.getType(), context);
//
//            // Add the currently processed filter information to currentFilterInfo as appropriate
//            setCurrentFilterInfo(cacheId.toString(), nc.toString(), verificationResultBean.getJson());
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            log.error(ex.toString());
//        }
//        if (log.isDebugEnabled()) {
//            log.debug("ClientNavigationCriteriaServiceImpl.getNavigationCriteriaJSON() Successful NCID:="
//                    + bean.getECId()
//                    + " verificationResultBean: " + verificationResultBean);
//        }
//        return verificationResultBean;
//    }
//
//    private boolean verifiedAndUpdateFilters(NavigationCriteria nc, NavCriteriaContext navContext,
//            List<String> messageList) {
//        boolean isValidFilters = true;
//        try {
//            Map<NavigationFilter2, FilterVerificationResult> filterVerifiedMap = NavigationCriteriaHelper.service
//                    .verifyFilters(nc, navContext);
//            Set<Entry<NavigationFilter2, FilterVerificationResult>> set = filterVerifiedMap.entrySet();
//            for (Entry<NavigationFilter2, FilterVerificationResult> entry : set) {
//                if (entry != null && entry.getKey() != null && entry.getValue() != null) {
//                    FilterVerificationResult filterVerificationResult = entry.getValue();
//                    // if filter is not valid then check if adjusted filter is provided, otherwise remove the filter
//                    if (!filterVerificationResult.isValid()) {
//                        if (log.isDebugEnabled()) {
//                            log.debug("ClientNavigationCriteriaServiceImpl.verifiedAndUpdateFilters() " +
//                                    "Removing the filter as it is invalid. Filter:"
//                                    + entry.getKey().getFilterType());
//                        }
//                        isValidFilters = false;
//                        if (filterVerificationResult.getMessage() != null) {
//                            messageList.add(filterVerificationResult.getMessage().getLocalizedMessage(
//                                    SessionHelper.getLocale()));
//                        }
//                        nc.getFilters().remove(entry.getKey());
//                        NavigationFilter2 adjustedFilter = entry.getValue().getAdjustedFilter();
//                        if (adjustedFilter != null) {
//                            if (log.isDebugEnabled()) {
//                                log.debug("ClientNavigationCriteriaServiceImpl.verifiedAndUpdateFilters() " +
//                                        "Updating the filter as adjusted filter is provided. Filter:"
//                                        + adjustedFilter.getFilterType());
//                            }
//                            nc.getFilters().add(adjustedFilter);
//                        }
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            log.error(ex.toString());
//        }
//        return isValidFilters;
//    }
//
//    private boolean verifiedAndUpdateConfigSpecs(NavigationCriteria nc,
//            NavCriteriaContext navContext, List<String> messageList) {
//        boolean isValidCS = true;
//        try {
//            Map<ConfigSpec, ConfigSpecVerificationResult> configVerifiedMap = NavigationCriteriaHelper.service
//                    .verifyConfigSpecs(nc, navContext);
//            Set<Entry<ConfigSpec, ConfigSpecVerificationResult>> set = configVerifiedMap.entrySet();
//            for (Entry<ConfigSpec, ConfigSpecVerificationResult> entry : set) {
//                if (entry != null && entry.getKey() != null && entry.getValue() != null) {
//                    ConfigSpecVerificationResult configVerificationResult = entry.getValue();
//                    // config spec is not valid then check if adjusted filter is provided, otherwise remove thefilter
//                    if (!configVerificationResult.isValid()) {
//                        isValidCS = false;
//                        if (log.isDebugEnabled()) {
//                            log.debug("ClientNavigationCriteriaServiceImpl.verifiedAndUpdateConfigSpecs() " +
//                                    "Removing the config spec as it is invalid. Config spec:"
//                                    + entry.getKey().getClass());
//                        }
//                        if (configVerificationResult.getMessage() != null) {
//                            messageList.add(configVerificationResult.getMessage().getLocalizedMessage(
//                                    SessionHelper.getLocale()));
//                        }
//                        nc.getConfigSpecs().remove(entry.getKey());
//                        ConfigSpec adjustedConfig = entry.getValue().getAdjustedConfigSpec();
//                        if (adjustedConfig != null) {
//                            if (log.isDebugEnabled()) {
//                                log.debug("ClientNavigationCriteriaServiceImpl.verifiedAndUpdateConfigSpecs() " +
//                                        "Updating the config spec as adjusted is provided. Config spec:"
//                                        + adjustedConfig.getClass());
//                            }
//                            nc.getConfigSpecs().add(adjustedConfig);
//                        }
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            log.error(ex.toString());
//        }
//        return isValidCS;
//    }
//
//    /**
//     * This method will get the Navigation Criteira Object from the Default Cookie, when NCID is provided.<BR>
//     * This method assumes that NCID is present in the Default Cookie.<BR>
//     * Default Cookie is used by StructureComponent applications like PSB, MSR etc.
//     *
//     * @param ncid
//     *            the ncid
//     * @return NavigationCriteria object
//     */
//    public NavigationCriteria getNavigationCriteriaFromDefaultCookie(String ncid) {
//        return getNavigationCriteriaFromCookie(ncid, new ClientNavigationCriteriaBean(
//                ClientNavigationCriteriaService.DEFAULT_APP));
//    }
//
//    /**
//     * This method will get the Navigation Criteira Object from the passed Cookie and provided NCID.<BR>
//     * This method assumes that NCID is present in the Cookie, otherwise null will be returned.<BR>
//     *
//     * @param ncid
//     *            the ncid
//     * @param clientInfoBean
//     *            the client info bean
//     * @return the navigation criteria from ncid
//     */
//    public NavigationCriteria getNavigationCriteriaFromCookie(String ncid,
//            ClientNavigationCriteriaBean clientInfoBean) {
//        if (log.isDebugEnabled()) {
//            log.debug("Enter => getNavigationCriteriaFromCookie().");
//            log.debug("ncid: " + ncid + " clientInfoBean: " + clientInfoBean);
//        }
//        try {
//            NavigationCriteria nc = NavigationCriteriaHelper.service.getCachedNavigationCriteria(ncid,
//                    getNavCriteriaCacheId(clientInfoBean));
//            if (log.isDebugEnabled()) {
//                log.debug("nc from NavigationCriteriaHelper.service.getCachedNavigationCriteria(): " + nc);
//            }
//            return nc;
//        } catch (WTException e) {
//            log.error("Exception while fetching the NC from cache.");
//        }
//        log.debug("Returning a null.");
//        return null;
//    }
//
//    /**
//     * This method will copy the Navigation Criteira of NCID from the Default Cookie to New Cookie provided.<BR>
//     * Default Cookie is used by StructureComponent applications like PSB, MSR etc.
//     *
//     * @param ncid
//     *            the ncid
//     * @param toCookie
//     *            the to cookie
//     */
//    public void copyNavigationCriteriaFromDefaultCookie(String ncid, ClientNavigationCriteriaBean toCookie) {
//        copyNavigationCriteria(ncid, new ClientNavigationCriteriaBean(ClientNavigationCriteriaService.DEFAULT_APP),
//                toCookie);
//    }
//
//    /**
//     * This method will copy the Navigation Criteira of NCID from the Passed Cookie to New Cookie provided.<BR>
//     *
//     * @param ncid
//     *            the ncid
//     * @param fromCookie
//     *            the from cookie
//     * @param toCookie
//     *            the to cookie
//     */
//    public void copyNavigationCriteria(String ncid, ClientNavigationCriteriaBean fromCookie,
//            ClientNavigationCriteriaBean toCookie) {
//        if (log.isDebugEnabled()) {
//            log.debug("ClientNavigationCriteriaServiceImpl.copyNavigationCriteria(): NCID: " + ncid
//                    + " FromCookieInfo: " + fromCookie
//                    + " ToCookieInfo: " + toCookie);
//        }
//        try {
//            NavigationCriteriaHelper.service.updateCachedNavigationCriteria(ncid,
//                    getNavigationCriteriaFromCookie(ncid, fromCookie),
//                    getNavCriteriaCacheId(toCookie));
//
//        } catch (WTException e) {
//            log.error(
//                    "ClientNavigationCriteriaServiceImpl.copyNavigationCriteria() failes on updating Cache NC" + ncid,
//                    e);
//        }
//    }
//
//    /**
//     * Gets the short description.
//     *
//     * @param id
//     *            the id
//     * @param clientInfoBean
//     *            the client info bean
//     *
//     * @return the short description
//     */
//    public String getShortDescription(String id, ClientNavigationCriteriaBean clientInfoBean) {
//        if (log.isDebugEnabled()) {
//            log.debug("getShortDescription(): NCID: " + id
//                    + " ApplicationInfo: " + clientInfoBean);
//        }
//        try {
//            NavigationCriteria nc = NavigationCriteriaHelper.service.getCachedNavigationCriteria(id,
//                    ClientNavigationCriteriaServiceImpl.getNavCriteriaCacheId(clientInfoBean));
//            if (log.isDebugEnabled()) {
//                log.debug("nc from NavigationCriteriaHelper.service.getCachedNavigationCriteria(): " + nc);
//            }
//            if (nc == null) {
//                log.debug("Returning an error, as nc is null.");
//                return ECExceptionConstants.NCID_INVALID_EXCEPTION;
//            }
//            return getShortDescription(nc);
//        } catch (WTException e) {
//            log.error("Problem in getting short description for the ncid=" + id, e);
//        }
//        return "";
//    }
//
//    /**
//     * Gets the short description corresponding to the input filter criteria.
//     *
//     * @param nc
//     *            - NavigationCriteria object representing the input filter criteria.
//     * @deprecated
//     * @return the short description for the input filter criteria.
//     */
//    @Deprecated
//    public String getShortDescription(NavigationCriteria nc) {
//        return getShortDescription(nc, (WTContainerRef) null);
//    }
//
//    /**
//     * Gets the short description corresponding to the input filter criteria and for the input container reference.
//     *
//     * @param nc
//     *            - NavigationCriteria object representing the input filter criteria.
//     * @param containerRef
//     *            - container reference to be passed to a downstream API.
//     * @return the short description for the input filter criteria.
//     */
//    public String getShortDescription(NavigationCriteria nc, WTContainerRef containerRef) {
//        List<FilterBean> filters = ExpansionCriteriaServiceImpl.getFiltersList("DefaultTemplates");
//        StringBuilder shortString = new StringBuilder("");
//        for (FilterBean filter : filters) {
//            try {
//                if (filter.getPopulator() != null) {
//                    if (shortString.length() > 0) {
//                        shortString.append(", ");
//                    }
//                    FilterPopulator filterPopulator = (FilterPopulator) (Class
//                            .forName(filter.getPopulator()).newInstance());
//                    String filterShortDesc = filterPopulator.getShortDescription(nc, containerRef);
//                    if (filterShortDesc != null && !filterShortDesc.isEmpty()) {
//                        shortString.append(filterShortDesc);
//                    }
//                }
//            } catch (Exception e) {
//                log.error("Exception while getting the short desc from EC filters", e);
//            }
//        }
//        return shortString.toString();
//    }
//
//    /**
//     * Gets the short description corresponding to the input filter criteria.
//     *
//     * @param nc
//     *            - NavigationCriteria object representing the input filter criteria.
//     * @param containerOid
//     *            - Object ID of the container whose reference is to be passed to an overloaded API.
//     * @return the short description for the input filter criteria.
//     */
//    public String getShortDescription(NavigationCriteria nc, String containerOid) {
//        log.debug("Enter => getShortDescription(NavigationCriteria, String).");
//        WTContainerRef containerRef = getContainerRef(containerOid);
//        String shortDesc = getShortDescription(nc, containerRef);
//
//        if (log.isDebugEnabled()) {
//            log.debug("Returning: " + shortDesc);
//        }
//        return shortDesc;
//    }
//
//    private String getSeedTypeFromSeedOid(String seedOid) {
//        String seedType = "";
//        if (seedOid != null && !seedOid.isEmpty()) {
//            char ch = ':';
//            int firstIndex = seedOid.indexOf(ch); // to strip VR:/OR:
//            int lastIndex = firstIndex == -1 ? seedOid.length() : seedOid.lastIndexOf(ch); // to strip nos from
//                                                                                           // xx.WTPart:1234
//            if (firstIndex == lastIndex) {
//                firstIndex = -1; // no VR:/OR:
//            }
//            seedType = seedOid.substring(firstIndex + 1, lastIndex);
//        }
//        return seedType;
//    }
//
//    /**
//     * Retrieves the cached navigation criteria and updates it for the application specific context information provided
//     * in the applicationMap. <br>
//     * Uses application name to identify the UpdateNavCriteriaDelegate for updating the navigation delegate
//     */
//    @Override
//    public String getUpdatedCachedNavigationCriteriaID(String root, Map<String, Object> applicationMap,
//            ClientNavigationCriteriaBean clientInfoBean,
//            Map<String, String> context) {
//
//        String ncId = context.get(ExpansionCriteriaConstants.NCID);
//        if (ncId != null && applicationMap != null) {
//            List<String> seedOids = new ArrayList<>(1);
//            seedOids.add(root);
//            NavCriteriaCacheId cacheId = null;
//            final String appName = context.get(ExpansionCriteriaConstants.URL_APP);
//            try {
//
//                cacheId = getNavCriteriaCacheId(clientInfoBean);
//                NavigationCriteria nc = NavigationCriteriaHelper.service.getCachedNavigationCriteria(ncId, cacheId);
//
//                if (nc != null) {
//
//                    if (appName != null && !appName.isEmpty()) {
//                        UpdateNavCriteriaDelegate delegate = updateNavCriteriaDelegateFactory.getDelegate(appName);
//                        if (delegate != null) {
//                            final NavCriteriaContext navContext = new NavCriteriaContext();
//                            navContext.setApplicationName(appName);
//                            navContext.setMap(applicationMap);
//                            WTCollection seedObjects = getInflatedCollection(seedOids);
//
//                            navContext.setSeeds(seedObjects);
//                            nc = delegate.updateNavigationCriteria(nc, navContext);
//                            // a new cached ncId should be generated so that ECInfo Widget will be refreshed
//                            // to show the updated current filter
//                            ncId = NavigationCriteriaHelper.service.cacheNavigationCriteria(nc,
//                                    getNavCriteriaCacheId(clientInfoBean));
//                        }
//
//                    }
//
//                }
//
//            } catch (WTException wte) {
//                log.error("Can't get Updated NC ID from Cache for " + seedOids, wte);
//            }
//        }
//        return ncId;
//
//    }
//
//}
