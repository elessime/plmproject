package com.ikea.ipim.utils.mass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.log4j.Logger;

import com.ikea.ipim.utils.types.TypeUtils;
import com.ptc.core.lwc.server.PersistableAdapter;

import wt.epm.EPMDocument;
import wt.epm.build.EPMBuildRule;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.fc.ReferenceFactory;
import wt.fc.WTReference;
import wt.fc.collections.WTArrayList;
import wt.fc.collections.WTHashSet;
import wt.method.RemoteAccess;
import wt.part.WTPart;
import wt.part.WTPartHelper;
import wt.part.WTPartMaster;
import wt.part.WTPartUsageLink;
import wt.pds.StatementSpec;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.util.WTException;
import wt.util.WTMessage;
import wt.vc.config.ConfigException;
import wt.vc.config.ConfigSpec;
import wt.vc.config.LatestConfigSpec;
import wt.vc.struct.StructHelper;

/**
 * This class is used for obtaining and calculating the masses of a BOM of WTParts. It contains methods used for traversing the BOM,
 * summing up the masses for a node as well as helper methods that support the aftermentioned processes.
 */

public class IPIMMassAttributesHelper implements RemoteAccess {

    public static final String CALCULATED_WEIGHT_IBA = "calculatedWeight";
    public static final String CALCULATED_WEIGHT_UNIT_IBA = "calculatedWeightUnit";
    public static final String ACTUAL_WEIGHT_IBA = "actualWeight";
    public static final String ACTUAL_WEIGHT_UNIT_IBA = "actualWeightUnit";
    public static final String CAD_MASS_IBA = "mass";
    public static final String SALESTYPE_IBA = "SalesPartType";
    public static final String SALESTYPE_ARTICLE = "article";
    public static final String VOLUME = "volume";
    public static final String DENSITY = "density";
    public static final String NO_DENSITY = "noDensity";
    public static final String NO_VOLUME = "noVolume";
    public static final String NO_ACTUAL_MASS = "noActuallMass";
    public static final String NO_ASSOCIATED_PART_FOR_DENSITY = "noPartForDensity";
    public static final String NO_ASSOCIATED_CAD_FOR_VOLUME = "noCADForVolume";
    
    private static final Logger LOG = Logger.getLogger(IPIMMassAttributesHelper.class);
    
    /**
     * Gets the mass of a selected WTPart. First it will get the actual and cad-based weights on the part and create an instance of 
     * the {@link Mass} class representing the calculated weight. If the mass was not on the WTPart, then it will
     * go to the CAD Document linked to the part via the Owner link association and get the volume from there. Next it will go to the 
     * density IBA of an associated Material Part and compute the mass from this info.
     * After that it will create an instance of the {@link Mass} class representing the actual weight and then group it under an
     * instance of the {@link MassPair} class.
     *
     * @param partToCheck - the part to get the weights from
     * @return Instance of the MassPair class representing the weights found
     * @throws WTException - thrown on any exception in the code execution
     */
    public static MassPair getPartMass(WTPart partToCheck, HashMap<String, WTArrayList> nonCalculableObjects) throws WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getPartMass().");
            LOG.debug("Part is -> " + partToCheck.getIdentity());
        }
        PersistableAdapter partAdapter = new PersistableAdapter(partToCheck, null, null, null);
        partAdapter.load(CALCULATED_WEIGHT_IBA, CALCULATED_WEIGHT_UNIT_IBA, ACTUAL_WEIGHT_IBA, ACTUAL_WEIGHT_UNIT_IBA);
        Mass calculatedPair = new Mass(partAdapter.get(CALCULATED_WEIGHT_IBA), partAdapter.get(CALCULATED_WEIGHT_UNIT_IBA), true, partToCheck);
        if (calculatedPair.getMass().equals(Mass.NO_MASS_MARKER)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("The part has no calculated weight present. Will try to obtain it from the density stored on a related Material part and volume of a related EPM Document");
            }
            calculatedPair = getMassByVolumeAndDensity(partToCheck, nonCalculableObjects);
        }
        Mass actualPair = new Mass(partAdapter.get(ACTUAL_WEIGHT_IBA), partAdapter.get(ACTUAL_WEIGHT_UNIT_IBA), false, partToCheck);
        if (actualPair.getMass().equals(Mass.NO_MASS_MARKER)) {
            WTArrayList theList = nonCalculableObjects.get(NO_ACTUAL_MASS);
            if (theList!= null && !theList.contains(partToCheck)) {
                theList.add(partToCheck);
            }
        }
        MassPair toReturn = new MassPair(calculatedPair, actualPair, partToCheck);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method getPartMass().");
        }
        return toReturn;
    }
    
    private static WTPart findDensestMaterial(ArrayList<WTPart>relatedMaterials) {
        WTPart toReturn = null;
        if (relatedMaterials != null && relatedMaterials.size() == 1) {
            toReturn = relatedMaterials.get(0);
        } else if (relatedMaterials != null && relatedMaterials.size() > 1) {
            
        }
        return toReturn;
    }
    
    /**
     * The method will 
     * @param partToCheck - the part to get the weights from
     * @throws WTException - thrown on any exception in the code execution
     */
    private static Mass getMassByVolumeAndDensity(WTPart partToCheck, HashMap<String, WTArrayList> nonComputableParts) throws WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getRelatedCADMass().");
            LOG.debug("Part is -> " + partToCheck.getIdentity());
        }
        Mass toReturn =  new Mass(Mass.NO_MASS_MARKER, Mass.GRAM_UNIT, true, partToCheck);
        Object volumeObject = null;
        Object densityObject = null;
        EPMDocument cadToCheck = getCADFromPart(partToCheck);
        ArrayList<WTPart>relatedMaterials = getMaterialPartsFromPart(partToCheck);
        if (cadToCheck != null) {
            PersistableAdapter cadAdapter = new PersistableAdapter(cadToCheck, null, null, null);
            cadAdapter.load(VOLUME);
            //if (cadAdapter.isLoaded(VOLUME)) {volumeObject = cadAdapter.get(VOLUME)}
            volumeObject = cadAdapter.get(VOLUME);
            if (volumeObject == null) {
                WTArrayList theList = nonComputableParts.get(NO_VOLUME);
                if (theList!= null && !theList.contains(partToCheck)) {
                    theList.add(partToCheck);
                } 
            }
           
        } if (relatedMaterials.size() >=1 ) {
            PersistableAdapter materialAdapter = new PersistableAdapter(relatedMaterials.get(0), null, null, null);
            materialAdapter.load(DENSITY);
          //if (materialAdapter.isLoaded(DENSITY)) {densityObject = materialAdapter.get(DENSITY)}
            densityObject = materialAdapter.get(DENSITY);
            if (densityObject == null) {
                WTArrayList theList = nonComputableParts.get(NO_DENSITY);
                if (theList!= null && !theList.contains(partToCheck)) {
                    theList.add(partToCheck);
                } 
            }
        }
        if (volumeObject != null && densityObject !=null) {
            toReturn.setMassByDensityAndVolume(densityObject, volumeObject);
        }
        if (relatedMaterials == null || relatedMaterials.isEmpty()) {
            WTArrayList theList = nonComputableParts.get(NO_ASSOCIATED_PART_FOR_DENSITY);
            if (theList!= null && !theList.contains(partToCheck)) {
                theList.add(partToCheck);
            } 
        } if (cadToCheck == null) {
            WTArrayList theList = nonComputableParts.get(NO_ASSOCIATED_CAD_FOR_VOLUME);
            if (theList!= null && !theList.contains(partToCheck)) {
                theList.add(partToCheck);
            } 
        }
        return toReturn;
    }

    private static ArrayList<WTPart> getMaterialPartsFromPart(WTPart partToCheck) throws ConfigException, WTException {
        ArrayList<WTPart> toReturn = new ArrayList<>();
        QueryResult res = StructHelper.service.navigateUsesToIteration(partToCheck, WTPartUsageLink.class, true, new LatestConfigSpec());
        while (res.hasMoreElements()) {
            WTPart foundPart = (WTPart)res.nextElement();
            if (TypeUtils.isMaterialPart(foundPart)) {
                toReturn.add(foundPart);
            }
        }
        return toReturn;
    }

    
    /**
     * Checks if the part is of the Article softtype.
     *
     * @param rootOid - a string representing the OID of the WTPart
     * @return true, if part is an Article
     * @throws WTException - thrown on any exception in the code execution
     */
    public static boolean isPartAnArticle(String rootOid) throws WTException {
        boolean toReturn = false;
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method isPartAnArticle().");
            LOG.debug("Part OID is -> " + rootOid);
        }
        Persistable toGet = getPersistableByOid(rootOid);
        if (!(toGet instanceof WTPart)) {
            throw new WTException(WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.EX_INVALID_OID));
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Part described by the passed OID is -> " + toGet.getIdentity());
        }
        PersistableAdapter partAdapter = new PersistableAdapter(toGet, null, null, null);
        partAdapter.load(SALESTYPE_IBA);
        Object partType = partAdapter.get(SALESTYPE_IBA);
        if (partType != null && partType instanceof String && partType.toString().equalsIgnoreCase(SALESTYPE_ARTICLE)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Found part IS AN ARTICLE!");
            }
            toReturn = true;
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method isPartAnArticle().");
        }
        return toReturn;
    }
    
    /**
     * This method checks if the part represented by the OID has all of its immediate
     * children of the soft type Packaging only.
     *
     * @param rootOid - a string representing the OID of the WTPart
     * @return true, if the WTPart has linked only Packaging objects only
     * @throws ConfigException the config exception
     * @throws WTException - thrown on any exception in the code execution
     */
    public static boolean hasPartOnlyPackages(String rootOid) throws ConfigException, WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method hasPartOnlyPackages().");
            LOG.debug("Part OID is -> " + rootOid);
        }
        boolean toReturn = true;
        Persistable toGet = getPersistableByOid(rootOid);
        if (!(toGet instanceof WTPart)) {
            throw new WTException(WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.EX_INVALID_OID));
        }
        WTArrayList immediateChildren = getChildParts((WTPart) toGet);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will now check the immediate children of the part for the softtype.");
        }
        Iterator<?> iterateOverChildren = immediateChildren.persistableIterator();
        while (iterateOverChildren.hasNext()) {
            WTPart toCheck = (WTPart) iterateOverChildren.next();
            if (!TypeUtils.isWTPartPackage(toCheck)) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Found a part that is not of the Packaging softtype -> " + toCheck.getIdentity());
                }
                toReturn = false;
                break;
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method hasPartOnlyPackages().");
        }
        return toReturn;
    }
    
    /**
     * The method gets the child parts based on the passed OID representing the WTPart.
     *
     * @param rootOid - a string representing the OID of the WTPart
     * @return a WTArrayList representing the child parts.
     * @throws ConfigException the config exception
     * @throws WTException - thrown on any exception in the code execution
     */
    public static WTArrayList getChildParts(String rootOid) throws ConfigException, WTException {
        Persistable persistable = getPersistableByOid(rootOid);
        if (!(persistable instanceof WTPart)) {
            throw new WTException(WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.EX_INVALID_OID));
        }
        return getChildParts((WTPart)persistable);
    }
    
    /**
     * The method gets the immediate child parts that are related in the BOM to the selected parent part
     *
     * @param partToCheck - the part to have the children found and returned
     * @return a WTArrayList representing the child parts.
     * @throws ConfigException the config exception
     * @throws WTException - thrown on any exception in the code execution
     */
    public static WTArrayList getChildParts(WTPart partToCheck) throws ConfigException, WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method hasPartOnlyPackages().");
            LOG.debug("Part to check is -> " + partToCheck.getIdentity());
        }
        WTArrayList toReturn = new WTArrayList();
        QueryResult result = StructHelper.service.navigateUsesToIteration(partToCheck, WTPartUsageLink.class, true, new LatestConfigSpec());
        while (result.hasMoreElements()) {
            Object nextElement = result.nextElement();
            if(nextElement instanceof WTPart) {
                WTPart child = (WTPart)nextElement;
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Found child part -> " + child.getIdentity());
                } 
                if (!TypeUtils.isMaterialPart(child) && !TypeUtils.isColorPart(child)) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Found child part -> " + child.getIdentity() + " is NOT A COLOR OR MATERIAL! Will add it to the returned list!");
                    } 
                    toReturn.add(child);
                }
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method getChildParts().");
        }
        return toReturn;
    }
  
    /**
     * Gets the persistable that is represented by the oid string.
     *
     * @param rootOid - a string representing the OID of the WTPart
     * @return the persistable representing the WTPart
     * @throws WTException - thrown on any exception in the code execution
     */
    public static Persistable getPersistableByOid (String rootOid) throws WTException {
        ReferenceFactory rf = new ReferenceFactory();
        WTReference ref = (WTReference) rf.getReference(rootOid);
        Persistable persistable = (Persistable) ref.getObject();
        return persistable;
    }
   
    /**
     * This method will traverse the whole BOM structure that is represented by the root WTPart. 
     * It will use a queue to do a non-recursive tree search from top of the BOM down to the very leafs of it.
     * For each of the traversed nodes, the method will store the immediate children in a HashMap, where the key
     * is the currently analyzed part and the value - an array list of all its immediate children.
     *
     * @param root - WTPart representing the root of the BOM tree.
     * @return A HashMap reflecting the whole BOM
     * @throws WTException - thrown on any exception in the code execution
     */
    public static HashMap<WTPart, WTArrayList> getAllDescendants(WTPart root) throws WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getAllDescendants().");
            LOG.debug("Part to check is -> " + root.getIdentity());
        }
        HashMap<WTPart, WTArrayList> structureMap = new HashMap<WTPart, WTArrayList>();
        ConfigSpec configSpec = new LatestConfigSpec();
        Queue<WTPart> partsToCheck = new LinkedList<WTPart>();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Adding part to queue -> " + root.getIdentity());
        }
        partsToCheck.add(root);
        while (!partsToCheck.isEmpty()) {
            WTPart partToCheck = partsToCheck.poll();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Taken the part from the queue -> " + partToCheck.getIdentity());
                LOG.debug("Will now try to find any descendants");
            }
            QueryResult queryResult = WTPartHelper.service.getUsesWTParts(partToCheck, configSpec);
            while (queryResult.hasMoreElements()) {
                Persistable[] persistables = (Persistable[]) queryResult.nextElement();
                WTPart part = (WTPart) persistables[1];
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Found a child part -> " + part.getIdentity());
                }
                if (!structureMap.containsKey(partToCheck)) {
                    structureMap.put(partToCheck, new WTArrayList());
                }
                structureMap.get(partToCheck).add(part);
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Added the part to the Hash Map and puting it to the queue");
                }
                partsToCheck.add(part);
                
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method getAllDescendants().");
        }
        return structureMap;
    }

    /**
     * The method will calculate the mass of a selected WTPart from the BOM structure. It will do so on a recursion-based process for every node in the HashMap found thus
     * computing the mass for a given part bottom-up. If a Part can't have its mass computed,
     * then that part will be placed in the WTArrayList containing such object in the whole structure
     *
     * @param nodePart - WTPart representing the node of the BOM tree
     * @param treeStructure - a HashMap reflecting the whole BOM
     * @param nonComputableParts - a WTArrayList containing parts that can't have their weights obtained
     * @return An instance of the {@link Mass} type containing the calculated mass of the analyzed node
     * @throws WTException - thrown on any exception in the code execution
     */
    public static Mass calculateMassOfNode(WTPart nodePart, HashMap<WTPart, WTArrayList> treeStructure, HashMap<String, WTArrayList> nonComputableParts) throws WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method calculateMassOfNode().");
            LOG.debug("Curent part to check is -> " + nodePart.getIdentity());
        }
        ArrayList<Mass> childrenNodeMasses = new ArrayList<Mass>();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will now try to obtain weights of the part...");
        }
        MassPair actualMassPair = getPartMass(nodePart, nonComputableParts);
        if (LOG.isDebugEnabled()) {
            LOG.debug( "Actual mass for part is -> " + actualMassPair.getActualMass().getMassForReports()+ " "+actualMassPair.getActualMass().getMassUnit());
            LOG.debug( "Calculated mass for part is -> " + actualMassPair.getCalculatedMass().getMassForReports()+ " "+actualMassPair.getCalculatedMass().getMassUnit());
        }
        if (actualMassPair.isEmptyMassPair()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("The following part could not have its mass calculated/obtained -> " + nodePart.getIdentity());
            }
          
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will now try to get the immediate children of the part...");
        }
        WTArrayList children = treeStructure.get(nodePart);
        if (children != null) {
            Iterator<?> iterateOverChildren = children.persistableIterator();
            while (iterateOverChildren.hasNext()) {
                WTPart theChild = (WTPart) iterateOverChildren.next();
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Found child part -> " + theChild.getIdentity());
                }
                Double multiplier = getQuantityOfUsageLink(nodePart, theChild.getMaster());
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Found child part " + theChild.getIdentity() +" has a quantity of " + multiplier);
                }
                if (getChildParts(theChild).isEmpty()) {
                    //We have a leaf - will  get the masses and multiply them against the quantity
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Found child part " + theChild.getIdentity() +" is a leaf of the BOM!");
                    }
                    MassPair toAdd = getPartMass(theChild, nonComputableParts);
                    toAdd.multiplyAgainstUsage(multiplier);
                    childrenNodeMasses.add(toAdd.getResultingMass());

                } else {
                    //We have a non-leaf: need to calculate the mass recursively
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Found child part " + theChild.getIdentity() +" is NOT a leaf of the BOM!");
                        LOG.debug("Will now calculate the mass of the found child part " + theChild.getIdentity());
                    }
                    Mass toAdd = calculateMassOfNode(theChild, treeStructure, nonComputableParts);
                    toAdd.multiply(multiplier);
                    childrenNodeMasses.add(toAdd);
                }
            }
        }
        childrenNodeMasses = findPartsWithoutWeight(nonComputableParts, childrenNodeMasses);
        Mass summedMass = convertAndSumMasses(childrenNodeMasses, nodePart);
        summedMass = compareAndChangeCalculatedMass(actualMassPair, summedMass);
        if (LOG.isDebugEnabled()) {
            LOG.debug( "Obtained calculated mass for part is -> " + summedMass.getMassForReports()+ " "+summedMass.getMassUnit());
            LOG.debug("Exiting method calculateMassOfNode().");
        }
        return summedMass;
    }

    /**
     * The method will compare and change calculated mass of the part.
     *
     * @param actualMassPair - Instance of {@link MassPair} representing the actual mass pair
     * @param summedMass - Instance of {@link Mass} representing the mass that was calculated by summing the masses of the child nodes
     * @return the final calculated mass of a part
     * @throws WTException - thrown on any exception in the code execution
     */
    private static Mass compareAndChangeCalculatedMass(MassPair actualMassPair, Mass summedMass) throws WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method compareAndChangeCalculatedMass().");
        }
        if (summedMass == null) {
            throw new WTException(WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.EX_NULL_MASS));
        }
        if (actualMassPair == null) {
            throw new WTException(WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.EX_NULL_MASS_PAIR));
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug( "Actual mass for part is -> " + actualMassPair.getActualMass().getMassForReports()+ " "+actualMassPair.getActualMass().getMassUnit());
            LOG.debug( "CAD calculated mass for part is -> " + actualMassPair.getCalculatedMass().getMassForReports()+ " "+actualMassPair.getCalculatedMass().getMassUnit());
            LOG.debug( "Summed mass for part is -> " + summedMass.getMassForReports()+ " "+summedMass.getMassUnit());
        }
        Mass actualReturnedMass = actualMassPair.getResultingMass();
        if (!actualReturnedMass.getMass().equals(Mass.NO_MASS_MARKER)) {
            //not calculated and a valid mass -> must be the manually entered mass.
            if (LOG.isDebugEnabled()) {
                LOG.debug("Exiting method compareAndChangeCalculatedMass().");
                LOG.debug("Will return the following mass -> " + actualReturnedMass.getMassForReports()+ " "+actualReturnedMass.getMassUnit());
            }
            return actualReturnedMass;
        } else if (actualReturnedMass.getMass().equals(Mass.NO_MASS_MARKER)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Exiting method compareAndChangeCalculatedMass().");
                LOG.debug("Will return the following mass -> " + summedMass.getMassForReports()+ " "+summedMass.getMassUnit());
            }
            return summedMass;
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method compareAndChangeCalculatedMass().");
            LOG.debug("WILL RETURN NULL.");
        }
        return null;
    }

    /**
     * The method will find parts without weight.
     *
     * @param nonComputableParts - a WTArrayList containing the parts that could not have their masses calculated
     * @param massesToCheck - an ArrayList of {@link Mass} objects containing the masses that need to be checked
     * @return massesToCheck
     */
    private static ArrayList<Mass> findPartsWithoutWeight(HashMap<String, WTArrayList> nonComputableParts, ArrayList<Mass> massesToCheck) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method findPartsWithoutWeight().");
        }
        if (massesToCheck != null) {
            for (Mass toCheck: massesToCheck) {
                if (toCheck.getMass().equals(Mass.NO_MASS_MARKER)) {
                    toCheck.setMass("0");
                }
            }
            if (LOG.isDebugEnabled()) {
                LOG.debug("Exiting method findPartsWithoutWeight().");
                LOG.debug("Number of parts without masses is -> " + nonComputableParts.size());
            }
            return massesToCheck;
        } else {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Exiting method findPartsWithoutWeight().");
                LOG.debug("NO parts without masses found so far!");
            }
            return new ArrayList<Mass>();
        }
    }

    /**
     * The method will do a unit conversion to the smallest found unit among the masses and sum them.
     *
     * @param childrenNodeMasses - an ArrayList of {@link Mass} objects that need to be summed
     * @param nodePart - the part that will have the summed masses attached to
     * @return the summed masses
     * @throws WTException - thrown on any exception in the code execution
     */
    private static Mass convertAndSumMasses(ArrayList<Mass> childrenNodeMasses, WTPart nodePart) throws WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method convertAndSumMasses().");
            LOG.debug("WTPart for which the sum will be done -> " + nodePart.getIdentity());
        }
       String foundMassUnit = Mass.convertToSameUnits(childrenNodeMasses);
       if (LOG.isDebugEnabled()) {
           LOG.debug("Masses will be converted to the following units -> " + foundMassUnit);
       }
       Mass toReturn = new Mass(0L, foundMassUnit, true, nodePart);
       for (Mass toCheck:childrenNodeMasses) {
           toReturn.addMass(toCheck);
       }
       if (LOG.isDebugEnabled()) {
           LOG.debug("Exiting method convertAndSumMasses().");
           LOG.debug("Returning the sum -> " + toReturn.getMassForReports() + " " + toReturn.getMassUnit());
       }
       return toReturn;
    }
    
    /**
     * The method will build a string representing the parts identity based on the passed OID String.
     *
     * @param rootOid - String representing the Object Identifier of a Part
     * @return a string representing the parts identity
     * @throws WTException - thrown on any exception in the code execution
     */
    public static String getPartIdentityFromOid(String rootOid) throws WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getPartIdentityFromOid().");
            LOG.debug("OID representing the WTPart is -> " + rootOid);
        }
        StringBuilder toReturn = new StringBuilder();
        Persistable persistable = getPersistableByOid(rootOid);
        if (!(persistable instanceof WTPart)) {
            throw new WTException(WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.EX_INVALID_OID));
        }
        WTPart partToCheck = (WTPart) persistable;
        if (LOG.isDebugEnabled()) {
            LOG.debug("Found WTPart -> " + partToCheck.getIdentity());
        }
        toReturn.append(partToCheck.getNumber()).append(", ").append(partToCheck.getName()).append(", ").append(partToCheck.getVersionInfo().getIdentifier().getValue()).append(".").append(partToCheck.getIterationInfo().getIdentifier().getValue());
        if (partToCheck.getViewName() != null && !partToCheck.getViewName().isEmpty()) {
            toReturn.append(" (").append(partToCheck.getViewName()).append(")");
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method getPartIdentityFromOid().");
            LOG.debug("Identity of part is -> " + toReturn.toString());
        }
        return toReturn.toString(); 
    }

    /**
     * The method is serving as an entry point of generating the weight report. It will take the Object Identifier
     * string, try to find a part that is represented by it and if successful, it will start the weight calculations.
     *
     * @param rootOid - String representing the Object Identifier of a Part
     * @param nonCalculatableParts - a WTArrayList containing the parts that could not have their masses calculated
     * @return A string representing the weight after the calculations have finished
     * @throws WTException - thrown on any exception in the code execution
     */
    public static String generateReportForSelectedNode(String rootOid, HashMap<String, WTArrayList> nonCalculatableParts) throws WTException {
        StringBuilder toReturn = new StringBuilder();
        WTPart root = validateAndGetPartByOid(rootOid);
        if (nonCalculatableParts == null) { 
            nonCalculatableParts = new HashMap<String, WTArrayList> ();
            nonCalculatableParts.put(NO_DENSITY, new WTArrayList());
            nonCalculatableParts.put(NO_VOLUME, new WTArrayList());
            nonCalculatableParts.put(NO_ACTUAL_MASS, new WTArrayList());
        }
        HashMap<WTPart, WTArrayList> hashMapOfChildren = getAllDescendants(root);
        Mass articleWeight =  calculateMassOfNode(root, hashMapOfChildren, nonCalculatableParts);
        toReturn.append (WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.INFO_CALCULATED_MASS, new Object [] {articleWeight.getMassForReports(), articleWeight.getMassUnit()}));
        return toReturn.toString();
    }
    
    /**
     * The method is serving as an entry point of generating the weight report for a leaf part. It will take the Object Identifier
     * string, try to find a part that is represented by it and if successful, it will start the weight calculations.
     * @param rootOid - String representing the Object Identifier of a Part
     * @return A string representing the weight after the calculations have finished
     * @throws WTException - thrown on any exception in the code execution
     */
    public static String generateReportForOnePart(String rootOid, HashMap<String, WTArrayList> nonCalculatableParts) throws WTException {
        StringBuilder toReturn = new StringBuilder();
        WTPart root = validateAndGetPartByOid(rootOid);
        MassPair theMassPairs = getPartMass(root, nonCalculatableParts);
        if (!theMassPairs.getResultingMass().getMass().equals(Mass.NO_MASS_MARKER)) {
            Mass articleWeight = theMassPairs.getResultingMass();
            toReturn.append(WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.INFO_CALCULATED_MASS, new Object [] {articleWeight.getMassForReports(), articleWeight.getMassUnit()}));
        } else if (theMassPairs.getResultingMass().getMass().equals(Mass.NO_MASS_MARKER)) {
            toReturn.append(WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.ERROR_NO_CALCULATED_MASS));
        }
        return toReturn.toString();
    }
    
    /**
     * Checks if is part an MBOM root.
     *
     * @param partOid - String representing the Object Identifier of a Part
     * @return true, if part is an MBOM root
     * @throws ConfigException the config exception
     * @throws WTException - thrown on any exception in the code execution
     */
    public static boolean isPartAnMBOMRoot (String partOid) throws ConfigException, WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method isPartAnMBOMRoot().");
            LOG.debug("OID representing the WTPart is -> " + partOid);
        }
        boolean isPartAnArticle = isPartAnArticle(partOid);
        boolean hasPartOnlyPackages = hasPartOnlyPackages(partOid);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method isPartAnMBOMRoot().");
            LOG.debug("Is the part an article -> " + isPartAnArticle);
            LOG.debug("Is the part a father of ONLY packaging objects -> " + hasPartOnlyPackages);
        }
        return isPartAnArticle & hasPartOnlyPackages;
    }
    
    /**
     * Gets the quantity of usage link.
     *
     * @param father - a {@link WTPart} in the father role.
     * @param child - a {@link WTPart} in the child role.
     * @return the quantity value found on the usage link between father and child
     * @throws WTException - thrown on any exception in the code execution
     */
    private static double getQuantityOfUsageLink(WTPart father, WTPartMaster child) throws WTException {
        WTPartUsageLink theLink = getWTPartUsageLink(father, child);
        if (theLink != null) {
            return theLink.getQuantity().getAmount();
        } 
        return 0;
    }

    /**
     * The method will try to find a related instance of the {@link EPMDocument} via an "Owner" association for a given WTPart
     *
     * @param toGet - Inastance of a {@link WTPart} to have the CAD Document obtained
     * @return an instance of {@link EPMDocument} that is associated to the part
     * @throws WTException - thrown on any exception in the code execution
     */
    private static EPMDocument getCADFromPart(WTPart toGet) throws WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getCADFromPart().");
            LOG.debug("WTPart for obtaining the related EPM document is -> " + toGet.getIdentity());
        }
        EPMDocument toReturn = null;
        QuerySpec qs = new QuerySpec(EPMBuildRule.class);
        qs.appendWhere(new SearchCondition(EPMBuildRule.class, "roleBObjectRef.key.branchId", SearchCondition.EQUAL, toGet.getBranchIdentifier()), new int[]{0});
        QueryResult qr = PersistenceHelper.manager.find((StatementSpec) qs);
        while (qr.hasMoreElements()) {
            EPMBuildRule rule = (EPMBuildRule) qr.nextElement();
            toReturn = ((EPMDocument) rule.getBuildSource());
            if (LOG.isDebugEnabled()) {
                LOG.debug("The related EPM document is -> " + toReturn.getIdentity());
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method getCADFromPart().");
        }
        return toReturn;
    }
    
    /**
     * This method will try to obtain a WPTpart that is identified by a string representing the object identifier.
     * If the obtained {@link Persistable} is not a WTPart the method will trhrow an exception - otherwise it will return that 
     * persistable.
     * @param rootOid - String representing the Object Identifier of a Part
     * @return an instance of the {@link WTPart} represented by the OID string.
     * @throws WTException - thrown on any exception in the code execution
     */
    private static WTPart validateAndGetPartByOid(String rootOid) throws WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method validateAndGetPartByOid().");
            LOG.debug("OID representing the WTPart is -> " + rootOid);
        }
        Persistable persistable = getPersistableByOid(rootOid);
        if (!(persistable instanceof WTPart)) {
            throw new WTException ("The passed on object identifier does not point to a WTPart object!");
        }
        WTPart root = (WTPart) persistable;
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method validateAndGetPartByOid().");
            LOG.debug("Found WTPart is -> " + root.getIdentity());
        }
        return root;
    }
    
    /**
     * The method will take the WTArrayList containing references to Windchill Persistables and convert it to an Array List
     *
     * @param theList - an instance of the {@link WTArrayList} containing the parts without mass
     * @return an instance of an ArrayList containing the parts without mass
     * @throws WTException - thrown on any exception in the code execution
     */
    public static List<WTPart> getPartsWithoutMass(WTArrayList theList) throws WTException {
        List<WTPart> toReturn = new ArrayList<WTPart>();
        WTHashSet uniqueParts = new WTHashSet(theList);
        Iterator iterateOverParts = uniqueParts.persistableIterator();
        while (iterateOverParts.hasNext()) {
            WTPart partToCheck = (WTPart)iterateOverParts.next();
            if (!TypeUtils.isColorPart(partToCheck) && !TypeUtils.isMaterialPart(partToCheck)) {
                toReturn.add(partToCheck);
            }
        }
        return toReturn;
    }
    
    /**
     * The method will obtain a {@link WTPartUsageLink} that exists between a child and a parent part.
     *
     * @param father - a {@link WTPart} in the father role.
     * @param child - a {@link WTPart} in the child role.
     * @return the WTPartUsageLink that is used to connect the two parts
     * @throws WTException - thrown on any exception in the code execution
     */
    private static WTPartUsageLink getWTPartUsageLink(WTPart father, WTPartMaster child) throws WTException {
        QueryResult linksQueryResult = StructHelper.service.navigateUsedBy(child, false);
        while (linksQueryResult.hasMoreElements()) {
            WTPartUsageLink link = (WTPartUsageLink) linksQueryResult.nextElement();
            WTPart parentPart = (WTPart) link.getUsedBy();
            if (parentPart.equals(father)) {
                return link;
            }
        }
        return null;
    }
 
}
