package com.ikea.ipim.utils.mass;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import wt.fc.WTReference;
import wt.part.WTPart;
import wt.units.FloatingPointWithUnits;
import wt.util.WTException;
import wt.util.WTMessage;

/**
 * This class is used to represent the weight of a selected WTPart. It will store the mass value, mass unit and a reference to the subject part. 
 * It can handle the mass operations twofold - either by a setting of the weight attribute directly or by a computation via the density and volume of a part.
 * It also consists of a marker denoting whether the mass is coming from the user directly (if set to false) or obtained via autocalculation (if true).
 */

public class Mass {
    public static final String GRAM_UNIT = "g";
    public static final String KILOGRAM_UNIT = "kg";
    public static final String MILLIGRAM_UNIT = "mg";
    public static final String NO_MASS_MARKER = "-";
    
    private String mass = NO_MASS_MARKER;
    private String massUnit = KILOGRAM_UNIT;
    private double density = -1.0d;
    private double volume = -1.0d;
    private WTReference partReference;
    private boolean isCalculated;
    private static final Logger LOG = Logger.getLogger(Mass.class);
    
    /**
     * Instantiates a new mass.
     *
     * @param mass - the object representing a mass value
     * @param massUnit - the object representing a mass unit
     * @param isCalculated - the boolean marker denoting whether the mass is coming from the user directly (if set to false) or obtained via autocalculation from a  related EPMdocument (if true).
     * @param part - the WTPart instance that will be the referenced subject
     */
    public Mass (Object mass, Object massUnit, boolean isCalculated, WTPart part) {
        if (mass instanceof Long) {
            this.mass = Long.toString((Long)mass);
        } 
        if (massUnit instanceof String) {
            this.massUnit = massUnit.toString();
        }
        this.isCalculated = isCalculated;
    }
    
    /**
     * Instantiates a new mass.
     *
     * @param mass - the object representing a mass value
     * @param massUnit - the object representing a mass unit
     */
    public Mass (Object mass, Object massUnit) {
        if (mass instanceof Long) {
            this.mass = Long.toString((Long)mass);
        } 
        if (massUnit instanceof String) {
            this.massUnit = massUnit.toString();
        }
        this.isCalculated = false;
    }
    
    /**
     * Instantiates a new mass from the density and volume.
     *
     * @param density - the object representing the density of a part
     * @param volume - the object representing the volume of a part
     * @param massUnit - the object representing a mass unit
     */
    public Mass (FloatingPointWithUnits density, FloatingPointWithUnits volume) {
        setMassByDensityAndVolume(density, volume);
    }
    
    /**
     * Instantiates a new mass from the density and volume.
     *
     * @param density - the object representing the density of a part
     * @param volume - the object representing the volume of a part
     * @param massUnit - the object representing a mass unit
     * @param isCalculated - the boolean marker denoting whether the mass is coming from the user directly (if set to false) or obtained via autocalculation from a  related EPMdocument (if true).
     */
    public Mass (Object density, Object volume, Object massUnit, boolean isCalculated) {
        setupMassByDensity(density, volume);
        if (massUnit instanceof String) {
            this.massUnit = massUnit.toString();
        }
        this.isCalculated = isCalculated;
    }
    
    /**
     * Gets the mass value.
     *
     * @return A string representing the mass value.
     */
    public String getMass() {
        return this.mass;
    }
    
    /**
     * Gets the mass value attribute for reports.
     *
     * @return A string representing the mass value for reports.
     */
    public String getMassForReports() {
        if (this.getMassAsInt() == 0) {
            return NO_MASS_MARKER;
        } else return this.getMass();
    }
    
    /**
     * Gets the mass value attribute for publishing.
     *
     * @return A string representing the mass value for publishing.
     */
    public String getMassForPublishing() {
        if (this.getMassAsInt() == 0) {
            return "";
        } else return this.getMass();
    }
   
    /**
     * Gets the mass as an Integer.
     *
     * @return the mass as an Integer
     */
    public int getMassAsInt() {
        if (mass != null && !mass.equals(NO_MASS_MARKER)) {
            return Integer.parseInt(this.mass);
        }
        else return 0;
    }
    
    /**
     * Sets the mass.
     *
     * @param mass the new mass
     */
    public void setMass(String mass) {
        this.mass = mass;
    }
    
    /**
     * Gets the mass unit.
     *
     * @return the mass unit
     */
    public String getMassUnit() {
        return this.massUnit;
    }
    
    public Double getDensity() {
        return this.density;
    }
    
    public Double getVolume() {
        return this.volume;
    }
    
    /**
     * Sets the mass by computing it through the density and volume. It will try to obtain the 
     * weight unit from the density object
     *
     * @param density the density of an object
     * @param volume the density of an object
     */
    private void setupMassByDensity(Object density, Object volume) {
        setupMassByDensity(density, volume, null);
    }
    
    /**
     * Sets the mass by computing it through the density and volume.
     *
     * @param density the density of an object
     * @param volume the density of an object
     * @param massUnit the unit of mass of an object
     */
    private void setupMassByDensity(Object density, Object volume, Object massUnit) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method setupMassByDensity()");
            LOG.debug("Will now try to calculate the mass of the object from the density");
        }
        if (massUnit != null && massUnit instanceof String) {
            this.massUnit = (String) massUnit;
            if (LOG.isDebugEnabled()) {
                LOG.debug("Unit of mass is a string -> " + this.massUnit);
            }
        }
        if (volume instanceof FloatingPointWithUnits) {
            FloatingPointWithUnits volumeAndUnit = (FloatingPointWithUnits)volume;
            this.volume = volumeAndUnit.getValue();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Volume of the object is a floating point with units -> " + this.volume);
            }
        } else if (volume instanceof String) {
            this.volume = Double.parseDouble((String) volume);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Volume of the object is a string -> " + this.volume);
            }
        } else if (volume instanceof Double) {
            this.volume = (double) volume;
            if (LOG.isDebugEnabled()) {
                LOG.debug("Volume of the object is a double -> " + this.volume);
            }
        }
        if (density instanceof FloatingPointWithUnits) {
            FloatingPointWithUnits densityAndUnit = (FloatingPointWithUnits)density;
            this.density = densityAndUnit.getValue();
            String densityUnit = densityAndUnit.getUnits();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Density of the object is a floating point with units -> " + this.density + " " + densityUnit);
            }
            this.massUnit = densityUnit.substring(0, densityUnit.indexOf("/"));
        } else if (density instanceof String) {
            this.density = Double.parseDouble((String) density);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Density of the object is a string -> " + this.density);
            }
        } else if (density instanceof Double) {
            this.density = (double) density;
            if (LOG.isDebugEnabled()) {
                LOG.debug("Density of the object is a double -> " + this.density);
            }
        }
        double massAsDouble = this.volume * this.density;
        if (massAsDouble < 1) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("The calculated mass is a fraction -> " + massAsDouble + ". Will convert it to a smaller unit!");
            }
            //a fraction of a kilogram/gram - need to convert downward
            boolean noFractionDetected = false;
            while (!noFractionDetected) {
                massAsDouble = 1000*massAsDouble;
                if (this.massUnit.equals(Mass.KILOGRAM_UNIT)) {
                    this.massUnit = Mass.GRAM_UNIT;
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Converted to grams unit!");
                    }
                } else if (this.massUnit.equals(Mass.GRAM_UNIT)) {
                    this.massUnit = Mass.MILLIGRAM_UNIT;
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Converted to milligrams unit!");
                    }
                }
                if (massAsDouble >= 1) {
                    noFractionDetected = true;
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Converted to smaller units!");
                    }
                }
            }
            
        }
        this.mass = Long.toString(Math.round(massAsDouble));
        if (LOG.isDebugEnabled()) {
            LOG.debug("Calculated weight is -> " + this.mass + " " + this.massUnit);
            LOG.debug("Exiting method setupMassByDensity()");
        }
    }
    
    /**
     * Sets the mass by computing it through the density and volume. It will try to obtain the 
     * weight unit from the density object. Also - this method will implicitly set the calculated weight marker to true.
     *
     * @param density the density of an object
     * @param volume the density of an object
     */
    public void setMassByDensityAndVolume(Object density, Object volume) {
        setupMassByDensity(density, volume);
        this.isCalculated = true;
    }
    
    /**
     * Checks if the mass is calculated.
     *
     * @return true, if is calculated
     */
    public boolean isCalculated() {
        return this.isCalculated;
    }
    
    /**
     * Multiply mass by a multiplier.
     *
     * @param multiplier the multiplier
     * @return the mass after multiplication
     */
    public String multiply(Double multiplier) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method multiply()");
            LOG.debug("Multiplier value is -> " + multiplier);
        }
       int multiplierInt =  multiplier.intValue();
       if (mass != null && !mass.equals(NO_MASS_MARKER)) {
           if (LOG.isDebugEnabled()) {
               LOG.debug("Will now multiply the mass " + this.mass + " by multiplier " + multiplier);
           }
           Integer massInteger = Integer.parseInt(mass);
           Integer afterMultiplication = massInteger * multiplierInt;
           this.mass = afterMultiplication.toString();
       }
       if (LOG.isDebugEnabled()) {
           LOG.debug("Exiting method multiply()");
           LOG.debug("Mass value after operation is -> " + mass);
       }
       return mass;
    } 
    
    /**
     * Adds one mass to another.
     *
     * @param massToAdd the mass to add
     * @throws WTException - thrown on any exception in the code execution
     */
    public void addMass(Mass massToAdd) throws WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method addMass()");
        }
        if (massToAdd == null) {
            throw new WTException (WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.EX_NULL_MASS));
        }
        if (!this.massUnit.equals(massToAdd.getMassUnit())) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Will now determine the lower mass unit... ");
            }
            String lowerMassUnit = getLowerMassUnit(this.massUnit, massToAdd.getMassUnit());
            if (LOG.isDebugEnabled()) {
                LOG.debug("Lower mass unit is -> " + lowerMassUnit);
                LOG.debug("Will use this unit for conversion and adding!");
            }
            if (this.massUnit.equals(lowerMassUnit)) {
                massToAdd.convertToUnits(lowerMassUnit);
            } else if (massToAdd.getMassUnit().equals(lowerMassUnit)) {
                this.convertToUnits(lowerMassUnit);
            }
        }
        BigDecimal currentMass = new BigDecimal(this.getMass());
        currentMass = currentMass.add(new BigDecimal(massToAdd.getMass()));
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method addMass()");
            LOG.debug("Mass after sum is -> " + currentMass.toPlainString());
        }
        this.setMass(currentMass.toPlainString());
     } 
    
    /**
     * Converts the mass to a specified unit.
     *
     * @param newUnit - the new unit that will be used for conversion
     * @return A string representing the mass value after conversion
     */
    public String convertToUnits(String newUnit) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method convertToUnits()");
        }
        if (newUnit == null || newUnit.equals(getMassUnit())) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Exiting method convertToUnits()");
                LOG.debug("No conversion has been done!");
            }
            return getMass();
        } if (getMassUnit().equals(KILOGRAM_UNIT) && newUnit.equals(GRAM_UNIT)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Converting kilograms to grams!");
            }
            BigDecimal milligramDecimal = new BigDecimal(getMassAsInt());
            BigDecimal toSet = milligramDecimal.multiply(new BigDecimal(1000));
            setMass(toSet.toPlainString()); 
        } else if (getMassUnit().equals(KILOGRAM_UNIT) && newUnit.equals(MILLIGRAM_UNIT)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Converting kilograms to milligrams!");
            }
            BigDecimal milligramDecimal = new BigDecimal(getMassAsInt());
            milligramDecimal.multiply(new BigDecimal(1000000));
            setMass(milligramDecimal.toPlainString()); 
        } else if (getMassUnit().equals(GRAM_UNIT) && newUnit.equals(MILLIGRAM_UNIT)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Converting grams to milligrams!");
            }
            BigDecimal milligramDecimal = new BigDecimal(getMassAsInt());
            BigDecimal toSet = milligramDecimal.multiply(new BigDecimal(1000));
            setMass(toSet.toPlainString());
        } else if (getMassUnit().equals(GRAM_UNIT) && newUnit.equals(KILOGRAM_UNIT)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Converting grams to kilograms!");
            }
            BigDecimal milligramDecimal = new BigDecimal(getMassAsInt());
            BigDecimal toSet = milligramDecimal.divide(new BigDecimal(1000));
            setMass(toSet.toPlainString());
        }  else if (getMassUnit().equals(MILLIGRAM_UNIT) && newUnit.equals(KILOGRAM_UNIT)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Converting milligrams to kilograms!");
            }
            BigDecimal milligramDecimal = new BigDecimal(getMassAsInt());
            BigDecimal toSet = milligramDecimal.divide(new BigDecimal(1000000));
            setMass(toSet.toPlainString());
        } else if (getMassUnit().equals(MILLIGRAM_UNIT) && newUnit.equals(GRAM_UNIT)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Converting milligrams to grams!");
            }
            BigDecimal milligramDecimal = new BigDecimal(getMassAsInt());
            BigDecimal toSet = milligramDecimal.divide(new BigDecimal(1000));
            setMass(toSet.toPlainString());
        }
        this.massUnit = newUnit;
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method convertToUnits()");
            LOG.debug("Weight value after conversion -> " + getMass());
        }
      return getMass();
    }

    /**
     * Gets the {@link WTReference} to the part related to the instance of the mass
     *
     * @return the part reference
     */
    public WTReference getPartReference() {
        return partReference;
    }

    /**
     * Sets the {@link WTReference} to the part related to the instance of the mass.
     *
     * @param partReference the new part reference
     */
    public void setPartReference(WTReference partReference) {
        this.partReference = partReference;
    }
    
    /**
     * Gets the lower mass unit.
     *
     * @param firstUnit the first mass unit
     * @param secondUnit the second mass unit
     * @return the lower mass unit
     */
    public static String getLowerMassUnit(String firstUnit, String secondUnit) {
        if (firstUnit.equals(secondUnit)) {
            return firstUnit;
        } if ((firstUnit.equals(Mass.KILOGRAM_UNIT) && secondUnit.equals(Mass.GRAM_UNIT)) || (firstUnit.equals(Mass.GRAM_UNIT) && secondUnit.equals(Mass.MILLIGRAM_UNIT)) || (firstUnit.equals(Mass.KILOGRAM_UNIT) && secondUnit.equals(Mass.MILLIGRAM_UNIT))) {
            return secondUnit;
        } if ((secondUnit.equals(Mass.KILOGRAM_UNIT) && firstUnit.equals(Mass.GRAM_UNIT)) || (secondUnit.equals(Mass.GRAM_UNIT) && firstUnit.equals(Mass.MILLIGRAM_UNIT)) || (secondUnit.equals(Mass.KILOGRAM_UNIT) && firstUnit.equals(Mass.MILLIGRAM_UNIT))) {
            return firstUnit;
        }
        return firstUnit;
    }
    
    /**
     * Convert an array list of Mass objects to the lowest unit of measure found in that list.
     *
     * @param toConvert - an array list of masses that needs to be converted
     * @return the string
     */
    public static String convertToSameUnits (ArrayList<Mass> toConvert) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method convertToSameUnits()");
            LOG.debug("Will now try to find the lowest unit of mass in the list");
        }
        String lowestFoundUnit = Mass.KILOGRAM_UNIT;
        String lowestFoundUnitActual = Mass.KILOGRAM_UNIT;
        for (Mass massToConvert:toConvert) {
            String unitOfActualMass = massToConvert.getMassUnit();
            lowestFoundUnitActual = getLowerMassUnit(lowestFoundUnitActual, unitOfActualMass);
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Lowest unit of mass in the list is -> " + lowestFoundUnitActual);
        }
        for (Mass massToConvert:toConvert) {
           massToConvert.convertToUnits(lowestFoundUnitActual);
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method convertToSameUnits()");
        }
        return lowestFoundUnit;
    }
    
}
