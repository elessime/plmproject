package com.ikea.ipim.utils.mass;

import wt.fc.ObjectReference;
import wt.part.WTPart;
import wt.util.WTException;
import wt.util.WTMessage;

/**
 * This class is used to represent a pair of masses - the one autocalculated from the EPM Document and another one - obtained from the user via the UI
 */
public class MassPair {

    private Mass calculatedMass;
    private Mass actualMass;
    
    /**
     * Instantiates a new mass pair.
     */
    public MassPair() {
        
    }
    
    /**
     * Checks if the current mass pair is empty.
     *
     * @return true, if the current mass pair is empty.
     */
    public boolean isEmptyMassPair() {
        boolean toReturn = false;
        if (this.getActualMass().getMass().equals(Mass.NO_MASS_MARKER) && this.getCalculatedMass().getMass().equals(Mass.NO_MASS_MARKER)) {
            toReturn = true;
        }
        return toReturn;
    }
    
    /**
     * Based on the existence of the Mass fields the method will return a resulting mass. Note - the mass provided by the user will always override
     * the autocalculated mass as per the need of the project.
     *
     * @return the resulting mass
     */
    public Mass getResultingMass() {
        Mass toReturn = null;
        if (!this.getActualMass().getMass().equals(Mass.NO_MASS_MARKER) && this.getCalculatedMass().getMass().equals(Mass.NO_MASS_MARKER)) {
            toReturn = this.getActualMass();
        } else if (this.getActualMass().getMass().equals(Mass.NO_MASS_MARKER) && !this.getCalculatedMass().getMass().equals(Mass.NO_MASS_MARKER)) {
            toReturn = this.getCalculatedMass();
        } else if (!this.getActualMass().getMass().equals(Mass.NO_MASS_MARKER) && !this.getCalculatedMass().getMass().equals(Mass.NO_MASS_MARKER)) {
            toReturn = this.getActualMass();
        } else if (this.getActualMass().getMass().equals(Mass.NO_MASS_MARKER) && this.getCalculatedMass().getMass().equals(Mass.NO_MASS_MARKER)) {
            toReturn = this.getCalculatedMass();
        }
        return toReturn;
    }
    
    /**
     * Sets the calculated mass.
     *
     * @param mass the new calculated mass
     * @throws WTException the WT exception
     */
    public void setCalculatedMass(Mass mass) throws WTException {
        if (mass.isCalculated()) {
            calculatedMass = mass;
        } else throw new WTException (WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.EX_NOT_A_CALCULATED_MASS));
    }
    
    /**
     * Sets the actual mass.
     *
     * @param mass the new actual mass
     * @throws WTException the WT exception
     */
    public void setActualMass(Mass mass) throws WTException {
        if (!mass.isCalculated()) {
            actualMass = mass;
        } else throw new WTException (WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.EX_NOT_AN_MANUAL_MASS));
    }
    
    /**
     * Multiply against usage.
     *
     * @param quantity the quantity
     * @throws WTException the WT exception
     */
    public void multiplyAgainstUsage(Double quantity) throws WTException {
        if (calculatedMass != null) {
            calculatedMass.multiply(quantity);
        } if (actualMass != null) {
            actualMass.multiply(quantity);
        }
    }
    
    /**
     * Instantiates a new mass pair.
     *
     * @param massOne the mass one
     * @param massTwo the mass two
     * @param thePart the the part
     * @throws WTException the WT exception
     */
    public MassPair (Mass massOne, Mass massTwo, WTPart thePart) throws WTException {
        if (massOne.isCalculated()) {
            this.calculatedMass = massOne;
            this.actualMass = massTwo;
        } else if (massTwo.isCalculated()) {
            this.calculatedMass = massTwo;
            this.actualMass = massOne;
        }
        if (thePart != null) {
            this.calculatedMass.setPartReference(ObjectReference.newObjectReference(thePart));
            this.actualMass.setPartReference(ObjectReference.newObjectReference(thePart));
        } else throw new WTException(WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.EX_NO_REFERENCED_PART));
    }
    
    /**
     * Gets the calculated mass.
     *
     * @return the calculated mass
     */
    public Mass getCalculatedMass() {
        return calculatedMass;
    }
    
    /**
     * Gets the actual mass.
     *
     * @return the actual mass
     */
    public Mass getActualMass() {
        return actualMass;
    }
  
}
