package com.ikea.ipim.utils.mass;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("com.ikea.ipim.utils.mass.massRollupRB")
public class massRollupRB extends WTListResourceBundle {

    @RBEntry("Mass Rollup Report for Part")
    public static final String PRIVATE_CONSTANT_02 = "massRollup.massRollupReport.title";
    
    @RBEntry("Mass Rollup Report for Part")
    public static final String PRIVATE_CONSTANT_03 = "massRollup.massRollupReport.description";
    
    @RBEntry("Mass Rollup Report for Part")
    public static final String PRIVATE_CONSTANT_04 = "massRollup.massRollupReport.tooltip";
    
    @RBEntry("Mass Rollup Report for Part")
    public static final String ROLLUP_WIZARD_TITLE = "mass.massRollupReport.title";
    
    @RBEntry("Mass Rollup Report for Part: {0}")
    public static final String ROLLUP_FOR_PART_TITLE = "ROLLUP_FOR_PART_TITLE";
    
    @RBEntry("List of parts without actual weight present")
    public static final String ROLLUP_PARTS_WITH_NO_MASS_TABLE = "ROLLUP_PARTS_WITH_NO_MASS_TABLE";
    
    @RBEntry("List of parts without any density present")
    public static final String ROLLUP_PARTS_WITH_NO_DENSITY_TABLE = "ROLLUP_PARTS_WITH_NO_DENSITY_TABLE";
    
    @RBEntry("List of parts without any volume present")
    public static final String ROLLUP_PARTS_WITH_NO_VOLUME_TABLE = "ROLLUP_PARTS_WITH_NO_VOLUME_TABLE";
    
    @RBEntry("List of parts without an associated Part for density present")
    public static final String ROLLUP_PARTS_WITH_NO_ASSOCIATED_MATERIAL_TABLE = "ROLLUP_PARTS_WITH_NO_ASSOCIATED_MATERIAL_TABLE";
    
    @RBEntry("List of parts without an associated CAD for volume present")
    public static final String ROLLUP_PARTS_WITH_NO_ASSOCIATED_VOLUME_TABLE = "ROLLUP_PARTS_WITH_NO_ASSOCIATED_CAD_FOR_VOLUME_TABLE";
    
    @RBEntry("Multi-Level BoM Report With Mass")
    @RBComment("This string is used for the Multi-Level BoM Report With additional mass calculation label in the jsp page")
    public static final String MULTI_LEVEL_BOM_WITH_MASS_LABEL = "MULTI_LEVEL_BOM_WITH_MASS_LABEL";
    
    @RBEntry("Multi-Level BoM Report With Mass")
    public static final String PSB_MASSREPORT_DESCRIPTION = "psb.psbReportMultiLevelBOMWithMassGWT.description";
     
    @RBEntry("Multi-Level BoM Report With Mass")
    public static final String PSB_MASSREPORT_TOOLTIP = "psb.psbReportMultiLevelBOMWithMassGWT.tooltip";
 
    @RBEntry("Multi-Level BoM Report With Mass")
    public static final String PSB_MASSREPORT_TITLE = "psb.psbReportMultiLevelBOMWithMassGWT.title";
    
    @RBEntry("Default With Masses")
    public static final String DEFAULT_WITH_MASSES = "DEFAULT_WITH_MASSES";
    
    @RBEntry("View default for the Multi-Level BOM with masses")
    public static final String DEFAULT_WITH_MASSES_DESCRIPTION = "DEFAULT_WITH_MASSES_DESCRIPTION";
    
    
    @RBEntry("Referenced oid does not point to an instance of WTPart!")
    public static final String EX_INVALID_OID = "EX_INVALID_OID";
    
    @RBEntry("The Mass object comming to the method IS NULL!")
    public static final String EX_NULL_MASS = "EX_NULL_MASS";
    
    @RBEntry("The MassPair object comming to the method IS NULL!")
    public static final String EX_NULL_MASS_PAIR = "EX_NULL_MASS_PAIR";
    
    @RBEntry("Obtained weight: {0} {1}")
    public static final String INFO_CALCULATED_MASS = "INFO_CALCULATED_MASS";
    
    @RBEntry("The specified part has NO VALID MASS PRESENT!")
    public static final String ERROR_NO_CALCULATED_MASS = "ERROR_NO_CALCULATED_MASS";
    
    @RBEntry("The incomming mass object is NOT CALCULATED!")
    public static final String EX_NOT_A_CALCULATED_MASS = "EX_NOT_A_CALCULATED_MASS";
    
    @RBEntry("The incomming mass object is NOT AN ACTUAL one!")
    public static final String EX_NOT_AN_MANUAL_MASS = "EX_NOT_AN_MANUAL_MASS";
    
    @RBEntry("No Part for the mass has been referenced!")
    public static final String EX_NO_REFERENCED_PART = "EX_NO_REFERENCED_PART";
}
