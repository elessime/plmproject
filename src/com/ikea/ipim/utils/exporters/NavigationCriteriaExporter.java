package com.ikea.ipim.utils.exporters;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import wt.fc.ReferenceFactory;
import wt.fc.WTObject;
import wt.filter.NavigationCriteriaHelper;
import wt.inf.container.WTContainerHelper;
import wt.org.OrganizationServicesHelper;
import wt.org.WTUser;
import wt.part.WTPartMaster;
import wt.session.SessionHelper;
import wt.util.WTException;
import wt.util.WTProperties;

public class NavigationCriteriaExporter {
    /** The Constant LOG. */
    private static final Logger LOG = Logger.getLogger(NavigationCriteriaExporter.class);

    /**
     * Instantiates a new navigation criteria exporter.
     */
    private NavigationCriteriaExporter() {}

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: windchill com.philips.cplm.ebommbom.NavigationCriteriaExporter <SavedFilterName> <owner (optional)>");
            return;
        }
        try {
            export(args[0], args.length > 1 ? args[1] : null);
        } catch (Exception e) {
            System.out.println("Error: " + e.getLocalizedMessage());
            LOG.error("Error", e);
        }
    }

    /**
     * Export.
     *
     * @param savedNcName the saved nc name
     * @param owner the owner
     * @throws WTException the WT exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void export(String savedNcName, String owner) throws WTException, IOException {
        WTObject ownerObj = null;
        if (owner != null) {
            WTUser usr = OrganizationServicesHelper.manager.getUser(owner);
            if (usr != null) {
                ownerObj = usr;
            } else {
                System.out.println("Couldn't find specified owner: " + owner + ". Can you check the specified input, please?");
                return;
            }
        } else {
            ownerObj = SessionHelper.manager.getPrincipal();
        }
        String nc_json = null;
        ReferenceFactory rf = new ReferenceFactory();
        ArrayList<String> obj2exp = new ArrayList<String>();
        JSONObject ncJSOs = new JSONObject();
        String expFn = null;
        try {
            nc_json =
                    NavigationCriteriaHelper.service.getJSONFromNavigationCriteria(NavigationCriteriaHelper.service.getNavigationCriteria(savedNcName,
                            ownerObj, true));

            // we could try to xmlize the json but what is the added value?
            ncJSOs = new JSONObject(nc_json);
            JSONArray csJSs = ncJSOs.getJSONArray("configSpecs");
            for (int i = 0; i < csJSs.length(); i++) {
                JSONObject nccs = csJSs.getJSONObject(i);
                // now extract effectiveContext oid to replace by Name (and
                // CONT_PATH)!?
                JSONObject efCtx = nccs.optJSONObject("effectiveContext");
                if (efCtx != null) {
                    String oid = efCtx.getString("oid");
                    obj2exp.add(oid);
                    WTPartMaster pm = (WTPartMaster) rf.getReference(oid).getObject();
                    String name = pm.getName();
                    String contPath = WTContainerHelper.getPath(pm.getContainerReference());
                    JSONObject ecOidObj = new JSONObject();
                    ecOidObj.put("name", name);
                    ecOidObj.put("CONT_PATH", contPath);
                    efCtx.put("obj", ecOidObj);
                    System.out.println("Please include the following Part in your loadFiles: Part Number: " + pm.getNumber());
                }
                // now extract view oid to replace by name
                JSONObject view = nccs.optJSONObject("view");
                if (view != null) {
                    String viewname = view.getString("label");
                    System.out.println("Please include the following View in your loadFiles: " + viewname);
                }
            }
            String wttemp = WTProperties.getLocalProperties().getProperty("wt.temp");
            File expF = new File(wttemp, "NavigationCriteriaExport_" + URLEncoder.encode(savedNcName, "UTF-8") + ".json");
            expFn = expF.getAbsolutePath();
            PrintWriter pw = new PrintWriter(expF);
            pw.print(ncJSOs.toString(3));
            pw.flush();
            pw.close();
        } catch (Exception wtex) {
            throw new WTException(wtex);
        }
        System.out.println("Exported Saved Filter: '" + savedNcName + "' to file: " + expFn);
    }

}
