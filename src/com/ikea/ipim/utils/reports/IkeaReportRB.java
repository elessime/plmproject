package com.ikea.ipim.utils.reports;

import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("com.ikea.ipim.utils.reports.IkeaReportRB")
public class IkeaReportRB  extends WTListResourceBundle {

    @RBEntry("Equivalent Parts")
    public static final String PRIVATE_CONSTANT_03 = "object.upstreamDownstreamRoleTab.description";

    @RBEntry("Equivalent Parts")
    public static final String PRIVATE_CONSTANT_04 = "object.upstreamDownstreamRoleTab.title";
    
    @RBEntry("Downstream Equivalent Parts")
    public static final String PRIVATE_CONSTANT_05 = "ikeaPart.downstreamRoleTabForPart.description";

    @RBEntry("Downstream Equivalent Parts")
    public static final String PRIVATE_CONSTANT_06 = "ikeaPart.downstreamRoleTabForPart.title";

    @RBEntry("Upstream Equivalent Parts")
    public static final String PRIVATE_CONSTANT_07 = "ikeaPart.upstreamRoleTabForPart.description";

    @RBEntry("Upstream Equivalent Parts")
    public static final String PRIVATE_CONSTANT_08 = "ikeaPart.upstreamRoleTabForPart.title";
    
    @RBEntry("Downstream Equivalent Parts")
    public static final String DOWNSTREAM_PARTS = "DOWNSTREAM_PARTS";

    @RBEntry("Upstream Equivalent Parts")
    public static final String UPSTREAM_PARTS = "UPSTREAM_PARTS";
    
}
