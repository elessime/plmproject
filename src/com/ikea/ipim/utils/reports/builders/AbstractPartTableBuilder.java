package com.ikea.ipim.utils.reports.builders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ikea.ipim.utils.types.IkeaPartUtils;
import com.ptc.core.components.descriptor.DescriptorConstants;
import com.ptc.core.htmlcomp.components.AbstractConfigurableTableBuilder;
import com.ptc.core.htmlcomp.tableview.ConfigurableTable;
import com.ptc.jca.mvc.components.JcaColumnConfig;
import com.ptc.jca.mvc.components.JcaComponentParams;
import com.ptc.mvc.components.ColumnConfig;
import com.ptc.mvc.components.ComponentBuilder;
import com.ptc.mvc.components.ComponentConfig;
import com.ptc.mvc.components.ComponentConfigFactory;
import com.ptc.mvc.components.ComponentParams;
import com.ptc.mvc.components.TableConfig;
import com.ptc.netmarkets.util.beans.NmCommandBean;
import com.ptc.netmarkets.util.beans.NmHelperBean;

import wt.associativity.EquivalenceLink;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.part.WTPart;
import wt.util.WTException;
import wt.vc.config.LatestConfigSpec;
import com.ptc.windchill.enterprise.part.views.DocumentsPartsDescTableViews;

@ComponentBuilder("com.ikea.ipim.utils.reports.builders.AbstractPartTableBuilder")
public abstract class AbstractPartTableBuilder  extends AbstractConfigurableTableBuilder {

    private static final String OBJECT = "object";
    private static final String VIEW = "view";
    private static final String TABLE_LABEL = "Abstract table label";
    private static final String ITERATION_DISPLAY_IDENTIFIER = "iterationDisplayIdentifier";

    @Override
    public ConfigurableTable buildConfigurableTable(String arg0) throws WTException {
        return new DocumentsPartsDescTableViews();
    }

    @Override
    public ComponentConfig buildComponentConfig(ComponentParams arg0) throws WTException {
        ComponentConfigFactory factory = getComponentConfigFactory();

        TableConfig bomTable = factory.newTableConfig();
        bomTable.setType(WTPart.class.getName());
        bomTable.setConfigurable(true);
        bomTable.setLabel(TABLE_LABEL);
        bomTable.setSelectable(true);
        bomTable.setShowCount(true);
        bomTable.setSingleViewOnly(true);

        bomTable.addComponent(factory.newColumnConfig(DescriptorConstants.ColumnIdentifiers.ICON, false));
        bomTable.addComponent(factory.newColumnConfig(DescriptorConstants.ColumnIdentifiers.NUMBER, false));
        bomTable.addComponent(factory.newColumnConfig(DescriptorConstants.ColumnIdentifiers.NAME, false));
        bomTable.addComponent(factory.newColumnConfig(DescriptorConstants.ColumnIdentifiers.INFO_ACTION, false));

        ColumnConfig versionColumn = factory.newColumnConfig(DescriptorConstants.ColumnIdentifiers.VERSION, false);
        versionColumn.setNeed(ITERATION_DISPLAY_IDENTIFIER);
        bomTable.addComponent(versionColumn);

        ColumnConfig nmactionsColumn = factory.newColumnConfig(DescriptorConstants.ColumnIdentifiers.NM_ACTIONS, false);
        ((JcaColumnConfig) nmactionsColumn).setDescriptorProperty(DescriptorConstants.ActionProperties.ACTION_NAME, VIEW);
        ((JcaColumnConfig) nmactionsColumn).setDescriptorProperty(DescriptorConstants.ActionProperties.OBJECT_TYPE, OBJECT);
        bomTable.addComponent(nmactionsColumn);

        bomTable.addComponent(factory.newColumnConfig(DescriptorConstants.ColumnIdentifiers.NAME, false));
        bomTable.addComponent(factory.newColumnConfig(DescriptorConstants.ColumnIdentifiers.CONTAINER_NAME, false));
        bomTable.addComponent(factory.newColumnConfig(DescriptorConstants.ColumnIdentifiers.STATE, false));
        bomTable.addComponent(factory.newColumnConfig(DescriptorConstants.ColumnIdentifiers.LAST_MODIFIED, false));

        return bomTable;
    }

    protected List<WTPart> getPartComponentDataByQuery(ComponentParams params, String direction) throws WTException {
        List<WTPart> parts = new ArrayList<>();

        NmHelperBean helper = ((JcaComponentParams) params).getHelperBean();
        NmCommandBean commandBean = helper.getNmCommandBean();
        Persistable object = (Persistable) commandBean.getPageOid().getReferencedIterationObject();

        QueryResult result = PersistenceHelper.manager.navigate(object, direction, EquivalenceLink.class, true);
        if (direction.equals(EquivalenceLink.UPSTREAM_ROLE)) {
            LatestConfigSpec configSpec = new LatestConfigSpec();
            result = configSpec.process(result);
        } else if (direction.equals(EquivalenceLink.DOWNSTREAM_ROLE)) {
            HashMap<String, WTPart> identifiedParts = new HashMap<String, WTPart>();
            while (result.hasMoreElements()) {
                WTPart partToIdentify = (WTPart) result.nextElement();
                String partKeyInMap = IkeaPartUtils.getPartIdentifier(partToIdentify);
                if (!identifiedParts.containsKey(partKeyInMap)) {
                    identifiedParts.put(partKeyInMap, partToIdentify);
                } else {
                    WTPart partAlreadyInMap = identifiedParts.get(partKeyInMap);
                    WTPart obtainedPart = IkeaPartUtils.getNewerPart(partAlreadyInMap, partToIdentify);
                    identifiedParts.put(partKeyInMap, obtainedPart);
                }
            }
            result = new QueryResult();
            for (WTPart toAdd:identifiedParts.values()) {
                result.getObjectVector().addElement(toAdd);
            }
        }

        while (result != null && result.hasMoreElements()) {
            parts.add((WTPart) result.nextElement());
        }

        return parts;
    }
    
 
}
