package com.ikea.ipim.utils.reports.builders;

import com.ikea.ipim.utils.reports.IkeaReportRB;
import com.ptc.mvc.components.ComponentBuilder;
import com.ptc.mvc.components.ComponentConfig;
import com.ptc.mvc.components.ComponentParams;

import wt.associativity.EquivalenceLink;
import wt.util.WTException;
import wt.util.WTMessage;

@ComponentBuilder("com.ikea.ipim.utils.reports.builders.DownstreamRoleLinkBuilder")
public class DownstreamRoleLinkBuilder extends AbstractPartTableBuilder {
    protected static final String RESOURCE = com.ikea.ipim.utils.reports.IkeaReportRB.class.getName();
    @Override
    public Object buildComponentData(ComponentConfig arg0, ComponentParams params) throws Exception {
        return getPartComponentDataByQuery(params, EquivalenceLink.DOWNSTREAM_ROLE);
    }

    @Override
    public ComponentConfig buildComponentConfig(ComponentParams arg0) throws WTException {
        ComponentConfig config = super.buildComponentConfig(arg0);
        config.setLabel(WTMessage.getLocalizedMessage(RESOURCE, IkeaReportRB.DOWNSTREAM_PARTS));
        return config;
    }
}

