package com.ikea.ipim.utils.navcriteriatools;

import com.ptc.jca.mvc.components.JcaTableConfig;
import com.ptc.mvc.components.AbstractComponentBuilder;
import com.ptc.mvc.components.ColumnConfig;
import com.ptc.mvc.components.ComponentBuilder;
import com.ptc.mvc.components.ComponentConfig;
import com.ptc.mvc.components.ComponentConfigFactory;
import com.ptc.mvc.components.ComponentParams;

import wt.effectivity.EffectivityHelper;
import wt.effectivity.WTDatedEffectivity;
import wt.fc.ObjectReference;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.fc.collections.WTArrayList;
import wt.filter.NavigationCriteria;
import wt.filter.NavigationCriteriaHelper;
import wt.part.WTPart;
import wt.part.WTPartMaster;
import wt.pds.StatementSpec;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.util.WTException;

@ComponentBuilder("com.ikea.ipim.utils.navcriteriatools.NavCritBuilder")
public class NavCritBuilder extends AbstractComponentBuilder {

    @Override
    public Object buildComponentData(ComponentConfig compConfig, ComponentParams compParams) throws Exception {
        WTArrayList navCrit = new WTArrayList();
        Object contextObject = compParams.getContextObject();
        if (contextObject instanceof WTPart) {
            WTPart thePart = (WTPart) contextObject;
            QueryResult effects = EffectivityHelper.service.getEffectivities(thePart);
            while (effects.hasMoreElements()) {
               Object efftoCheck = effects.nextElement();
               if (efftoCheck instanceof WTDatedEffectivity) {
                   WTDatedEffectivity theDateEffect = ((WTDatedEffectivity)efftoCheck);
                   ObjectReference effReference = theDateEffect.getEffContextReference();
                   WTPartMaster test = (WTPartMaster) (effReference.getObject());
                   String contName = test.getName();
                   QueryResult navCritsResult = getNavCriteriaByContextName(contName);
                   while (navCritsResult.hasMoreElements()) {
                       navCrit.add(navCritsResult.nextElement());
                   }
                   
               }
            }
        }
      return navCrit;
    }

    @Override
    public ComponentConfig buildComponentConfig(ComponentParams arg0) throws WTException {
        ComponentConfigFactory factory = getComponentConfigFactory();
        JcaTableConfig table = (JcaTableConfig) factory.newTableConfig();
        table.setId("com.ikea.ipim.utils.navcriteriatools.NavCritBuilder");
        table.setLabel("List of related Navigation Criteria");
        table.setConfigurable(false);
        table.setSelectable(true);
        table.setSingleSelect(true);
        
        ColumnConfig colPrimar = factory.newColumnConfig("name", false);
        colPrimar.setLabel("Name");
        table.addComponent(colPrimar);

        
        return table;
    }
    
    private static QueryResult getNavCriteriaByContextName(String name) throws WTException {
        QuerySpec qs = new QuerySpec(NavigationCriteria.class);
        qs.appendWhere(
         new SearchCondition(NavigationCriteria.class, NavigationCriteria.NAME, SearchCondition.LIKE, name + "%"),
        new int[] { 0 });
      QueryResult qr = PersistenceHelper.manager.find((StatementSpec) qs);
      return qr;
    }

}
