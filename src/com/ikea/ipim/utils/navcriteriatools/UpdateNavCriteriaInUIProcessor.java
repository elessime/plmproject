package com.ikea.ipim.utils.navcriteriatools;

import java.util.ArrayList;
import java.util.List;

import com.ptc.core.components.beans.ObjectBean;
import com.ptc.core.components.forms.DefaultObjectFormProcessor;
import com.ptc.core.components.forms.FormProcessingStatus;
import com.ptc.core.components.forms.FormResult;
import com.ptc.expansionui.client.ExpansionCriteriaController;
import com.ptc.expansionui.client.ui.beans.ExpansionCriteriaBean;
import com.ptc.expansionui.server.ExpansionCriteriaServiceImpl;
import com.ptc.expansionui.server.UpdateNavCriteriaDelegate;
//import com.ptc.expansionui.server.UpdateNavCriteriaDelegateFactory;
import com.ptc.netmarkets.model.NmOid;
import com.ptc.netmarkets.util.beans.NmCommandBean;
import com.ptc.netmarkets.util.misc.NmContext;

import wt.fc.ReferenceFactory;
import wt.fc.WTReference;
import wt.filter.NavCriteriaContext;
import wt.filter.NavigationCriteria;
//import wt.filter.NavigationCriteriaConstants;
import wt.filter.NavigationCriteriaHelper;
import wt.part.WTPart;
import wt.util.WTException;
import wt.vc.config.ConfigSpec;

public class UpdateNavCriteriaInUIProcessor extends DefaultObjectFormProcessor {

    @Override
    public FormResult doOperation(NmCommandBean clientData, List<ObjectBean> objectList) throws WTException {
        FormResult result = new FormResult();
        result.setStatus(FormProcessingStatus.SUCCESS);
        NmOid partOid = clientData.getActionOid();
        Object toCheck = partOid.getRefObject();
        ArrayList theSelectedNavCrit = clientData.getSelected();
        if (theSelectedNavCrit != null || !theSelectedNavCrit.isEmpty()) {
            NmContext theSelectedNavCritContext = (NmContext) theSelectedNavCrit.get(0);
            String navCritOir = theSelectedNavCritContext.getElemAddressStr();
            navCritOir = navCritOir.replaceAll("!\\*", "");
            ReferenceFactory refFact = new ReferenceFactory();
           WTReference navCritRef =  refFact.getReference(navCritOir);
           NavigationCriteria crit = (NavigationCriteria) navCritRef.getObject();
           List<ConfigSpec> theList = crit.getConfigSpecs();
           if (theList != null || !theList.isEmpty()) {
               ConfigSpec theSpec = theList.get(0);
               ExpansionCriteriaBean bean = new ExpansionCriteriaBean();
              String navCritJson = NavigationCriteriaHelper.service.getJSONFromNavigationCriteria(crit);
              bean.setNavCriteriaJSONString(navCritJson);
//              UpdateNavCriteriaDelegateFactory factory = new UpdateNavCriteriaDelegateFactory();
//              UpdateNavCriteriaDelegate delegate = factory.getDelegate(NavigationCriteriaConstants.CONFIGURATION_CONTEXT_APP_NAME);
              NavCriteriaContext context = new NavCriteriaContext();
              context.setApplicableType(WTPart.class);
//              NavigationCriteria newCrit = delegate.updateNavigationCriteria(crit, context);
//              newCrit.getClass();
           }
        }
        return result;
    }
}
