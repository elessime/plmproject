package com.ikea.ipim.utils.navcriteriatools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.ikea.ipim.utils.types.IkeaPartUtils;

import wt.eff.format.EffType;
import wt.filter.NavigationCriteria;
import wt.filter.NavigationCriteriaHelper;
import wt.method.RemoteAccess;
import wt.method.RemoteMethodServer;
import wt.org.WTPrincipal;
import wt.part.WTPart;
import wt.part.WTPartConfigSpec;
import wt.part.WTPartEffectivityConfigSpec;
import wt.part.WTPartMaster;
import wt.session.SessionHelper;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;
import wt.vc.views.View;
import wt.vc.views.ViewHelper;

public class NavCriteriaCreationHelper implements RemoteAccess, Serializable  {

    private static final long serialVersionUID = 8601413609794439153L;

    protected static Logger logger = Logger.getLogger(NavCriteriaCreationHelper.class.getName());
    
    private static BufferedReader bufferedread = new BufferedReader(new InputStreamReader(System.in));
    private static String[] credentials = new String[2];
    private static String[] info = new String[3];
    private static RemoteMethodServer remoteMethodServer;
    
    
    public static void createNavigationCriteria(HashMap<String, Object> values) throws WTPropertyVetoException, WTException, ParseException {
        NavigationCriteria navCriteriaToCreate = NavigationCriteria.newNavigationCriteria();
        navCriteriaToCreate.setApplicableType(WTPart.class.getName());
        navCriteriaToCreate.setApplyToTopLevelObject(false);
        navCriteriaToCreate.setCentricity(false);
//        navCriteriaToCreate.setHideUnresolvedDependents(false);
        navCriteriaToCreate.setName(((WTPartMaster)values.get("EFF_CONTEXT_OBJECT")).getName() + " - " + values.get("EFF_TIMESTAMP"));
        navCriteriaToCreate.setSharedToAll(false);
        navCriteriaToCreate.setUseDefaultForUnresolved(false);
        WTPartConfigSpec theSpec = getPartConfigSpec(values);
        navCriteriaToCreate.setConfigSpec(theSpec);
        NavigationCriteriaHelper.service.saveNavigationCriteria(navCriteriaToCreate);
    }
    
    private static WTPartConfigSpec getPartConfigSpec (HashMap<String, Object> values) throws WTException, WTPropertyVetoException, ParseException {
      SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
      Date parsedDate = dateFormat.parse((String) values.get("EFF_TIMESTAMP"));
      WTPartEffectivityConfigSpec effectConfigSpec = WTPartEffectivityConfigSpec.newWTPartEffectivityConfigSpec();
      effectConfigSpec.setEffType(EffType.DATE.toString());
      effectConfigSpec.setEffectiveDate(new Timestamp(parsedDate.getTime()));
      effectConfigSpec.setEffectiveProduct((WTPartMaster) values.get("EFF_CONTEXT_OBJECT"));
      effectConfigSpec.setView((View) values.get("EFF_VIEW"));
      WTPartConfigSpec toReturn = WTPartConfigSpec.newWTPartConfigSpec(effectConfigSpec);
      return toReturn;
    }
    
    public static void doCreationOfCriteria (String [] userData) throws WTException, WTPropertyVetoException, ParseException {
        WTPart effContext = IkeaPartUtils.getWTPartByNumber(userData[0]);
        if (effContext == null) {
            logger.error("No effectivity context part found for the following number " + userData[0]);
            return;
        }
        View theView = ViewHelper.service.getView(userData[1]);
        if (theView == null) {
            logger.error("No view found for the following name " + userData[1]);
            return;
        }
       String effDate = userData[2];
       HashMap<String, Object> values = new HashMap<>();
       values.put("EFF_TIMESTAMP", effDate);
       values.put("EFF_CONTEXT_OBJECT", effContext.getMaster());
       values.put("EFF_VIEW", theView);
       createNavigationCriteria(values);
    }
    
    public static void main(String[] args) {
        try {
            if (args.length == 0 || args == null) {
                printUsage();
                provideData(true);
                authenticateAndRun();
            } else {
                if (Arrays.asList(args).contains("help")) {
                    printUsage();
                    return;
                }
                if (!validateArgs(args)) {
                    System.out.println("Improper number of parameters passed to the tool! To see the proper use, please run the tool with the 'help' option.");
                    return;
                } else {
                    credentials[0] = args[0].trim();
                    credentials[1] = args[1].trim();
                    info[0] = args[2].trim();
                    info[1] = args[3].trim();
                    info[2] = args[4].trim();
                    authenticateAndRun();
                }
            }
        } catch (Exception ex) {
            logger.error("Authentication failed! \n" + NavCriteriaCreationHelper.class.getName() + " ends with status 1");
            logger.error(" Exception :");
            ex.printStackTrace();
            System.exit(1);
        }
    }
    
    private static void authenticateAndRun() {
        remoteMethodServer = RemoteMethodServer.getDefault();
        authenticate();
        String returnedMessage = "";
        try {
            System.out.println("Now creating and persisting the Navigation Criteria. Please observe the MethodServer for output.");
            remoteMethodServer.invoke( "doCreationOfCriteria", NavCriteriaCreationHelper.class.getName(), null, new Class[] { String[].class }, new Object[] { info });
            System.out.println(returnedMessage);
        } catch (RemoteException | InvocationTargetException e) {
            logger.error("Execution of Navigation Criteria Filter generation failed! " + e.getLocalizedMessage() + "\n" + NavCriteriaCreationHelper.class.getName() + " ends with status 1");
            logger.error(" Exception :");
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    private static void getCredentials() throws IOException {
        String uname = getInputFromUser("Login");
        String upass = getInputFromUser("Password");
        credentials[0] = uname.trim();
        credentials[1] = upass.trim();
    }

    private static void getEffectivityData() throws IOException {
        String effContext = getInputFromUser("Effectivity context number");
        String viewName = getInputFromUser("View name");
        String effDate = getInputFromUser("Effectivity date");
        info[0] = effContext.trim();
        info[1] = viewName.trim();
        info[2] = effDate.trim();
    }

    private static void provideData(boolean additionalInfoRequested) throws Exception {
        System.out.println("Since no parameters were passed in the command line, now You will be asked to enter your \ncredentials and the data needed to perform a load of the projects from a CSV file. \n\nHit enter to continue... .");
        bufferedread.readLine();
        getCredentials();
        if (additionalInfoRequested) {
            getEffectivityData();
        }

    }

    private static void authenticate() {
        remoteMethodServer.setUserName(credentials[0]);
        remoteMethodServer.setPassword(credentials[1]);
        try {
            WTPrincipal currentUser = SessionHelper.manager.getPrincipal();
            WTPrincipal wtAdministrator = SessionHelper.manager.getAdministrator();
            if (!currentUser.equals(wtAdministrator)) {
                logger.error("Invalid user! " + NavCriteriaCreationHelper.class.getName() + " may be launched by Windchill Administrator only \n" + NavCriteriaCreationHelper.class.getName() + " ends with status 1");
                System.exit(1);
            }
        } catch (WTException e) {
            logger.error("Authentication failed! " + e.getLocalizedMessage() + "\n" + NavCriteriaCreationHelper.class.getName() + " ends with status 1");
            logger.error(" Exception :");
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static String getInputFromUser(String whatToTake) throws IOException {
        System.out.println("Enter " + whatToTake + ": ");
        String toReturn = bufferedread.readLine().trim();
        if (toReturn.trim().isEmpty()) {
            System.out.println("The parameter value " + whatToTake + " was not entered!");
            return getInputFromUser(whatToTake);
        } if (whatToTake.equals("Effectivity date")) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            try {
                Date parsedDate = dateFormat.parse((toReturn));
            } catch (ParseException ex) {
                System.out.println("THe specified date could not be parsed - ensure it is in the MM/dd/yyyy format!");
                return getInputFromUser(whatToTake);
            }
          
           }
        return toReturn.trim();
    }
   
    private static void printUsage() {
        System.out.println("This tool will genarate and persists a date effectivity Navigation Criteria filter based on the user-provided data.\n");
        System.out.println("Proper usage of NavCriteria generator tool: ");
        System.out.println("windchill windchill com.ikea.ipim.utils.navcriteriatools.NavCriteriaCreationHelper user_login user_password NUMBER_OF_EFF_CONTEXT VIEW_NAME EFFECTIVITY_DATE(in mm/dd/yyyy format)");
        System.out.println("where:");
        System.out.println("user_login - the login of the user from Windchill system");
        System.out.println("user_password - the password that is a valid Windchill password for the user");
        System.out.println("NUMBER_OF_EFF_CONTEXT - the number of the part to be used as the effectivity context");
        System.out.println("VIEW_NAME - name of the BOM view to be used in the filter");
        System.out.println("EFFECTIVITY_DATE - the effectivity date to be set in filter (in mm/dd/yyyy format)\n\n");
        
    }

    private static boolean validateArgs(String args[]) {
        if (args.length == 5) {
            return true;
        } else {
            return false;
        }
    }
    
}
