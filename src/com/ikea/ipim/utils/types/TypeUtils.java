package com.ikea.ipim.utils.types;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ikea.ipim.properties.IkeaProperties;
import com.ptc.core.meta.common.TypeIdentifier;
import com.ptc.windchill.esi.tgt.ESITarget;

import wt.fc.Persistable;
import wt.fc.WTObject;
import wt.log4j.LogR;
import wt.part.WTPart;
import wt.type.ClientTypedUtility;
import wt.util.WTException;

public class TypeUtils {

    private static final Logger log = LogR.getLogger(TypeUtils.class.getName());
    private static final String PACKAGE_TYPE = "com.ikea.Packaging";
    private static final String BOX_TYPE = "com.ikea.Box";
    private static final String COLOR_TYPE = "com.ikea.IKEACoating";
    private static final String MATERIAL_TYPE = "com.ikea.IKEAMaterial";
    private static final String AZURE_DT_TYPE = "com.ikea.AzureFileDistributionTarget";
    private static final String KAFKA_DT_TYPE = "com.ikea.KafkaFileDistributionTarget";
    private static final String IKEA_PART_TYPE = "com.ikea.IKEAPart";
    private static final String MANUFACTURER_PART_TYPE = "com.ptc.windchill.suma.part.ManufacturerPart";
    private static final String VENDOR_PART_TYPE = "com.ptc.windchill.suma.part.VendorPart";
    
    /**
     * Gets the short type name from object.
     *
     * @param obj
     *            the object
     * @return the short type name
     */
    public static String getShortTypeName(Object obj) {
        String fullTypeName = getTypeName(obj);
        return getShortTypeName(fullTypeName);
    }

    /**
     * Gets the short type name from full type name.
     *
     * @param fullTypeName
     *            the full type name
     * @return the short type name
     */
    public static String getShortTypeName(String fullTypeName) {
        String shortTypeName = fullTypeName;
        int index = fullTypeName.lastIndexOf("|") + 1;
        if (index >= 0) {
            shortTypeName = fullTypeName.substring(index);
        }
        return shortTypeName;
    }
    
    /**
     * Get full type name of given object
     *
     * @param object
     *            the object to check it's type
     * @return a type name. If type cannot be retrieved or object is null then empty
     *         string is returned
     */
    public static String getTypeName(Object object) {
        if (log.isDebugEnabled()) {
            log.debug("START - getTypeName for " + object);
        }
        String typename = "";
        if (object != null) {
            try {
                typename = StringUtils.defaultString(getTypeIdentifier(object).getTypename());
            } catch (WTException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("END - getTypeName for " + object +" Returned value: " + typename);
        }
        return typename;
    }

    public static TypeIdentifier getTypeIdentifier(Object object) throws WTException {
        return ClientTypedUtility.getTypeIdentifier(object);
    }
    
    public static boolean isWTPartPackage(WTPart part) {
        return isTypeOf(part, PACKAGE_TYPE);
    }
    
    public static boolean isWTPartBox(WTPart part) {
        return isTypeOf(part, BOX_TYPE);
    }
    
    public static boolean isDTargetKafka(ESITarget target) {
        return isTypeOf(target, KAFKA_DT_TYPE);
    }
    
    public static boolean isDTargetAzure(ESITarget target) {
        return isTypeOf(target, AZURE_DT_TYPE);
    }
    
    public static boolean isColorPart(WTPart part) {
        return isTypeOf(part, COLOR_TYPE);
    }
    
    public static boolean isMaterialPart(WTPart part) {
        return isTypeOf(part, MATERIAL_TYPE);
    }
    
    public static boolean isIkeaPart(WTPart part) {
        return isTypeOf(part, IKEA_PART_TYPE);
    }
    
    public static boolean isManufacturerPart(WTPart part) {
        return isTypeOf(part, MANUFACTURER_PART_TYPE);
    } 
    public static boolean isVendorPart(WTPart part) {
        return isTypeOf(part, VENDOR_PART_TYPE);
    }
    /**
     * Check if the object is a subtype of a given type
     *
     * @param object
     *            being checked
     * @return true if it's subtype instance
     */
    public static <T extends WTObject> boolean isTypeOf(T object, String subtypeName) {
        return getShortTypeName(object).equals(subtypeName);
    }
    
    
    public static boolean isOfReadOnlyShareTypes(Persistable persistToCheck) {
        boolean toReturn = false;
        List<String> listOfTypes = IkeaProperties.getProperties().getListProperty("com.ikea.sbom.listOfReadOnlyShareTypes", "com.ikea.IKEACoating,com.ikea.IKEAMaterial,com.ptc.windchill.suma.part.ManufacturerPart,com.ptc.windchill.suma.part.VendorPart");
        String fullTypeName = getTypeName(persistToCheck);
        for (String theType: listOfTypes) {
            if (fullTypeName.contains(theType)) {
                toReturn = true;
                break;
            }
        }
        return toReturn;
    }
}
