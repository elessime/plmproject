package com.ikea.ipim.utils.types;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.ikea.ipim.properties.IkeaProperties;
import com.ikea.ipim.sbom.IkeaSBOMRB;
import com.ikea.ipim.sbom.utils.SBOMTransformHelper;
import com.ptc.core.components.util.FeedbackMessage;
import com.ptc.core.ui.resources.FeedbackType;
import com.ptc.windchill.associativity.bll.AssociativeEquivalenceExecutionReport;
import com.ptc.windchill.associativity.service.AssociativityServiceLocator;
import com.ptc.windchill.associativity.transform.ClassicAssociativeTransformationParam;
import com.ptc.windchill.associativity.transform.ClassicAssociativeTransformationResult;
import com.ptc.windchill.associativity.transform.TransformService;
import com.ptc.windchill.associativity.transform.TransformationContext;
import com.ptc.windchill.associativity.transform.exceptions.TransformException;
import com.ptc.windchill.suma.part.ManufacturerPart;
import com.ptc.windchill.suma.part.ManufacturerPartMaster;
import com.ptc.windchill.suma.part.VendorPart;
import com.ptc.windchill.suma.part.VendorPartMaster;

import wt.associativity.Associative;
import wt.associativity.EquivalenceLink;
import wt.dataops.sandbox.sandboxResource;
import wt.enterprise.CopyObjectInfo;
import wt.enterprise.EnterpriseHelper;
import wt.enterprise.RevisionControlled;
import wt.fc.ObjectReference;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.PersistenceServerHelper;
import wt.fc.QueryResult;
import wt.fc.collections.WTArrayList;
import wt.fc.collections.WTCollection;
import wt.fc.collections.WTSet;
import wt.folder.Folder;
import wt.folder.FolderHelper;
import wt.inf.container.WTContainerRef;
import wt.inf.sharing.DataSharingHelper;
import wt.inf.sharing.SharedContainerMap;
import wt.org.WTOrganization;
import wt.part.WTPart;
import wt.part.WTPartMaster;
import wt.part.WTPartUsageLink;
import wt.pds.StatementSpec;
import wt.projmgmt.admin.Project2;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.sandbox.SandboxHelper;
import wt.session.SessionHelper;
import wt.session.SessionServerHelper;
import wt.util.WTException;
import wt.util.WTMessage;
import wt.util.WTPropertyVetoException;
import wt.util.WTRuntimeException;
import wt.vc.views.Variation2;
import wt.vc.views.View;
import wt.vc.wip.WorkInProgressHelper;
import wt.vc.wip.Workable;

public class IkeaProjectUtils {
    private static Logger logger = Logger.getLogger(IkeaProjectUtils.class);
    private static final String PART_NOT_AUTHORISED = "partToShareIsNotAuthorised";
    private static final String PART_NOT_CHECKED_OUT = "partNotCheckedOut";
    private static final String PART_CHECKED_OUT = "partCheckedOut";
    private static final String PART_CHECKOUT_EXCEPTION = "partCheckOutException";
    private static final String PART_TRANSFORMATION_EXCEPTION = "partTransformationException";
    private static final String AFTER_TRANSFORM_PART_EXCEPTION = "afterTransformBOMException";
    private static final String SBOM_RESOURCE = IkeaSBOMRB.class.getName();
    private static final String RESOURCE = "wt.dataops.sandbox.sandboxResource";

    public static Project2 getProjectByName(String projectName) {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getProjectByName() for the following project name -> " + projectName);
        }
        Project2 project = null;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Will now try to build a query spec for the following project name ->  " + projectName);
            }
            QuerySpec query = new QuerySpec();
            int[] fromIndicies = new int[1];
            fromIndicies[0] = query.addClassList(Project2.class, true);
            SearchCondition sc = new SearchCondition(Project2.class, Project2.NAME, SearchCondition.EQUAL, projectName);
            query.appendWhere(sc, fromIndicies);
            QueryResult qres = PersistenceHelper.manager.find((StatementSpec) query);
            if (qres.hasMoreElements()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Found a project that has a matching name ->  " + projectName);
                }
                Persistable[] pers = (Persistable[]) qres.nextElement();
                project = (Project2) pers[0];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getProjectByName() for the following project name -> " + projectName);
            logger.debug("Returning " + project.getIdentity());
        }
        return project;

    }

    public static Map<ObjectReference, ObjectReference> pdmCheckout(WTCollection iopObjects, Project2 project) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method pdmCheckout() for the following project -> " + project.getIdentity());
            Iterator persistableIterator = iopObjects.persistableIterator();
            while (persistableIterator.hasNext()) {
                Persistable toReport = (Persistable) persistableIterator.next();
                logger.debug("Will share the following object in checkout mode -> " + toReport);
            }
        }
        String checkoutComment = WTMessage.getLocalizedMessage(RESOURCE, sandboxResource.WORKS, null, SessionHelper.getLocale());
        Map<ObjectReference, ObjectReference> projectCheckoutMap = SandboxHelper.service.checkout(iopObjects, WTContainerRef.newWTContainerRef(project), project.getDefaultCabinet(), checkoutComment, true);
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method pdmCheckout() for the following project -> " + project.getIdentity() + ". All objects have been shared!");
        }
        return projectCheckoutMap;
    }

    public static Boolean isPartSharedToProject(WTPart partToCheck, String projectName) throws WTException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method isPartSharedToProject() for the following project name -> " + projectName);
            logger.debug("Part to check is -> " + partToCheck.getIdentity());
        }
        Project2 theProject = getProjectByName(projectName);
        if (theProject == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting method isPartSharedToProject() -> NO Project has been found!");
            }
            return false;
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method isPartSharedToProject() -> returning the value from SandboxHelper.service.isCheckedOutToSandbox()");
        }
        return SandboxHelper.service.isCheckedOutToSandbox(partToCheck, WTContainerRef.newWTContainerRef(theProject));
    }

    public static Boolean isPartInProject(WTPart partToCheck, String projectName) throws WTException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method isPartSharedToProject() for the following project name -> " + projectName);
            logger.debug("Part to check is -> " + partToCheck.getIdentity());
        }
        Project2 theProject = getProjectByName(projectName);
        if (theProject == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting method isPartSharedToProject() -> NO Project has been found!");
            }
            return false;
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method isPartSharedToProject() -> returning the value from SandboxHelper.service.isCheckedOutToSandbox()");
        }
        return SandboxHelper.service.isWorkingCopy(partToCheck);
    }

    public static WTCollection shareBOMToProject(WTPart toShare, Project2 projectToUse) throws WTException {
        ArrayList<String> theAuthorisedSuppliers = IkeaPartUtils.getListOfAuthorisedSuppliers(toShare);
        if (!theAuthorisedSuppliers.contains(projectToUse.getName().replaceAll(" ", ""))) {
            throw new WTException(WTMessage.getLocalizedMessage(SBOM_RESOURCE, PART_NOT_AUTHORISED, new Object[]{toShare.getNumber(), projectToUse.getName()}));
        }
        Folder folder = FolderHelper.service.getFolder("/Default", WTContainerRef.newWTContainerRef(projectToUse));
        WTArrayList listOfChildrenInBOM = IkeaPartUtils.getAllDescendantsWithoutVariation(toShare);
        WTCollection sharedObjects = null;
        boolean checkSuppliersOnChildren = IkeaProperties.getProperties().getProperty("com.ikea.sbom.checkSuppliersForChildren", false);
        if (!checkSuppliersOnChildren) {
            sharedObjects = DataSharingHelper.service.shareVersions(listOfChildrenInBOM, WTContainerRef.newWTContainerRef(projectToUse), folder);
            SandboxHelper.service.addToSandboxBaseline(sharedObjects, WTContainerRef.newWTContainerRef(projectToUse), false);
        } else if (checkSuppliersOnChildren) {
            Iterator<Persistable> iterateOverParts = listOfChildrenInBOM.persistableIterator();
            while (iterateOverParts.hasNext()) {
                Persistable toCheck = iterateOverParts.next();
                if (toCheck instanceof WTPart) {
                    ArrayList<String> AuthorisedSuppliersForChild = IkeaPartUtils.getListOfAuthorisedSuppliers((WTPart) toCheck);
                    if (!AuthorisedSuppliersForChild.contains(projectToUse.getName().replaceAll(" ", ""))) {
                        iterateOverParts.remove();
                    }
                }
            }
            sharedObjects = DataSharingHelper.service.shareVersions(listOfChildrenInBOM, WTContainerRef.newWTContainerRef(projectToUse), folder);
            SandboxHelper.service.addToSandboxBaseline(sharedObjects, WTContainerRef.newWTContainerRef(projectToUse), false);
        }
        return sharedObjects;
    }
    
    public static SharedContainerMap sharePartToProject(WTPart toShare, String projectName) throws WTException {
        Project2 projectToUse = getProjectByName(projectName);
        if (projectToUse != null) {
            Folder folder = FolderHelper.service.getFolder("/Default", WTContainerRef.newWTContainerRef(projectToUse));
            SharedContainerMap toReturn = DataSharingHelper.service.shareObject(toShare, WTContainerRef.newWTContainerRef(projectToUse), folder);
            WTArrayList sharedToBaseline = new WTArrayList();
            sharedToBaseline.add(toShare);
            SandboxHelper.service.addToSandboxBaseline(sharedToBaseline, WTContainerRef.newWTContainerRef(projectToUse), false);
            return toReturn;
        }
        return null;
    }

    public static ArrayList<FeedbackMessage> transformAndCheckoutBOMToProject(WTPart root, Variation2 supplierVariation, View theView) throws WTException, WTPropertyVetoException, WTRuntimeException {
        ArrayList<FeedbackMessage> toReturn = new ArrayList<>();
        List<ClassicAssociativeTransformationParam> params = new ArrayList<>();
        WTArrayList theFoundParts = IkeaPartUtils.getAllDescendantsWithoutVariation(root);
        TransformationContext context = SBOMTransformHelper.getTransformationContext(root, (View) root.getView().getObject(), theView);
        TransformService transformService = (new AssociativityServiceLocator()).getTransformService();
        Iterator iterateOverPartsToTransform = theFoundParts.persistableIterator();
        while (iterateOverPartsToTransform.hasNext()) {
            WTPart toAdd = (WTPart) iterateOverPartsToTransform.next();
            ArrayList<String> authorisedSuppliers = IkeaPartUtils.getListOfAuthorisedSuppliers(toAdd);
            if (TypeUtils.isColorPart(toAdd) || TypeUtils.isMaterialPart(toAdd) || TypeUtils.isManufacturerPart(toAdd) || TypeUtils.isVendorPart(toAdd)) {
                continue;
            }
            boolean checkSuppliersOnChildren = IkeaProperties.getProperties().getProperty("com.ikea.sbom.checkSuppliersForChildren", false);
            if (checkSuppliersOnChildren && !authorisedSuppliers.contains(supplierVariation.getDisplay().replaceAll(" ", ""))) {
                continue;
            }
            if (IkeaPartUtils.getWTPartByNumberAndVariation(toAdd.getNumber(), supplierVariation.getDisplay()) == null) {
                ClassicAssociativeTransformationParam paramsForTransformation = SBOMTransformHelper.getTransformParams(toAdd, (View) toAdd.getView().getObject(), theView, supplierVariation);
                params.add(paramsForTransformation);
            } else {
                logger.info("Part " + root.getNumber() + " ALREADY EXISTS for manufacturer " + supplierVariation.getDisplay() + "! Will skip the creation of the view for it!");
            }
        }
        ClassicAssociativeTransformationResult transformResult = new ClassicAssociativeTransformationResult();
        try {
            for (ClassicAssociativeTransformationParam param : params) {
                List <ClassicAssociativeTransformationParam> toProcess = new ArrayList<>();
                toProcess.add(param);
                ClassicAssociativeTransformationResult transformPartResult = transformService.doClassicAssociativeTransformation(context, toProcess);
                transformResult.merge(transformPartResult);
            }
        } catch (TransformException except) {
            except.printStackTrace();
            toReturn.add(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, PART_TRANSFORMATION_EXCEPTION, new Object[]{root.getNumber(), theView.getName(), supplierVariation.getDisplay(), except.getLocalizedMessage()})));
        }
      
        for (Throwable thrown : transformResult.getExceptions()) {
            toReturn.add(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, AFTER_TRANSFORM_PART_EXCEPTION, new Object[]{thrown.getLocalizedMessage()})));
        }
        Map<Associative, List<Associative>> theLinkedDownstreams = transformResult.getUpstreamToDownstreamsMap();
        Project2 toShareInto = IkeaProjectUtils.getProjectByName(supplierVariation.getDisplay());
        WTArrayList checkoutParts = new WTArrayList();
        WTArrayList shareParts = new WTArrayList();
        Set<Associative> upstreamParts = theLinkedDownstreams.keySet();
        for (Associative theUpstream : upstreamParts) {
            List<Associative> downstreamList = theLinkedDownstreams.get(theUpstream);
            for (Associative theDownstream : downstreamList) {
                if (toShareInto != null) {
                    if (theDownstream instanceof WTPart && (TypeUtils.isIkeaPart((WTPart) theDownstream) || TypeUtils.isWTPartPackage((WTPart) theDownstream) || TypeUtils.isWTPartBox((WTPart) theDownstream)) && isPartInViewAndSupplier(supplierVariation, theView, (WTPart) theDownstream)) {
                        checkoutParts.add((WTPart) theDownstream);
                    } else if (theDownstream instanceof WTPart && TypeUtils.isOfReadOnlyShareTypes(theDownstream)) {
                        shareParts.add(theDownstream);
                    }
                }
            }
        }
        try {
            shareCommonParts(shareParts, supplierVariation.getDisplay());
            Map<ObjectReference, ObjectReference> checkoutMap = IkeaProjectUtils.pdmCheckout(checkoutParts, toShareInto);
            if (!checkoutMap.isEmpty()) {
                toReturn.add(new FeedbackMessage(FeedbackType.SUCCESS, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, PART_CHECKED_OUT, new Object[]{root.getNumber(), theView.getName(), supplierVariation.getDisplay()})));
            } else {
                toReturn.add(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, PART_NOT_CHECKED_OUT, new Object[]{root.getNumber(), theView.getName(), supplierVariation.getDisplay()})));
            }
        } catch (Exception e) {
            e.printStackTrace();
            toReturn.add(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, PART_CHECKOUT_EXCEPTION, new Object[]{root.getNumber(), theView.getName(), supplierVariation.getDisplay(), e.getMessage()})));
        }
        return toReturn;
    }

    private static boolean isPartInViewAndSupplier(Variation2 supplierVariation, View theView, WTPart toCheck) {
        boolean toReturn = false;
        Variation2 supplierSetVariation = toCheck.getVariation2();
        if (supplierSetVariation != null && supplierSetVariation.equals(supplierVariation) && toCheck.getViewName().equals(theView.getName())) {
            toReturn = true;
        }
        return toReturn;
    }

    private static void shareCommonParts(WTArrayList theFoundColorMaterialParts, String projectName) throws WTException {
        if (theFoundColorMaterialParts != null) {
            Iterator<Persistable> iterateOverParts = theFoundColorMaterialParts.persistableIterator();
            WTArrayList thePartsToBeShared = new WTArrayList();
            while (iterateOverParts.hasNext()) {
                Persistable toCheck = iterateOverParts.next();
                if (toCheck instanceof WTPart && (TypeUtils.isColorPart((WTPart) toCheck) || TypeUtils.isMaterialPart((WTPart) toCheck) || TypeUtils.isManufacturerPart((WTPart) toCheck) || TypeUtils.isVendorPart((WTPart) toCheck))) {
                    WTPart colorMaterialPart = (WTPart) toCheck;
                    colorMaterialPart = (WTPart) PersistenceHelper.manager.refresh(colorMaterialPart);
                    if (!isPartSharedToProject(colorMaterialPart, projectName)) {
                        thePartsToBeShared.add(colorMaterialPart);
                    }
                }
            }
            Project2 projectToUse = getProjectByName(projectName);
            if (projectToUse != null) {
                Folder folder = FolderHelper.service.getFolder("/Default", WTContainerRef.newWTContainerRef(projectToUse));
                DataSharingHelper.service.shareVersions(thePartsToBeShared, WTContainerRef.newWTContainerRef(projectToUse), folder);
                SandboxHelper.service.addToSandboxBaseline(thePartsToBeShared, WTContainerRef.newWTContainerRef(projectToUse), true);
            }
        }
        
    }

    public static ArrayList<FeedbackMessage> reShareBOMToProject(WTPart partInSupplierView, Variation2 supplierVariation) throws WTException {
        ArrayList<FeedbackMessage> toReturn = new ArrayList<>();
        Project2 toShareInto = IkeaProjectUtils.getProjectByName(supplierVariation.getDisplay());
        WTArrayList partsToCheck = IkeaPartUtils.getAllDescendantsByVariation(partInSupplierView, supplierVariation.toString());
        WTCollection checkoutParts = new WTArrayList();
        WTCollection shareParts = new WTArrayList();
        Iterator<Persistable> iterateOverParts = partsToCheck.persistableIterator();
        while (iterateOverParts.hasNext()) {
            WTPart toCheck = (WTPart) iterateOverParts.next();
            ArrayList<String> authorisedSuppliers = IkeaPartUtils.getListOfAuthorisedSuppliers(toCheck);
            boolean checkSuppliersOnChildren = IkeaProperties.getProperties().getProperty("com.ikea.sbom.checkSuppliersForChildren", false);
            if (checkSuppliersOnChildren && !authorisedSuppliers.contains(supplierVariation.getDisplay().replaceAll(" ", ""))) {
                continue;
            }
            if (IkeaProjectUtils.isPartSharedToProject(toCheck, supplierVariation.getDisplay())) {
                continue;
            }
            checkoutParts.add(toCheck);
            QueryResult resultForCheckedOut = PersistenceHelper.manager.navigate(toCheck, WTPartUsageLink.ROLE_BOBJECT_ROLE, WTPartUsageLink.class, true);
            while (resultForCheckedOut.hasMoreElements()) {
                WTPartMaster theMaster = (WTPartMaster) resultForCheckedOut.nextElement();
                WTPart obtainedPart = IkeaPartUtils.getWTPartByNumber(theMaster.getNumber());
                if ((TypeUtils.isColorPart(obtainedPart) || TypeUtils.isMaterialPart(obtainedPart) || TypeUtils.isManufacturerPart(obtainedPart) || TypeUtils.isVendorPart(obtainedPart))) {
                    shareParts.add(obtainedPart);
                }
                
            }
        }
        
        if (toShareInto != null) {
            try {
                Map<ObjectReference, ObjectReference> checkoutMap = IkeaProjectUtils.pdmCheckout(checkoutParts, toShareInto);
                IkeaProjectUtils.shareCommonParts((WTArrayList) shareParts, toShareInto.getName());
                if (!checkoutMap.isEmpty()) {
                    toReturn.add(new FeedbackMessage(FeedbackType.SUCCESS, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, PART_CHECKED_OUT, new Object[]{partInSupplierView.getNumber(), partInSupplierView.getViewName(), supplierVariation.getDisplay()})));
                } else {
                    toReturn.add(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, PART_NOT_CHECKED_OUT, new Object[]{partInSupplierView.getNumber(), partInSupplierView.getViewName(), supplierVariation.getDisplay()})));
                }
            } catch (Exception e) {
                e.printStackTrace();
                toReturn.add(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, PART_CHECKOUT_EXCEPTION, new Object[]{partInSupplierView.getNumber(), partInSupplierView.getViewName(), supplierVariation.getDisplay(), e.getMessage()})));
            }
        }
        return toReturn;
    }

    public static Project2 getProjectByOrgName(String orgName) throws WTException {
        Project2 project = null;
        boolean enforceAccess = SessionServerHelper.manager.setAccessEnforced(false);
        try {
            WTOrganization theOrg = getWTOrganization(orgName);
            if (theOrg != null) {
                QuerySpec query = new QuerySpec();
                int[] a_fromIndicies = new int[1];
                a_fromIndicies[0] = query.addClassList(Project2.class, true);
                SearchCondition sc = new SearchCondition(Project2.class, "organizationReference.key.id", SearchCondition.EQUAL, theOrg.getPersistInfo().getObjectIdentifier().getId());
                query.appendWhere(sc, a_fromIndicies);
                QueryResult qres = PersistenceHelper.manager.find((StatementSpec) query);
                if (qres.hasMoreElements()) {
                    Persistable[] pers = (Persistable[]) qres.nextElement();
                    project = (Project2) pers[0];
                }
            }
        } finally {
            SessionServerHelper.manager.setAccessEnforced(enforceAccess);
        }
        return project;
    }

    public static WTOrganization getWTOrganization(String ogranizationName) throws WTException {
        QuerySpec querySpec = new QuerySpec(WTOrganization.class);
        querySpec.appendWhere(new SearchCondition(WTOrganization.class, WTOrganization.NAME, SearchCondition.EQUAL, ogranizationName), new int[]{0});
        QueryResult queryResult = PersistenceHelper.manager.find((StatementSpec) querySpec);
        WTOrganization wtOrganization = null;
        if (queryResult.hasMoreElements()) {
            wtOrganization = (WTOrganization) queryResult.nextElement();
        }
        return wtOrganization;
    }
    
    public static void unShareFromProject (Project2 projectToUse, HashMap<String, WTArrayList> sharedBom) throws WTException {
        WTSet origCopies;
        try {
            origCopies = (WTSet) SandboxHelper.service.undoCheckout(sharedBom.get(IkeaPartUtils.INTEROP_MARKER), true);
            origCopies.addAll(sharedBom.get(IkeaPartUtils.SHARED_READ_ONLY_MARKER));
            Iterator<Persistable> iterateOverSharedParts = origCopies.persistableIterator();
            while (iterateOverSharedParts.hasNext()) {
                Persistable toCheckForReadOnlyShares = iterateOverSharedParts.next();
                if (toCheckForReadOnlyShares instanceof WTPart) {
                    DataSharingHelper.service.removeShare(toCheckForReadOnlyShares, WTContainerRef.newWTContainerRef(projectToUse));
                }
            }
        } catch (WTPropertyVetoException e) {
            e.printStackTrace();
            throw new WTException(e);
        }
    }
    
    public static void copyOverBOM(RevisionControlled[] partsToCopy, Project2 projectToUse) throws WTException {
        CopyObjectInfo[] newCopyObjectInfoArray = EnterpriseHelper.service.newMultiObjectCopy(partsToCopy);
        for (CopyObjectInfo copyInfo:newCopyObjectInfoArray) {
             if (copyInfo.getCopy() instanceof WTPart) {
                 try {
                     WTPart theOriginal = (WTPart)copyInfo.getOriginal();
                    ((WTPart)copyInfo.getCopy()).setContainer(projectToUse);
                    ((WTPart)copyInfo.getCopy()).setView(theOriginal.getView());
                    ((WTPart)copyInfo.getCopy()).setName(theOriginal.getName());
                    if (TypeUtils.isManufacturerPart(theOriginal) ) {
                       ManufacturerPart originalOrg = (ManufacturerPart) theOriginal;
                       ManufacturerPartMaster originalMaster = (ManufacturerPartMaster) originalOrg.getMaster();
                       WTOrganization orgToUse = originalMaster.getOrganization();
                       ManufacturerPart copyToOrg = (ManufacturerPart) copyInfo.getCopy();
                       ManufacturerPartMaster copyMaster = (ManufacturerPartMaster) copyToOrg.getMaster();
                       copyMaster.setOrganization(orgToUse);
                    } else if (TypeUtils.isVendorPart(theOriginal) ) {
                        VendorPart originalOrg = (VendorPart) theOriginal;
                        VendorPartMaster originalMaster = (VendorPartMaster) originalOrg.getMaster();
                        WTOrganization orgToUse = originalMaster.getOrganization();
                        VendorPart copyToOrg = (VendorPart) copyInfo.getCopy();
                        VendorPartMaster copyMaster = (VendorPartMaster) copyToOrg.getMaster();
                        copyMaster.setOrganization(orgToUse);
                     }
                } catch (WTPropertyVetoException e) {
                    e.printStackTrace();
                    throw new WTException(e);
                }
             }
        }
        EnterpriseHelper.service.saveMultiObjectCopy(newCopyObjectInfoArray);
    }

    public static void reshareAfterEquivalenceUpdate(AssociativeEquivalenceExecutionReport report, Collection<EquivalenceLink> theCurrentEquivalences, WTPart toShare) throws WTException {
        Map<Associative, Associative> checkedOut = report.getCheckoutMap();
        for (Associative associatedPart : checkedOut.keySet()) {
            WTPart workingCopy = (WTPart) checkedOut.get(associatedPart);
            Workable wrk;
            try {
                wrk = WorkInProgressHelper.service.checkin(workingCopy, "");
                Collection<EquivalenceLink> linksCreated = report.getEquivalenceLinks();
                theCurrentEquivalences.addAll(linksCreated);
                Collection<EquivalenceLink> equivalencesForUpstreamAfterUpdate = IkeaPartUtils.getEquivalenceLinksForUpstream(toShare);
                equivalencesForUpstreamAfterUpdate.removeAll(theCurrentEquivalences);
                for (EquivalenceLink equivToDelete : equivalencesForUpstreamAfterUpdate) {
                    PersistenceServerHelper.manager.remove(equivToDelete);
                }
                IkeaProjectUtils.reShareBOMToProject((WTPart) wrk, ((WTPart) wrk).getVariation2());
            } catch (WTPropertyVetoException e) {
                e.printStackTrace();
                throw new WTException(e);
            }
        }
    }

}
