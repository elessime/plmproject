package com.ikea.ipim.utils.types;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.log4j.Logger;

import com.ptc.core.components.util.OidHelper;
import com.ptc.core.foundation.associativity.AssociativityProperties;
import com.ptc.core.lwc.server.PersistableAdapter;
import com.ptc.windchill.associativity.AssociativePath;
import com.ptc.windchill.associativity.AssociativityConstants;
import com.ptc.windchill.associativity.bll.AssociativeEquivalenceExecutionReport;
import com.ptc.windchill.associativity.copyover.CopyOverContext;
import com.ptc.windchill.associativity.copyover.CopyOverContext.Mode;
import com.ptc.windchill.associativity.part.PartAssociativePath;
import com.ptc.windchill.associativity.service.AssociativeEquivalenceService;
import com.ptc.windchill.associativity.service.AssociativityServiceLocator;
import com.ptc.windchill.baseserver.util.TypeFilter;

import wt.associativity.Associative;
import wt.associativity.EquivalenceLink;
import wt.associativity.EquivalenceStatus;
import wt.associativity.NCServerHolder;
import wt.fc.ObjectReference;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.PersistenceServerHelper;
import wt.fc.QueryResult;
import wt.fc.collections.WTArrayList;
import wt.fc.collections.WTCollection;
import wt.fc.collections.WTHashSet;
import wt.inf.container.WTContainerRef;
import wt.inf.sharing.DataSharingHelper;
import wt.inf.sharing.SharedContainerMap;
import wt.part.WTPart;
import wt.part.WTPartMaster;
import wt.part.WTPartUsageLink;
import wt.pds.StatementSpec;
import wt.projmgmt.admin.Project2;
import wt.query.ClassAttribute;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.sandbox.IOPState;
import wt.sandbox.SandboxHelper;
import wt.session.SessionHelper;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;
import wt.vc.Iterated;
import wt.vc.OneOffVersionInfo;
import wt.vc.VersionControlHelper;
import wt.vc.config.IteratedOrderByPrimitive;
import wt.vc.config.VersionedOrderByPrimitive;
import wt.vc.config.ViewManageableOrderByPrimitive;
import wt.vc.config.ViewManageableOrderByVariation1Primitive;
import wt.vc.config.ViewManageableOrderByVariation2Primitive;
import wt.vc.views.View;
import wt.vc.views.ViewHelper;
import wt.vc.views.ViewManageable;
import wt.vc.views.ViewReference;
import wt.vc.wip.WorkInProgressHelper;
import wt.vc.wip.Workable;

public class IkeaPartUtils {
    private static Logger logger = Logger.getLogger(IkeaPartUtils.class);
    public static final String AUTHORISED_SUPPLIERS_IBA = "authorisedSuppliers";
    public static final String INTEROP_MARKER = "interopParts";
    public static final String SHARED_READ_ONLY_MARKER = "shareReadOnlyParts";

    public static WTPart getNewerPart(WTPart partAlreadyInMap, WTPart toCheck) {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getNewerPart()");
            logger.debug("Parts to compare are " + partAlreadyInMap.getIdentity() + " and " + toCheck.getIdentity());
        }
        String existingPartVersion = partAlreadyInMap.getVersionInfo().getIdentifier().getValue();
        String newPartVersion = toCheck.getVersionInfo().getIdentifier().getValue();
        if (logger.isDebugEnabled()) {
            logger.debug("Comparing versions of parts");
        }
        if (existingPartVersion.compareTo(newPartVersion) > 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting method getNewerPart()");
                logger.debug("Newer part is " + partAlreadyInMap.getIdentity());
            }
            return partAlreadyInMap;
        } else if (existingPartVersion.compareTo(newPartVersion) < 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting method getNewerPart()");
                logger.debug("Newer part is " + toCheck.getIdentity());
            }
            return toCheck;
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Versions of parts are equal - will compare their iterations");
            }
            String existingPartIteration = partAlreadyInMap.getIterationInfo().getIdentifier().getValue();
            String newPartIteration = toCheck.getIterationInfo().getIdentifier().getValue();
            if (existingPartIteration.compareTo(newPartIteration) > 0) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Exiting method getNewerPart()");
                    logger.debug("Newer part is " + partAlreadyInMap.getIdentity());
                }
                return partAlreadyInMap;
            } else if (existingPartIteration.compareTo(newPartIteration) < 0) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Exiting method getNewerPart()");
                    logger.debug("Newer part is " + toCheck.getIdentity());
                }
                return toCheck;
            } else
                return partAlreadyInMap;
        }
    }

    public static String getPartIdentifier(WTPart toCheck) {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getPartIdentifier()");
            logger.debug("Part to get the String identifier is -> " + toCheck.getIdentity());
        }
        StringBuilder builder = new StringBuilder();
        String number = toCheck.getNumber();
        String partView = toCheck.getViewName();
        String partAlternateNumber = "";
        if (toCheck.getVariation2() != null) {
            partAlternateNumber = toCheck.getVariation2().getStringValue();
        }
        builder.append(number);
        builder.append(",");
        if (partView != null && !partView.isEmpty()) {
            builder.append(partView);
            builder.append(",");
        }
        if (partAlternateNumber != null && !partAlternateNumber.isEmpty()) {
            builder.append(partAlternateNumber);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getPartIdentifier()");
            logger.debug("String identifier is -> " + builder.toString());
        }
        return builder.toString();
    }
    
    public static String getPartIdentifierWithRevision(WTPart toCheck) {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getPartIdentifier()");
            logger.debug("Part to get the String identifier is -> " + toCheck.getIdentity());
        }
        StringBuilder builder = new StringBuilder();
        OneOffVersionInfo oneOff = toCheck.getOneOffVersionInfo();
        String oneOffNumber = "";
        if (oneOff.getIdentifier()!=null) {
            oneOffNumber = oneOff.getIdentifier().getValue();
        }
        String partAlternateNumber = "";
        if (toCheck.getVariation2() != null) {
            partAlternateNumber = toCheck.getVariation2().getDisplay();
        }
        builder.append(toCheck.getNumber()).append(", ").append(toCheck.getName());
        if (toCheck.getVersionIdentifier().getValue() != null && !toCheck.getVersionIdentifier().getValue().isEmpty()) {
            builder.append(", ");
            builder.append(toCheck.getVersionIdentifier().getValue());
        } if (oneOffNumber != null && !oneOffNumber.isEmpty()) {
            builder.append("-");
            builder.append(oneOffNumber);
        } if (toCheck.getIterationIdentifier().getValue() != null && !toCheck.getIterationIdentifier().getValue().isEmpty()) {
            builder.append(".");
            builder.append(toCheck.getIterationIdentifier().getValue());
        }
        if (toCheck.getViewName() != null && !toCheck.getViewName().isEmpty()) {
            builder.append(", ");
            builder.append(toCheck.getViewName());
        }
        if (partAlternateNumber != null && !partAlternateNumber.isEmpty()) {
            builder.append(", ");
            builder.append(partAlternateNumber);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getPartIdentifier()");
            logger.debug("String identifier is -> " + builder.toString());
        }
        return builder.toString();
    }

    private static Object getAuthorisedSuppliers(WTPart thePart) throws WTException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getAuthorisedSuppliers()");
            logger.debug("Part to be used is -> " + thePart.getIdentity());
        }
        PersistableAdapter partAdapter = new PersistableAdapter(thePart, null, null, null);
        partAdapter.load(AUTHORISED_SUPPLIERS_IBA);
        Object theListOfSuppliers = partAdapter.get(AUTHORISED_SUPPLIERS_IBA);
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getAuthorisedSuppliers()");
        }
        return theListOfSuppliers;
    }

    public static ArrayList<String> getListOfAuthorisedSuppliers(WTPart thePart) throws WTException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getListOfAuthorisedSuppliers()");
            logger.debug("Part to be used is -> " + thePart.getIdentity());
        }
        ArrayList<String> toReturn = new ArrayList<>();
        Object theListOfSuppliers = getAuthorisedSuppliers(thePart);
        if (theListOfSuppliers instanceof String) {
            if (logger.isDebugEnabled()) {
                logger.debug("One supplier found -> " + theListOfSuppliers.toString());
            }
            toReturn.add(theListOfSuppliers.toString());
        } else if (theListOfSuppliers instanceof Object[]) {
            if (logger.isDebugEnabled()) {
                logger.debug("Multiple suppliers found");
            }
            Object[] list = (Object[]) theListOfSuppliers;
            for (Object supp : list) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Supplier found ->" + supp.toString());
                }
                toReturn.add(supp.toString());
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getListOfAuthorisedSuppliers()");
        }
        return toReturn;
    }

    public static WTPart getPartInManufViewByNumber(String partNumber) throws WTException, WTPropertyVetoException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getPartInManufViewByNumber()");
            logger.debug("Part number to be used is -> " + partNumber);
            logger.debug("Trying to get the part denoted by the number");
        }
        WTPart foundPart = getWTPartByNumber(partNumber);
        if (foundPart != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("Found part -> " + foundPart.getIdentity());
                logger.debug("Will now search for a manufacturing view of found part");
            }
            foundPart = getWTPartInView(foundPart.getMaster(), ViewHelper.service.getView("Manufacturing"), null, false);
            if (foundPart != null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Found part in Manufacturing view -> " + foundPart.getIdentity());
                }
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("NO PART in Manufacturing view found!");
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getPartInManufViewByNumber()");
        }
        return foundPart;
    }

    public static WTPart getWTPartInView(WTPartMaster master, View theView, String variation2, boolean latestOnly) throws WTException, WTPropertyVetoException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getWTPartInView()");
            logger.debug("Part to be used is -> " + master.getIdentity());
            logger.debug("View to be used is -> " + theView.getName());
            logger.debug("Supplier variation to be used is -> " + variation2);
            logger.debug("Returning latest iterations only -> " + latestOnly);
        }
        WTPart toReturn = null;
        ViewReference viewRef = ViewReference.newViewReference(theView);
        QuerySpec qs = new QuerySpec();
        if (logger.isDebugEnabled()) {
            logger.debug("Building the query spec");
        }
        final int[] vmIndexArray = {qs.appendClassList(WTPart.class, true)};
        qs.setDescendantsIncluded(false, vmIndexArray[0]);
        qs.appendWhere(VersionControlHelper.getSearchCondition(WTPart.class, master), vmIndexArray);
        qs = ViewHelper.appendWhereView(qs, WTPart.class, vmIndexArray, viewRef, false);
        qs.appendAnd();
        if (variation2 != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("Supplier variation is present! Adding it to query spec.");
            }
            qs.appendWhere(new SearchCondition(WTPart.class, WTPart.VARIATION2, SearchCondition.LIKE, variation2), vmIndexArray);
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Supplier variation is NOT present! Adding a null value to the query spec.");
            }
            qs.appendWhere(new SearchCondition(new ClassAttribute(WTPart.class, WTPart.VARIATION2), SearchCondition.IS_NULL), vmIndexArray);
        }
        if (latestOnly) {
            qs.appendAnd();
            qs.appendWhere(VersionControlHelper.getSearchCondition(WTPart.class, true), vmIndexArray);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Appending the sorting instructions that return the result from latest to oldest.");
        }
        new ViewManageableOrderByPrimitive().appendOrderBy(qs, 0, true);
        new ViewManageableOrderByVariation1Primitive().appendOrderBy(qs, 0, true);
        new ViewManageableOrderByVariation2Primitive().appendOrderBy(qs, 0, true);
        new VersionedOrderByPrimitive().appendOrderBy(qs, 0, true);
        new IteratedOrderByPrimitive().appendOrderBy(qs, 0, true);
        QueryResult qr = PersistenceServerHelper.manager.query((StatementSpec) qs);
        if (qr.hasMoreElements()) {
            if (logger.isDebugEnabled()) {
                logger.debug("Found a part in a specific view and of specific variation!.");
            }
            Object foundPart = qr.nextElement();
            if (foundPart instanceof Persistable[]) {
                Persistable[] table = (Persistable[]) foundPart;
                toReturn = (WTPart) table[0];
            } else if (foundPart instanceof WTPart) {
                toReturn = (WTPart) foundPart;
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getWTPartInView()");
        }
        return toReturn;
    }

    public static WTPart getWTPartByNumber(String number) throws WTException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getWTPartByNumber()");
            logger.debug("Part number to be used is -> " + number);
        }
        WTPart toReturn = null;
        if (logger.isDebugEnabled()) {
            logger.debug("Building the query spec");
        }
        QuerySpec queryspec = new QuerySpec();
        int wtPartIndex = queryspec.appendClassList(WTPart.class, true);
        queryspec.appendWhere(new SearchCondition(WTPart.class, WTPart.NUMBER, SearchCondition.EQUAL, number), new int[]{wtPartIndex});
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", SearchCondition.IS_TRUE), new int[]{wtPartIndex});
        if (logger.isDebugEnabled()) {
            logger.debug("Appending the sorting instructions that return the result from latest to oldest.");
        }
        new ViewManageableOrderByPrimitive().appendOrderBy(queryspec, 0, true);
        new ViewManageableOrderByVariation1Primitive().appendOrderBy(queryspec, 0, true);
        new ViewManageableOrderByVariation2Primitive().appendOrderBy(queryspec, 0, true);
        new VersionedOrderByPrimitive().appendOrderBy(queryspec, 0, true);
        new IteratedOrderByPrimitive().appendOrderBy(queryspec, 0, true);
        QueryResult objects = PersistenceHelper.manager.find((StatementSpec) queryspec);
        while (objects.hasMoreElements()) {
            if (logger.isDebugEnabled()) {
                logger.debug("Found a part that matches the number!.");
            }
            Object object = objects.nextElement();
            if (object instanceof Persistable[]) {
                Persistable[] tab = (Persistable[]) object;
                if (tab[0] instanceof WTPart) {
                    toReturn = (WTPart) tab[0];
                    break;
                }
            } else if (object instanceof WTPart) {
                toReturn = (WTPart) object;
                break;
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getWTPartByNumber()");
        }
        return toReturn;
    }

    public static WTPart getWTPartByNumberAndVariation(String number, String variation) throws WTException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getWTPartByNumberAndVariation()");
            logger.debug("Part number to be used is -> " + number);
            logger.debug("Supplier variation to be used is -> " + variation);
        }
        QuerySpec queryspec = new QuerySpec();
        WTPart toReturn = null;
        if (logger.isDebugEnabled()) {
            logger.debug("Building the query spec");
        }
        int wtPartIndex = queryspec.appendClassList(WTPart.class, true);
        queryspec.appendWhere(new SearchCondition(WTPart.class, WTPart.NUMBER, SearchCondition.EQUAL, number), new int[]{wtPartIndex});
        queryspec.appendAnd();
        if (variation == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("Supplier variation is NOT present! Adding a null value to the query spec.");
            }
            queryspec.appendWhere(new SearchCondition(new ClassAttribute(WTPart.class, WTPart.VARIATION2), SearchCondition.IS_NULL), new int[]{wtPartIndex});
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Supplier variation is present! Adding it to query spec.");
            }
            queryspec.appendWhere(new SearchCondition(WTPart.class, WTPart.VARIATION2, SearchCondition.LIKE, variation), new int[]{wtPartIndex});
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Appending the sorting instructions that return the result from latest to oldest.");
        }
        new ViewManageableOrderByPrimitive().appendOrderBy(queryspec, 0, true);
        new ViewManageableOrderByVariation1Primitive().appendOrderBy(queryspec, 0, true);
        new ViewManageableOrderByVariation2Primitive().appendOrderBy(queryspec, 0, true);
        new VersionedOrderByPrimitive().appendOrderBy(queryspec, 0, true);
        new IteratedOrderByPrimitive().appendOrderBy(queryspec, 0, true);
        QueryResult objects = PersistenceHelper.manager.find((StatementSpec) queryspec);
        while (objects.hasMoreElements()) {
            if (logger.isDebugEnabled()) {
                logger.debug("Found a part that matches the number and variation!.");
            }
            Object object = objects.nextElement();
            if (object instanceof Persistable[]) {
                Persistable[] tab = (Persistable[]) object;
                if (tab[0] instanceof WTPart) {
                    toReturn = (WTPart) tab[0];
                    break;
                }
            } else if (object instanceof WTPart) {
                toReturn = (WTPart) object;
                break;
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getWTPartByNumberAndVariation()");
        }
        return toReturn;
    }

    public static WTArrayList getAllDescendantsWithoutVariation(WTPart root) throws WTException {
        return getAllDescendantsByVariation(root, null);
    }

    public static WTArrayList getAllDescendantsByVariation(WTPart root, String variation) throws WTException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getAllDescendantsByVariation()");
            logger.debug("Part to be used is -> " + root.getIdentity());
            logger.debug("Supplier variation to be used is -> " + variation);
        }
        WTArrayList toReturn = new WTArrayList();
        HashMap<WTPart, WTArrayList> structureMap = new HashMap<WTPart, WTArrayList>();
        Queue<WTPart> partsToCheck = new LinkedList<WTPart>();
        partsToCheck.add(root);
        while (!partsToCheck.isEmpty()) {
            WTPart partToCheck = partsToCheck.poll();
            QueryResult queryResult = PersistenceHelper.manager.navigate(partToCheck, WTPartUsageLink.ROLE_BOBJECT_ROLE, WTPartUsageLink.class, true);
            while (queryResult.hasMoreElements()) {
                WTPartMaster theMaster = (WTPartMaster) queryResult.nextElement();
                WTPart part = getWTPartByNumberAndVariation(theMaster.getNumber(), variation);
                if (!structureMap.containsKey(partToCheck)) {
                    structureMap.put(partToCheck, new WTArrayList());
                }
                if (part != null) {
                    structureMap.get(partToCheck).add(part);
                    partsToCheck.add(part);
                }

            }
            if (!structureMap.containsKey(partToCheck)) {
                structureMap.put(partToCheck, new WTArrayList());
            }
        }
        toReturn.addAll(structureMap.keySet());
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getAllDescendantsByVariation()");
        }
        return toReturn;
    }

    //getLatestRevisionOfVersionInViewVariations
    public static QueryResult getListOfSupplierPartsInContainerType(WTPart root, String containerClass) throws WTException, WTPropertyVetoException {
        QuerySpec queryspec = new QuerySpec();
        if (logger.isDebugEnabled()) {
            logger.debug("Building the query spec");
        }
        int wtPartIndex = queryspec.appendClassList(WTPart.class, true);
        queryspec.appendWhere(new SearchCondition(WTPart.class, WTPart.NUMBER, SearchCondition.EQUAL, root.getNumber()), new int[]{wtPartIndex});
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(new ClassAttribute(WTPart.class, WTPart.VARIATION2), SearchCondition.NOT_NULL), new int[]{wtPartIndex});
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "containerReference.key.classname", SearchCondition.LIKE, containerClass), new int[]{wtPartIndex});
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, Iterated.LATEST_ITERATION, SearchCondition.IS_TRUE), new int[]{wtPartIndex});
        QueryResult objects = PersistenceHelper.manager.find((StatementSpec) queryspec);
        return objects;
    }

    public static Collection<Persistable> getSupplierPartsSharedToSuppliers(WTPart partToCheck) throws WTException, WTPropertyVetoException {
        Collection<Persistable> foundShared = SandboxHelper.service.getInteropObjects(partToCheck);
        removeWorkingCopies(foundShared);
        foundShared = removeUnneededRevisions(foundShared);
        return foundShared;
    }
    
//    public static EquivalenceStatus getStatusBetweenUpAndDownstream(WTPart upstreamPart, WTPart downstreamPart) throws WTException {
//        NCServerHolder upstream = NCServerHolder.makeForIterated(upstreamPart);
//        View view = ViewHelper.getView((ViewManageable) downstreamPart);
//        NCServerHolder downstream;
//        try {
//            downstream = NCServerHolder.makeForView(view, null, ((WTPart) downstreamPart).getVariation2());
//        } catch (WTPropertyVetoException e) {
//            e.printStackTrace();
//            throw new WTException(e);
//        }
//        AssociativityServiceLocator locator = new AssociativityServiceLocator();
//        List<ObjectReference> theUpstreams = new ArrayList<ObjectReference>();
//        theUpstreams.add(OidHelper.getObjectReference(upstreamPart));
////        List<EquivalenceStatus> theResult = locator.getBomService().getDownstreamEquivalentStatus(theUpstreams, upstream, downstream, TypeFilter.includeAll(WTPart.class));
//        if (theResult == null ||theResult.isEmpty()) {
//            return EquivalenceStatus.NO_EQUIVALENT;
//        } else return theResult.get(0);
//    }
//    
    private static void removeWorkingCopies(Collection<Persistable> collection) throws WTException {
        for (Iterator<Persistable> iterateOverPers = collection.iterator(); iterateOverPers.hasNext();) {
            Persistable toCheck = iterateOverPers.next();
            if (toCheck instanceof Workable) {
                Workable workableToCheck = (Workable) toCheck;
                if (WorkInProgressHelper.isWorkingCopy(workableToCheck)) {
                    iterateOverPers.remove();
                }
            }
        }
    }
    private static Collection<Persistable> removeUnneededRevisions(Collection<Persistable> collection) throws WTException {
        ArrayList<WTPart> theOneOffs = new ArrayList<>();
        HashMap<String, ArrayList<SharedContainerMap>> theSharedParts = new HashMap<>();
        for (Persistable toCheck:collection) {
            if (toCheck instanceof WTPart) {
                theOneOffs.add((WTPart) toCheck);
            } else if (toCheck instanceof SharedContainerMap) {
                SharedContainerMap theSharedObject = (SharedContainerMap)toCheck;
               if (theSharedObject.getShared() instanceof WTPart && theSharedObject.getTargetContainerRef().getObject() instanceof Project2) {
                   StringBuilder sharedPartIdentity =new StringBuilder( ((WTPart)theSharedObject.getShared()).getNumber());
                   if (((WTPart)theSharedObject.getShared()).getViewName() != null) {
                       sharedPartIdentity.append("_").append(((WTPart)theSharedObject.getShared()).getViewName());
                   }  if (((WTPart)theSharedObject.getShared()).getVariation2() != null) {
                       sharedPartIdentity.append("_").append(((WTPart)theSharedObject.getShared()).getVariation2().getDisplay());
                   } if (((Project2)theSharedObject.getTargetContainerRef().getObject()) != null) {
                       sharedPartIdentity.append("_").append(((Project2)theSharedObject.getTargetContainerRef().getObject()).getName());
                   }
                   if (!theSharedParts.containsKey(sharedPartIdentity.toString())) {
                       ArrayList<SharedContainerMap> toPlace = new ArrayList<>();
                       toPlace.add(theSharedObject);
                       theSharedParts.put(sharedPartIdentity.toString(), toPlace);
                   } else {
                       ArrayList<SharedContainerMap> toPlace = theSharedParts.get(sharedPartIdentity.toString());
                       toPlace.add(theSharedObject);
                   }
               }
            }
        }
        ArrayList<Persistable> toReturn = new ArrayList<>();
        Set<String> partSet = theSharedParts.keySet();
        for (String partId:partSet) {
            if (theSharedParts.get(partId).size()>1) {
                WTPart theResult;
                ArrayList<SharedContainerMap> toFilter = theSharedParts.get(partId);
                QueryResult result = new QueryResult();
                for (SharedContainerMap containerResult:toFilter) {
                    result.getObjectVector().addElement(containerResult.getShared());
                }
                theResult = (WTPart) DataSharingHelper.getGreatestAndLatest(result);
                Iterator<SharedContainerMap> iterateOverMaps = toFilter.iterator();
                while (iterateOverMaps.hasNext()) {
                    SharedContainerMap iterated = iterateOverMaps.next();
                    if (!(iterated.getShared().equals(theResult))) {
                        iterateOverMaps.remove();
                    }
                }
            }
        }
        for (String partId:partSet) {
            if (theSharedParts.get(partId).size()>0) {
                toReturn.addAll(theSharedParts.get(partId));
            }
        }
        toReturn.addAll(theOneOffs);
        return toReturn;
    }

    public static HashMap<String, WTArrayList> getInterOpBOM(WTPart partToUnshare, String supplierName) throws WTException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getAllDescendantsByVariation()");
            logger.debug("Part to be used is -> " + partToUnshare.getIdentity());
            logger.debug("Supplier variation to be used is -> " + supplierName);
        }
        HashMap<String, WTArrayList> toReturn = new HashMap<String, WTArrayList>(); 
        WTArrayList theCheckedOutParts = new WTArrayList();
        WTArrayList theSharedReadParts = new WTArrayList();
        HashMap<WTPart, WTArrayList> structureMap = new HashMap<WTPart, WTArrayList>();
        Queue<WTPart> partsToCheck = new LinkedList<WTPart>();
        partsToCheck.add(partToUnshare);
        while (!partsToCheck.isEmpty()) {
            WTPart partToCheck = partsToCheck.poll();
            QueryResult queryResult = PersistenceHelper.manager.navigate(partToCheck, WTPartUsageLink.ROLE_BOBJECT_ROLE, WTPartUsageLink.class, true);
            while (queryResult.hasMoreElements()) {
                WTPartMaster theMaster = (WTPartMaster) queryResult.nextElement();
                WTPart part = getInterOpPart(theMaster.getNumber(), supplierName);
                if (part !=null) {
                    if (!structureMap.containsKey(partToCheck)) {
                        structureMap.put(partToCheck, new WTArrayList());
                    }
                    structureMap.get(partToCheck).add(part);
                    partsToCheck.add(part);
                } else {
                        part = getSharedReadPart(theMaster.getNumber(), supplierName);
                    if (part != null) {
                        theSharedReadParts.add(part);
                    }
                }

            }
            if (!structureMap.containsKey(partToCheck)) {
                structureMap.put(partToCheck, new WTArrayList());
            }
        }
        theCheckedOutParts.addAll(structureMap.keySet());
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getAllDescendantsByVariation()");
        }
        toReturn.put(INTEROP_MARKER, theCheckedOutParts);
        WTHashSet set = new WTHashSet(theSharedReadParts);
        toReturn.put(SHARED_READ_ONLY_MARKER, new WTArrayList(set));
        return toReturn;
    }

    private static WTPart getSharedReadPart(String number, String supplierName) throws WTException {
       WTPart partToCheck = getPartByNumber(number);
       Project2 theProject = IkeaProjectUtils.getProjectByName(supplierName);
       if  (partToCheck != null && theProject != null &&  DataSharingHelper.service.isSharedTo(partToCheck, WTContainerRef.newWTContainerRef(theProject))) {
           WTArrayList theShares = new WTArrayList();
           theShares.add(partToCheck);
           WTCollection sharedPart = DataSharingHelper.service.getSharedObjects(theShares,  WTContainerRef.newWTContainerRef(theProject));
           if (sharedPart != null && !sharedPart.isEmpty()) {
               return (WTPart) sharedPart.persistableIterator().next();
           }
       }
       return null;
    }

    private static WTPart getInterOpPart(String number, String supplierName) throws WTException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering method getWTPartByNumberAndVariation()");
            logger.debug("Part number to be used is -> " + number);
            logger.debug("Supplier variation to be used is -> " + supplierName);
        }
        QuerySpec queryspec = new QuerySpec();
        WTPart toReturn = null;
        if (logger.isDebugEnabled()) {
            logger.debug("Building the query spec");
        }
        int wtPartIndex = queryspec.appendClassList(WTPart.class, true);
        queryspec.appendWhere(new SearchCondition(WTPart.class, WTPart.NUMBER, SearchCondition.EQUAL, number), new int[]{wtPartIndex});
        queryspec.appendAnd();
        if (supplierName == null || supplierName.isEmpty()) {
            if (logger.isDebugEnabled()) {
                logger.debug("Supplier variation is NOT present! Adding a null value to the query spec.");
            }
            queryspec.appendWhere(new SearchCondition(new ClassAttribute(WTPart.class, WTPart.VARIATION2), SearchCondition.IS_NULL), new int[]{wtPartIndex});
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Supplier variation is present! Adding it to query spec.");
            }
            queryspec.appendWhere(new SearchCondition(WTPart.class, WTPart.VARIATION2, SearchCondition.LIKE, supplierName.replace(" ", "")), new int[]{wtPartIndex});
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Appending the sorting instructions that return the result from latest to oldest.");
        }
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(WTPart.class, "interopInfo.iopState", SearchCondition.LIKE, IOPState.IOP_O_O.toString()), new int[]{wtPartIndex});
        queryspec.appendAnd();
        queryspec.appendWhere(new SearchCondition(new ClassAttribute(WTPart.class, "oneOffVersionInfo.identifier.oneOffVersionId"), SearchCondition.NOT_NULL), new int[]{wtPartIndex});
        new ViewManageableOrderByPrimitive().appendOrderBy(queryspec, 0, true);
        new ViewManageableOrderByVariation2Primitive().appendOrderBy(queryspec, 0, true);
        new VersionedOrderByPrimitive().appendOrderBy(queryspec, 0, true);
        new IteratedOrderByPrimitive().appendOrderBy(queryspec, 0, true);
        QueryResult objects = PersistenceHelper.manager.find((StatementSpec) queryspec);
        while (objects.hasMoreElements()) {
            if (logger.isDebugEnabled()) {
                logger.debug("Found a part that matches the number and variation!.");
            }
            Object object = objects.nextElement();
            if (object instanceof Persistable[]) {
                Persistable[] tab = (Persistable[]) object;
                if (tab[0] instanceof WTPart) {
                    toReturn = (WTPart) tab[0];
                    break;
                }
            } else if (object instanceof WTPart) {
                toReturn = (WTPart) object;
                break;
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Exiting method getWTPartByNumberAndVariation()");
        }
        return toReturn;
    }
    
    private static WTPart getPartByNumber (String partNumber) {
        WTPart result = null;
        partNumber = partNumber.trim ();
        try
        {
            QuerySpec querySpec = new QuerySpec ();
            querySpec.addClassList (WTPart.class, true);
            querySpec.addClassList (WTPartMaster.class, false);
            SearchCondition searchCondition =
                new SearchCondition (WTPart.class, "masterReference.key.id", WTPartMaster.class, "thePersistInfo.theObjectIdentifier.id");
            querySpec.appendWhere(searchCondition, new int [] {0, 1});
            SearchCondition nameCondition1 =
                new SearchCondition (WTPartMaster.class, WTPartMaster.NUMBER, SearchCondition.EQUAL, partNumber);
            querySpec.appendAnd ();
            querySpec.appendWhere (nameCondition1, new int [] {1});
            QueryResult queryResults = PersistenceHelper.manager.find ((StatementSpec) querySpec);
            while (queryResults.hasMoreElements ()) {
                Persistable [] persist = (Persistable []) queryResults.nextElement ();
                WTPart part = (WTPart) persist [0];
                if (part.isLatestIteration ())  {
                    result = part;
                    break;
                }
            }
        }
        catch (WTException exc) {
            exc.printStackTrace ();
        }
        return result;
    }

    public static Collection<EquivalenceLink> getEquivalenceLinksForUpstream (WTPart theUpstream) throws WTException {
        Collection<EquivalenceLink> toReturn = new ArrayList<EquivalenceLink>();
        QueryResult theResult = PersistenceHelper.manager.navigate(theUpstream, EquivalenceLink.DOWNSTREAM_ROLE, EquivalenceLink.class, false);
        while (theResult.hasMoreElements()) {
            Persistable toCheck = (Persistable) theResult.nextElement();
            if (toCheck instanceof EquivalenceLink) {
                toReturn.add((EquivalenceLink)toCheck);
            }
        }
        return toReturn;
    }
    
//    public static AssociativeEquivalenceExecutionReport updateEquivalenceBetweenParts(WTPart partToUnshare, WTPart toShare, NCServerHolder upstreamNC, NCServerHolder downstreamNC) throws WTException {
//        List<ObjectReference> downStreamReferences = new ArrayList<>();
//        downStreamReferences.add(OidHelper.getObjectReference(partToUnshare));
//        List<AssociativePath> associativePaths = new ArrayList<>();
//        AssociativePath thePath = new PartAssociativePath(partToUnshare, null, null);
//        associativePaths.add(thePath);
//        AssociativeEquivalenceService service = AssociativityServiceLocator.getInstance().getAssociativeEquivalenceService();
////        AssociativeEquivalenceExecutionReport report = service.updateEquivalenceLinksAndFirstLevelLinkedChildren(downStreamReferences, toShare.getPersistInfo().getObjectIdentifier(), associativePaths, toShare, upstreamNC, downstreamNC, true, SessionHelper.getLocale(), null);
//        CopyOverContext copyContext = new AssociativityServiceLocator().getCopyOverManager().getCopyOverContext(AssociativityProperties.getDefault(), AssociativityConstants.prefixCopyOverProperty, Mode.UPDATE, null);
//        new AssociativityServiceLocator().getCopyOverManager().copyOverUsingEqLink(copyContext, report.getEquivalenceLinks());
//        return report;
//    }
    
}
