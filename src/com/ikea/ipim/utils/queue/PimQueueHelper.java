package com.ikea.ipim.utils.queue;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.Calendar;

import org.apache.log4j.Logger;

import com.ikea.ipim.interfaces.JSONToPIMInterface;
import com.ikea.ipim.interfaces.KafkaToWindchillInterface;

import wt.method.RemoteAccess;
import wt.method.RemoteMethodServer;
import wt.org.WTPrincipal;
import wt.preference.PreferenceHelper;
import wt.queue.QueueHelper;
import wt.queue.ScheduleQueue;
import wt.session.SessionHelper;
import wt.session.SessionServerHelper;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;

public class PimQueueHelper implements RemoteAccess, Serializable  {

    private static final long serialVersionUID = 660072875309023300L;
    private static final Logger LOGGER = Logger.getLogger(PimQueueHelper.class);
    

    public static void main(String[] args) throws WTException, WTPropertyVetoException, RemoteException, InvocationTargetException {
        int argsLength = args.length;
        String userName = "";
        String userPass = "";
        for (int i = 0; i < argsLength; i++) {
            if (args[i].equals("-u")) {
                i++;
                if (i < argsLength) {
                    userName = new String(args[i]);
                }
            } else if (args[i].equals("-p")) {
                i++;
                if (i < argsLength) {
                    userPass = new String(args[i]);
                }
            }
        }

        if (userName != null && !userName.equals("")) {
            RemoteMethodServer.getDefault().setUserName(userName);
            if (userPass != null) {
                RemoteMethodServer.getDefault().setPassword(userPass);
            }
        }
        RemoteMethodServer.getDefault().invoke("startupKafkaToWindchillInterface", PimQueueHelper.class.getName(), null, null, null);
    }
    
    
    public static void addNewQueueEntry(ScheduleQueue queue, int resheduleTime, String targetMethod, String targetClass, Class<?>[] parameterClasses, Object[] parameterValues) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering method addNewQueueEntry()");
            LOGGER.debug("Time for rescheduling (in minutes) -> " + resheduleTime);
            LOGGER.debug("Method to execute -> " + targetMethod);
            LOGGER.debug("Class to be used -> " + targetClass);
            LOGGER.debug("Queue to be used -> " + queue.getName());
            for (Class<?> clazz:parameterClasses) {
                LOGGER.debug("Class passed to method signature -> " + clazz.getName());
            }
            for (Object parameterValue:parameterValues) {
                LOGGER.debug("value passed as argument to method -> " + parameterValue.toString());
            }
        }
        
        Timestamp now = new Timestamp(Calendar.getInstance().getTimeInMillis());
        long seconds = resheduleTime*60;
        Timestamp timestampToUse = new Timestamp(now.getTime() + seconds * 1000L);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entry to be executed at -> " + timestampToUse.toString());
        }
        queue.addEntry(SessionHelper.manager.getAdministrator(), targetMethod, targetClass, parameterClasses, parameterValues, timestampToUse);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting method addNewQueueEntry()");
        }
    }
    

    /**
     * Adds the method parsing the incoming files for the ELDORADO interface.
     *
     * @param queueName the queue name
     * @param groupName the group name
     * @throws Exception the exception
     */
    public static void startupKafkaToWindchillInterface() throws Exception {
        ScheduleQueue queue = createScheduleQueue();
        Integer queueRetriger = (Integer) PreferenceHelper.service.getValue(KafkaToWindchillInterface.RS_TO_WNC_KAFKA_INTERFACE_POOL_TIME, JSONToPIMInterface.CLIENT_NAME);
        addNewQueueEntry(queue, queueRetriger, "executeBatchPooling", KafkaToWindchillInterface.class.getName(),
                new Class[] {},
                new Object[] {});
    }
    
    /**
     * Creates the schedule queue.
     *
     * @param queueName
     *            the name
     * @param queueGroup
     *            the queue group
     * @return the processing queue
     * @throws Exception
     *             the exception
     */
    public static ScheduleQueue createScheduleQueue(String queueName) throws Exception {
        ScheduleQueue schedulequeue = null;
        boolean accessEnforced = SessionServerHelper.manager.isAccessEnforced();
        WTPrincipal principal = SessionHelper.getPrincipal();
        try {
            SessionHelper.manager.setAdministrator();
            SessionServerHelper.manager.setAccessEnforced(true);
            schedulequeue = (ScheduleQueue) QueueHelper.manager.getQueue(queueName, ScheduleQueue.class);
            if (schedulequeue == null) {
                schedulequeue = (ScheduleQueue) QueueHelper.manager.createScheduleQueue(queueName, "Default");
            }
            schedulequeue = QueueHelper.manager.startQueue(schedulequeue);
        } catch (WTException exc) {
            LOGGER.error("Exception", exc);
        } finally {
            SessionServerHelper.manager.setAccessEnforced(accessEnforced);
            SessionHelper.manager.setPrincipal(principal.getName());
        }

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("createScheduleQueue - queue: " + queueName + " queueGroup: " + "Default");
        }
        return schedulequeue;
    }
    
    public static void addNewEntryInKafkaToWindchillQueue(int resheduleTime, String targetMethod, String targetClass, Class<?>[] parameterClasses, Object[] parameterValues) throws Exception {
        addNewQueueEntry(createScheduleQueue(), resheduleTime, targetMethod, targetClass, parameterClasses, parameterValues);
    }


    private static ScheduleQueue createScheduleQueue() throws Exception {
        String queueName = (String) PreferenceHelper.service.getValue(KafkaToWindchillInterface.RS_TO_WNC_WNC_QUEUE_NAME, JSONToPIMInterface.CLIENT_NAME);
        return createScheduleQueue(queueName);
    }
    
}
