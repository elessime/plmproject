package com.ikea.ipim.utils.odata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Parameter;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;

import com.ptc.netmarkets.model.NmOid;
import com.ptc.odata.core.ValidNavigation;
import com.ptc.odata.core.entity.function.FunctionProcessorData;
import com.ptc.odata.core.entity.navigation.NavigationProcessorData;
import com.ptc.odata.core.entity.processor.EntityProcessorData;
import com.ptc.odata.core.entity.property.EntityAttribute;

import wt.access.AccessControlHelper;
import wt.access.AccessPermission;
import wt.fc.ObjectIdentifier;
import wt.fc.ObjectReference;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.fc.ReferenceFactory;
import wt.fc.collections.WTArrayList;
import wt.fc.collections.WTCollection;
import wt.org.WTPrincipal;
import wt.part.WTPart;
import wt.part.WTPartAlternateLink;
import wt.part.WTPartHelper;
import wt.part.WTPartMaster;
import wt.part.WTPartSubstituteLink;
import wt.part.WTPartUsageLink;
import wt.session.SessionHelper;
import wt.util.WTException;
import wt.util.WTRuntimeException;
import wt.vc.VersionControlHelper;

public class PartAlternatesHelper {

    public ValidNavigation isValidNavigation(String navPropName, Object sourceObj, String targetObjectId, EntityProcessorData processorData) {
        navPropName.length();
        return ValidNavigation.VALID;
    }

    public Map getRelatedPartAlternates(NavigationProcessorData processorData) {
        Map<WTPart, List<Object>> map = new HashMap<>();
        WTArrayList sourceParts = new WTArrayList(processorData.getSourceObjects());
        String targetName = processorData.getTargetSetName();
        try {
            if (targetName.equals("Alternates")) {
                Iterator IterateOverParts = sourceParts.persistableIterator();
                while (IterateOverParts.hasNext()) {
                    Persistable persist = (Persistable) IterateOverParts.next();
                    if (persist instanceof WTPart) {
                        WTPart thePart = (WTPart) persist;
                        ArrayList alternates = new ArrayList<>();
                        QueryResult result = PersistenceHelper.manager.navigate(thePart.getMaster(), WTPartAlternateLink.ALTERNATES_ROLE, WTPartAlternateLink.class, true);
                        while (result.hasMoreElements()) {
                            WTPartMaster toPutMaster = (WTPartMaster) result.nextElement();
                            QueryResult versionsOf = VersionControlHelper.service.allIterationsOf(toPutMaster);
                            if (versionsOf.size() > 0) {
                                WTPart toPut = (WTPart) versionsOf.nextElement();
                                alternates.add(toPut);
                            }
                        }
                        map.put(thePart, alternates);
                    }
                }
            } else if (targetName.equals("SubstituteParts")) {
                Iterator IterateOverParts = sourceParts.persistableIterator();
                while (IterateOverParts.hasNext()) {
                    Persistable persist = (Persistable) IterateOverParts.next();
                    if (persist instanceof WTPart) {
                        WTPart thePart = (WTPart) persist;
                        WTCollection theSubstitutes = WTPartHelper.service.getSubstituteLinksAnyAssembly((WTPartMaster) thePart.getMaster());
                        if (theSubstitutes.size() > 0) {
                            List<Object> linksToAdd = new ArrayList<>();
                            Iterator iterateOverLinks = theSubstitutes.persistableIterator();
                            while (iterateOverLinks.hasNext()) {
                                linksToAdd.add(iterateOverLinks.next());
                            }
                            map.put(thePart, linksToAdd);
                        }
                    }
                }
            }
        } catch (WTException e) {
            e.printStackTrace();
        }
        return map;
    }

    public EntityCollection getSubstituteParts(FunctionProcessorData data, Map params) throws ODataApplicationException, WTException {
        EntityCollection toReturn = new EntityCollection();
        Parameter param = (Parameter) params.get("thePart");
        Entity theValue = (Entity) param.getValue();
        String theOid = (String) theValue.getProperty("ID").getValue();
        ReferenceFactory refFact = new ReferenceFactory();
        WTCollection theSubstitutes = new WTArrayList();
        WTPrincipal user = SessionHelper.getPrincipal();
        try {
            Persistable thePersist = refFact.getReference(theOid).getObject();
            if (thePersist instanceof WTPart) {
                WTPart toCheck = (WTPart) thePersist; 
                try {
                    SessionHelper.manager.setAdministrator(); 
                    theSubstitutes = WTPartHelper.service.getSubstituteLinksAnyAssembly((WTPartMaster) toCheck.getMaster());
                } finally {
                    SessionHelper.manager.setPrincipal(user.getName());
                }
                if (theSubstitutes.size() > 0) {
                    List<Object> linksToAdd = new ArrayList<>();
                    Iterator iterateOverLinks = theSubstitutes.persistableIterator();
                    while (iterateOverLinks.hasNext()) {
                        WTPartSubstituteLink theSubstituteLink = (WTPartSubstituteLink)iterateOverLinks.next();
                        WTPartUsageLink usageLinkObject = (WTPartUsageLink) theSubstituteLink.getRoleAObject();
                        WTPartMaster substituteMasterObject = (WTPartMaster) theSubstituteLink.getRoleBObject();
                        WTPart theSubstitutePart = null;
                        WTPart theSubstituteContextPart = (WTPart) usageLinkObject.getRoleAObject();
                        QueryResult theSubstituteResult = VersionControlHelper.service.allIterationsOf(substituteMasterObject);
                        if (theSubstituteResult.size() > 0) {
                            theSubstitutePart = (WTPart) theSubstituteResult.nextElement();
                        }
                        boolean hasUserAccess = AccessControlHelper.manager.hasAccess(theSubstituteContextPart, AccessPermission.READ);
                        Entity entity = data.getProcessor().toEntity(theSubstituteLink, data);
                        if (theSubstitutePart != null && hasUserAccess) {
                            EntityAttribute theSubstitute = new EntityAttribute();
                            theSubstitute.setName("ReplacementPartID");
                            theSubstitute.setValue(ValueType.PRIMITIVE, theSubstitutePart.getPersistInfo().getObjectIdentifier().toString());
                            entity.getProperties().add(theSubstitute);
                            
                            EntityAttribute theContext = new EntityAttribute();
                            theContext.setName("ContextPartID");
                            theContext.setValue(ValueType.PRIMITIVE, theSubstituteContextPart.getPersistInfo().getObjectIdentifier().toString());
                            entity.getProperties().add(theContext);
                            
                            toReturn.getEntities().add(entity);
                        }
                    }
                 
                }
            }
        } catch (WTRuntimeException | WTException | ODataApplicationException e) {
            e.printStackTrace();
            throw new ODataApplicationException("Exception when executing getSubstituteParts function", HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode(), data.getLocale(), e);
        }
        return toReturn;

    }

}
