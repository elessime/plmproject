package com.ikea.ipim.utils.delegates;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import com.ikea.ipim.properties.IkeaIconDelegateProperties;
import com.ikea.ipim.utils.types.TypeUtils;
import com.ptc.core.lwc.server.PersistableAdapter;
import com.ptc.windchill.enterprise.part.commands.delegate.WTPartIconDelegate;

import com.ptc.core.meta.common.impl.WCTypeIdentifier;
import com.ptc.core.meta.server.TypeIdentifierUtility;

import wt.fc.WTObject;
import wt.generic.GenericType;
import wt.part.WTPart;
import wt.util.IconSelector;
import wt.util.WTException;

public class IkeaPartIconDelegate extends WTPartIconDelegate {

    private static String VARIATION_IBA = "variation2";
    
    public IconSelector getStandardIconSelector() throws WTException, IllegalAccessException, InvocationTargetException {
        IconSelector selector = super.getStandardIconSelector();
        HashMap<String, HashMap<String, String>> mapping = IkeaIconDelegateProperties.getMapping();
        // retrieve the object
        WTObject datum = super.getObject();
        if (datum instanceof WTPart) {
            WTPart theVersionedPart = (WTPart) datum;
            String theIconSource = selector.getIconKey();
            // Retrieve the name of the soft type (only the leaf)
            String matchingTypeName = ((WCTypeIdentifier) TypeIdentifierUtility.getTypeIdentifier(theVersionedPart)).getLeafName();
            // check if we have mapping for this type:
            if (mapping.containsKey(matchingTypeName)) {
                HashMap<String, String> typeMapping = mapping.get(matchingTypeName);
                //check the view of the part
                String viewName = theVersionedPart.getViewName();
                if (typeMapping.containsKey(viewName)) {
                    //check if this is an alternate
                    boolean isVariation = false;
                    boolean isConfigPart = false;
                    boolean isEndItem = false;
                    isEndItem = theVersionedPart.isEndItem();
                    PersistableAdapter partAdapt = new PersistableAdapter(theVersionedPart, null, null, null);
                     partAdapt.load(VARIATION_IBA);
                     String alternateValue = (String) partAdapt.get(VARIATION_IBA);
                    if (alternateValue != null && !alternateValue.isEmpty())  {
                        isVariation = true;
                    }
                    //check if this is a configurable part
                    GenericType genType = theVersionedPart.getGenericType();
                    if (genType.getStringValue().equals(GenericType.DYNAMIC.getStringValue())) {
                        isConfigPart = true;
                    }
                    if (isVariation && !isConfigPart && !isEndItem) {
                        theIconSource = typeMapping.get(viewName+ "_" + IkeaIconDelegateProperties.ALTERNATE_BOM_NAME) ;
                    } else if (!isVariation && isConfigPart && !isEndItem) {
                        theIconSource = typeMapping.get(viewName+ "_" + IkeaIconDelegateProperties.CONFIGURABLE_NAME) ;
                    } else if (!isVariation && !isConfigPart && isEndItem) {
                        theIconSource = typeMapping.get(viewName+ "_" + IkeaIconDelegateProperties.END_ITEM_NAME);
                    }  else if (!isVariation && isConfigPart && isEndItem) {
                        theIconSource = typeMapping.get(viewName+ "_" + IkeaIconDelegateProperties.CONFIGURABLE_NAME +"_" + IkeaIconDelegateProperties.END_ITEM_NAME);
                    } 
                    else if (!isVariation && !isConfigPart && !isEndItem) {
                        theIconSource = typeMapping.get(viewName);
                    }
                    
                    Vector<?> originalAdornments = selector.getAdornments();
                    selector = new IconSelector(theIconSource);
                    selector.setAdornments(originalAdornments);
                }
            }
        }
        return selector;
    }
    
    private static String getFirstMatchingTypeIcon(Set<String> keyTypes, WTPart partToIdentify) {
        String toReturn = "";
        String theTypeIdentifier = TypeUtils.getTypeName(partToIdentify);
        List<String> typeList = Arrays.asList(theTypeIdentifier.split("\\|"));
        Collections.reverse(typeList);
        for (String type:typeList) {
           if (keyTypes.contains(type)) {
               return type;
           }
        }
        return toReturn;
    }
    
}
