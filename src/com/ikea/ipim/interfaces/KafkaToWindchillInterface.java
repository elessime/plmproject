package com.ikea.ipim.interfaces;

import java.io.IOException;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.log4j.Logger;

import com.ikea.ipim.utils.queue.PimQueueHelper;
import com.ptc.windchill.keystore.WTKeyStoreUtil;

import wt.preference.PreferenceHelper;
import wt.util.WTException;
import wt.util.WTProperties;

public class KafkaToWindchillInterface {
    public static final String RS_TO_WNC_KAFKA_TOPIC = "RS_TO_WNC_KAFKA_TOPIC";
    public static final String RS_TO_WNC_WNC_QUEUE_NAME = "RS_TO_WNC_WNC_QUEUE_NAME";
    public static final String RS_TO_WNC_KAFKA_INTERFACE_POOL_TIME = "RS_TO_WNC_KAFKA_INTERFACE_POOL_TIME";
    private static final String TRUSTSTORE_KEY = "/codebase/com/ikea/properties/store/kafka.client.truststore.jks";
    private static final String KEYSTORE_KEY = "/codebase/com/ikea/properties/store/kafka.client.keystore.jks";
    private static final Logger LOG = Logger.getLogger(JSONToPIMInterface.class);

    public static void executeBatchPooling() {
        try {
            String server = (String) PreferenceHelper.service.getValue(JSONToPIMInterface.KAFKA_URL, JSONToPIMInterface.CLIENT_NAME);
            String topic = (String) PreferenceHelper.service.getValue(RS_TO_WNC_KAFKA_TOPIC, JSONToPIMInterface.CLIENT_NAME);
            int rescheduleTime = (Integer) PreferenceHelper.service.getValue(RS_TO_WNC_KAFKA_INTERFACE_POOL_TIME, JSONToPIMInterface.CLIENT_NAME);
            String windchillInstanceId = WTProperties.getLocalProperties().getProperty(JSONToPIMInterface.WNC_HOSTNAME_KEY);
            Consumer<Long, String> kafkaConsumer = createConsumer(server, windchillInstanceId);
            kafkaConsumer.subscribe(Collections.singletonList(topic));
            ConsumerRecords<Long, String> records = kafkaConsumer.poll(1000);
            if (!records.isEmpty()) {
                for (ConsumerRecord<Long, String> recordToProcess : records) {
                    String recordValue = recordToProcess.value();
                    //entrypoint for parsing whatever recieved from KAFKA
                    if (LOG.isDebugEnabled()) {
                        LOG.debug(recordValue);
                    }
                    kafkaConsumer.commitSync();
                }
            }
            PimQueueHelper.addNewEntryInKafkaToWindchillQueue(rescheduleTime, "executeBatchPooling", KafkaToWindchillInterface.class.getName(), new Class[]{}, new Object[]{});
        } catch (Exception ex) {
            try {
                PimQueueHelper.addNewEntryInKafkaToWindchillQueue(1, "executeBatchPooling", KafkaToWindchillInterface.class.getName(), new Class[]{}, new Object[]{});
                ex.printStackTrace();
            } catch (Exception e) {

                e.printStackTrace();
            }
        }

    }

    /**
     * The method will create a KAFKA consumer instance based on a set if properties
     * 
     * @param serverName - the name of the KAFKA server to be used
     * @param clientId - the identifier of the KAFKA client
     * @throws WTException
     * @throws IOException
     */
    private static Consumer<Long, String> createConsumer(String serverName, String clientId) throws WTException, IOException {
        Properties props = new Properties();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method createConsumer()");
            LOG.debug("Will now try to establish a connection to KAFKA server...");
        }
        String wtHomeProperty = WTProperties.getLocalProperties().getProperty(JSONToPIMInterface.WT_HOME_PROPERTY);
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, serverName);
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, clientId);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Server name of KAFKA is -> " + serverName);
            LOG.debug("Client ID of Windchill for KAFKA is -> " + clientId);
        }
        props.put("enable.auto.commit", "false");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class.getName());

        if ((boolean) PreferenceHelper.service.getValue(JSONToPIMInterface.KAFKA_SSL_ENCRYPT, JSONToPIMInterface.CLIENT_NAME)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("SSL Encryption for KAFKA is required by Windchill!");
            }
            props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, JSONToPIMInterface.SSL_PROTOCOL);
            String kafkaKeyStorePath = wtHomeProperty + TRUSTSTORE_KEY;
            props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, kafkaKeyStorePath);
            String encryptedKafkaTruststorePass = WTKeyStoreUtil.decryptProperty(JSONToPIMInterface.KAFKA_PASSWORD_PROPERTY, JSONToPIMInterface.KAFKA_ENC_PASSWORD_PROPERTY, wtHomeProperty);
            props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, encryptedKafkaTruststorePass);
        }
        if ((boolean) PreferenceHelper.service.getValue(JSONToPIMInterface.KAFKA_USE_PASSKEY_FOR_SSL, JSONToPIMInterface.CLIENT_NAME)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("SSL Encryption for KAFKA requires a trusted keystore!");
            }
            String kafkaKeyStorePath = wtHomeProperty + KEYSTORE_KEY;
            props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, kafkaKeyStorePath);
            if ((boolean) PreferenceHelper.service.getValue(JSONToPIMInterface.KAFKA_USE_PASSKEY_FOR_SSL_TRUSTSTORE, JSONToPIMInterface.CLIENT_NAME)) {
                String encryptedKafkaKeystorePass = WTKeyStoreUtil.decryptProperty(JSONToPIMInterface.KAFKA_PASSWORD_FOR_KEYSTORE_PROPERTY, JSONToPIMInterface.KAFKA_ENC_PASSWORD_FOR_KEYSTORE_PROPERTY, wtHomeProperty);
                props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, encryptedKafkaKeystorePass);
            }
            if ((boolean) PreferenceHelper.service.getValue(JSONToPIMInterface.KAFKA_USE_PASSKEY_FOR_SSL_HANDSHAKE, JSONToPIMInterface.CLIENT_NAME)) {
                String encryptedKafkaSslPass = WTKeyStoreUtil.decryptProperty(JSONToPIMInterface.KAFKA_PASSWORD_FOR_SSL_HANDSHAKE_PROPERTY, JSONToPIMInterface.KAFKA_ENC_PASSWORD_FOR_SSL_HANDSHAKE_PROPERTY, wtHomeProperty);
                props.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, encryptedKafkaSslPass);
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method createConsumer()");
        }
        return new KafkaConsumer<>(props);
    }

}
