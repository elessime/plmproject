package com.ikea.ipim.interfaces;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.io.FilenameUtils;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.log4j.Logger;
import org.apache.log4j.lf5.util.StreamUtils;
import org.json.JSONException;

import com.ikea.ipim.utils.types.TypeUtils;
import com.infoengine.SAK.BasicTasklet;
import com.infoengine.object.IeAtt;
import com.infoengine.object.IeCollection;
import com.infoengine.object.IeGroup;
import com.infoengine.object.IeNode;
import com.microsoft.azure.eventhubs.ConnectionStringBuilder;
import com.microsoft.azure.eventhubs.EventData;
import com.microsoft.azure.eventhubs.EventHubClient;
import com.microsoft.azure.eventhubs.EventHubException;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageCredentials;
import com.microsoft.azure.storage.StorageCredentialsAccountAndKey;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlobDirectory;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.ptc.windchill.esi.tgt.ESITarget;
import com.ptc.windchill.keystore.WTKeyStoreUtil;

import wt.adapter.BasicWebjectDelegate;
import wt.content.ApplicationData;
import wt.content.ContentItem;
import wt.content.ContentRoleType;
import wt.content.DataFormat;
import wt.content.HolderToContent;
import wt.content.Streamed;
import wt.doc.WTDocument;
import wt.fc.ObjectVector;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.preference.PreferenceHelper;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.util.WTException;
import wt.util.WTMessage;
import wt.util.WTProperties;
import wt.util.WTPropertyVetoException;

/**
 * This class is used as the interface between Windchill ESI and Riversand PIM platform.
 * Hence the communication might be done by Azure EventHub or Apache KAFKA. The class relies on the Info*Engine distribution target task
 * that will be triggered upon ESI release.
 * After the I*E task that generates the ESI XML will run and the XML will get converted to the JSON format and saved to
 * a file, the I*E task will trigger this class
 * by passing on either a string referencing the complete path to the file OR a handler directly representing the file.
 * The file will get read and sent to the
 * Azure event hub specified by a combination of preferences in Windchill and a key stored in Java keystore or/and to a
 * KAFKA topic specified by its URL, topic name and if need be by additional SSL configuration (certificates and keys stored in JAva keystore).
 * NOTE: This class may be used to send all kinds of files - not only JSON files.
 */
public class JSONToPIMInterface {

    public static final String PASSWORD_PROPERTY = "com.ikea.pim.azure.key";
    public static final String ENC_PASSWORD_PROPERTY = "encrypted.com.ikea.pim.azure.key";
    public static final String KAFKA_PASSWORD_PROPERTY = "com.ikea.pim.kafka.ssl.cert.key";
    public static final String KAFKA_ENC_PASSWORD_PROPERTY = "encrypted.com.ikea.pim.kafka.ssl.cert.key";
    public static final String WT_HOME_PROPERTY = "wt.home";
    public static final String WT_TEMP_PROPERTY = "wt.temp";
    public static final String CLIENT_NAME = "WINDCHILL";
    public static final String PIM_NAMESPACE = "PIM_INTERFACE_NAMESPACE";
    public static final String PIM_HUB_NAME = "PIM_INTERFACE_HUB_NAME";
    public static final String PIM_KEY_NAME = "PIM_INTERFACE_KEY_NAME";
    public static final String EVENT_HUB_TYPE_KEY = "com.ikea.pim.interface.type";
    public static final String KAFKA_URL = "PIM_KAFKA_INTERFACE_ADDRESS";
    public static final String KAFKA_TOPIC = "PIM_KAFKA_INTERFACE_TOPIC";
    public static final String KAFKA_SSL_ENCRYPT = "PIM_KAFKA_INTERFACE_USE_SSL";
    public static final String KAFKA_USE_PASSKEY_FOR_SSL = "PIM_KAFKA_INTERFACE_USE_PASSWORD_FOR_SSL";
    public static final String WNC_HOSTNAME_KEY = "wt.rmi.server.hostname";
    public static final String BLOB_STORE_PROTOCOL = "PIM_AZURE_BLOB_STORE_PROTOCOL";
    public static final String BLOB_STORE_ACCOUNT="PIM_AZURE_BLOB_STORE_ACCOUNT";
    public static final String BLOB_STORE_CONTAINER = "PIM_AZURE_BLOB_STORE_CONTAINER";
    public static final String BLOB_STORE_CONTAINER_PATH = "PIM_AZURE_BLOB_STORE_CONTAINER_PATH";
    public static final String BLOB_STORE_ENDPOINT = "PIM_AZURE_BLOB_STORE_ENDPOINT";
    public static final String BLOB_PASSWORD_PROPERTY = "com.ikea.pim.azure.blob.key";
    public static final String ENC_BLOB_PASSWORD_PROPERTY = "encrypted.com.ikea.pim.azure.blob.key";
    public static final String IE_RELATED_DOCS_PROPERTY = "relatedDocuments";
    public static final String IE_OBJECT_ID_PROPERTY = "ObjectID";
    public static final String HTTPS_PROTOCOL = "https";
    public static final String QS_FORMAT_KEY = "format.key.id";
    public static final String IE_ADDED_DOCS = "AddedDocuments";
    public static final String IE_CHANGED_DOCS = "ChangedDocuments";
    public static final String IE_ADDED_PARTS = "AddedParts";
    public static final String IE_CHANGED_PARTS = "ChangedParts";
    public static final String IE_UNCHANGED_PARTS = "UnchangedParts";
    public static final String SALES_PART_TYPE = "SalesPartType";
    public static final String ARTICLE_MARKER = "article";
    public static final String IE_UNCHANGED_DOCS = "UnchangedDocuments";
    public static final String SSL_PROTOCOL = "SSL";
    public static final String KAFKA_USE_PASSKEY_FOR_SSL_TRUSTSTORE = "PIM_KAFKA_INTERFACE_USE_ENCRYPTED_STORE_FOR_SSL_HANDSHAKE";
    public static final String KAFKA_PASSWORD_FOR_KEYSTORE_PROPERTY = "com.ikea.pim.kafka.ssl.client.keystore.key";
    public static final String KAFKA_ENC_PASSWORD_FOR_KEYSTORE_PROPERTY = "encrypted.com.ikea.pim.kafka.ssl.client.keystore.key";
    public static final String KAFKA_USE_PASSKEY_FOR_SSL_HANDSHAKE = "PIM_KAFKA_INTERFACE_USE_PASSWORD_FOR_SSL_HANDSHAKE";
    public static final String KAFKA_PASSWORD_FOR_SSL_HANDSHAKE_PROPERTY = "com.ikea.pim.kafka.ssl.client.connection.key";
    public static final String KAFKA_ENC_PASSWORD_FOR_SSL_HANDSHAKE_PROPERTY = "encrypted.com.ikea.pim.kafka.ssl.client.connection.key";
    
    private static final String TRUSTSTORE_KEY = "/codebase/com/ikea/properties/store/kafka.client.truststore.jks";
    private static final String KEYSTORE_KEY ="/codebase/com/ikea/properties/store/kafka.client.keystore.jks";
    private static final Logger LOG = Logger.getLogger(JSONToPIMInterface.class);

    /**
     * This method takes up a file handler and tries to publish it to Azure EventHub. It will first check if the file is
     * valid,
     * that is: it is not null, the handler on the file exists and the file is NOT a folder. Once the check is OK, it
     * will
     * then check if the Distribution Target object is related to KAFKA or to Azure - once it establishes that it will 
     * try sending out the data to the appropriate platform.
     *
     * @param jsonToPublish the handler of the file to be published
     * @throws EventHubException the event hub exception
     * @throws ExecutionException the execution exception
     * @throws InterruptedException the interrupted exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws WTException the WT exception
     * @throws JSONException
     */
    public static void publishJSON(File jsonToPublish, ESITarget theTarget) throws EventHubException, ExecutionException, InterruptedException, IOException, WTException, JSONException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method publishJSON() for a File handler.");
        }
        if (jsonToPublish == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("File is null - will throw an exception.");
            }
            throw new WTException(WTMessage.getLocalizedMessage(jsonToPimInterfaceRB.class.getName(), jsonToPimInterfaceRB.EX_NULL_FILE_PASSED));
        }
        if (!jsonToPublish.exists()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("File does not exist on disk - will throw an exception.");
            }
            throw new WTException(WTMessage.getLocalizedMessage(jsonToPimInterfaceRB.class.getName(), jsonToPimInterfaceRB.EX_NONEXISTENT_FILE_PASSED));
        }
        if (jsonToPublish.isDirectory()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("The handler points to a directory - will throw an exception.");
            }
            throw new WTException(WTMessage.getLocalizedMessage(jsonToPimInterfaceRB.class.getName(), jsonToPimInterfaceRB.EX_DIRECTORY_PASSED));
        }
        String property = WTProperties.getLocalProperties().getProperty(WT_HOME_PROPERTY);
        if (TypeUtils.isDTargetKafka(theTarget)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Will now try to send the following file to KAFKA hub -> " + jsonToPublish.getAbsolutePath());
            }
            String server = (String) PreferenceHelper.service.getValue(KAFKA_URL, CLIENT_NAME);
            String topic = (String) PreferenceHelper.service.getValue(KAFKA_TOPIC, CLIENT_NAME);
            String windchillInstanceId = WTProperties.getLocalProperties().getProperty(WNC_HOSTNAME_KEY);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Setting up a connection to KAFKA... ");
            }
            Producer kafkaProducer = createProducer(server, windchillInstanceId);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Reading contents of the file...");
            }
            byte[] encoded = Files.readAllBytes(jsonToPublish.toPath());
            String toSend = new String(encoded);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Sending data to KAFKA...");
            }
            ProducerRecord<Long, String> record = new ProducerRecord<Long, String>(topic, toSend);
            RecordMetadata metadata = (RecordMetadata) kafkaProducer.send(record).get();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Sending data to KAFKA - obtained metadata");
                LOG.debug("Message stored on queue -> " + metadata.topic());
                LOG.debug("Message stored on partition -> " + metadata.partition());
                LOG.debug("Message stored at offset -> " + metadata.offset());
            }
            kafkaProducer.flush();
            kafkaProducer.close();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Data sent to KAFKA - closed connection...");
            }
        } else if (TypeUtils.isDTargetAzure(theTarget)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Will now try to send the following file to Azure EventHub -> " + jsonToPublish.getAbsolutePath());
            }

            String namespace = (String) PreferenceHelper.service.getValue(PIM_NAMESPACE, CLIENT_NAME);
            String hubName = (String) PreferenceHelper.service.getValue(PIM_HUB_NAME, CLIENT_NAME);
            String keyName = (String) PreferenceHelper.service.getValue(PIM_KEY_NAME, CLIENT_NAME);
            String encryptedAzurePass = WTKeyStoreUtil.decryptProperty(PASSWORD_PROPERTY, ENC_PASSWORD_PROPERTY, property);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Setting up a connection to Azure... ");
            }
            ConnectionStringBuilder connectionString = new ConnectionStringBuilder();
            connectionString.setNamespaceName(namespace);
            connectionString.setEventHubName(hubName);
            connectionString.setSasKeyName(keyName);
            connectionString.setSasKey(encryptedAzurePass);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Reading contents of the file...");
            }
            byte[] jsonBytes = Files.readAllBytes(jsonToPublish.toPath());
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            EventData sendEvent = EventData.create(jsonBytes);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Opening connection to Azure...");
            }
            EventHubClient client = EventHubClient.createSync(connectionString.toString(), executorService);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Sending data to Azure...");
            }
            client.sendSync(sendEvent);
            client.closeSync();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Closed connection");
                LOG.debug("Exiting method publishJSON() for a File handler.");
            }

        } else {
            throw new WTException(WTMessage.getLocalizedMessage(jsonToPimInterfaceRB.class.getName(), jsonToPimInterfaceRB.EX_UNNKNOWN_INTERFACE_PASSED));
        }

    }

    /**
     * This method takes up a string representing a path to a file and tries to publish it to Azure EventHub. First it
     * will check if the string is not null and non-empty.
     * If this check succeeds, a handler to the file represented by the path will be created and processed further.
     *
     * @param jsonPath the full path to the file to be transfered onto Azure.
     * @throws EventHubException the event hub exception
     * @throws ExecutionException the execution exception
     * @throws InterruptedException the interrupted exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws WTException the WT exception
     * @throws JSONException
     */
    public static void publishJSON(String jsonPath, ESITarget theTarget) throws EventHubException, ExecutionException, InterruptedException, IOException, WTException, JSONException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method publishJSON() for a string.");
        }
        if (jsonPath == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("File path string is null - will throw an exception.");
            }
            throw new WTException(WTMessage.getLocalizedMessage(jsonToPimInterfaceRB.class.getName(), jsonToPimInterfaceRB.EX_NULL_FILEPATH_PASSED));
        }
        if (jsonPath.isEmpty()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("File path string is empty - will throw an exception.");
            }
            throw new WTException(WTMessage.getLocalizedMessage(jsonToPimInterfaceRB.class.getName(), jsonToPimInterfaceRB.EX_EMPTY_FILEPATH_PASSED));
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Creating a file handler for path -> " + jsonPath);
        }
        File jsonFile = new File(jsonPath);
        publishJSON(jsonFile, theTarget);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method publishJSON() for a string.");
        }

    }

    /**
     * This method is invoked on the Info*Engine group generated by the ESI BEFORE it gets sent to PIM.
     * This method will handle the two following things:
     *  <li>obtaining the list of added/changed documents and articles
     *  <li>for each article found it will execute a custom logic that will find all related documents to that article, publish them on Azure store and adjust the 'relatedDocuments' tag.
     * @param theTask - an Info*Engine task that we want to upload the related documents 
     * @throws StorageException 
     * @throws URISyntaxException 
     * @throws RuntimeException 
     * @throws WTException 
     * @throws IOException 
     * @throws WTPropertyVetoException 
     * @throws InvalidKeyException 
     * 
     */
    public static void adjustForPIMSend(BasicTasklet theTask) throws InvalidKeyException, WTPropertyVetoException, IOException, WTException, RuntimeException, URISyntaxException, StorageException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method adjustForPIMSend()");
        }
        IeCollection theCollection = theTask.getVdb();
        ArrayList<IeNode> foundDocuments = getRelevantDocuments(theCollection);
        ArrayList<IeNode> unchangedDocuments = getUnchangedDocuments(theCollection);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Will now prepare all of the associated documents to be sent to Azure and linked to thestructure");
            }
            prepareArticleForDocSend(foundDocuments, unchangedDocuments);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method adjustForPIMSend()");
        }
    }

    private static ArrayList<IeNode> getUnchangedDocuments(IeCollection theCollection) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getUnchangedDocuments()");
        }
        ArrayList<IeNode> toReturn = new ArrayList<IeNode>();
        IeGroup unchangedDocs = theCollection.getGroup(IE_UNCHANGED_DOCS);
        toReturn.addAll(getDocumentsFromGroup(unchangedDocs));
        if (LOG.isDebugEnabled()) {
            LOG.debug("Below is a list of returned entries found in the I*E group for UNCHANGED documents");
            for (IeNode theNode:toReturn) {
                LOG.debug(theNode.toString());
            }
            LOG.debug("Exiting method getUnchangedDocuments()");
        }
        return toReturn;
    }

    /**
     * This method is responsible for the following actions on a set of documents identified to be belonging to an article:
     *  <li>for each document find the primary content of it
     *  <li>from the primary content do a copy of it into a temp directory, changing the name of the content to the number of the related WTDocument,
     *  <li>upload the copy of the primary content to Azure using a set of preferences available from SITE level
     *  <li>adjust the 'relatedDocuments' tag by appending the uploaded file name to Azure
     *  <li>try removing the uploaded file from the temp folder
     * @param topNode - the Info*Engine tag representing the Article node
     * @param foundDocuments - an array list of I*E nodes that represent the related documents to be uploaded
     * @param unchangedDocuments - an array list of I*E nodes that represent the documents that have been uploaded previously to PIM. If the document has been uploaded previously, the list will assure that it won't get republished. 
     * @throws StorageException 
     * @throws URISyntaxException 
     * @throws RuntimeException 
     * @throws WTException 
     * @throws IOException 
     * @throws WTPropertyVetoException 
     * @throws InvalidKeyException 
     * 
     */
    private static void prepareArticleForDocSend( ArrayList<IeNode> foundDocuments, ArrayList<IeNode> unchangedDocuments) throws IOException, WTException, WTPropertyVetoException, InvalidKeyException, RuntimeException, URISyntaxException, StorageException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method prepareArticleForDocSend()");
        }
        WTProperties props = WTProperties.getLocalProperties();
        String tempPath = props.getProperty(WT_TEMP_PROPERTY);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will be using WNC temp dir -> " + tempPath);
        }
        //IeAtt relatedDocsAtt = topNode.getAtt(IE_RELATED_DOCS_PROPERTY);
        for (IeNode documentNode : foundDocuments) {
            String theExtention = "";
            String docUfidString = documentNode.getAtt(IE_OBJECT_ID_PROPERTY).getDatum().getNodeValue();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Identified an UFID of a related document -> " + docUfidString);
            }
            Persistable recievedObject = BasicWebjectDelegate.getObjectByUfid(docUfidString);
            if (recievedObject instanceof WTDocument) {
                WTDocument docToSend = (WTDocument) recievedObject;
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Identified the WTDocument based of the UFID -> " + docToSend.getIdentity());
                    LOG.debug("Will now search for any primary content related to the WTDocument -> " + docToSend.getIdentity());
                }
                QueryResult obtainedContent = getContent(docToSend, ContentRoleType.PRIMARY, null);
                while (obtainedContent.hasMoreElements()) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Identified the WTDocument based of the UFID -> " + docToSend.getIdentity());
                    }
                    Object primaryContent = obtainedContent.nextElement();
                    if (primaryContent instanceof ApplicationData) {
                        ApplicationData thePrimaryContent = (ApplicationData) primaryContent;
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Identified a primary content of a WTDocument held in the Application Data " + thePrimaryContent.getIdentity());
                        }
                        Streamed streamData = (Streamed) thePrimaryContent.getStreamData().getObject();
                        InputStream primaryContentStream = streamData.retrieveStream();
                        File tempFolder = new File(tempPath);
                        if (!tempFolder.exists() && !tempFolder.mkdirs()) {
                            LOG.error("Could not create folders");
                            throw new WTException(WTMessage.getLocalizedMessage(jsonToPimInterfaceRB.class.getName(), jsonToPimInterfaceRB.EX_FILEPATH_NOT_CREATED, new Object [] {tempPath}));
                        }
                        theExtention = FilenameUtils.getExtension(thePrimaryContent.getFileName());
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Extention of the primary content is -> " + theExtention);
                        }
                        if (!unchangedDocuments.contains(documentNode)) {
                            File exportFile = new File(tempFolder, docToSend.getNumber() + "." + theExtention);
                            if (LOG.isDebugEnabled()) {
                                LOG.debug("Will duplicate the file to the following path -> " + exportFile.getAbsolutePath());
                            }
                            FileOutputStream outputStream = new FileOutputStream(exportFile);
                            StreamUtils.copy(primaryContentStream, outputStream);
                            outputStream.flush();
                            outputStream.close();
                            if (LOG.isDebugEnabled()) {
                                LOG.debug("File copied - will now transfer it to the Azure store");
                            }
                            uploadFileToStorage(exportFile);
                            if (LOG.isDebugEnabled()) {
                                LOG.debug("Update of I*E group complete - will now try deleting the temporary file -> " + exportFile.getAbsolutePath());
                            }
                            boolean wasDeleted = exportFile.delete();
                            if (wasDeleted) {
                                if (LOG.isDebugEnabled()) {
                                    LOG.debug("Deleted file -> " + exportFile.getAbsolutePath());
                                }
                            } else if (!wasDeleted) {
                                LOG.error("COULD NOT DELETE THE TEMPORARY FILE  -> " + exportFile.getAbsolutePath());
                            }
                        }
                    }
                }
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Will now update the entry for the I*E Attribute in the group");
                }
                //Turning off the related documents aggregation for the I*E article - we now only SUBMIT files and do not change the group
               // relatedDocsAtt = updateRelatedDocsEntry(relatedDocsAtt, docToSend.getNumber() + "." + theExtention);
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method prepareArticleForDocSend()");
        }
    }

    /**
     * This method will obtain the connection to the Azure storage and upload the file selected to a specific path in a specific container. The path and share name are obtained from a SITE level set of properties.
     * @param exportFile - the file that will be uploaded to Azure
     * @throws StorageException 
     * @throws URISyntaxException 
     * @throws RuntimeException 
     * @throws WTException 
     * @throws IOException 
     * @throws WTPropertyVetoException 
     * @throws InvalidKeyException 
     * 
     */
    private static void uploadFileToStorage(File exportFile) throws InvalidKeyException, RuntimeException, IOException, URISyntaxException, StorageException, WTException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method uploadFileToStorage()");
            LOG.debug("File to send -> " + exportFile.getAbsolutePath());
        }
        CloudBlobClient fileClient = getBlobClientReference();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Obtained the reference to Azure filestore!");
        }
        String shareName = (String) PreferenceHelper.service.getValue(BLOB_STORE_CONTAINER, CLIENT_NAME);
        String folderPath = (String) PreferenceHelper.service.getValue(BLOB_STORE_CONTAINER_PATH, CLIENT_NAME);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Share name is -> " + shareName);
            LOG.debug("Path in share to use is -> " + folderPath);
        }
        CloudBlobContainer theShare = fileClient.getContainerReference(shareName);
        theShare.createIfNotExists();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Obtained the reference to Azure share of name -> " + shareName);
        }
        CloudBlobDirectory dirToSaveTo = getBlobFolderShare(theShare, folderPath, fileClient.getDirectoryDelimiter());
        CloudBlockBlob blob = dirToSaveTo.getBlockBlobReference(exportFile.getName());
        if (LOG.isDebugEnabled()) {
            LOG.debug("Now uploading the file -> " + exportFile.getAbsolutePath());
        }
        blob.uploadFromFile(exportFile.getAbsolutePath());
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method uploadFileToStorage()");
        }
    }

    /**
     * This method will take the Azure cloud storage container reference and return a reference to the folder to be used to store the files
     * @param theRootDirectory - reference to the used Azure BLOB storage
     * @param folderPath - the string representing the folder path within the Azure share to be used as a destination directory
     * @param defaultDelimiter - the Azure default delimiter for folder separators
     * @throws StorageException 
     * @throws URISyntaxException 
     * @throws WTException 
     * 
     */
    private static CloudBlobDirectory getBlobFolderShare(CloudBlobContainer theRootDirectory, String folderPath, String defaultDelimiter) throws URISyntaxException, StorageException, WTException {
        CloudBlobDirectory toReturn = null;
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getBlobFolderShare()");
            LOG.debug("Folder path is -> " + folderPath);
            LOG.debug("Azure delimiter is -> " + defaultDelimiter);
        }
        if (folderPath == null || folderPath.isEmpty()) {
            throw new WTException(WTMessage.getLocalizedMessage(jsonToPimInterfaceRB.class.getName(), jsonToPimInterfaceRB.EX_AZURE_FOLDER_PATH_NOT_FOUND));
        } if (theRootDirectory == null) {
            throw new WTException(WTMessage.getLocalizedMessage(jsonToPimInterfaceRB.class.getName(), jsonToPimInterfaceRB.EX_AZURE_SHARE_NOT_FOUND));
        }
        if (folderPath.startsWith(defaultDelimiter)) {
            folderPath = folderPath.substring(1);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Folder path is now trimmed to -> " + folderPath);
            }
        }
        toReturn = theRootDirectory.getDirectoryReference(folderPath);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Returning the azure share folder reference!");
            LOG.debug("Exiting method getBlobFolderShare()");
        }
        return toReturn; 
    }

    /**
     * This method will return a reference to the Azure storage based upon a set of values obtained from SITE-level preferences.

     * @throws StorageException 
     * @throws URISyntaxException 
     * @throws WTException 
     * @throws RuntimeException 
     * @throws InvalidKeyException
     * @throws IOException
     */
    private static CloudBlobClient getBlobClientReference() throws RuntimeException, IOException, URISyntaxException, InvalidKeyException, StorageException, WTException {
        boolean isHttps = false;
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getBlobClientReference()");
        }
        String protocolType = (String) PreferenceHelper.service.getValue(BLOB_STORE_PROTOCOL, CLIENT_NAME);
        if (protocolType.equalsIgnoreCase(HTTPS_PROTOCOL)) {
            isHttps = true; 
            if (LOG.isDebugEnabled()) {
                LOG.debug("Will use HTTPS protocol");
            }
        } 
        String endpointToUse = (String) PreferenceHelper.service.getValue(BLOB_STORE_ENDPOINT, CLIENT_NAME);
        String accountToUseName = (String) PreferenceHelper.service.getValue(BLOB_STORE_ACCOUNT, CLIENT_NAME);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Endpoint to use -> " + endpointToUse);
            LOG.debug("Account name to use -> " + accountToUseName);
        }
        String property = WTProperties.getLocalProperties().getProperty(WT_HOME_PROPERTY);
        String passwordForStorage = WTKeyStoreUtil.decryptProperty(BLOB_PASSWORD_PROPERTY, ENC_BLOB_PASSWORD_PROPERTY, property);
        StorageCredentials credentials = new StorageCredentialsAccountAndKey(accountToUseName, passwordForStorage);
        CloudStorageAccount storageAccount = new CloudStorageAccount(credentials, isHttps, endpointToUse, accountToUseName);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Setting up a connection to the Azure webstore");
            LOG.debug("Exiting method getBlobClientReference()");
        }
        return storageAccount.createCloudBlobClient();
    }


    /**
     * Gets the content item objects from WTDocument.
     * 
     * @param document
     *            the WTDocument
     * @param contentRoleType
     *            the ContentRoleType object. If null all content items will be returned
     * @param dataFormat
     *            the DataFormat object. If null method will return all items.
     * @return the content QueryResult
     * @throws WTException
     *             the WTException
     * @throws WTPropertyVetoException
     *             the wT property veto exception
     */
    public static QueryResult getContent(WTDocument document, ContentRoleType contentRoleType, DataFormat dataFormat) throws WTException, WTPropertyVetoException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getContent()");
            LOG.debug("Related document is -> " + document.getIdentity());
            if (contentRoleType != null) {
                LOG.debug("Content type to get is -> " + contentRoleType.getDisplay());
            }
        }
        QuerySpec criteria = new QuerySpec(wt.content.ContentItem.class, wt.content.HolderToContent.class);
        if (contentRoleType != null) {
            criteria.appendWhere(new SearchCondition(ContentItem.class, ContentItem.ROLE, SearchCondition.EQUAL, contentRoleType), new int[]{});
        }
        if (dataFormat != null) {
            if (contentRoleType != null) {
                criteria.appendAnd();
            }
            long dataFormatId = dataFormat.getPersistInfo().getObjectIdentifier().getId();
            criteria.appendWhere(new SearchCondition(ContentItem.class, QS_FORMAT_KEY, SearchCondition.EQUAL, dataFormatId), new int[]{});
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Will now try finding the required content file for document -> " + document.getIdentity());
        }
        QueryResult navigationResults = PersistenceHelper.manager.navigate(document, HolderToContent.CONTENT_ITEM_ROLE, criteria, false);
        ObjectVector objectVector = new ObjectVector(new Vector<Object>());
        HolderToContent holderLink = null;
        while (navigationResults.hasMoreElements()) {
            holderLink = (HolderToContent) navigationResults.nextElement();
            ContentItem contentItem = holderLink.getContentItem();
            contentItem.setHolderLink(holderLink);
            objectVector.addElement(contentItem);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Found content item -> " + contentItem.getIdentity());
            }
        }
        QueryResult results = new QueryResult(objectVector);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method getContent()");
        }
        return results;
    }

    /**
     * This method will return an array list of all changed/added documents found in the passed Info*Engine group.

     * @param theCollection - an I*E collection representing the results of an Info*Engine task
     */
    private static ArrayList<IeNode> getRelevantDocuments(IeCollection theCollection) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getRelevantDocuments()");
        }
        ArrayList<IeNode> toReturn = new ArrayList<IeNode>();
        IeGroup addedDocs = theCollection.getGroup(IE_ADDED_DOCS);
        IeGroup changedDocs = theCollection.getGroup(IE_CHANGED_DOCS);
        IeGroup unchangedDocs = theCollection.getGroup(IE_UNCHANGED_DOCS);
        toReturn.addAll(getDocumentsFromGroup(addedDocs));
        toReturn.addAll(getDocumentsFromGroup(changedDocs));
        toReturn.addAll(getDocumentsFromGroup(unchangedDocs));
        if (LOG.isDebugEnabled()) {
            LOG.debug("Below is a list of returned entries found in the I*E group");
            for (IeNode theNode:toReturn) {
                LOG.debug(theNode.toString());
            }
            LOG.debug("Exiting method getRelevantDocuments()");
        }
        return toReturn;
    }
    /**
     * This method will return an array list of all changed/added articles found in the passed Info*Engine group.

     * @param theCollection - an I*E collection representing the results of an Info*Engine task
     */
    private static ArrayList<IeNode> getRelevantArticles(IeCollection theCollection) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getRelevantArticles()");
        }
        ArrayList<IeNode> toReturn = new ArrayList<IeNode>();
        IeGroup addedParts = theCollection.getGroup(IE_ADDED_PARTS);
        IeGroup changedParts = theCollection.getGroup(IE_CHANGED_PARTS);
        IeGroup unchangedParts = theCollection.getGroup(IE_UNCHANGED_PARTS);
        toReturn.addAll(getArticlesFromGroup(addedParts));
        toReturn.addAll(getArticlesFromGroup(changedParts));
        toReturn.addAll(getArticlesFromGroup(unchangedParts));
        if (LOG.isDebugEnabled()) {
            LOG.debug("Below is a list of returned entries found in the I*E group");
            for (IeNode theNode:toReturn) {
                LOG.debug(theNode.toString());
            }
            LOG.debug("Exiting method getRelevantDocuments()");
        }
        return toReturn;
    }
    /**
     * This method will return an array list of all documents found in the passed Info*Engine group.

     * @param IeGroup ieGroup - an I*E group to be searched for documents
     */
    private static ArrayList<IeNode> getDocumentsFromGroup(IeGroup ieGroup) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getDocumentsFromGroup()");
            LOG.debug("I*E group is -> " + ieGroup.getName());
        }
        ArrayList<IeNode> toReturn = new ArrayList<IeNode>();
        Enumeration partsEnumeration = ieGroup.getChildren();
        while (partsEnumeration.hasMoreElements()) {
            Object ieEntryObject = partsEnumeration.nextElement();
            if (ieEntryObject instanceof IeNode) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Adding node -> " + ((IeNode) ieEntryObject).toString());
                }
                toReturn.add((IeNode) ieEntryObject);
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method getDocumentsFromGroup()");
        }
        return toReturn;

    }
    /**
     * This method will return an array list of all articles found in the passed Info*Engine group.

     * @param IeGroup ieGroup - an I*E group to be searched for articles
     */
    private static ArrayList<IeNode> getArticlesFromGroup(IeGroup ieGroup) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method getArticlesFromGroup()");
            LOG.debug("I*E group is -> " + ieGroup.getName());
        }
        ArrayList<IeNode> toReturn = new ArrayList<IeNode>();
        Enumeration enumeration = ieGroup.getChildren();
        while (enumeration.hasMoreElements()) {
            Object ieEntryObject = enumeration.nextElement();
            if (ieEntryObject instanceof IeNode) {
                IeNode ieNodeObject = (IeNode) ieEntryObject;
                IeAtt salesTypeAtt = ieNodeObject.getAtt(SALES_PART_TYPE);
                if (salesTypeAtt != null && salesTypeAtt.getDatum().getNodeValue().equals(ARTICLE_MARKER)) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Adding node -> " + ((IeNode) ieEntryObject).toString());
                    }
                    toReturn.add(ieNodeObject);
                }
            }

        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method getArticlesFromGroup()");
        }
        return toReturn;
    }

    /**
     * The method will create a KAFKA producer instance based on a set if properties
     * @param serverName - the name of the KAFKA server to be used
     * @param clientId - the identifier of the KAFKA client
     * @throws WTException 
     * @throws IOException 
     * */
    private static Producer<Long, String> createProducer(String serverName, String clientId) throws WTException, IOException {
        Properties props = new Properties();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method createProducer()");
            LOG.debug("Will now try to establish a connection to KAFKA server...");
        }
        String wtHomeProperty = WTProperties.getLocalProperties().getProperty(WT_HOME_PROPERTY);
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, serverName);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, clientId);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Server name of KAFKA is -> " + serverName);
            LOG.debug("Client ID of Windchill for KAFKA is -> " + clientId);
        }
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        if ((boolean) PreferenceHelper.service.getValue(KAFKA_SSL_ENCRYPT, CLIENT_NAME)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("SSL Encryption for KAFKA is required by Windchill!");
            }
            props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, SSL_PROTOCOL);
            String kafkaKeyStorePath = wtHomeProperty + TRUSTSTORE_KEY;
            props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, kafkaKeyStorePath);
            String encryptedKafkaTruststorePass = WTKeyStoreUtil.decryptProperty(KAFKA_PASSWORD_PROPERTY, KAFKA_ENC_PASSWORD_PROPERTY, wtHomeProperty);
            props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, encryptedKafkaTruststorePass);
        }
        if ((boolean) PreferenceHelper.service.getValue(KAFKA_USE_PASSKEY_FOR_SSL, CLIENT_NAME)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("SSL Encryption for KAFKA requires a trusted keystore!");
            }
            String kafkaKeyStorePath = wtHomeProperty + KEYSTORE_KEY;
            props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, kafkaKeyStorePath);
            if ((boolean) PreferenceHelper.service.getValue(KAFKA_USE_PASSKEY_FOR_SSL_TRUSTSTORE, CLIENT_NAME)) {
                String encryptedKafkaKeystorePass = WTKeyStoreUtil.decryptProperty(KAFKA_PASSWORD_FOR_KEYSTORE_PROPERTY, KAFKA_ENC_PASSWORD_FOR_KEYSTORE_PROPERTY, wtHomeProperty);
                props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, encryptedKafkaKeystorePass);
            }
           if ((boolean) PreferenceHelper.service.getValue(KAFKA_USE_PASSKEY_FOR_SSL_HANDSHAKE, CLIENT_NAME)) {
            String encryptedKafkaSslPass = WTKeyStoreUtil.decryptProperty(KAFKA_PASSWORD_FOR_SSL_HANDSHAKE_PROPERTY, KAFKA_ENC_PASSWORD_FOR_SSL_HANDSHAKE_PROPERTY, wtHomeProperty);
            props.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, encryptedKafkaSslPass);
           }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method createProducer()");
        }
        return new KafkaProducer<>(props);
    }
    
    /**
     * The method will create a KAFKA producer instance based on a set if properties
     * @param toUpdate - the IeAtt to have it's value modified
     * @param fileName - the filename of the uploaded document to be added to the IeAtt
     * */
    private static IeAtt updateRelatedDocsEntry(IeAtt toUpdate, String fileName) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method updateRelatedDocsEntry()");
        }
        if (toUpdate.getDatum().getNodeValue().isEmpty()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("IeAtt has no value - will create a new one with the entry -> " + fileName);
            }
            toUpdate.getDatum().setNodeValue(fileName);
        } else {
            
           String currentValue = toUpdate.getDatum().getNodeValue();
           if (LOG.isDebugEnabled()) {
               LOG.debug("IeAtt a value of -> " + currentValue);
               LOG.debug("Will append a value of -> " + fileName);
           }
           currentValue = "," + currentValue;
           toUpdate.getDatum().setNodeValue(currentValue);
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method updateRelatedDocsEntry()");
        }
        return toUpdate;
    }

}
