package com.ikea.ipim.interfaces;

import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("com.ikea.ipim.interfaces.jsonToPimInterfaceRB")
public class jsonToPimInterfaceRB extends WTListResourceBundle{

    @RBEntry("File passed to the interface is null!")
    public static final String EX_NULL_FILE_PASSED = "EX_NULL_FILE_PASSED";
    
    @RBEntry("File passed to the interface is does not exist!")
    public static final String EX_NONEXISTENT_FILE_PASSED = "EX_NONEXISTENT_FILE_PASSED";
    
    @RBEntry( "The interface supports only files! Directory traversal is not supported!")
    public static final String EX_DIRECTORY_PASSED = "EX_DIRECTORY_PASSED";
    
    @RBEntry( "An unnknown interface definition for the PIM system has been found! Cannot send the message through it!")
    public static final String EX_UNNKNOWN_INTERFACE_PASSED = "EX_UNNKNOWN_INTERFACE_PASSED";
    
    @RBEntry( "File path string passed to the interface is null!")
    public static final String EX_NULL_FILEPATH_PASSED = "EX_NULL_FILEPATH_PASSED";
    
    @RBEntry("File path string passed to the interface is empty!")
    public static final String EX_EMPTY_FILEPATH_PASSED = "EX_EMPTY_FILEPATH_PASSED";
    
    @RBEntry("Could not create folder paths: {0}")
    public static final String EX_FILEPATH_NOT_CREATED = "EX_FILEPATH_NOT_CREATED";
    
    @RBEntry("No folder path specified in the shared Azure container")
    public static final String EX_AZURE_FOLDER_PATH_NOT_FOUND = "EX_AZURE_FOLDER_PATH_NOT_FOUND";
    
    @RBEntry("No Azure container to be used for the share was specified")
    public static final String EX_AZURE_SHARE_NOT_FOUND = "EX_AZURE_SHARE_NOT_FOUND";
}
