package com.ikea.ipim.esi.renderers;

import java.lang.reflect.Field;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.infoengine.object.factory.Att;
import com.infoengine.object.factory.Element;
import com.ptc.core.components.factory.dataUtilities.CsmClassificationPickerHelper;
import com.ptc.core.lwc.server.PersistableAdapter;
import com.ptc.core.meta.common.TypeIdentifierHelper;
import com.ptc.core.meta.common.UpdateOperationIdentifier;
import com.ptc.core.meta.container.common.AttributeTypeSummary;
import com.ptc.core.meta.type.common.TypeInstance;
import com.ptc.core.meta.type.server.TypeInstanceUtility;
import com.ptc.windchill.esi.esipart.ESIWTPartRenderer;
import com.ptc.windchill.esi.rnd.ESIRendererException;

import wt.eff.Eff;
import wt.part.WTPart;
import wt.session.SessionHelper;

/**
 * This class is used for rendering the ESI XML that will be sent out to the distribution targets as a JSON.
 * Use this class when there is a need of custom re-calculating/readjusting IBA fields from code-behind defined for the ESI XML.
 */
public class IkeaESIPartRenderer extends ESIWTPartRenderer {

    private static String TYPE_ID_FIELD = "softTypeIdentifier";
    private static String CLASSIFICATION_ID_FIELD = "ProductClass";
    private static String TYPE_IDENTIFIER_ID_FIELD = "ti";
    private static final Logger LOG = Logger.getLogger(IkeaESIPartRenderer.class);
    
    /* (non-Javadoc)
     * @see com.ptc.windchill.esi.esipart.ESIWTPartRenderer#adjustPartElement(com.infoengine.object.factory.Element, java.lang.String, wt.part.WTPart, wt.eff.Eff[], java.util.Collection)
     */
    @Override
    protected Element adjustPartElement(Element element, String group, WTPart part, Eff[] effs, Collection targets)
    throws ESIRendererException {
        element = super.adjustPartElement(element, group, part, effs, targets);
        element = obtainSoftType(element, part);
        element = adjustToFullPath(element, part);
        return element;
    }
    
    /**
     * Method will obtain the soft type from the WTPart and set it in the marked ESI XML attribute.
     *
     * @param element - the I*E element that will have the softtype attribute populated 
     * @param part - the part representing the I*E Element that will have the softtype extracted.
     * @return the element
     */
    private Element obtainSoftType(Element element, WTPart part) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method obtainSoftType()");
            LOG.debug("WTPart is -> " + part.getIdentity());
        }
        Att att = element.getAtt(TYPE_ID_FIELD);
        if (att != null) {
           att.setValue(TypeIdentifierHelper.getType(part).getTypeInternalName());
           if (LOG.isDebugEnabled()) {
               LOG.debug("Attribute value set to -> " + att.getValue());
           }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method obtainSoftType()");
            LOG.debug("Element after adjusting is -> " + element.toString());
        }
        return element;
    }
    
    /**
     * The method will adjust the Classification attribute to the full path as seen in Windchill InfoPage for the part.
     *
     * @param element - the I*E element that will have the full classification path attribute populated 
     * @param part - the part representing the I*E Element that will have the full classification path extracted.
     * @return the element
     * @throws ESIRendererException the ESI renderer exception
     */
    private Element adjustToFullPath(Element element, WTPart part) throws ESIRendererException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Entering method obtainSoftType()");
            LOG.debug("WTPart is -> " + part.getIdentity());
        }
        Att att = element.getAtt(CLASSIFICATION_ID_FIELD);
        if (att != null) {
            try {
                PersistableAdapter adapter = new PersistableAdapter(part, null, SessionHelper.getLocale(), new UpdateOperationIdentifier());
                adapter.load(CLASSIFICATION_ID_FIELD);
                String theClassification = (String) adapter.get(CLASSIFICATION_ID_FIELD);
                if (theClassification != null && !theClassification.isEmpty()) {
                    AttributeTypeSummary ats = adapter.getAttributeDescriptor(CLASSIFICATION_ID_FIELD);
                     if (CsmClassificationPickerHelper.hasClassificationConstraint(ats)) {
                         Field privateTypeInstanceField = PersistableAdapter.class.getDeclaredField(TYPE_IDENTIFIER_ID_FIELD);
                         privateTypeInstanceField.setAccessible(true);
                         Object theTypeIdentifierObject = privateTypeInstanceField.get(adapter);
                         TypeInstance typeInstance = null;
                         if (theTypeIdentifierObject instanceof TypeInstance) {
                             typeInstance = (TypeInstance)theTypeIdentifierObject;
                             TypeInstanceUtility.populateMissingTypeContent(typeInstance, null);
                         }
                         if (typeInstance != null) {
                             String toSet = CsmClassificationPickerHelper.getFullHierarchyOfClassificationNode(ats, typeInstance, theClassification, SessionHelper.getLocale());
                             att.setValue(toSet);
                             if (LOG.isDebugEnabled()) {
                                 LOG.debug("Attribute value set to -> " + toSet);
                             }
                         }
                     }
                    
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new ESIRendererException(e);
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exiting method adjustToFullPath()");
            LOG.debug("Element after adjusting is -> " + element.toString());
        }
        return element;
    }
 
}
