package com.ikea.ipim.esi.renderers;


import java.util.Collection;

import org.apache.commons.io.FilenameUtils;

import com.ikea.ipim.interfaces.JSONToPIMInterface;
import com.infoengine.object.factory.Att;
import com.infoengine.object.factory.Element;
import com.ptc.windchill.esi.esidoc.ESIDocumentsRenderer;
import com.ptc.windchill.esi.rnd.ESIRendererException;

import wt.content.ApplicationData;
import wt.content.ContentRoleType;
import wt.doc.WTDocument;
import wt.fc.BinaryLink;
import wt.fc.QueryResult;
import wt.part.WTPartDescribeLink;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;

public class IkeaESIDocRenderer extends ESIDocumentsRenderer {

    @Override
    protected Element adjustDocLinkElement(Element element, String group, BinaryLink documentLink, Collection targets) throws ESIRendererException {
        element = super.adjustDocLinkElement(element, group, documentLink, targets);
        return obtainDocumentExtension(element, documentLink);
    }

    private Element obtainDocumentExtension(Element element, BinaryLink documentLink) throws ESIRendererException {
        if (documentLink instanceof WTPartDescribeLink) {
            WTPartDescribeLink theLink = (WTPartDescribeLink)documentLink;
            WTDocument theDocument = theLink.getDescribedBy();
            try {
                QueryResult theObtainedFiles = JSONToPIMInterface.getContent(theDocument, ContentRoleType.PRIMARY, null);
                if (theObtainedFiles.size() > 0) {
                    Object primaryContent = theObtainedFiles.nextElement();
                    if (primaryContent instanceof ApplicationData) {
                        ApplicationData theFileContent = (ApplicationData)primaryContent;
                        String theExtension = FilenameUtils.getExtension(theFileContent.getFileName());
                        Att att = element.getAtt("DocumentExtension");
                        if (att != null) {
                           att.setValue(theExtension);
                        }
                    }
                }
            } catch (WTException | WTPropertyVetoException e) {
                e.printStackTrace();
                throw new ESIRendererException(e);
            }
        }
        return element;
    }

    
}
