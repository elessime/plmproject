package com.ikea.ipim.sbom;

import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("com.ikea.ipim.sbom.IkeaSBOMRB")
public class IkeaSBOMRB extends WTListResourceBundle {
    @RBEntry("Manage Suppliers")
    public static final String PRIVATE_CONSTANT_03 = "sbom.manageSuppliers.description";

    @RBEntry("Manage Suppliers")
    public static final String PRIVATE_CONSTANT_04 = "sbom.manageSuppliers.title";
    
    @RBEntry("List of involved suppliers")
    public static final String INVOLVED_SUPPLIERS_TITLE = "INVOLVED_SUPPLIERS_TITLE";
    
    @RBEntry("List of involved suppliers for part {0}")
    public static final String INVOLVED_SUPPLIERS_FOR_PART_TITLE = "INVOLVED_SUPPLIERS_FOR_PART_TITLE";
    
    @RBEntry("List of suppliers to share the part {0}")
    public static final String SUPPLIERS_TO_SHARE_PART_TITLE = "SUPPLIERS_TO_SHARE_PART_TITLE";
    
    @RBEntry("List of suppliers to update the part {0}")
    public static final String SUPPLIERS_TO_UPDATE_PART_TITLE = "SUPPLIERS_TO_UPDATE_PART_TITLE";
    
    @RBEntry("Involved Suppliers")
    public static final String involvedSupplier = "involvedSupplier";
    
    @RBEntry("Send to suppliers")
    public static final String SEND_TO_SUPPLIERS_TITLE = "SEND_TO_SUPPLIERS_TITLE";
    
    @RBEntry("Checkout Status")
    public static final String checkedOutForSupplier = "checkedOutForSupplier";
    
    @RBEntry("Send to suppliers")
    public static final String PRIVATE_CONSTANT_05 = "sbom.supplierSendButton.description";

    @RBEntry("Send to suppliers")
    public static final String PRIVATE_CONSTANT_06 = "sbom.supplierSendButton.title";
    
    @RBEntry("Update suppliers")
    public static final String PRIVATE_CONSTANT_07 = "sbom.supplierUpdateButton.description";

    @RBEntry("Update suppliers")
    public static final String PRIVATE_CONSTANT_08 = "sbom.supplierUpdateButton.title";
    
    @RBEntry("Available Suppliers")
    public static final String availableSuppliers = "availableSuppliers";
    
    @RBEntry("Share in read-only mode?")
    public static final String shareToSelectedSupplier = "shareToSelectedSupplier";

    @RBEntry("Share in check-out mode?")
    public static final String checkOutShareToSelectedSupplier = "checkOutShareToSelectedSupplier";
    
    @RBEntry("No project exists for supplier {0}")
    public static final String noProjectForSelectedSupplier = "noProjectForSelectedSupplier";
    
    @RBEntry("Shared version")
    public static final String sharedToSupplierVersion = "sharedToSupplierVersion";
    
    @RBEntry("Could not identify selected share for part {0}")
    public static final String selectedShareNotIdentified = "selectedShareNotIdentified";
    
    @RBEntry("Part {0} is already shared to project {1}. Will not re-share it again!")
    public static final String selectedShareAlreadyInContainer = "selectedShareAlreadyInContainer";
    
    @RBEntry("Part {0} is not an authorised part for supplier {1}. Will not share it!")
    public static final String partToShareIsNotAuthorised = "partToShareIsNotAuthorised";
    
    @RBEntry("Part {0} has been shared for supplier {1}.")
    public static final String selectedShareAddedToContainer = "selectedShareAddedToContainer";
    
    @RBEntry("Supplier {0} is not in the list of known suppliers!")
    public static final String supplierNotOnResourceList = "supplierNotOnResourceList";
    
    @RBEntry("Could not identify the existence of part {0} in view {1} for supplier {2}!")
    public static final String partInViewAndSupplierNotFound = "partInViewAndSupplierNotFound";
    
    @RBEntry("Could not check-out the part {0} in view {1} to manufacturer {2}!")
    public static final String partNotCheckedOut = "partNotCheckedOut";
    
    @RBEntry("Part {0} and the associated BOM was shared in checkout mode in view {1} to manufacturer {2}!")
    public static final String partCheckedOut =  "partCheckedOut";
    
    @RBEntry("Could not check-out the part {0} in view {1} to manufacturer {2} due to an internal error {3}!")
    public static final String partCheckOutException =  "partCheckOutException";
    
    @RBEntry("Could not convert the part {0} to view {1} for manufacturer {2} due to an internal error {3}!")
    public static final String partTransformationException =  "partTransformationException";
    
    @RBEntry("Could not convert the part {0} to view {1} for manufacturer {2}!")
    public static final String partNotTransformed = "partNotTransformed";
    
    @RBEntry("Fond an error after the Supplier BOM transformation: {0}")
    public static final String AFTER_TRANSFORM_PART_EXCEPTION = "afterTransformBOMException";
    
    @RBEntry("Shared Supplier Status")
    public static final String sharedToSupplierStatus = "sharedToSupplierStatus";
    
    @RBEntry("Update shared part on suppliers")
    public static final String UPDATE_SUPPLIERS_TITLE = "UPDATE_SUPPLIERS_TITLE";
    
    @RBEntry("Reshare outdated part to supplier?")
    public static final String reshareOutdatedToSelectedSupplier = "reshareOutdatedToSelectedSupplier";
    
    @RBEntry("Part {0} has been reshared for supplier {1}.")
    public static final String selectedResharedToContainer = "selectedResharedToContainer";
    
    @RBEntry("Could not find the container to reshare the part {0} into!")
    public static final String couldNotDetectReshareContainer = "couldNotDetectReshareContainer";
    
    @RBEntry("Could not find the equivalent part to be unshared associated with selected part {0}!")
    public static final String couldNotDetectUnsharePart = "couldNotDetectUnsharePart";
    
    @RBEntry("Could not update the equivalence for part {0} because of an error! Error message is {1}")
    public static final String updateEquivalenceFailed = "updateEquivalenceFailed";
    
    @RBEntry("Could not copy the BOM structure, undo the sharing and update the equivalence related to part {0} for Supplier {1}")
    public static final String copyUnshareOperationFailed = "copyUnshareOperationFailed";
    
    @RBEntry("Could not reshare the BOM structure after updating the equivalence related to part {0} for Supplier {1}")
    public static final String resharedToContainerFailed = "resharedToContainerFailed";
}
