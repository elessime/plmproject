package com.ikea.ipim.sbom.datautility;

import org.apache.log4j.Logger;

import com.ikea.ipim.utils.types.IkeaPartUtils;
import com.ptc.core.components.descriptor.ModelContext;
import com.ptc.core.components.factory.dataUtilities.DefaultDataUtility;
import com.ptc.core.components.rendering.guicomponents.IconComponent;
import com.ptc.netmarkets.model.NmOid;
import com.ptc.windchill.enterprise.associativity.asb.client.ASBMessages;

import wt.associativity.EquivalenceStatus;
import wt.fc.ObjectIdentifier;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.part.WTPart;
import wt.util.WTException;
import wt.util.WTMessage;

public class SharedSupplierStatusUtility extends DefaultDataUtility {

    private static final Logger LOGGER = Logger.getLogger(SharedSupplierStatusUtility.class);
    public static final String OUT_OF_DATE_ICON = "com/ptc/windchill/explorer/config/images/outofdate9x9.gif";
    public static final String IN_SYNC_ICON = "com/ptc/windchill/explorer/config/images/accept.gif";
    public static final String NO_ASSOCIATION_ICON = "com/ptc/windchill/explorer/config/images/warning9x9.gif";
//    @Override
//    public Object getDataValue(String componentId, Object datum, ModelContext modelContext) throws WTException {
//        Object component = super.getDataValue(componentId, datum, modelContext);
//        if (datum instanceof WTPart) {
//            WTPart toCheck = (WTPart)datum;
//            if (LOGGER.isDebugEnabled()) {
//                LOGGER.debug("Found a shared part -> " + toCheck.getIdentity());
//            }
//            NmOid theOid =  modelContext.getNmCommandBean().getPrimaryOid();
//            ObjectIdentifier idOfPart = theOid.getOidObject();
//            Persistable persistToCheck = PersistenceHelper.manager.refresh(idOfPart);
//            if (persistToCheck instanceof WTPart) {
//                String tooltip ="";
//                EquivalenceStatus theResult = IkeaPartUtils.getStatusBetweenUpAndDownstream((WTPart)persistToCheck, toCheck);
//                    if (theResult.equals(EquivalenceStatus.OUT_OF_SYNCH_EQUIVALENT)) {
//                        tooltip = WTMessage.getLocalizedMessage(ASBMessages.class.getName(), ASBMessages.EQUIVALENT_NOT_LATEST);
//                        component = IconComponent.getIconComponent(OUT_OF_DATE_ICON, tooltip);
//                    } else if (theResult.equals(EquivalenceStatus.UNKNOWN_EQUIVALENT) || theResult.equals(EquivalenceStatus.NO_EQUIVALENT)) {
//                        tooltip = WTMessage.getLocalizedMessage(ASBMessages.class.getName(), ASBMessages.EQUIVALENT_OCCURRENCE_DOES_NOT_EXIST);
//                        component = IconComponent.getIconComponent(NO_ASSOCIATION_ICON, tooltip);
//                    } else if (theResult.equals(EquivalenceStatus.UPDATED_EQUIVALENT)) {
//                        tooltip = WTMessage.getLocalizedMessage(ASBMessages.class.getName(), ASBMessages.EQUIVALENT_LATEST);
//                        component = IconComponent.getIconComponent(IN_SYNC_ICON, tooltip);
//                    }
//                
//            }
//       
//        }
//        return component;
//    }
    
}
