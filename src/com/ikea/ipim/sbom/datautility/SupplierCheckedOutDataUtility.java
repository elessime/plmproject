package com.ikea.ipim.sbom.datautility;

import org.apache.log4j.Logger;

import com.ikea.ipim.utils.types.IkeaProjectUtils;
import com.ptc.core.components.descriptor.ModelContext;
import com.ptc.core.components.factory.dataUtilities.DefaultDataUtility;
import com.ptc.core.components.rendering.guicomponents.IconComponent;
import com.ptc.core.htmlcomp.jstable.IconToolTipResource;

import wt.part.WTPart;
import wt.util.WTException;
import wt.util.WTMessage;
import wt.vc.views.Variation2;

public class SupplierCheckedOutDataUtility extends DefaultDataUtility {
    private static final Logger LOGGER = Logger.getLogger(SupplierCheckedOutDataUtility.class);
    private static String CHECKED_OUT_RB_ENTRY = "CHECKED_OUT_TO_SANDBOX";
    private static String ICON_PATH = "com/ptc/core/htmlcomp/images/checkedout_fromPDM9x9.gif";
    @Override
    public Object getDataValue(String componentId, Object datum, ModelContext modelContext) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering getDataValue() method");
            LOGGER.debug("Component ID is -> " + componentId);
            LOGGER.debug("Passed datum is -> " + datum.toString());
        }
        Object component = super.getDataValue(componentId, datum, modelContext);
        if (datum instanceof WTPart) {
            WTPart toCheck = (WTPart)datum;
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Found a shared part -> " + toCheck.getIdentity());
                }
            Variation2 supplierVariation = toCheck.getVariation2();
            if (supplierVariation==null) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Exiting getDataValue() method with the default UI component");
                }
                return component;
            }
            if (IkeaProjectUtils.isPartInProject(toCheck, supplierVariation.getDisplay())) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Part " + toCheck.getIdentity() + " is shared as checked out to project " + supplierVariation.getDisplay());
                    }
                String tooltip = WTMessage.getLocalizedMessage(IconToolTipResource.class.getName(), CHECKED_OUT_RB_ENTRY);
                component = IconComponent.getIconComponent(ICON_PATH, tooltip);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Exiting getDataValue() method with the icon component set to checked out from PDM");
                }
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting getDataValue() method");
        }
        return component;
    }
}
