package com.ikea.ipim.sbom.datautility;

import org.apache.log4j.Logger;

import com.ptc.core.components.descriptor.ModelContext;
import com.ptc.core.components.factory.dataUtilities.DefaultDataUtility;
import com.ptc.core.components.rendering.guicomponents.TextDisplayComponent;

import wt.fc.Persistable;
import wt.inf.sharing.SharedContainerMap;
import wt.part.WTPart;
import wt.projmgmt.admin.Project2;
import wt.util.WTException;
import wt.vc.views.Variation2;

public class InvolvedSupplierDataUtility extends DefaultDataUtility {

    private static final Logger LOGGER = Logger.getLogger(InvolvedSupplierDataUtility.class);
    @Override
    public Object getDataValue(String componentId, Object datum, ModelContext modelContext) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering getDataValue() method");
            LOGGER.debug("Component ID is -> " + componentId);
            LOGGER.debug("Passed datum is -> " + datum.toString());
        }
        Object component = super.getDataValue(componentId, datum, modelContext);
           if (datum instanceof WTPart) {
               WTPart toCheck = (WTPart)datum;
               if (LOGGER.isDebugEnabled()) {
                   LOGGER.debug("Part to be used is -> " + toCheck.getIdentity());
                   }
               Variation2 supplierVariation = toCheck.getVariation2();
               String supplierName = supplierVariation.getDisplay();
               if (LOGGER.isDebugEnabled()) {
                   LOGGER.debug("Supplier bound to part is -> " + supplierName);
                   }
               TextDisplayComponent variationDisplay = new TextDisplayComponent(supplierName, supplierName);
               variationDisplay.setValueHidden(false);
               component = variationDisplay;
           } else if (datum instanceof SharedContainerMap) {
               SharedContainerMap theMap = (SharedContainerMap)datum;
               if (LOGGER.isDebugEnabled()) {
                   LOGGER.debug("Found a shared container map object - will take the supplier from it!");
                   }
               Persistable targetContainer = theMap.getTargetContainerRef().getObject();
               if (targetContainer instanceof Project2) {
                   if (LOGGER.isDebugEnabled()) {
                       LOGGER.debug("Supplier bound to part is -> " +  ((Project2)targetContainer).getName());
                       }
                   TextDisplayComponent variationDisplay = new TextDisplayComponent(((Project2)targetContainer).getName(), ((Project2)targetContainer).getName());
                   variationDisplay.setValueHidden(false);
                   component = variationDisplay;
               }
           }  if (LOGGER.isDebugEnabled()) {
               LOGGER.debug("Exiting getDataValue() method");
           }
        return component;
    }
    
}
