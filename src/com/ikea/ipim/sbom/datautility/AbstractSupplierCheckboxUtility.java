package com.ikea.ipim.sbom.datautility;

import org.apache.log4j.Logger;

import com.ikea.ipim.sbom.beans.SharedSupplierBean;
import com.ikea.ipim.sbom.beans.UpdateSupplierBean;
import com.ptc.core.components.descriptor.ModelContext;
import com.ptc.core.components.factory.dataUtilities.DefaultDataUtility;
import com.ptc.core.components.rendering.guicomponents.CheckBox;

import wt.util.WTException;

public abstract class AbstractSupplierCheckboxUtility extends DefaultDataUtility {
    private static final Logger LOGGER = Logger.getLogger(AbstractSupplierCheckboxUtility.class);
    @Override
    public Object getDataValue(String componentId, Object datum, ModelContext modelContext) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering getDataValue() method");
            LOGGER.debug("Component ID is -> " + componentId);
            LOGGER.debug("Passed datum is -> " + datum.toString());
        }
        if (datum instanceof SharedSupplierBean || datum instanceof UpdateSupplierBean) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Supplier Bean detected - will createw a checkbox for it");
            }
            CheckBox toReturn = getCheckboxForUI(componentId, datum);
            if (isCheckboxReadOnly(datum)) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Setting the check box to read-only mode!");
                }
                toReturn.setReadOnly(true);
                toReturn.setEditable(false);
            }      
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Exiting getDataValue() method with a check box UI element");
            }
            return toReturn;
        } else {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Exiting getDataValue() method with a default UI element");
            }
            return super.getDataValue(componentId, datum, modelContext);
        }
        
    }
    protected abstract CheckBox getCheckboxForUI(String componentId, Object datum);
    protected abstract boolean isCheckboxReadOnly(Object theBean);
}
