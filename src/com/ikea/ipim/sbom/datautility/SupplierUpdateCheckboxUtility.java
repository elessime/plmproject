package com.ikea.ipim.sbom.datautility;

import org.apache.log4j.Logger;
import com.ikea.ipim.sbom.beans.UpdateSupplierBean;
import com.ptc.core.components.rendering.guicomponents.CheckBox;

public class SupplierUpdateCheckboxUtility extends AbstractSupplierCheckboxUtility {
    private static final String RESHARED_BOX_MARKER = "_ReShareBox";
    private static final String RESHARED_MARKER = "_ReShare";
    private static final String RESHARED_DU_MARKER = "isToBeUpdated";
    private static final Logger LOGGER = Logger.getLogger(SupplierUpdateCheckboxUtility.class);
    @Override
    protected CheckBox getCheckboxForUI(String componentId, Object datum) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering getCheckboxForUI() method");
            LOGGER.debug("Component ID is -> " + componentId);
            LOGGER.debug("Passed datum is -> " + datum.toString());
        }
        CheckBox toReturn = new CheckBox();
        if (componentId.equals(RESHARED_DU_MARKER) && datum instanceof UpdateSupplierBean) {
            toReturn.setId(((UpdateSupplierBean) datum).getOid() + RESHARED_BOX_MARKER);
            toReturn.setChecked(((UpdateSupplierBean) datum).isOutdated());
            toReturn.setName(((UpdateSupplierBean) datum).getOid() + RESHARED_MARKER);
        }
        return toReturn;
    }

    @Override
    protected boolean isCheckboxReadOnly(Object theBean) {
        boolean toReturn = false;
        if (theBean instanceof UpdateSupplierBean && !((UpdateSupplierBean)theBean).isOutdated()) {
            toReturn = true;
        } return toReturn;
    }

}
