package com.ikea.ipim.sbom.datautility;

import org.apache.log4j.Logger;

import com.ptc.core.components.descriptor.ModelContext;
import com.ptc.windchill.enterprise.object.dataUtilities.VersionDisplayDataUtility;

import wt.fc.Persistable;
import wt.inf.sharing.SharedContainerMap;
import wt.part.WTPart;
import wt.util.WTException;

public class InvolvedSupplierVersionUtility extends VersionDisplayDataUtility {

    private static String VERSION_COLUMN = "version";
    private static final Logger LOGGER = Logger.getLogger(InvolvedSupplierVersionUtility.class);
    @Override
    public Object getDataValue(String componentId, Object datum, ModelContext modelContext) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering getDataValue() method");
            LOGGER.debug("Component ID is -> " + componentId);
            LOGGER.debug("Passed datum is -> " + datum.toString());
        }
        Object component = super.getDataValue(VERSION_COLUMN, datum, modelContext);
        if (datum instanceof SharedContainerMap) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Found a shared container map object - will take the shared object from it!");
                }
            SharedContainerMap theMap = (SharedContainerMap)datum;
            Persistable sharedPersistable = theMap.getShared();
            if(sharedPersistable instanceof WTPart) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Found a shared part -> " + ((WTPart)sharedPersistable).getIdentity());
                    }
                component = super.getDataValue(VERSION_COLUMN, sharedPersistable, modelContext);
            }
        } if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting getDataValue() method");
        }
        return component;
    }

}
