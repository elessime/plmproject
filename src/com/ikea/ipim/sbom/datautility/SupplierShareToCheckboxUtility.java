package com.ikea.ipim.sbom.datautility;

import org.apache.log4j.Logger;

import com.ikea.ipim.sbom.beans.SharedSupplierBean;
import com.ptc.core.components.rendering.guicomponents.CheckBox;

public class SupplierShareToCheckboxUtility extends AbstractSupplierCheckboxUtility {
    private static final String SHARED_BOX_MARKER = "_ShareBox";
    private static final String SHARED_MARKER = "_Share";
    private static final String SHARED_DU_MARKER = "isShared";
    private static final Logger LOGGER = Logger.getLogger(SupplierShareToCheckboxUtility.class);

    @Override
    protected CheckBox getCheckboxForUI(String componentId, Object datum) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering getCheckboxForUI() method");
            LOGGER.debug("Component ID is -> " + componentId);
            LOGGER.debug("Passed datum is -> " + datum.toString());
        }
        CheckBox toReturn = new CheckBox();
        if (componentId.equals(SHARED_DU_MARKER) && datum instanceof SharedSupplierBean) {
            toReturn.setId(((SharedSupplierBean) datum).getOid() + SHARED_BOX_MARKER);
            toReturn.setChecked(((SharedSupplierBean) datum).isShared());
            toReturn.setName(((SharedSupplierBean) datum).getOid() + SHARED_MARKER);
        }
        return toReturn;
    }

    @Override
    protected boolean isCheckboxReadOnly(Object theBean) {
       boolean toReturn = false;
       if (theBean instanceof SharedSupplierBean && (((SharedSupplierBean)theBean).isShared() || ((SharedSupplierBean)theBean).getProject() == null || ((SharedSupplierBean)theBean).isCheckedOutToProj())) {
           toReturn = true;
       }
       return toReturn;
    }

}
