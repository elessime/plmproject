package com.ikea.ipim.sbom.datautility;

import org.apache.log4j.Logger;

import com.ikea.ipim.sbom.beans.SharedSupplierBean;
import com.ptc.core.components.rendering.guicomponents.CheckBox;


public class SupplierCheckOutCheckboxUtility  extends AbstractSupplierCheckboxUtility {
    private static final String CHECKOUT_BOX_MARKER = "_CheckOutBox";
    private static final String CHECKOUT_MARKER = "_CheckOut";
    private static final String CHECKOUT_DU_MARKER = "isCheckedOutToProj";
    private static final Logger LOGGER = Logger.getLogger(SupplierCheckOutCheckboxUtility.class);
    
    protected CheckBox getCheckboxForUI(String componentId, Object datum) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering getCheckboxForUI() method");
            LOGGER.debug("Component ID is -> " + componentId);
            LOGGER.debug("Passed datum is -> " + datum.toString());
        }
        CheckBox toReturn = new CheckBox(); 
        if (componentId.equals(CHECKOUT_DU_MARKER) && datum instanceof SharedSupplierBean) {
            toReturn.setId(((SharedSupplierBean)datum).getOid() + CHECKOUT_BOX_MARKER);
            toReturn.setChecked(((SharedSupplierBean)datum).isCheckedOutToProj());
            toReturn.setName(((SharedSupplierBean)datum).getOid() + CHECKOUT_MARKER);
        }
        return toReturn;
    }
    @Override
    protected boolean isCheckboxReadOnly(Object theBean) {
        boolean toReturn = false;
        if (theBean instanceof SharedSupplierBean && (((SharedSupplierBean)theBean).isCheckedOutToProj() || ((SharedSupplierBean)theBean).getProject() == null)) {
            toReturn = true;
        }
        return toReturn;
    }
}
