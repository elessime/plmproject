package com.ikea.ipim.sbom.builders;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

import com.ptc.core.lwc.common.dynamicEnum.EnumerationEntryInfo;
import com.ptc.core.lwc.common.dynamicEnum.EnumerationInfo;
import com.ptc.core.lwc.common.dynamicEnum.EnumerationInfoManager;
import com.ptc.core.lwc.common.dynamicEnum.EnumerationInfoProvider;

import wt.meta.LocalizedValues;
import wt.util.WTException;
import wt.util.resource.SerializedResourceBundle;

public class SupplierEnumerationListBuilder implements EnumerationInfoProvider {
    private EnumerationInfoManager enumInfoManager = null;
    private String parameters = null;
    private EnumerationInfo enumerationInfo = null;
    
    private static final String ENUMERATION_RESOURCES = "wt.vc.views.Variation2RB";
    
    @Override
    public EnumerationInfo getEnumerationInfo() {
        return enumerationInfo;
    }

    @Override
    public EnumerationInfoManager getManager() {
        return enumInfoManager;
    }

    @Override
    public String getParameters() {
       return parameters;
    }

    @Override
    public void initialize(EnumerationInfoManager manager, String params) {
        if (manager == null) {
            String errorMsg = "Argument EnumerationInfoManager must be not null.";
            throw new IllegalArgumentException(errorMsg);
        }

        this.enumInfoManager = manager;
        this.parameters = params;


        /* Populate the enumeration */
        try {
            this.enumerationInfo = createEnumerationInfo();
        } catch (WTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private EnumerationInfo createEnumerationInfo() throws WTException {
        EnumerationInfo enumInfo = new EnumerationInfo();
        enumInfo.setNonLocalizableProperty(EnumerationInfo.DEFAULT_LOCALE, Locale.ENGLISH);
        Set<EnumerationEntryInfo> enumEntryInfos = createEnumerationEntryInfos();
        enumInfo.addEnumerationEntryInfos(enumEntryInfos);
        return enumInfo;
    }

    private Set<EnumerationEntryInfo> createEnumerationEntryInfos() throws WTException {
        Set<EnumerationEntryInfo> enumEntryInfos = new LinkedHashSet<EnumerationEntryInfo>();
        ResourceBundle serializedBundle = SerializedResourceBundle.getSerializedBundle(ENUMERATION_RESOURCES);
        Enumeration<String> keys = serializedBundle.getKeys();
        Set<String> valuesToDisplay = new HashSet<>();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            if (key.endsWith(".display")) {
                LocalizedValues locVal = (LocalizedValues) serializedBundle.getObject(key);
                String entryId = key.substring(0, key.indexOf("."));
                entryId = entryId + ".selectable";
                Boolean isSelectable = (Boolean) serializedBundle.getObject(entryId);
                if (isSelectable) {
                    valuesToDisplay.add(locVal.getDisplay());
                }
            }
        }
        for (String supplier:valuesToDisplay) {
            EnumerationEntryInfo enumEntryInfo = new EnumerationEntryInfo(supplier.replace(" ", ""));
            enumEntryInfo.setNonLocalizableProperty(EnumerationEntryInfo.SELECTABLE, Boolean.TRUE);
            enumEntryInfo.setLocalizableProperty(EnumerationEntryInfo.DISPLAY_NAME, Locale.ENGLISH, supplier);
            enumEntryInfo.setLocalizableProperty(EnumerationEntryInfo.DESCRIPTION, Locale.ENGLISH, supplier);
            enumEntryInfo.setLocalizableProperty(EnumerationEntryInfo.TOOLTIP, Locale.ENGLISH, supplier);
            enumEntryInfo.setNonLocalizableProperty(EnumerationEntryInfo.PARENT, null);
            enumEntryInfos.add(enumEntryInfo);
        }
        return enumEntryInfos;
    }

}
