package com.ikea.ipim.sbom.builders;

import org.apache.log4j.Logger;

import com.ikea.ipim.sbom.IkeaSBOMRB;
import com.ikea.ipim.utils.types.IkeaPartUtils;
import com.ptc.mvc.components.ComponentBuilder;
import com.ptc.mvc.components.ComponentConfig;
import com.ptc.mvc.components.ComponentConfigFactory;
import com.ptc.mvc.components.ComponentParams;
import com.ptc.mvc.components.TableConfig;

import wt.fc.collections.WTArrayList;
import wt.part.WTPart;
import wt.util.WTException;
import wt.util.WTMessage;

@ComponentBuilder("com.ikea.ipim.sbom.builders.InvolvedSuppliersInfoBuilder")
public class InvolvedSuppliersInfoBuilder extends AbstractSupplierTableBuilder {
    private static final Logger LOGGER = Logger.getLogger(InvolvedSuppliersInfoBuilder.class);
    @Override
    public Object buildComponentData(ComponentConfig config, ComponentParams params) throws Exception {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering buildComponentData() method");
        }
        WTArrayList relatedManufAlternateParts = new WTArrayList();
        Object contextObj = params.getContextObject();
        if (contextObj instanceof WTPart) {
            WTPart toCheck = (WTPart)contextObj;
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Exiting buildComponentData() method");
                LOGGER.debug("Will obtain shared revisions for Part -> " + toCheck.getIdentity());
            }
            return IkeaPartUtils.getSupplierPartsSharedToSuppliers(toCheck);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering buildComponentData() method");
            LOGGER.debug("Will return an empty array");
        }
        return relatedManufAlternateParts;
    }

    @Override
    public ComponentConfig buildComponentConfig(ComponentParams params) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering buildComponentConfig() method");
        }
        ComponentConfigFactory factory = getComponentConfigFactory();
        TableConfig table = factory.newTableConfig();
        table.setId(INVOLVED_SUPPLIERS_TABLE_ID);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Set table ID -> " + table.getId());
        }
        table.setLabel(getLabelForTable(params, IkeaSBOMRB.INVOLVED_SUPPLIERS_FOR_PART_TITLE));
        table.setSelectable(false);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Set table label -> " + table.getLabel());
        }
        table.addComponent(getReferencedColumConfig(SUPPLIER_DESCRIPTOR, WTMessage.getLocalizedMessage(SBOM_RESOURCE, SUPPLIER_DESCRIPTOR), SUPPLIER_DESCRIPTOR));
        table.addComponent(getReferencedColumConfig(CHECKOUT_STATUS_DESCRIPTOR, WTMessage.getLocalizedMessage(SBOM_RESOURCE, CHECKOUT_STATUS_DESCRIPTOR), CHECKOUT_STATUS_DESCRIPTOR));
        table.addComponent(getReferencedColumConfig(SHARED_VERSION_DESCRIPTOR, WTMessage.getLocalizedMessage(SBOM_RESOURCE, SHARED_VERSION_DESCRIPTOR), SHARED_VERSION_DESCRIPTOR));
        table.addComponent(getReferencedColumConfig(SHARED_VERSION_STATUS_DESCRIPTOR, WTMessage.getLocalizedMessage(SBOM_RESOURCE, SHARED_VERSION_STATUS_DESCRIPTOR), SHARED_VERSION_STATUS_DESCRIPTOR));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting buildComponentConfig() method");
        }
        return table;
    }

}
