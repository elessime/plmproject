package com.ikea.ipim.sbom.builders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.ikea.ipim.sbom.IkeaSBOMRB;
import com.ikea.ipim.sbom.beans.UpdateSupplierBean;
import com.ikea.ipim.utils.types.IkeaPartUtils;
import com.ikea.ipim.utils.types.IkeaProjectUtils;
import com.ptc.mvc.components.ComponentBuilder;
import com.ptc.mvc.components.ComponentConfig;
import com.ptc.mvc.components.ComponentConfigFactory;
import com.ptc.mvc.components.ComponentParams;
import com.ptc.mvc.components.TableConfig;
import wt.associativity.EquivalenceStatus;
import wt.fc.Persistable;
import wt.meta.LocalizedValues;
import wt.part.WTPart;
import wt.projmgmt.admin.Project2;
import wt.sandbox.SandboxHelper;
import wt.util.WTException;
import wt.util.WTMessage;
import wt.util.resource.SerializedResourceBundle;

@ComponentBuilder("com.ikea.ipim.sbom.builders.UpdateSuppliersTableBuilder")
public class UpdateSuppliersTableBuilder extends AbstractSupplierTableBuilder {
    private static String SUPPLIER_NAME = "supplierName";
    private static String UPDATE_CHECK = "isToBeUpdated";
    private static String DISPLAY_MARKER = ".display";
    private static String SELECTABLE_MARKER = ".selectable";
    private static final Logger LOGGER = Logger.getLogger(AbstractSupplierTableBuilder.class);

    @Override
    public Object buildComponentData(ComponentConfig config, ComponentParams params) throws Exception {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering buildComponentData() method");
        }
        Object contextObj = params.getContextObject();
        ArrayList<UpdateSupplierBean> toReturn = new ArrayList<>();
        if (contextObj instanceof WTPart) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Will now obtain a list suppliers that can have the part reshared -> " + ((WTPart) contextObj).getIdentity());
            }
            toReturn = getListOfAffectedSuppliers((WTPart) contextObj);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting buildComponentData() method");
            LOGGER.debug("Number of found projects to be used is -> " + toReturn.size());
        }
        return toReturn;
    }

    private ArrayList<UpdateSupplierBean> getListOfAffectedSuppliers(WTPart thePart) throws WTException {
        ArrayList<UpdateSupplierBean> toReturn = new ArrayList<>();
        ResourceBundle serializedBundle = SerializedResourceBundle.getSerializedBundle(ENUMERATION_RESOURCES);
        Enumeration<String> keys = serializedBundle.getKeys();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Obtaining the list of allowed suppliers and the projects it is shared to for part -> " + thePart.getIdentity());
        }
        ArrayList<String> allowedSuppliers = IkeaPartUtils.getListOfAuthorisedSuppliers(thePart);
        Collection<Persistable> sharedParts = SandboxHelper.service.getInteropObjects(thePart);
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            if (key.endsWith(DISPLAY_MARKER)) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Found a display entry in Resource Bundle -> " + key);
                    LOGGER.debug("Will now determine if part is shared to the container related to the entry");
                }
                checkAndAddToSuppliers(thePart, serializedBundle, toReturn, key, allowedSuppliers, sharedParts);
            }
        }
        return toReturn;

    }

    @Override
    public ComponentConfig buildComponentConfig(ComponentParams params) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering buildComponentData() method");
        }
        ComponentConfigFactory factory = getComponentConfigFactory();
        TableConfig table = factory.newTableConfig();
        table.setId(SUPPLIERS_TO_SHARE_TABLE_ID);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Set table ID -> " + table.getId());
        }
        table.setLabel(getLabelForTable(params, IkeaSBOMRB.SUPPLIERS_TO_UPDATE_PART_TITLE));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Set table label -> " + table.getLabel());
        }
        table.setSelectable(false);
        table.addComponent(getReferencedColumConfig(SUPPLIER_NAME, WTMessage.getLocalizedMessage(SBOM_RESOURCE, AVAILABLE_SUPPLIER_DESCRIPTOR), null));
        table.addComponent(getReferencedColumConfig(UPDATE_CHECK, WTMessage.getLocalizedMessage(SBOM_RESOURCE, RESHARE_OUTDATED_DESCRIPTOR), RESHARE_OUTDATED_DESCRIPTOR));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting getDataValue() method");
        }
        return table;
    }

    private void checkAndAddToSuppliers(WTPart parentPart, ResourceBundle serializedBundle, ArrayList<UpdateSupplierBean> toReturn, String key, ArrayList<String> allowedSuppliers, Collection<Persistable> sharedParts) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering checkAndAddToSuppliers() method");
        }
        LocalizedValues locVal = (LocalizedValues) serializedBundle.getObject(key);
        String entryId = key.substring(0, key.indexOf("."));
        String keyId = key.substring(0, key.indexOf("."));
        entryId = entryId + SELECTABLE_MARKER;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("key ID is -> " + keyId);
            LOGGER.debug("entry to check ID is -> " + entryId);
        }
        if ((Boolean) serializedBundle.getObject(entryId) && allowedSuppliers.contains(keyId.replaceAll(" ", ""))) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Resource bundle entry is enabled for display and is found on the list of allowed suppliers");
            }
            Project2 projectAssociated = IkeaProjectUtils.getProjectByName(locVal.getDisplay());
            if (projectAssociated != null) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Resource bundle entry has a project related to it -> " + projectAssociated.getIdentity());
                }
                for (Persistable persToCheck : sharedParts) {
                    if (persToCheck instanceof WTPart && projectAssociated.equals(((WTPart) persToCheck).getContainer())) {
//                       EquivalenceStatus theResult = IkeaPartUtils.getStatusBetweenUpAndDownstream(parentPart, (WTPart) persToCheck);
//                       createUpdateSupplierBean(theResult, toReturn, (WTPart) persToCheck, projectAssociated);
                       break;
                    }
                }
            }

        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting checkAndAddToSuppliers() method");
        }
    }
    
    private void createUpdateSupplierBean(EquivalenceStatus theResult, ArrayList<UpdateSupplierBean> toReturn, WTPart persToCheck, Project2 projectAssociated) {
        UpdateSupplierBean suppBean = null;
        if (theResult.equals(EquivalenceStatus.OUT_OF_SYNCH_EQUIVALENT)) {
            suppBean = new UpdateSupplierBean(persToCheck, projectAssociated, projectAssociated.getName(), true, true);
        } else if (theResult.equals(EquivalenceStatus.UPDATED_EQUIVALENT)) {
            suppBean = new UpdateSupplierBean(persToCheck, projectAssociated, projectAssociated.getName(), false, true);
        } else if (theResult.equals(EquivalenceStatus.NO_EQUIVALENT) || theResult.equals(EquivalenceStatus.UNKNOWN_EQUIVALENT)) {
           suppBean = new UpdateSupplierBean (persToCheck, projectAssociated, projectAssociated.getName(), false, false);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Will add the entry on the list of projects to be used in the Share to suppliers UI");
        }
        toReturn.add(suppBean);
    }

}
