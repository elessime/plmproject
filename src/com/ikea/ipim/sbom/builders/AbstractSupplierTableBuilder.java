package com.ikea.ipim.sbom.builders;

import org.apache.log4j.Logger;

import com.ikea.ipim.sbom.IkeaSBOMRB;
import com.ikea.ipim.utils.types.IkeaPartUtils;
import com.ptc.mvc.components.AbstractComponentBuilder;
import com.ptc.mvc.components.ColumnConfig;
import com.ptc.mvc.components.ComponentConfigFactory;
import com.ptc.mvc.components.ComponentParams;

import wt.part.WTPart;
import wt.session.SessionHelper;
import wt.util.WTException;
import wt.util.WTMessage;

public abstract class AbstractSupplierTableBuilder extends AbstractComponentBuilder {
   
    private static final Logger LOGGER = Logger.getLogger(AbstractSupplierTableBuilder.class);
    public final static String SUPPLIER_DESCRIPTOR = "involvedSupplier";
    public final static String CHECKOUT_STATUS_DESCRIPTOR = "checkedOutForSupplier";
    public final static String SHARED_VERSION_DESCRIPTOR = "sharedToSupplierVersion";
    public final static String SHARED_VERSION_STATUS_DESCRIPTOR = "sharedToSupplierStatus";
    public final static String INVOLVED_SUPPLIERS_TABLE_ID = "involvedSuppliersInfoTable";
    public final static String SUPPLIERS_TO_SHARE_TABLE_ID = "suppliersToShareTable";
    public final static String AVAILABLE_SUPPLIER_DESCRIPTOR = "availableSuppliers";
    public final static String SHARE_STATUS_DESCRIPTOR = "shareToSelectedSupplier";
    public final static String RESHARE_OUTDATED_DESCRIPTOR = "reshareOutdatedToSelectedSupplier";
    public final static String CHECKOUT_SHARE_TO_SUPPLIER = "checkOutShareToSelectedSupplier";
    protected static final String ENUMERATION_RESOURCES = "wt.vc.views.Variation2RB";
    protected static final String SBOM_RESOURCE = IkeaSBOMRB.class.getName();

    protected ColumnConfig getReferencedColumConfig (String columnId, String columnLabel, String datautilityId) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering getReferencedColumConfig() method");
            LOGGER.debug("Column ID is -> " + columnId);
            LOGGER.debug("Label is -> " + columnLabel);
            LOGGER.debug("Data utility is -> " + datautilityId);
        }
        ComponentConfigFactory factory = getComponentConfigFactory();
        ColumnConfig columnConfig = factory.newColumnConfig(columnId, true);
        columnConfig.setColumnWrapped(false);
        columnConfig.setLabel(columnLabel);
        if (datautilityId!=null && !datautilityId.isEmpty()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Assigning datautility to column");
            }
            columnConfig.setDataUtilityId(datautilityId);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting getDataValue() method");
        }
        return columnConfig;
    }
    
    protected String getLabelForTable(ComponentParams params, String resourceTitleId) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering getLabelForTable() method");
            LOGGER.debug("Resource for table title is -> " + resourceTitleId);
        }
        String label ="";
        Object contextObj = params.getContextObject();
        if (contextObj != null && contextObj instanceof WTPart) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Obtaining the label for WTPart -> " + ((WTPart)contextObj).getIdentity());
            }
            label = WTMessage.getLocalizedMessage(SBOM_RESOURCE, resourceTitleId, new Object[] {IkeaPartUtils.getPartIdentifierWithRevision((WTPart) contextObj)}, SessionHelper.manager.getLocale());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting getDataValue() method with label -> " + label);
        }
        return label;
    }

}
