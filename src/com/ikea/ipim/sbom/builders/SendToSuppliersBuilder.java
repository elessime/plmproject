package com.ikea.ipim.sbom.builders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.ikea.ipim.sbom.IkeaSBOMRB;
import com.ikea.ipim.sbom.beans.SharedSupplierBean;
import com.ikea.ipim.utils.types.IkeaPartUtils;
import com.ikea.ipim.utils.types.IkeaProjectUtils;
import com.ptc.mvc.components.ComponentBuilder;
import com.ptc.mvc.components.ComponentConfig;
import com.ptc.mvc.components.ComponentConfigFactory;
import com.ptc.mvc.components.ComponentParams;
import com.ptc.mvc.components.TableConfig;

import wt.fc.Persistable;
import wt.inf.container.WTContainerRef;
import wt.inf.sharing.SharedContainerMap;
import wt.meta.LocalizedValues;
import wt.part.WTPart;
import wt.projmgmt.admin.Project2;
import wt.sandbox.SandboxHelper;
import wt.util.WTException;
import wt.util.WTMessage;
import wt.util.resource.SerializedResourceBundle;

@ComponentBuilder("com.ikea.ipim.sbom.builders.SendToSuppliersBuilder")
public class SendToSuppliersBuilder extends AbstractSupplierTableBuilder {
    private static final Logger LOGGER = Logger.getLogger(AbstractSupplierTableBuilder.class);
    private static String SUPPLIER_NAME = "supplierName";
    private static String SHARE_CHECK = "isShared";
    private static String CHECKOUT_CHECK = "isCheckedOutToProj";
    private static String CHECKOUT_DATA_UTILITY = "checkoutToSelectedSupplier";
    private static String DISPLAY_MARKER = ".display";
    private static String SELECTABLE_MARKER = ".selectable";
    @Override
    public Object buildComponentData(ComponentConfig config, ComponentParams params) throws Exception {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering buildComponentData() method");
        }
        Object contextObj = params.getContextObject();
        ArrayList<SharedSupplierBean> toReturn = new ArrayList<>();
        if (contextObj instanceof WTPart) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Will now obtain a list of allowed suppliers and their projects for the part -> " + ((WTPart)contextObj).getIdentity());
            }
            toReturn = getListOfSuppliers((WTPart) contextObj);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting buildComponentData() method");
            LOGGER.debug("Number of found projects to be used is -> " + toReturn.size());
        }
        return toReturn;
    }

    @Override
    public ComponentConfig buildComponentConfig(ComponentParams params) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering buildComponentData() method");
        }
        ComponentConfigFactory factory = getComponentConfigFactory();
        TableConfig table = factory.newTableConfig();
        table.setId(SUPPLIERS_TO_SHARE_TABLE_ID);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Set table ID -> " + table.getId());
        }
        table.setLabel(getLabelForTable(params, IkeaSBOMRB.SUPPLIERS_TO_SHARE_PART_TITLE));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Set table label -> " + table.getLabel());
        }
        table.setSelectable(false);
        table.addComponent(getReferencedColumConfig(SUPPLIER_NAME, WTMessage.getLocalizedMessage(SBOM_RESOURCE, AVAILABLE_SUPPLIER_DESCRIPTOR), null));
        table.addComponent(getReferencedColumConfig(SHARE_CHECK, WTMessage.getLocalizedMessage(SBOM_RESOURCE, SHARE_STATUS_DESCRIPTOR), SHARE_STATUS_DESCRIPTOR));
        table.addComponent(getReferencedColumConfig(CHECKOUT_CHECK, WTMessage.getLocalizedMessage(SBOM_RESOURCE, CHECKOUT_SHARE_TO_SUPPLIER), CHECKOUT_DATA_UTILITY));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting getDataValue() method");
        }
        return table;
    }
    
    private ArrayList<SharedSupplierBean> getListOfSuppliers(WTPart thePart) throws WTException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering getListOfSuppliers() method");
            LOGGER.debug("Part to be used is -> " + thePart.getIdentity());
        }
        ArrayList<SharedSupplierBean> toReturn = new ArrayList<>();
        ResourceBundle serializedBundle = SerializedResourceBundle.getSerializedBundle(ENUMERATION_RESOURCES);
        Enumeration<String> keys = serializedBundle.getKeys(); 
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Obtaining the list of allowed suppliers and the projects it is shared to for part -> " + thePart.getIdentity());
        }
        ArrayList<String> allowedSuppliers = IkeaPartUtils.getListOfAuthorisedSuppliers(thePart);
        Collection<Persistable> sharedParts =  SandboxHelper.service.getInteropObjects(thePart);
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            if (key.endsWith(DISPLAY_MARKER)) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Found a display entry in Resource Bundle -> " + key);
                    LOGGER.debug("Will now determine if part is shared to the container related to the entry");
                }
              checkAndAddToSuppliers(serializedBundle, toReturn, key, allowedSuppliers, sharedParts);
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting getListOfSuppliers() method");
            LOGGER.debug("Related projects found for part count is -> " + toReturn.size());
        }
        return toReturn;
    }

    private void checkAndAddToSuppliers(ResourceBundle serializedBundle, ArrayList<SharedSupplierBean> toReturn, String key, ArrayList<String> allowedSuppliers, Collection<Persistable> sharedParts) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Entering checkAndAddToSuppliers() method");
        }
        LocalizedValues locVal = (LocalizedValues) serializedBundle.getObject(key);
        String entryId = key.substring(0, key.indexOf("."));
        String keyId = key.substring(0, key.indexOf("."));
        entryId = entryId + SELECTABLE_MARKER;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("key ID is -> " + keyId);
            LOGGER.debug("entry to check ID is -> " + entryId);
        }
        if ((Boolean) serializedBundle.getObject(entryId) && allowedSuppliers.contains(keyId.replaceAll(" ", ""))) {
            boolean checkedOut = false;
            boolean shared = false;
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Resource bundle entry is enabled for display and is found on the list of allowed suppliers");
            }
            Project2 projectAssociated = IkeaProjectUtils.getProjectByName(locVal.getDisplay());
            if (projectAssociated != null) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Resource bundle entry has a project related to it -> " + projectAssociated.getIdentity());
                }
                for (Persistable persToCheck : sharedParts) {
                    if (persToCheck instanceof WTPart && projectAssociated.equals(((WTPart) persToCheck).getContainer())) {
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("Part is shared in check-out mode to the project!");
                        }
                        checkedOut = true;
                    } else if (persToCheck instanceof SharedContainerMap) {
                        SharedContainerMap sharedObjectInfo = (SharedContainerMap) persToCheck;
                        WTContainerRef containerRef = sharedObjectInfo.getTargetContainerRef();
                        if (containerRef.getObject().equals(projectAssociated)) {
                            if (LOGGER.isDebugEnabled()) {
                                LOGGER.debug("Part is shared in read-only mode to the project!");
                            }
                            shared = true;
                        }
                    }
                }
            }
            SharedSupplierBean suppBean = new SharedSupplierBean(projectAssociated, shared, checkedOut, locVal.getDisplay());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Will add the entry on the list of projects to be used in the Share to suppliers UI");
            }
            toReturn.add(suppBean);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Exiting checkAndAddToSuppliers() method");
        }
    }

}
