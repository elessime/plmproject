package com.ikea.ipim.sbom.beans;

import wt.projmgmt.admin.Project2;

public class SharedSupplierBean {
    private Project2 project;
    private boolean isShared, isCheckedOutToProj;
    private String supplierName;

    public SharedSupplierBean (Project2 project, boolean isShared, boolean isCheckedOutToProj, String supplierName) {
        this.isShared = isShared;
        this.setCheckedOutToProj(isCheckedOutToProj);
        this.project = project;
        this.supplierName = supplierName;
    }

    public Project2 getProject() {
        return project;
    }
    public void setProject(Project2 project) {
        this.project = project;
    }
    public boolean isShared() {
        return isShared;
    }
    
    public void setShared(boolean isShared) {
        this.isShared = isShared;
    }
    public String getSupplierName() {
        return supplierName;
    }
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
    
    public String getOid() {
        StringBuilder stringBuild = new StringBuilder();
        if (project != null) {
            stringBuild.append(project.getPersistInfo().getObjectIdentifier().getStringValue());
        } else {
            stringBuild.append("null");
        }
        stringBuild.append("_");
        stringBuild.append(supplierName);
        return stringBuild.toString();
    }

    public boolean isCheckedOutToProj() {
        return isCheckedOutToProj;
    }

    public void setCheckedOutToProj(boolean isCheckedOutToProj) {
        this.isCheckedOutToProj = isCheckedOutToProj;
    }
    
}
