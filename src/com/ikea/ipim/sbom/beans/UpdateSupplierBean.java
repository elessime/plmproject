package com.ikea.ipim.sbom.beans;

import wt.part.WTPart;
import wt.projmgmt.admin.Project2;

public class UpdateSupplierBean {
    private Project2 project;
    private boolean isOutdated, equivalenceExists;
    private String supplierName;
    private WTPart associatedPart;
    
    public UpdateSupplierBean (WTPart associatedPart, Project2 project, String supplierName, boolean isOutdated, boolean equivalenceExists) {
        this.associatedPart = associatedPart;
        this.project = project;
        this.supplierName = supplierName;
        this.isOutdated = isOutdated;
        this.equivalenceExists = equivalenceExists;
    }
    
    public Project2 getProject() {
        return project;
    }
    public void setProject(Project2 project) {
        this.project = project;
    }
    public WTPart getAssociatedPart() {
        return associatedPart;
    }
    
    public void setAssociatedPart(WTPart associatedPart) {
        this.associatedPart = associatedPart;
    }
    public String getSupplierName() {
        return supplierName;
    }
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
    
    public String getOid() {
        StringBuilder stringBuild = new StringBuilder();
        if (project != null) {
            stringBuild.append(project.getPersistInfo().getObjectIdentifier().getStringValue());
        } else {
            stringBuild.append("null");
        }
        stringBuild.append("_");
        if (associatedPart != null) {
            stringBuild.append(associatedPart.getPersistInfo().getObjectIdentifier().getStringValue());
        } else {
            stringBuild.append("null");
        }
        stringBuild.append("_");
        stringBuild.append(supplierName);
        stringBuild.append("_");
        stringBuild.append(isOutdated);
        stringBuild.append("_");
        stringBuild.append(equivalenceExists);
        return stringBuild.toString();
    }
    public boolean isOutdated() {
        return isOutdated;
    }
    public void setOutdated(boolean isOutdated) {
        this.isOutdated = isOutdated;
    }
    public boolean isEquivalenceExists() {
        return equivalenceExists;
    }
    public void setEquivalenceExists(boolean equivalenceExists) {
        this.equivalenceExists = equivalenceExists;
    }
    
}
