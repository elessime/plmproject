package com.ikea.ipim.sbom.utils;

import com.ptc.core.foundation.associativity.AssociativityProperties;
import com.ptc.windchill.associativity.AssociativityConstants;
import com.ptc.windchill.associativity.copyover.CopyOverContext;
import com.ptc.windchill.associativity.copyover.CopyOverContext.Mode;
import com.ptc.windchill.associativity.transform.ClassicAssociativeTransformationParam;
import com.ptc.windchill.associativity.transform.DefaultTransformOption;
import com.ptc.windchill.associativity.transform.TransformOption;
import com.ptc.windchill.associativity.transform.TransformationContext;

import wt.associativity.NCServerHolder;
import wt.part.WTPart;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;
import wt.vc.views.Variation2;
import wt.vc.views.View;
import wt.vc.views.ViewReference;

public class SBOMTransformHelper {
    private static String IKEA_SBOM_TRANSFORM = "IKEA_SBOM_TRANSFORMATION";
    public static CopyOverContext getCopyOverContext(TransformationContext newContext) {
        CopyOverContext copyOverContext = new CopyOverContext();
        copyOverContext.setPropertyBasePrefix(AssociativityConstants.prefixCopyOverProperty);
        copyOverContext.setPropertiesFile(AssociativityProperties.getDefault());
        copyOverContext.setMode(Mode.CREATE);
        TransformOption transformOption = newContext.getTransformOption();
        String[] additionalPrefixes = new String[2];
        String moduleName = newContext.getModuleName().toLowerCase();
        additionalPrefixes[0] = moduleName;
        String transformSelector = transformOption.getStringValue().substring(transformOption.getStringValue().lastIndexOf(".") + 1);
        additionalPrefixes[1] = transformSelector;
        copyOverContext.setAdditionalPrefixes(additionalPrefixes);
        return copyOverContext;
    }
   
    public static TransformationContext getTransformationContext (WTPart partTouseInTransform, View theUpstreamView, View theDownstreamView) throws WTPropertyVetoException, WTException {
       NCServerHolder upNCSHolder = NCServerHolder.makeForView(theUpstreamView, null, null);
       NCServerHolder downNCSHolder = NCServerHolder.makeForView(theDownstreamView, null, null);
       TransformationContext context = new TransformationContext(partTouseInTransform, DefaultTransformOption.NEW_BRANCH);
       context.setIsFirstLevel(true);
       context.setUpstreamNCServerHolder(upNCSHolder);
       context.setDownstreamNCServerHolder(downNCSHolder);
       context.setModuleName(IKEA_SBOM_TRANSFORM);
       context.setCopyOverContext(getCopyOverContext(context));
       return context;
   }
   
    public static ClassicAssociativeTransformationParam getTransformParams(WTPart partTouseInTransform, View theUpstreamView, View theDownstreamView, Variation2 supplierVariation) throws WTException {
       ClassicAssociativeTransformationParam paramsForTransformation = new ClassicAssociativeTransformationParam();
       paramsForTransformation.setUpstreamAssociative(partTouseInTransform);
       paramsForTransformation.setUpstreamContext(ViewReference.newViewReference(theUpstreamView));
       paramsForTransformation.setDownstreamContext(ViewReference.newViewReference(theDownstreamView));
       paramsForTransformation.setViewName(theDownstreamView.getName());
       paramsForTransformation.setVariation2(supplierVariation);
       paramsForTransformation.setPropagate(false);
       paramsForTransformation.setDuplicateStructure(true);
       paramsForTransformation.setFolderRef(partTouseInTransform.getParentFolder());
       paramsForTransformation.setNewContainerRef(partTouseInTransform.getContainerReference());
       paramsForTransformation.setDuplicateEquivalenceLinks(true);
       return paramsForTransformation;
   }
}
