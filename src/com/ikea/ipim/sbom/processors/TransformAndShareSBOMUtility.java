package com.ikea.ipim.sbom.processors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.ikea.ipim.sbom.utils.SBOMTransformHelper;
import com.ikea.ipim.utils.types.IkeaPartUtils;
import com.ikea.ipim.utils.types.IkeaProjectUtils;
import com.ptc.windchill.associativity.service.AssociativityServiceLocator;
import com.ptc.windchill.associativity.transform.ClassicAssociativeTransformationParam;
import com.ptc.windchill.associativity.transform.ClassicAssociativeTransformationResult;
import com.ptc.windchill.associativity.transform.TransformService;
import com.ptc.windchill.associativity.transform.TransformationContext;

import wt.associativity.Associative;
import wt.fc.Persistable;
import wt.fc.collections.WTArrayList;
import wt.fc.collections.WTCollection;
import wt.method.RemoteAccess;
import wt.method.RemoteMethodServer;
import wt.org.WTPrincipal;
import wt.part.WTPart;
import wt.projmgmt.admin.Project2;
import wt.session.SessionHelper;
import wt.util.WTException;
import wt.util.WTInvalidParameterException;
import wt.vc.views.Variation2;
import wt.vc.views.View;
import wt.vc.views.ViewHelper;

public class TransformAndShareSBOMUtility implements RemoteAccess, Serializable  {
    private static final long serialVersionUID = -2094947181064526078L;
    public final static String UNAME = "username";
    public final static String PWD = "password";
    public final static String PARTNUM = "part number";
    public final static String ENTER_1 = "Enter ";
    public final static String ENTER_2 = ": ";
    public final static String NOT_ENTERED_1 = "No ";
    public final static String NOT_ENTERED_2 = " provided! Try again.";
    private static BufferedReader bufferedread = new BufferedReader(new InputStreamReader(System.in));
    private static String[] credentials = new String[2];
    private static String[] info = new String[2];
    private static RemoteMethodServer remoteMethodServer;
    public final static String TOOL_NAME =  TransformAndShareSBOMUtility.class.getName();
    public static final String SUPPLIERS = "list of suppliers separated by commas";
    public static String MANUF_VIEW = "Manufacturing";
    
    private static Logger logger = Logger.getLogger(TransformAndShareSBOMUtility.class);
    
    public static void main(String[] args) {
        try {
            if (args.length == 0 || args == null) {
                printUsage();
                provideData();
                authenticateAndRun();
            } else {
                if (Arrays.asList(args).contains("help")) {
                    printUsage();
                    return;
                }
               
                if (!validateArgs(args)) {
                    System.out.println("Improper number of parameters passed to the tool! To see the proper use, please run the tool with the 'help' option.");
                    return;
                } else {
                    credentials[0] = args[0].trim();
                    credentials[1] = args[1].trim();
                    info[0] = args[2].trim();
                    info[1] = args[3].trim();
                    authenticateAndRun();
                }
            }
        } catch (Exception ex) {
            System.out.println("Authentication failed! \n" + TOOL_NAME + " ends with status 1");
            System.out.println(" Exception :");
            ex.printStackTrace();
            System.exit(1);
        }
    }
    
    
    private static void getDataFromUser() throws IOException {
        String uname = getInputFromUser(UNAME);
        String upass = getInputFromUser(PWD);
        credentials[0] = uname.trim();
        credentials[1] = upass.trim();
        String partNum = getInputFromUser(PARTNUM);
        String supplierList = getInputFromUser(SUPPLIERS);
        info[0] = partNum.trim();
        info[1] = supplierList.trim();
    }
    
    private static String getInputFromUser(String whatToTake) throws IOException {
        System.out.println(ENTER_1 + whatToTake + ENTER_2);
        String toReturn = bufferedread.readLine().trim();
        if (toReturn.trim().isEmpty()) {
            System.out.println(NOT_ENTERED_1 + whatToTake + NOT_ENTERED_2);
            return getInputFromUser(whatToTake);
        } 
        return toReturn.trim();
    }
    
    private static boolean validateArgs(String args[]) {
        if (args.length == 4) {
            return true;
        } else {
            return false;
        }
    }
    
    public static void startSBOMCreation(String[] data) {
        logger.setLevel(Level.INFO);
        String partNumber = data[0];
        String[] suppliers = data[1].split(",");
        logger.info("Passed part number is -> " + partNumber);
        logger.info("Passed supplier list is -> " + data[1]);
        try {
            View manufView = ViewHelper.service.getView(MANUF_VIEW);
            WTPart partTouseInTransform = IkeaPartUtils.getWTPartByNumberAndVariation(partNumber, null);
            ArrayList<String> allowedSuppliers = IkeaPartUtils.getListOfAuthorisedSuppliers(partTouseInTransform);
            Variation2 supplierVariation = null;
            for (String supplier:suppliers) {
                supplier = supplier.trim();
                String supplierInternalValue = supplier.replace(" ", "");
                if (!allowedSuppliers.contains(supplierInternalValue)) {
                    logger.info("Supplier " + supplier + " is NOT PRESENT on the list of authorised suppliers! Will skip that supplier!");
                    continue;
                }
               try {
                   supplierVariation = Variation2.toVariation2(supplier);
               } catch (WTInvalidParameterException ex) {
                   logger.error("No supplier variation of " + supplier + " found! Will skip that supplier!");
                   continue;
               }
              
                WTPart partInSupplierView = IkeaPartUtils.getWTPartInView(partTouseInTransform.getMaster(), manufView, supplier, false);
                if (partInSupplierView != null) {
                    logger.info("Part " + partNumber + "exists for manufacturer " + supplier + " will skip SBOM transformation for it!");
                    if (!IkeaProjectUtils.isPartSharedToProject(partInSupplierView, supplierVariation.getDisplay())) {
                        logger.info("Will now share the manufacturer part: " + partInSupplierView.getNumber() + " " + partInSupplierView.getViewName() + " " + partInSupplierView.getVariation2().getDisplay() + " and all of its children");
                        Project2 toShareInto = IkeaProjectUtils.getProjectByName(supplierVariation.getDisplay());
                        WTArrayList partsToCheck = IkeaPartUtils.getAllDescendantsByVariation(partInSupplierView, supplier);
                        WTCollection shareParts = new WTArrayList();
                        Iterator<Persistable> iterateOverParts = partsToCheck.persistableIterator();
                        while (iterateOverParts.hasNext()) {
                            WTPart toCheck = (WTPart) iterateOverParts.next();
                            logger.info("Part to check is -> " + toCheck.getIdentity());
                            ArrayList<String> authorisedSuppliers = IkeaPartUtils.getListOfAuthorisedSuppliers(toCheck);
                            if (!authorisedSuppliers.contains(supplierVariation.getDisplay().replaceAll(" ", ""))) {
                                logger.info("Supplier " + supplier + " is NOT PRESENT on the list of authorised suppliers for part "+toCheck.getIdentity()+"! Will skip that part!");
                                continue;
                            } if (IkeaProjectUtils.isPartSharedToProject(toCheck, supplier)) {
                                logger.info("Part " + toCheck.getIdentity() + " is ALREADY SHARED to the supplier " + supplier + ". Will skip that part!");
                                continue;
                            } 
                            logger.info("Will add the following part to be shared to a supplier -> " + toCheck.getIdentity());
                            shareParts.add(toCheck);
                        }
                        if (toShareInto != null) {
                            logger.info("Will now share all identified parts to the supplier -> " + supplierVariation.getDisplay());
                            IkeaProjectUtils.pdmCheckout(shareParts, toShareInto);
                        }
                        
                    }
                } else {
                    logger.info("Part " + partNumber + " DOES NOT EXIST for manufacturer " + supplier);
                    List<ClassicAssociativeTransformationParam> params = new ArrayList<>();
                    WTArrayList theFoundParts = IkeaPartUtils.getAllDescendantsWithoutVariation(partTouseInTransform);
                    TransformationContext context = SBOMTransformHelper.getTransformationContext(partTouseInTransform, (View) partTouseInTransform.getView().getObject(), manufView);
                    TransformService transformService = (new AssociativityServiceLocator()).getTransformService();
                    Iterator iterateOverPartsToTransform = theFoundParts.persistableIterator();
                    while (iterateOverPartsToTransform.hasNext()) {
                        WTPart toAdd = (WTPart) iterateOverPartsToTransform.next();
                        ArrayList<String> authorisedSuppliers = IkeaPartUtils.getListOfAuthorisedSuppliers(toAdd);
                        if (!authorisedSuppliers.contains(supplierVariation.getDisplay().replaceAll(" ", ""))) {
                            logger.info("Supplier " + supplier + " is NOT PRESENT on the list of authorised suppliers for part "+toAdd.getIdentity()+"! Will skip that supplier!");
                            continue;
                        }
                        if(IkeaPartUtils.getWTPartByNumberAndVariation(toAdd.getNumber(), supplierVariation.getDisplay()) == null) {
                            ClassicAssociativeTransformationParam paramsForTransformation = SBOMTransformHelper.getTransformParams(toAdd, (View) toAdd.getView().getObject(), manufView, supplierVariation);
                            params.add(paramsForTransformation);
                        } else {
                            logger.info("Part " + partNumber + " ALREADY EXISTS for manufacturer " + supplier + "! Will skip the creation of the view for it!");
                        }
                    }
                    ClassicAssociativeTransformationResult transformResult = transformService.doClassicAssociativeTransformation(context, params);
                    Map<Associative, List<Associative>> theLinkedDownstreams = transformResult.getUpstreamToDownstreamsMap();
                    Project2 toShareInto = IkeaProjectUtils.getProjectByName(supplierVariation.getDisplay());
                    WTCollection shareParts = new WTArrayList();
                    Set <Associative>upstreamParts = theLinkedDownstreams.keySet();
                    for (Associative theUpstream:upstreamParts) {
                        List<Associative> downstreamList = theLinkedDownstreams.get(theUpstream);
                        for (Associative theDownstream:downstreamList) {
                            if (theDownstream instanceof WTPart) {
                                if (toShareInto != null) {
                                    logger.info("Will add the following part to be shared to a supplier -> " + ((WTPart)theDownstream).getIdentity());
                                    shareParts.add((WTPart)theDownstream);
                                }
                            }
                        }
                    }
                    logger.info("Will now share all identified parts to the supplier -> " + supplierVariation.getDisplay());
                    IkeaProjectUtils.pdmCheckout(shareParts, toShareInto);
                }
            }
        } catch (Exception e) {
            logger.error("An unexpected exception happened during the run of the tool - check below for the stack trace:");
            e.printStackTrace();
        }
    }
    
    private static void authenticateAndRun() throws RemoteException, InvocationTargetException {
        String returnedMessage = "";
        remoteMethodServer = RemoteMethodServer.getDefault();
        authenticate();
        System.out.println("Now executing the creation of a part in a new supplier view and sharing it to the supplier. Please observe the MethodServer and logs for output.");
        remoteMethodServer.invoke("startSBOMCreation", TOOL_NAME, null,  new Class[] { String[].class }, new Object[] { info });
        System.out.println(returnedMessage);
    }
    
    private static void authenticate() {
        remoteMethodServer.setUserName(credentials[0]);
        remoteMethodServer.setPassword(credentials[1]);
        try {
            WTPrincipal currentUser = SessionHelper.manager.getPrincipal();
            WTPrincipal wtAdministrator = SessionHelper.manager.getAdministrator();
            if (!currentUser.equals(wtAdministrator)) {
                System.out.println("Invalid user! " + TOOL_NAME + " may be launched by Windchill Administrator only \n" + TOOL_NAME + " ends with status 1");
                System.exit(1);
            }
        } catch (WTException e) {
            System.out.println("Authentication failed! " + e.getLocalizedMessage() + "\n" + TOOL_NAME + " ends with status 1");
            System.out.println(" Exception :");
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void printUsage() {
        System.out.println("This tool will try to do a share of the given WTPart and the associated BOM to a selected array of suppliers.\nIt will try to search for a Manufacturing view of a given part and if it does find it, it will try to create a supplier-specific Manufacturing Alternate BOM of the part and share it to the defined project in check-out mode.");
        System.out.println("\n\nProper usage of the tool: ");
        System.out.println("windchill com.ikea.ipim.sbom.processors.TransformAndShareSBOMUtility user_login user_password part_number manufacturers");
        System.out.println("where:");
        System.out.println("user_login - the login of the user from Windchill system");
        System.out.println("user_password - the password that is a valid Windchill password for the user");
        System.out.println("part_number - the number of the part that You wish to be shared to the manufacturers");
        System.out.println("manufacturers - A comma-separated list of manufacturers to be used in the SBOM creation and sharing. Note that the supplier list will also be used for the project sharing, so the names of supplier views and projects must be the same.\n\n");
    }
    
    private static void provideData() throws Exception {
        System.out.println("Since no parameters were passed in the command line, now You will be asked to enter your \ncredentials and the data needed to perform the creation of a part in a new supplier view and sharing it to the supplier. \n\nHit enter to continue... .");
        bufferedread.readLine();
        getDataFromUser();
    }
}
