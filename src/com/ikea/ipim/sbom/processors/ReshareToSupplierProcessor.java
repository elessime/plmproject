package com.ikea.ipim.sbom.processors;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.ikea.ipim.sbom.IkeaSBOMRB;
import com.ikea.ipim.utils.types.IkeaPartUtils;
import com.ikea.ipim.utils.types.IkeaProjectUtils;
import com.ptc.core.components.beans.ObjectBean;
import com.ptc.core.components.forms.DefaultObjectFormProcessor;
import com.ptc.core.components.forms.FormProcessingStatus;
import com.ptc.core.components.forms.FormResult;
import com.ptc.core.components.util.FeedbackMessage;
import com.ptc.core.ui.resources.FeedbackType;
import com.ptc.netmarkets.model.NmOid;
import com.ptc.netmarkets.util.beans.NmCommandBean;
import com.ptc.windchill.associativity.bll.AssociativeEquivalenceExecutionReport;

import wt.associativity.EquivalenceLink;
import wt.associativity.NCServerHolder;
import wt.enterprise.RevisionControlled;
import wt.fc.Persistable;
import wt.fc.ReferenceFactory;
import wt.fc.collections.WTArrayList;
import wt.part.WTPart;
import wt.projmgmt.admin.Project2;
import wt.session.SessionHelper;
import wt.util.WTException;
import wt.util.WTMessage;
import wt.vc.views.View;

public class ReshareToSupplierProcessor extends DefaultObjectFormProcessor {
    private static final String RESHARE_OP_MARKER = "ReShare";
    private static final String SBOM_RESOURCE = IkeaSBOMRB.class.getName();
    private static final String RESHARE_MADE_OK = "selectedResharedToContainer";
    private static final String CONTAINER_NOT_FOUND = "couldNotDetectReshareContainer";
    private static final String UNSHARE_PART_NOT_FOUND = "couldNotDetectUnsharePart";
    private static final String UPDATE_EQUIV_FAILED = "updateEquivalenceFailed";
    private static final String COPY_UNSHARE_FAILED = "copyUnshareOperationFailed";
    private static final String RESHARE_FAILED = "resharedToContainerFailed";
    
    @Override
    public FormResult doOperation(NmCommandBean clientData, List<ObjectBean> objectList) throws WTException {
        FormResult result = new FormResult();
        result.setStatus(FormProcessingStatus.SUCCESS);
        NmOid partOid = clientData.getActionOid();
        Object toCheck = partOid.getRefObject();
        if (toCheck instanceof WTPart) {
            WTPart toShare = (WTPart) toCheck;
            Collection<EquivalenceLink> theCurrentEquivalences = IkeaPartUtils.getEquivalenceLinksForUpstream(toShare);
            Set<String> newCheckedOptions = clientData.getChecked().keySet();
            for (String newCheckedOption : newCheckedOptions) {
                String beanIdentifier = newCheckedOption.substring(0, newCheckedOption.lastIndexOf('_'));
                String operationIdentifier = newCheckedOption.substring(newCheckedOption.lastIndexOf('_') + 1);
                if (operationIdentifier.equals(RESHARE_OP_MARKER)) {
                    String[] shareParams = beanIdentifier.split("_");
                    if (shareParams.length == 5) {
                        String projectId = shareParams[0];
                        String outOfDatePart = shareParams[1];
                        String supplierName = shareParams[2];
                        ReferenceFactory refFactory = new ReferenceFactory();
                        Project2 projectToUse = (Project2) refFactory.getReference(projectId).getObject();
                        WTPart partToUnshare = (WTPart) refFactory.getReference(outOfDatePart).getObject();
                        if (projectToUse != null && partToUnshare != null) {
                            AssociativeEquivalenceExecutionReport report = null;
                            try {
                                HashMap<String, WTArrayList> sharedBom = IkeaPartUtils.getInterOpBOM(partToUnshare, supplierName);
                                RevisionControlled[] partsToCopy = convertToArray(sharedBom);
                                IkeaProjectUtils.copyOverBOM(partsToCopy, projectToUse);
                                IkeaProjectUtils.unShareFromProject(projectToUse, sharedBom);
                                NCServerHolder upstreamNC = NCServerHolder.makeForIterated(toShare);
                                partToUnshare = IkeaPartUtils.getWTPartInView(partToUnshare.getMaster(), (View) toShare.getView().getObject(), partToUnshare.getVariation2().toString(), true);
                                NCServerHolder downstreamNC = NCServerHolder.makeForIterated(partToUnshare);
//                                report = IkeaPartUtils.updateEquivalenceBetweenParts(partToUnshare, toShare, upstreamNC, downstreamNC);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                result.setStatus(FormProcessingStatus.NON_FATAL_ERROR);
                                result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, COPY_UNSHARE_FAILED, new Object[]{toShare.getNumber(), projectToUse.getName()})));
                            }
                            if (report != null && !report.isSuccess()) {
                                result.setStatus(FormProcessingStatus.NON_FATAL_ERROR);
                                Throwable error = report.getThrowable();
                                if (error != null) {
                                    result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, UPDATE_EQUIV_FAILED, new Object[]{toShare.getNumber(), error.getMessage()})));
                                } else {
                                    result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, UPDATE_EQUIV_FAILED, new Object[]{toShare.getNumber(), ""})));
                                }
                           } else if (report != null && report.isSuccess()) {
                               try {
                                   IkeaProjectUtils.reshareAfterEquivalenceUpdate(report, theCurrentEquivalences, toShare);
                                   result.addFeedbackMessage(new FeedbackMessage(FeedbackType.SUCCESS, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, RESHARE_MADE_OK, new Object[]{toShare.getNumber(), projectToUse.getName()})));
                               } catch (Exception ex) {
                                   result.setStatus(FormProcessingStatus.NON_FATAL_ERROR);
                                   result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, RESHARE_FAILED, new Object[]{toShare.getNumber(), projectToUse.getName()})));
                               }
                            }
                        } else {
                            result.setStatus(FormProcessingStatus.NON_FATAL_ERROR);
                            if (projectToUse == null) {
                                result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, CONTAINER_NOT_FOUND, new Object[]{toShare.getNumber()})));
                            } else if (partToUnshare == null) {
                                result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, UNSHARE_PART_NOT_FOUND, new Object[]{toShare.getNumber(), projectToUse.getName()})));
                            }
                        }
                    }
                }
            }
        }
        return result;
    }
    
    private RevisionControlled[] convertToArray(HashMap<String, WTArrayList> sharedBom) throws WTException {
        Iterator<Persistable> iterateOverParts = sharedBom.get(IkeaPartUtils.INTEROP_MARKER).persistableIterator();
        RevisionControlled[] partsToCopy = new RevisionControlled[sharedBom.get(IkeaPartUtils.INTEROP_MARKER).size() + sharedBom.get(IkeaPartUtils.SHARED_READ_ONLY_MARKER).size()];
        int tableIndex = 0;
        while (iterateOverParts.hasNext()) {
            partsToCopy[tableIndex] = (RevisionControlled) iterateOverParts.next();
           tableIndex++;
        } 
        iterateOverParts = sharedBom.get(IkeaPartUtils.SHARED_READ_ONLY_MARKER).persistableIterator();
        while (iterateOverParts.hasNext()) {
            partsToCopy[tableIndex] = (RevisionControlled) iterateOverParts.next();
           tableIndex++;
        } 
        return partsToCopy;
    }
}
