package com.ikea.ipim.sbom.processors;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.ikea.ipim.sbom.IkeaSBOMRB;
import com.ikea.ipim.utils.types.IkeaPartUtils;
import com.ikea.ipim.utils.types.IkeaProjectUtils;
import com.ptc.core.components.beans.ObjectBean;
import com.ptc.core.components.forms.DefaultObjectFormProcessor;
import com.ptc.core.components.forms.FormProcessingStatus;
import com.ptc.core.components.forms.FormResult;
import com.ptc.core.components.util.FeedbackMessage;
import com.ptc.core.ui.resources.FeedbackType;
import com.ptc.netmarkets.model.NmOid;
import com.ptc.netmarkets.util.beans.NmCommandBean;

import wt.fc.ReferenceFactory;
import wt.fc.collections.WTCollection;
import wt.part.WTPart;
import wt.projmgmt.admin.Project2;
import wt.session.SessionHelper;
import wt.util.WTException;
import wt.util.WTInvalidParameterException;
import wt.util.WTMessage;
import wt.util.WTPropertyVetoException;
import wt.vc.views.Variation2;
import wt.vc.views.View;
import wt.vc.views.ViewHelper;

public class ShareToSupplierProcessor extends DefaultObjectFormProcessor {
    private static final String SBOM_RESOURCE = IkeaSBOMRB.class.getName();
    private static final String NON_IDENTIFIED_SHARE = "selectedShareNotIdentified";
    private static final String SHARE_ALREADY_MADE = "selectedShareAlreadyInContainer";
    private static final String SHARE_MARKER = "Share";
    private static final String CHECKOUT_MARKER = "CheckOut";
    private static final String SHARE_MADE_OK = "selectedShareAddedToContainer";
    private static final String PART_NOT_AUTHORISED = "partToShareIsNotAuthorised";
    private static final String SUPPLIER_NOT_IDENTIFIED = "supplierNotOnResourceList";
    private static final String ERROR_WHILE_OBTAINING_SUPPLIER_PART = "partInViewAndSupplierNotFound";
    public static final String MANUF_VIEW = "Manufacturing";

    @Override
    public FormResult doOperation(NmCommandBean clientData, List<ObjectBean> objectList) throws WTException {
        FormResult result = new FormResult();
        result.setStatus(FormProcessingStatus.SUCCESS);
        NmOid partOid = clientData.getActionOid();
        Object toCheck = partOid.getRefObject();
        ArrayList<String> toProcess = new ArrayList<>();
        if (toCheck instanceof WTPart) {
            WTPart toShare = (WTPart) toCheck;
            Set<String> oldCheckedOptions = clientData.getOldChecked().keySet();
            Set<String> newCheckedOptions = clientData.getChecked().keySet();
            for (String newCheckedOption : newCheckedOptions) {
                String beanIdentifier = newCheckedOption.substring(0, newCheckedOption.lastIndexOf('_'));
                String operationIdentifier = newCheckedOption.substring(newCheckedOption.lastIndexOf('_') + 1);
                boolean canBeProcessed = true;
                for (String alreadyShared : oldCheckedOptions) {
                    if (alreadyShared.startsWith(beanIdentifier) && (operationIdentifier.equals(SHARE_MARKER) && alreadyShared.endsWith(CHECKOUT_MARKER) || operationIdentifier.equals(SHARE_MARKER) && alreadyShared.endsWith(SHARE_MARKER) || operationIdentifier.equals(CHECKOUT_MARKER) && alreadyShared.endsWith(CHECKOUT_MARKER))) {
                        canBeProcessed = false;
                            result.setStatus(FormProcessingStatus.NON_FATAL_ERROR);
                            String projectId = alreadyShared.substring(0, alreadyShared.indexOf('_'));
                            ReferenceFactory refFactory = new ReferenceFactory();
                            Project2 projectToUse = (Project2) refFactory.getReference(projectId).getObject();
                            result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, SHARE_ALREADY_MADE, new Object[]{toShare.getNumber(), projectToUse.getName()})));
                            break;
                    }
                }
                if (canBeProcessed) {
                    toProcess.add(newCheckedOption);
                }
            }
            for (String processToProject : toProcess) {
                String[] shareParams = processToProject.split("_");
                if (shareParams.length == 3) {
                    String projectId = shareParams[0];
                    String operation = shareParams[2];
                    ReferenceFactory refFactory = new ReferenceFactory();
                    Project2 projectToUse = (Project2) refFactory.getReference(projectId).getObject();
                    if (operation.equals(SHARE_MARKER)) {
                        WTCollection theCreatedShare = null;
                        try {
                            theCreatedShare = IkeaProjectUtils.shareBOMToProject(toShare, projectToUse);
                        } catch (WTException ex) {
                            ex.printStackTrace();
                            result.setStatus(FormProcessingStatus.NON_FATAL_ERROR);
                            result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, ex.getLocalizedMessage(SessionHelper.getLocale())));
                            continue;
                        }
                        if (theCreatedShare != null) {
                            result.addFeedbackMessage(new FeedbackMessage(FeedbackType.SUCCESS, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, SHARE_MADE_OK, new Object[]{toShare.getNumber(), projectToUse.getName()})));
                        }
                    } else if (operation.equals(CHECKOUT_MARKER)) {
                        View manufView = ViewHelper.service.getView(MANUF_VIEW);
                        ArrayList<String> allowedSuppliers = IkeaPartUtils.getListOfAuthorisedSuppliers(toShare);
                        String supplierInternalValue = projectToUse.getName().replace(" ", "");
                        if (!allowedSuppliers.contains(supplierInternalValue)) {
                            result.setStatus(FormProcessingStatus.NON_FATAL_ERROR);
                            result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, PART_NOT_AUTHORISED, new Object[]{toShare.getNumber(), projectToUse.getName()})));
                        } else {
                            Variation2 supplierVariation = null;
                            try {
                                supplierVariation = Variation2.toVariation2(supplierInternalValue);
                            } catch (WTInvalidParameterException ex) {
                                ex.printStackTrace();
                                result.setStatus(FormProcessingStatus.NON_FATAL_ERROR);
                                result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, SUPPLIER_NOT_IDENTIFIED, new Object[]{ projectToUse.getName()})));
                                continue;
                            }
                            try {
                                WTPart partInSupplierView = IkeaPartUtils.getWTPartInView(toShare.getMaster(), manufView, supplierVariation.getDisplay(), false);
                                if (partInSupplierView != null) {
                                    if (!IkeaProjectUtils.isPartSharedToProject(partInSupplierView, supplierVariation.getDisplay())) {
                                        ArrayList<FeedbackMessage> messages = IkeaProjectUtils.reShareBOMToProject(partInSupplierView, supplierVariation);
                                        for (FeedbackMessage message : messages) {
                                            result.addFeedbackMessage(message);
                                        }
                                    }
                                } else {
                                    ArrayList<FeedbackMessage> messages = IkeaProjectUtils.transformAndCheckoutBOMToProject(toShare, supplierVariation, manufView);
                                    for (FeedbackMessage message : messages) {
                                        result.addFeedbackMessage(message);
                                    }
                                }
                            } catch (WTPropertyVetoException ex) {
                                ex.printStackTrace();
                                result.setStatus(FormProcessingStatus.NON_FATAL_ERROR);
                                result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, ERROR_WHILE_OBTAINING_SUPPLIER_PART, new Object[]{toShare.getNumber(), manufView.getName(), projectToUse.getName()})));
                            }
                        }
                    }

                } else {
                    result.setStatus(FormProcessingStatus.NON_FATAL_ERROR);
                    result.addFeedbackMessage(new FeedbackMessage(FeedbackType.FAILURE, SessionHelper.getLocale(), null, null, WTMessage.getLocalizedMessage(SBOM_RESOURCE, NON_IDENTIFIED_SHARE, new Object[]{toShare.getNumber()})));
                }
            }
        }
        return result;
    }
}
