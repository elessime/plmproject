package com.ikea.ipim.properties;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import wt.util.WTContext;
import wt.util.WTException;
import wt.util.WTProperties;
import wt.vc.views.View;
import wt.vc.views.ViewException;
import wt.vc.views.ViewHelper;

public class IkeaIconDelegateProperties extends WTProperties implements Serializable {
    private static final long serialVersionUID = 4882601730451853078L;

    /**
     * Logger for this class
     */
    private static final Logger LOGGER = Logger.getLogger(IkeaIconDelegateProperties.class);

    /**
     * Singleton instance.
     */
    private static IkeaIconDelegateProperties instance;
    
    /**
     * Path of the property file.
     */
    private static final String PROPERTIES_PATH = "/com/ikea/properties/ipimIconDelegate.properties";

    /**
     * The default base property name to look for when reading mapping
     * configuration
     */
    public final static String BASE_PROPERTY_NAME = "IconDelegate";
    
    public final static String ALTERNATE_BOM_NAME = "alternate_bom";
    
    private final static HashMap<String, HashMap<String, String>> mapping = new HashMap<String, HashMap<String, String>>();

    public static final String CONFIGURABLE_NAME = "configurable";
    
    public static final String END_ITEM_NAME = "end_item";
    
    static {
        try {
            // build mapping from property file
            buildMappingFromProperties();

        } catch (Throwable throwable) {
            LOGGER.error(IkeaIconDelegateProperties.class.getName() + ": error reading properties file - ", throwable);
            throwable.printStackTrace();
        }
    }

    
    protected IkeaIconDelegateProperties(Properties properties) {
        super(properties);
    }
    
    public static WTProperties getIkeaIconDelegateProperties() {

        if (instance == null) {

            try {
                Properties properties = new Properties();
                InputStream inputStream = WTContext.getContext().getResourceAsStream(PROPERTIES_PATH);
                properties.load(inputStream);
                instance = new IkeaIconDelegateProperties(properties);

            } catch (IOException exception) {
                LOGGER.error("getIconDataUtilityProperties() - Cannot load " + PROPERTIES_PATH);
                LOGGER.error("getIconDataUtilityProperties()", exception);
                LOGGER.error(exception);
            }
        }

        return instance;
    }
    
    private static void buildMappingFromProperties() throws ViewException, WTException {
        WTProperties wtp = getIkeaIconDelegateProperties();
        // get all types from properties
        String types = wtp.getProperty(IkeaIconDelegateProperties.BASE_PROPERTY_NAME + ".types");
        if (types == null || types.isEmpty()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(IkeaIconDelegateProperties.class.getName() + ": no types specified.");
            }

        } else {
            // read mappings for all types, one by one
            StringTokenizer st = new StringTokenizer(types, ";");
            if (!st.hasMoreTokens()) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(IkeaIconDelegateProperties.class.getName() + ": cannot read type definition.");
                }
            }

            while (st.hasMoreTokens()) {

                String token = st.nextToken();
                String typeName = token.substring(0, token.lastIndexOf("="));
                String typeId = token.substring(token.lastIndexOf("=") + 1);
                HashMap<String, String> viewMapping = new HashMap<String, String>();
                
                for (View view:ViewHelper.service.getAllViews()) {
                    String viewProp = wtp.getProperty(IkeaIconDelegateProperties.BASE_PROPERTY_NAME + "." + typeId + "." + view.getName());
                    if (viewProp != null && !viewProp.isEmpty()) {
                        viewMapping.put(view.getName(), viewProp);
                        String alternateBomViewProp =  wtp.getProperty(IkeaIconDelegateProperties.BASE_PROPERTY_NAME + "." + typeId + "." + view.getName() + "_" + ALTERNATE_BOM_NAME);
                        if (alternateBomViewProp !=null && !alternateBomViewProp.isEmpty()) {
                            viewMapping.put(view.getName() +"_"+ALTERNATE_BOM_NAME, alternateBomViewProp);
                        }
                        String configurableViewProp =  wtp.getProperty(IkeaIconDelegateProperties.BASE_PROPERTY_NAME + "." + typeId + "." + view.getName() + "_" + CONFIGURABLE_NAME);
                        if (configurableViewProp !=null && !configurableViewProp.isEmpty()) {
                            viewMapping.put(view.getName() +"_"+CONFIGURABLE_NAME, configurableViewProp);
                        }
                        
                        String endItemViewProp =  wtp.getProperty(IkeaIconDelegateProperties.BASE_PROPERTY_NAME + "." + typeId + "." + view.getName() + "_" + END_ITEM_NAME);
                        if (endItemViewProp !=null && !endItemViewProp.isEmpty()) {
                            viewMapping.put(view.getName() +"_"+END_ITEM_NAME, endItemViewProp);
                        }
                        
                        String endItemConfigViewProp =  wtp.getProperty(IkeaIconDelegateProperties.BASE_PROPERTY_NAME + "." + typeId + "." + view.getName() + "_" + CONFIGURABLE_NAME +"_" + END_ITEM_NAME);
                        if (endItemConfigViewProp !=null && !endItemConfigViewProp.isEmpty()) {
                            viewMapping.put(view.getName() + "_" + CONFIGURABLE_NAME +"_" + END_ITEM_NAME, endItemConfigViewProp);
                        }
                    }
                }
                
                mapping.put(typeName, viewMapping);
            }
        }
    }
    
    /**
     * Getter for mapping
     * 
     * @return The mapping
     */
    public static HashMap<String, HashMap<String, String>> getMapping() {
        return mapping;
    }

}
