package com.ikea.ipim.properties;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ptc.core.meta.common.TypeIdentifier;

import wt.type.TypedUtility;
import wt.util.WTProperties;
import wt.util.WTRuntimeException;

public class IkeaProperties extends WTProperties {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(IkeaProperties.class);
    private IkeaProperties(Properties defaults) {
        super(defaults);
    }

    /** The local properties. */
    private static volatile IkeaProperties localProperties = null;

    /** The Constant LOCK. */
    private static final Object LOCK = new Object();

    /**
     * {@link IkeaProperties} singleton getter.
     *
     * @return {@link IkeaProperties} or only {@link WTProperties} local properties in case of error
     */
    public static IkeaProperties getProperties() {
        if (localProperties == null) {
            synchronized (LOCK) {
                // double-check (if localProperties already instantiated when waiting for de-lock)
                if (localProperties == null) {
                    LOG.debug("getProperties::Entry: initializing Ikea properties");
                    WTProperties wtProperties = null;
                    try {
                        wtProperties = WTProperties.getLocalProperties();
                        IkeaProperties threadLocalProperties = new IkeaProperties(wtProperties);
                        String windchillHome = threadLocalProperties.getProperty("wt.home");
                        String dirSep = threadLocalProperties.getProperty("dir.sep");
                        String path =
                                wtProperties.getProperty("com.ikea.configuration.properties.files",
                                        "codebase/com/ikea/properties/ikea.properties");
                        String[] propertyFiles = path.split(",");
                        for (String propertyFile : propertyFiles) {
                            try (InputStream is = new BufferedInputStream(new FileInputStream(windchillHome + dirSep + propertyFile))) {
                                threadLocalProperties.load(is);
                            }
                        }
                        localProperties = threadLocalProperties;
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("getProperties::Exit: " + threadLocalProperties.toString());
                        }
                    } catch (IOException e) {
                        if (wtProperties == null) {
                            LOG.error("getProperties:: error when initializing WTProperties", e);
                            throw new WTRuntimeException(e);
                        } else {
                            LOG.error("getProperties:: error when initializing Ikea properties", e);
                        }
                    }
                }
            }
        }
        return localProperties;
    }

    /**
     * Gets the list property.
     *
     * @param propertyKey propertyKey
     * @param defaultValue defaultValue
     * @return list
     */
    public List<String> getListProperty(String propertyKey, String defaultValue) {
        return getListProperty(propertyKey, defaultValue, ",");

    }

    /**
     * Gets the list property.
     *
     * @param propertyKey propertyKey
     * @param defaultValue defaultValue
     * @param delimiter delimiter
     * @return list
     */
    public List<String> getListProperty(String propertyKey, String defaultValue, String delimiter) {
        String propValue = getProperty(propertyKey, defaultValue);
        return Arrays.asList(StringUtils.split(propValue, delimiter));

    }

    /**
     * Gets the list property.
     *
     * @param propertyKey propertyKey
     * @param defaultValue defaultValue
     * @return list
     */
    public List<TypeIdentifier> getTypeIdentifierListProperty(String propertyKey, String defaultValue) {
        return getTypeIdentifierListProperty(propertyKey, defaultValue, ",");

    }

    /**
     * Gets the list property.
     *
     * @param propertyKey propertyKey
     * @param defaultValue defaultValue
     * @param delimiter delimiter
     * @return list
     */
    public List<TypeIdentifier> getTypeIdentifierListProperty(String propertyKey, String defaultValue, String delimiter) {
        List<String> stringListOfType = getListProperty(propertyKey, defaultValue, delimiter);
        List<TypeIdentifier> listofType = new ArrayList<>();
        for (String stringType : stringListOfType) {
            listofType.add(TypedUtility.getTypeIdentifier(stringType));
        }

        return listofType;
    }
    
}
