<%@ include file="/netmarkets/jsp/util/begin.jspf"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/components" prefix="jca"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/core" prefix="core"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/wrappers" prefix="w"%>
<%@ page import="wt.fc.collections.WTArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.ikea.ipim.utils.mass.IPIMMassAttributesHelper" %>
<%@ page import="com.ikea.ipim.utils.mass.massRollupRB"%>
<%
String partOid = request.getParameter("oid");
WTArrayList partStructure = IPIMMassAttributesHelper.getChildParts(partOid);
String reportMass = "";
String partIdentity = IPIMMassAttributesHelper.getPartIdentityFromOid(partOid);
HashMap<String, WTArrayList> nonCalculableParts = new HashMap<String, WTArrayList>();
nonCalculableParts.put(IPIMMassAttributesHelper.NO_DENSITY, new WTArrayList());
nonCalculableParts.put(IPIMMassAttributesHelper.NO_VOLUME, new WTArrayList());
nonCalculableParts.put(IPIMMassAttributesHelper.NO_ACTUAL_MASS, new WTArrayList());
nonCalculableParts.put(IPIMMassAttributesHelper.NO_ASSOCIATED_CAD_FOR_VOLUME, new WTArrayList());
nonCalculableParts.put(IPIMMassAttributesHelper.NO_ASSOCIATED_PART_FOR_DENSITY, new WTArrayList());
 if (partStructure.isEmpty()) {
     reportMass = IPIMMassAttributesHelper.generateReportForOnePart(partOid, nonCalculableParts);
 } else {
     reportMass = IPIMMassAttributesHelper.generateReportForSelectedNode(partOid, nonCalculableParts);
 }
 partIdentity = WTMessage.getLocalizedMessage(massRollupRB.class.getName(), massRollupRB.ROLLUP_FOR_PART_TITLE, new Object[] {partIdentity});
%>

<br>
<div style="text-align: center">
	<b> 
		<font size="5"> <%=partIdentity%> </font>
	</b>
</div>
<br />
<br/>
<br>
<div style="text-align: center">
	<b> 
		<font size="3"> <%=reportMass%> </font>
	</b>
</div>
<br />

<% if (nonCalculableParts.get(IPIMMassAttributesHelper.NO_ACTUAL_MASS) != null && nonCalculableParts.get(IPIMMassAttributesHelper.NO_ACTUAL_MASS).size() > 0) {
%>

	<fmt:setBundle basename="com.ikea.ipim.utils.mass.massRollupRB"/>
	<fmt:message var="tableTitle" key="ROLLUP_PARTS_WITH_NO_MASS_TABLE"/>
	<c:set var="foundParts" value="<%=nonCalculableParts.get(IPIMMassAttributesHelper.NO_ACTUAL_MASS)%>"/>
	<jca:describeTable var="tableDescriptor" id="ikea.ipim.mass.table.parts.nomass" configurable="true"
         type="wt.part.WTPart" label="${tableTitle}">
         
         <jca:describeColumn id="type_icon"/>
         <jca:describeColumn id="name"/>
         <jca:describeColumn id="number"/>
         <jca:describeColumn id="infoPageAction" sortable="false"/>
		 <jca:describeColumn id="version"/>
		 <jca:describeColumn id="nmActions" />
		
         
 	</jca:describeTable>
 	
 	<jca:getModel var="tableModel" descriptor="${tableDescriptor}" serviceName="com.ikea.ipim.utils.mass.IPIMMassAttributesHelper" methodName="getPartsWithoutMass">
		<jca:addServiceArgument value="${foundParts}" type="wt.fc.collections.WTArrayList"/>
	</jca:getModel>

	<jca:renderTable model="${tableModel}" showCount="true"/>
 
 <%} %>
 
 <% if (nonCalculableParts.get(IPIMMassAttributesHelper.NO_VOLUME) != null && nonCalculableParts.get(IPIMMassAttributesHelper.NO_VOLUME).size() > 0) {
%>

	<fmt:setBundle basename="com.ikea.ipim.utils.mass.massRollupRB"/>
	<fmt:message var="tableTitle" key="ROLLUP_PARTS_WITH_NO_VOLUME_TABLE"/>
	<c:set var="foundPartsNoVolume" value="<%=nonCalculableParts.get(IPIMMassAttributesHelper.NO_VOLUME)%>"/>
	<jca:describeTable var="tableDescriptor" id="ikea.ipim.mass.table.parts.novolume" configurable="true"
         type="wt.part.WTPart" label="${tableTitle}">
         
         <jca:describeColumn id="type_icon"/>
         <jca:describeColumn id="name"/>
         <jca:describeColumn id="number"/>
         <jca:describeColumn id="infoPageAction" sortable="false"/>
		 <jca:describeColumn id="version"/>
		 <jca:describeColumn id="nmActions" />
		
         
 	</jca:describeTable>
 	
 	<jca:getModel var="tableModel" descriptor="${tableDescriptor}" serviceName="com.ikea.ipim.utils.mass.IPIMMassAttributesHelper" methodName="getPartsWithoutMass">
		<jca:addServiceArgument value="${foundPartsNoVolume}" type="wt.fc.collections.WTArrayList"/>
	</jca:getModel>

	<jca:renderTable model="${tableModel}" showCount="true"/>
 
 <%} %>
 
  <% if (nonCalculableParts.get(IPIMMassAttributesHelper.NO_DENSITY) != null && nonCalculableParts.get(IPIMMassAttributesHelper.NO_DENSITY).size() > 0) {
%>

	<fmt:setBundle basename="com.ikea.ipim.utils.mass.massRollupRB"/>
	<fmt:message var="tableTitle" key="ROLLUP_PARTS_WITH_NO_DENSITY_TABLE"/>
	<c:set var="foundPartsNoDensity" value="<%=nonCalculableParts.get(IPIMMassAttributesHelper.NO_DENSITY)%>"/>
	<jca:describeTable var="tableDescriptor" id="ikea.ipim.mass.table.parts.nodensity" configurable="true"
         type="wt.part.WTPart" label="${tableTitle}">
         
         <jca:describeColumn id="type_icon"/>
         <jca:describeColumn id="name"/>
         <jca:describeColumn id="number"/>
         <jca:describeColumn id="infoPageAction" sortable="false"/>
		 <jca:describeColumn id="version"/>
		 <jca:describeColumn id="nmActions" />
		
         
 	</jca:describeTable>
 	
 	<jca:getModel var="tableModel" descriptor="${tableDescriptor}" serviceName="com.ikea.ipim.utils.mass.IPIMMassAttributesHelper" methodName="getPartsWithoutMass">
		<jca:addServiceArgument value="${foundPartsNoDensity}" type="wt.fc.collections.WTArrayList"/>
	</jca:getModel>

	<jca:renderTable model="${tableModel}" showCount="true"/>
 
 <%} %>
 
  <% if (nonCalculableParts.get(IPIMMassAttributesHelper.NO_ASSOCIATED_CAD_FOR_VOLUME) != null && nonCalculableParts.get(IPIMMassAttributesHelper.NO_ASSOCIATED_CAD_FOR_VOLUME).size() > 0) {
%>

	<fmt:setBundle basename="com.ikea.ipim.utils.mass.massRollupRB"/>
	<fmt:message var="tableTitle" key="ROLLUP_PARTS_WITH_NO_ASSOCIATED_CAD_FOR_VOLUME_TABLE"/>
	<c:set var="foundPartsNoCAD" value="<%=nonCalculableParts.get(IPIMMassAttributesHelper.NO_ASSOCIATED_CAD_FOR_VOLUME)%>"/>
	<jca:describeTable var="tableDescriptor" id="ikea.ipim.mass.table.parts.noCADForVolume" configurable="true"
         type="wt.part.WTPart" label="${tableTitle}">
         
         <jca:describeColumn id="type_icon"/>
         <jca:describeColumn id="name"/>
         <jca:describeColumn id="number"/>
         <jca:describeColumn id="infoPageAction" sortable="false"/>
		 <jca:describeColumn id="version"/>
		 <jca:describeColumn id="nmActions" />
		
         
 	</jca:describeTable>
 	
 	<jca:getModel var="tableModel" descriptor="${tableDescriptor}" serviceName="com.ikea.ipim.utils.mass.IPIMMassAttributesHelper" methodName="getPartsWithoutMass">
		<jca:addServiceArgument value="${foundPartsNoCAD}" type="wt.fc.collections.WTArrayList"/>
	</jca:getModel>

	<jca:renderTable model="${tableModel}" showCount="true"/>
 
 <%} %>
 
 <% if (nonCalculableParts.get(IPIMMassAttributesHelper.NO_ASSOCIATED_PART_FOR_DENSITY) != null && nonCalculableParts.get(IPIMMassAttributesHelper.NO_ASSOCIATED_PART_FOR_DENSITY).size() > 0) {
%>

	<fmt:setBundle basename="com.ikea.ipim.utils.mass.massRollupRB"/>
	<fmt:message var="tableTitle" key="ROLLUP_PARTS_WITH_NO_ASSOCIATED_MATERIAL_TABLE"/>
	<c:set var="foundPartsNoMaterial" value="<%=nonCalculableParts.get(IPIMMassAttributesHelper.NO_ASSOCIATED_PART_FOR_DENSITY)%>"/>
	<jca:describeTable var="tableDescriptor" id="ikea.ipim.mass.table.parts.noMaterialForDensity" configurable="true"
         type="wt.part.WTPart" label="${tableTitle}">
         
         <jca:describeColumn id="type_icon"/>
         <jca:describeColumn id="name"/>
         <jca:describeColumn id="number"/>
         <jca:describeColumn id="infoPageAction" sortable="false"/>
		 <jca:describeColumn id="version"/>
		 <jca:describeColumn id="nmActions" />
		
         
 	</jca:describeTable>
 	
 	<jca:getModel var="tableModel" descriptor="${tableDescriptor}" serviceName="com.ikea.ipim.utils.mass.IPIMMassAttributesHelper" methodName="getPartsWithoutMass">
		<jca:addServiceArgument value="${foundPartsNoMaterial}" type="wt.fc.collections.WTArrayList"/>
	</jca:getModel>

	<jca:renderTable model="${tableModel}" showCount="true"/>
 
 <%} %>
 
 
 <%@ include file="/netmarkets/jsp/util/end.jspf"%>