<%@ taglib prefix="jca" uri="http://www.ptc.com/windchill/taglib/components"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/fmt" prefix="fmt"%>
<%@ include file="/netmarkets/jsp/components/beginWizard.jspf"%>
<%@ include file="/netmarkets/jsp/components/includeWizBean.jspf"%>


<fmt:setBundle basename="com.ikea.ipim.sbom.IkeaSBOMRB"/>
<fmt:message var="wizardTitle" key="UPDATE_SUPPLIERS_TITLE"/>

<jca:wizard buttonList="NoStepsWizardButtons" title="${wizardTitle}">
	<jca:wizardStep action="updateSuppliersStep" type="sbom" />
</jca:wizard>

<%@ include file="/netmarkets/jsp/util/end.jspf"%>