<%@ include file="/netmarkets/jsp/util/begin.jspf"%>

<%@ taglib uri="http://www.ptc.com/windchill/taglib/components" prefix="jca"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/mvc" prefix="mvc"%>
<%@ taglib uri="http://www.ptc.com/windchill/taglib/fmt" prefix="fmt"%>
<jsp:include page="${mvc:getComponentURL('com.ikea.ipim.sbom.builders.SendToSuppliersBuilder')}" />

 <%@ include file="/netmarkets/jsp/util/end.jspf"%>