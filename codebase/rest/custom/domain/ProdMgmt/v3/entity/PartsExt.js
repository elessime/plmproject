function getRelatedEntityCollection(navigationData) {
    return getHelper().getRelatedPartAlternates(navigationData);
}

function isValidNavigation(navName, sourceObject, targetObjectId, processorData) {
    return getHelper().isValidNavigation(navName, sourceObject, targetObjectId, processorData);
}

function getHelper() {
    var PartAlternatesHelper = Java.type('com.ikea.ipim.utils.odata.PartAlternatesHelper');
    return new PartAlternatesHelper();
}

function function_GetSubstituteParts(data, params) {
	return getHelper().getSubstituteParts(data, params);
}
